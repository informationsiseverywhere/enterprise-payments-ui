# Assure Payments UI

This project provides the user interface (UI) for the Assure Payments application.
It is built with Angular CLI.

#### Links
* [Assure Payments UI Repository](https://github.csc.com/insurance/enterprise-payments-ui)
* [Assure Payments API Specification](https://cscnextdev.atlassian.net/wiki/spaces/XCD/pages/611024917/Payment+Center+Reference+API)
* [Assure Payments Documentation](https://clientsupport.csc.com/exceedJ.1/Help/systemdocumentation/Enterprise%20Payments/wwhelp/wwhimpl/js/html/wwhelp.htm)
* [PB360 Documentation](https://cscnextdev.atlassian.net/wiki/display/XCD/Exceed+J)
* [Angular CLI Command Reference](https://angular.io/cli)

- - -

#### Table of Contents  
[Prerequisites](#prereqs)  
[Obtain source code](#sourcecode)  
[Update configuration file](#configs1) 
[Install Angular Cli, Run and Start the application](#build1)  
[Build and Deploy the application](#build2)  
[Upgrade Angular CLI (optional)](#upgradeAngular)

<div id="prereqs"/>

## Prerequisites

* __Node.js__

   Download the latest stable version(> 14) of Node.js from: https://nodejs.org/en/  
   Install it using the default options.  

   Node.js includes npm (the Node.js package manager).  

   To verify the installation and check the installed versions of Node.js and npm, use the following commmands:  
       `node -v`  
       `npm -v`  

* __Git__

   Git is required for cloning the source code to your machine, keeping your code in sync with the GitHub repository, and for promoting code updates.

   Download the latest version of Git for Windows from: https://git-scm.com/download/win  
   Open a command prompt with Administrator privilege, and run the downloaded install file. Accept the default options.  
   Verify that the install added the _"\Git\bin"_ folder to your environment PATH variable. If not, add it manually.  

   From a command prompt, enter the following commands to configure your name and user id:
   ```
   git config --global user.name "Firstname Lastname"
   git config --global user.email "you@dxc.com"
   ```

<div id="sourcecode"/>

## Obtain source code

Clone the repository from GitHub to your machine, with a command like the following:  
  `git clone -b [branch or tag name] https://github.dxc.com/insurance/enterprise-payments-ui.git C:\[your local path]\enterprise-payments-ui`

If you plan to promote code updates, you should also fork the repository on Github.  
Then, from your local application folder, use the following command to set the remote URL for the **git push** command to your forked repo:  
   `git remote set-url --push origin https://github.dxc.com/[your git user id]/enterprise-payments-ui.git`


<div id="configs1"/>

## Update configuration file

* __src\assets\host-url.json__  
   Update the URLs in this file, based on whether you will run the other applications locally or point to a remote server for them.
   If you only need to work on the UI layer code, you can configure all the URLs to point to one of our development environments.
   That way you can run and test the UI screens without installing and launching the other applications on your own machine.


<div id="build1"/>


## Install Angular Cli, Run and Start the application

The `npm run app-start` command is used to both install `Angular Cli` and Start the application for first time.  
The application will automatically reload if you change any of the source files.  
  
After, that from the next time to launch UI applications use `npm start`. The application will automatically run and open in the browser. 

<div id="build2"/>

## Build and Deploy the application

From a command prompt, navigate to the project's root folder and enter this command:  
    `npm run app-build`

That will install the dependencies under the _"node_modules"_ subfolder.

The build artifacts will be written to the _"dist"_ subfolder.

<div id="upgradeAngular"/>

## Upgrade Angular CLI (optional)

To upgrade Angular CLI from an older version to the latest version, perform the following steps.  

Under the UI project's root folder:
   + Delete the old _"node_modules"_ subfolder.
   + Delete the old __package.json__ file

If you have several UI projects installed, do the above for each project.

Then open a command window and enter the following commands:
   + `npm cache clean --force`
   + `npm cache verify`
   + `npm run app-start`
