import { Link } from '../../shared/models/link.model';

export class User {
    userSequenceId: string;
    userId: string;
    name: string;
    email: string;
    phone: string;
    status: string;
    lastLoggedIn: string;
    password: string;
    confirmPassword: string;
    code: string;
    token: string;
    links: Array<Link>;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
