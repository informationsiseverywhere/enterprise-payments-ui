import { Link } from '../../shared/models/link.model';

export class SupportDataModel {
    public propertyName: string;
    public propertyValues: Array<string>;
    public defaultValue: string;
    public propertyDependsUpon: string;
    public links: Array<Link>;

    constructor(values: any = {}) {
        Object.assign(this, values);
    }

    public getFirstPropertyValues(): string{
        return (this.propertyValues?.length > 0) ? this.propertyValues[0] : null;
    }
}
