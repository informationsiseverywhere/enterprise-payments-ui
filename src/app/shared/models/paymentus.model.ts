export class PaymentusSecureTokenModel {
    iframe: boolean;
    postbackUrl?: string;
    paymentMethod?: string;
    emailId?: string;
    clientId?: string;

    constructor(values: any = {}) {
        this.iframe = values?.iframe;
        this.postbackUrl = values?.postbackUrl;
        this.paymentMethod = values?.paymentMethod
        this.clientId = values?.clientId;
        this.emailId = values?.emailId;
    }
}

export class PaymentusTokenModel {
    iframe: boolean;
    accountNumber?: string;
    amount?: string;
    firstName?: string;
    emailId?: string;
    lastName?: string;
    phoneNumber?: string;
    zipCode?: string;

    constructor(values: any = {}) {
        this.iframe = values?.iframe;
        this.accountNumber = values?.accountNumber;
        this.amount = values?.amount
        this.firstName = values?.firstName;
        this.lastName = values?.lastName;
        this.emailId = values?.emailId;
        this.phoneNumber = values?.phoneNumber;
        this.zipCode = values?.zipCode;
    }
}
