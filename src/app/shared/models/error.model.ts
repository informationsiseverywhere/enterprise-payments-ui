import { Link } from './link.model';
export class Error {
  statusCode = 0;
  message = '';
  referenceId = '';
  hasError = false; /*error or success*/
  isWarning = false;
  links: Link;

  constructor(values: any = {}) {
    Object.assign(this, values);
  }
}

export const ErrorStatusCode = [
  {
    status: 400,
    description: 'The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications',
    header: 'Bad Request'
  },
  {
    status: 401,
    description: 'The request requires user authentication and it has not been applied because it lacks valid authentication credentials for the target resource.',
    header: 'Unauthorized'
  },
  {
    status: 403,
    description: 'The server understood the request, but is refusing to fulfill it. Authorization will not help and the request SHOULD NOT be repeated.',
    header: 'Forbidden'
  },
  {
    status: 404,
    description: 'The server has not found anything matching the Request-URI. No indication is given of whether the condition is temporary or permanent.',
    header: 'Not Found'
  },
  {
    status: 405,
    description: 'The method specified in the Request-Line is not allowed for the resource identified by the Request-URI. The response MUST include an Allow header containing a list of valid methods for the requested resource.',
    header: 'Method Not Allowed'
  },
  {
    status: 408,
    description: 'The server did not receive a complete request message within the time that it was prepared to wait.',
    header: 'Request Timeout'
  },
  {
    status: 409,
    description: 'The request could not be completed due to a conflict with the current state of the target resource.',
    header: 'Conflict'
  },
  {
    status: 500,
    description: 'The server encountered an unexpected condition which prevented it from fulfilling the request.',
    header: 'Internal Server Error'
  },
  {
    status: 502,
    description: 'The server, while acting as a gateway or proxy, received an invalid response from an inbound server it accessed while attempting to fulfill the request.',
    header: 'Bad Gateway'
  },
  {
    status: 503,
    description: 'The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay.',
    header: 'Service Unavailable'
  },
  {
    status: 504,
    description: 'The server, while acting as a gateway or proxy, did not receive a timely response from an upstream server it needed to access in order to complete the request.',
    header: 'Gateway Timeout'
  },
  {
    status: 505,
    description: 'The server does not support, or refuses to support, the major version of HTTP that was used in the request message.',
    header: 'HTTP Version Not Supported'
  },
  {
    status: 422,
    description: 'The server understands the content type of the request entity (hence a 415 Unsupported Media Type status code is inappropriate).',
    header: 'Unprocessable Entity'
  },
  {
    status: 0,
    description: 'The server encountered an unexpected condition that prevented it from fulfilling the request..',
    header: 'It\'s a Technical Problem...'
  },
];

export const ErrorCode = [
  400,
  404,
  401,
  500,
  503,
  501,
  403,
  0
];
