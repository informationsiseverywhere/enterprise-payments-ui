import { Link } from '../../shared/models/link.model';
import { User } from './user.model';

export class Role {
    roleSequenceId:string = '';
    roleName:string = '';
    description:string = '';
    numberofUsers:string = '';
    numberofApplications:string = '';
    users:Array<User>;
    links:Array<Link>;

    constructor(values: Object = {}){
        Object.assign(this,values);
    }
}
