export class RoutePermissionModel {
    dependOnScreenName: string;
    parentName: string;
    parentRouteName: string;
    activatedParams: any;
    filterValue: string;
    screenName: string;
    additionalSecurityName: string;
    roleSequenceId: string;
    notUseHeaderTitleForMsg: string;
    only: any;
    redirectTo: any;

    constructor(values: any = {}) {
        Object.assign(this, values);
    }
}
