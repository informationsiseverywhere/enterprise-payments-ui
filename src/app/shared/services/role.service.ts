
import { Injectable } from '@angular/core';

import { RestService } from '../../shared/services/rest.service';
import { AppConfiguration } from '../../shared/services/app-config.service';
import { DynamicHostUrlService } from './dynamic-host-url.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  apiUrls: any;
  public objectToModal: any;
    constructor(
      private restService: RestService,
      private hostUrlService: DynamicHostUrlService,
      private appConfig: AppConfiguration) {}

    getRoleList(query?: string, size?: any, page?: any) {

        let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities) + 'v1/utils/roles';

        if (query) {
          url = url + '?query=' + query;
          if (page) {
            url += '&page=' + page;
        }
          if (size) {
              url += '&size=' + size;
          }
        } else if (page) {
            url += '?page=' + page;
            if (size) {
                url += '&size=' + size;
            }
        } else if (size) {
            url += '?size=' + size;
        }

        return this.restService.getByUrl(url);
    }

    getRoleDetail(roleId?: any) {
        const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities) + 'v1/utils/roles/'  + roleId;
        return this.restService.getByUrl(url);
    }

    checkIfRoleExists(match?: string) {
        let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities) + 'v1/utils/roles';

        if (match) {
          url = url + '?match=' + match + '&filter=roleName';
        }

        return this.restService.getByUrl(url);
    }
}
