import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { RestService } from '../../shared/services/rest.service';
import { AppConfiguration } from '../../shared/services/app-config.service';
import { DynamicHostUrlService } from './dynamic-host-url.service';
import { User } from '../../shared/models/user.model';
import { CommonService } from './common.service';
import { AppLogger } from './app-logger.service';
import { IconName } from '../enum/common.enum';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public currentUser: User;
  public currentUserChanged: BehaviorSubject<User> = new BehaviorSubject<User>(new User());
  public currentUserImageChanged: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  public _userProfileImageUrl = new BehaviorSubject<any>('');
  public currentPartyId: string;

  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    private appLoggerService: AppLogger,
    public commonService: CommonService) { }

  getUserData(userId?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities) + 'v1/utils/users?filters=userid:' + userId;

    return this.restService.getByUrl(url);
  }

  public getUserProfileImgUrl(email?: string): void {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client)}v1/parties?query=${encodeURIComponent(email)}&sort=_score&filters=emailId`;
    this.restService.getByUrl(url).subscribe(
      data => {
        if(data){
          const clientData = data ? data.content[0] : null;
          if(clientData){
            if(clientData.emailId){
              const profileImgUrl = this.commonService.getClientImageUrl(clientData);
              if(profileImgUrl){
                this._userProfileImageUrl.next(profileImgUrl);
              }
            }
            if(clientData.partyId){
              this.currentPartyId = clientData.partyId;
            }
          } else {
            this._userProfileImageUrl.next(IconName.person);
          }
        } else {
          this._userProfileImageUrl.next(IconName.person);
        }
      },
      err => {
        this._userProfileImageUrl.next(IconName.person);
        this.appLoggerService.catchError(err);
      }
    );
  }

  get userProfileImageUrl(): Observable<any> {
    return this._userProfileImageUrl.asObservable();
  }
}
