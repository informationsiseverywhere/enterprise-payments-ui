import { Output, EventEmitter } from '@angular/core';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AppConfiguration } from './../../shared/services/app-config.service';
import { RestService } from './../../shared/services/rest.service';
import { LoadingService } from './../../shared/services/loading.service';
import { AlertService } from '../../libs/alert/services/alert.service';
import { DynamicHostUrlService } from './dynamic-host-url.service';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import { RoleService } from 'src/app/shared/services/role.service';

@Injectable({
  providedIn: 'root'
})

export class SecurityEngineService {
  public securityData: any;

  public securityOpen = false;
  roleSequenceId: any;
  screenName: any;
  snapshotRoute: any;
  filterValue: any;
  public openConfigSection: any;
  public securitySectionData: any;
  public securityFieldData: any;
  public securitySectionList: any;
  public securityConfigList: any;

  public profileSettings: any;
  public additionalSecuritySettings: any;
  public currentFieldName: any;
  public applicationList: any;
  public supplementalFieldsSettings: any;
  public isSecurityDataLoading: boolean = false;
  public additionalSecurityName: string;
  public dynamicRedirectionSettings: any;
  public selectedRole: any;

  @Output() securityUpdated: EventEmitter<any> = new EventEmitter<any>();
  technicalKey: any;

  constructor(
    private restService: RestService,
    private loadingService: LoadingService,
    public activatedRoute: ActivatedRoute,
    private appConfig: AppConfiguration,
    private alertService: AlertService,
    private hostUrlService: DynamicHostUrlService,
    private router: Router,
    private roleService: RoleService) {
    this.snapshotRoute = activatedRoute.snapshot;
    this.applicationList = ComponentConfiguration.settings.applicationList;
  }

  public getSetting(roleSequenceId?: any, screenName?: any, parentName?: any) {
    let url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/authentications/${this.applicationList.applicationName}/screens/${screenName}`;
    
    const queryParams: any = [];
    this.filterValue && queryParams.push({ key: 'filterValue', value: this.filterValue });
    roleSequenceId && queryParams.push({ key: 'role', value: roleSequenceId });
    parentName && queryParams.push({ key: 'parent', value: parentName.fieldName });

    let queryParamsString = '';
    for (const [index, param] of queryParams.entries()){
      queryParamsString += param.key + ':' + param.value;
      if (index < queryParams.length - 1) { queryParamsString += ','; }
    }
    if (queryParamsString) { url += '?filters=' + queryParamsString; }

    return this.restService.getByUrl(url);
  }

  public updateSetting(roleSequenceId?: any, body?: any, data?: any, screenName?: any) {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/authentications/${this.applicationList.applicationName}/screens/${screenName}/fields/${data.fieldName}?filters=role:${roleSequenceId}`;

    return this.restService.patchByUrl(url, body);
  }

  public async getSecuritySetting(roleSequenceId?: any, screenName?: any, parentName?: any): Promise<any> {
    await this.getProfileSettings(roleSequenceId);
    if(this.additionalSecurityName) await this.getAdditionalSecuritySettings(roleSequenceId, this.additionalSecurityName);
    if(screenName){
      await this.getSetting(roleSequenceId, screenName, parentName).toPromise().then(
        data => {
          this.securityData = data ? data.fields : null;
          this.isSecurityDataLoading = false;
          this.securityUpdated.emit(this.securityData);
          this.roleSequenceId = roleSequenceId;
          this.router.navigate([], {
            queryParams: {
              roleSequenceId
            },
            queryParamsHandling: 'merge'
          });
        },
        err => {
          this.isSecurityDataLoading = false;
          if (err.status === 401 && err.statusText === 'Unauthorized') {
            this.roleSequenceId = null;
            this.alertService.error('Selected role does not have access to this page. Please select another role.');
          } else {
            this.alertService.error(err.error.errorDescription);
          }
        });
    }else{
      this.isSecurityDataLoading = false;
      this.roleSequenceId = roleSequenceId;
      this.router.navigate([], {
        queryParams: {
          roleSequenceId
        },
        queryParamsHandling: 'merge'
      });
    }
  }

  public async getComponentConfig(roleSequenceId?: any): Promise<any> {
    if (this.appConfig.isComponentSecurity && (this.screenName || this.additionalSecurityName)) {
      this.isSecurityDataLoading = true;
      await this.getSecuritySetting(roleSequenceId, this.screenName);
    }
  }

  public checkPermission(name?: any): string {
    if (this.securityData && this.securityData[name] && this.securityData[name].fieldProperty) {
      return this.securityData[name].fieldProperty;
    } else {
      if (this.profileSettings && this.profileSettings[name] && this.profileSettings[name].fieldProperty) {
        return this.profileSettings[name].fieldProperty;
      }
      if (this.additionalSecuritySettings && this.additionalSecuritySettings[name] && this.additionalSecuritySettings[name].fieldProperty) {
        return this.additionalSecuritySettings[name].fieldProperty;
      }
      if (this.dynamicRedirectionSettings && this.dynamicRedirectionSettings[name] && this.dynamicRedirectionSettings[name].fieldProperty) {
        return this.dynamicRedirectionSettings[name].fieldProperty;
      }
      return 'Show';
    }
  }

  public checkVisibility(name?: any, property?: any): boolean {
    if (!this.appConfig.isComponentSecurity) {
      if (property === 'Disable' || property === 'Hide') { return false; } else { return true; }
    }
    if (this.securityData && this.securityData[name] && this.securityData[name].fieldProperty === property) { return true; }
    if (this.additionalSecuritySettings && this.additionalSecuritySettings[name] && this.additionalSecuritySettings[name].fieldProperty === property) {
      return true;
    }
    if (this.securityData && !this.securityData[name] && this.dynamicRedirectionSettings && this.dynamicRedirectionSettings[name] && this.dynamicRedirectionSettings[name].fieldProperty === property) {
      return true;
    }
    return false;
  }

  public getSecurityControlSetting(roleSequenceId?: any, event?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    const objectData = this.securityData[event];
    this.getSetting(roleSequenceId, this.screenName, objectData).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.securityOpen = true;
        if (!event) { this.securityData = data ? data.fields : null; }
        if (event) {
          this.securitySectionData = data ? data.fields : null;
          this.securityFieldData = this.securitySectionData[this.openConfigSection.fieldName];
          const itemInConfig = this.securityConfigList.find(element => element.fieldName === this.openConfigSection.fieldName);
          if (itemInConfig.checkHideByChildren) {
            this.securityFieldData.checkHideByChildren = itemInConfig.checkHideByChildren;
          }
          this.securityFieldData.type = itemInConfig.type;
          this.securitySectionList = Object.keys(this.securitySectionData).map(key => (this.securitySectionData[key]));
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  public async getProfileSettings(roleSequenceId?: any): Promise<any> {
    await this.getSetting(roleSequenceId, 'profileSettings').toPromise().then(
      data => {
        this.profileSettings = data ? data.fields : null;
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  public async getAdditionalSecuritySettings(roleSequenceId?: any, securityName?: any): Promise<any> {
    await this.getSetting(roleSequenceId, securityName).toPromise().then(
      data => {
        this.additionalSecuritySettings = data ? data.fields : null;
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  public resetSecurityList(): void {
    this.securityConfigList = undefined;
    this.securitySectionData = undefined;
    this.securitySectionList = undefined;
    this.securityFieldData = undefined;
    this.supplementalFieldsSettings = undefined;
  }

  public getSupplementalFields(technicalKey: string) {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/apps/${this.applicationList.applicationName}/screens/${this.screenName}/identifiers/${technicalKey}/dynamicFields`;
    return this.restService.getByUrl(url);
  }

  public getRoleDetail(roleSequenceId?: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.roleService.getRoleDetail(roleSequenceId).subscribe(
      data => {
        this.selectedRole = data;
        this.loadingService.toggleLoadingIndicator(false);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

}
