import { Injectable } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';

@Injectable({
  providedIn: 'root'
})
export class AppsService {
  public userSequenceId: any;

  constructor(
    private restService: RestService,
    private appConfig: AppConfiguration) { }

  public getAllApplications(userSequenceId?: any): any {
    const url = `${this.appConfig.apiUrls.utilities}v1/utils/users/${userSequenceId}/apps`;
    return this.restService.getByUrl(url);
  }
}


