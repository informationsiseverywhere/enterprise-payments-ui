import { Injectable } from '@angular/core';
import { IComponentConfig } from 'src/app/shared/interfaces/component-config.interface';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ComponentConfiguration {
  static settings: IComponentConfig;

  constructor(private http: HttpClient) { }

  load() {
    const jsonFile = 'assets/configuration/componentConfig.json';
    return new Promise((resolve) => {
      this.http
        .get(jsonFile)
        .subscribe((response) => {
          ComponentConfiguration.settings = response as IComponentConfig;
          resolve(true);
        });
    });
  }
}
