import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { AppConfiguration } from '../../shared/services/app-config.service';
import { LoadingService } from '../../shared/services/loading.service';
import { AlertService } from '../../libs/alert/services/alert.service';
import { AppLogger } from '../../shared/services/app-logger.service';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { HalProcessor } from '../../shared/services/utilities.service';
import { RestService } from './rest.service';
import { ErrorCode } from '../models/error.model';
import { EVENT_KEYS_MAP, IconName } from '../enum/common.enum';
import { MultiselectItem } from 'src/app/shared/models/multiselect-item.model';
import { SupportDataModel } from 'src/app/shared/models/support-data.model';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private forgeRockLoginService: any;
  pageObject: any;
  userId: any;
  tokenId: any;
  multiHostUrls: any;
  authUrls: any;
  uiUrls: any;
  errorCode: any;
  constructor(
    public appConfig: AppConfiguration,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private appLogger: AppLogger,
    private router: Router,
    private halProcessor: HalProcessor,
    private restService: RestService,
    private securityEngineService: SecurityEngineService) {
    this.multiHostUrls = this.appConfig.multiHostUrls;
    this.authUrls = this.appConfig.authUrls;
    this.uiUrls = this.appConfig.uiUrls;
    this.errorCode = ErrorCode;
  }

  public setForgeRockLoginService(forgeRockLoginService: any): void {
    this.forgeRockLoginService = forgeRockLoginService;
  }

  public getForgeRockLoginService(): any {
    return this.forgeRockLoginService;
  }

  removeSpaces(status: string) {
    if (status) { return status.replace(/ /g, ''); }
  }

  removeSpacesAndFWDash(status: string) {
    if (status) {
      let result = status.replace(/ /g, '');
      result = result.replace(/\//g, '');
      return result;
    }
  }

  public getClientImageUrl(client: any): string {
    let defaultImageUrl = IconName.person;
    const commonPictureIds = ['Business', 'Person_Female', 'Agency', 'Person_Male', 'Person'];
    if(client && IconName.userProfilePath){
      if(client?.userId){
        return this.getAWSs3ImageUrl(client?.userId);
      }else if(client?.pictureId) {
        if(commonPictureIds.indexOf(client?.pictureId) > -1){
          return `${IconName.userProfilePath}${client.pictureId}.png`;
        }else{
          return this.getAWSs3ImageUrl(client?.pictureId);
        }
      }else{
        switch (client?.type) {
          case 'Person':
            return IconName.userProfilePath + (client.details.gender ? 'Person_' + client.details.gender : 'Person') + '.png';
          case 'Business':
            return IconName.userProfilePath + 'Business.png';
          case 'Agent':
            return IconName.userProfilePath + 'Agency.png';
          default: return defaultImageUrl;
         }
      }
    }
    return defaultImageUrl;
  }

  public getAWSs3ImageUrl(pictureId: any) {
    const currentDate = new Date();
    if(this.multiHostUrls.imageUrl){
      return `${this.multiHostUrls.imageUrl}${pictureId}.png?t=${pictureId + currentDate.getTime()}`;
    }else{
      return IconName.person;
    }
  }

  checkTokenIsExpired(alertMsg: any) {
    if (alertMsg.includes('Token is expired')) {
      if (this.authUrls.loginMethod === 'Forgerock' || this.authUrls.loginMethod === 'Cognito') {
        window.location.href = this.appConfig.uaaCommonLogoutURL + '?appid=pb360&returnUrl=' +
          encodeURIComponent(window.location.href);
      }
    }
  }

  public handleErrorAPIResponse(component?: any, loadedName?: string, dataName?: string, err?: any): void {
    if (component) {
      component[loadedName] = true;
      component[dataName] = null;
    }
    if (err && (err.status || err.status === 0)) {
      const statusCode = this.errorCode.indexOf(err.status) >= 0;
      if (statusCode && !(err.error && (err.error.errorDescription || err.error.defaultMessage || err.error.errors))) {
        const elements = document.getElementsByClassName('modal-backdrop');
        while (elements.length > 0) {
          elements[0].parentNode.removeChild(elements[0]);
        }
        this.handleRedirectErrorPageResponse(err);
      } else {
        this.handleErrorResponse(err);
      }
    } else {
      this.handleErrorResponse(err);
    }
  }

  public handleErrorResponse(err?: any): void {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    if (resultQueryParam) {
      this.userId = resultQueryParam.userId;
      this.tokenId = resultQueryParam.tokenId;
    }
    this.loadingService.toggleLoadingIndicator(false);
    if ((this.authUrls.loginMethod === 'Forgerock' ||
      this.authUrls.loginMethod === 'Cognito') && !this.tokenId) {
      window.location.href = this.appConfig.uaaCommonLogoutURL + '?appid=pb360&returnUrl=' +
        encodeURIComponent(window.location.href);
    } else if (!this.userId) {
      window.location.href = this.uiUrls.dashboard + 'spring/logout';
    }
    if (err?.error?.errorDescription) {
      this.alertService.error(err.error.errorDescription);
    } else if (err?.error?.defaultMessage) {
      this.alertService.error(err.error.defaultMessage);
    } else {
      if (err?.error?.errors) {
        this.alertService.error('', err.error.errors);
      } else {
        const error = `${err.status} - Connection Error. Please contact your Administrator`;
        this.alertService.error(error);
      }
    }
  }

  handleRedirectErrorPageResponse(err?: any) {
    this.loadingService.toggleLoadingIndicator(false);
    this.appLogger.updateMessage({ errorCode: err });
    this.router.navigate(this.appConfig.errorPageLink);
  }

  public getFilterParams(filter?: any) {
    let filterArray = filter;
    if (filter) {
      if (filter instanceof Array && filter.length > 0) {
        if (filter.length === 1) { filterArray = filter[0].split(','); }
      } else {
        filterArray = filter.split(',');
      }
    }
    return filterArray;
  }

  public getComponentConfiguration(componentName?: string, componentProperty?: string): any {
    if (!componentName) { return; }
    if (componentName && componentProperty) { return ComponentConfiguration.settings[componentName][componentProperty]; }
    return ComponentConfiguration.settings[componentName];
  }

  public convertDateFormat(date?: any): any {
    if (date) { return moment(date).format(ComponentConfiguration.settings['datePickerOptions']['dateFormat']); }
  }
  public convertDateAndTimeFormat(date?: any): any {
    if (date) { return moment(date).format(ComponentConfiguration.settings['dateAndTimeFormat']); }
  }

  public dateTimeFormatAMPM(date?: any): any {
    if (date) { return moment(date).format(ComponentConfiguration.settings['dateTimeFormatAMPM']); }
  }
  public removeQueryParam(parameter, url): string {
    const urlparts = url.split('?');
    if (urlparts.length >= 2) {
      const prefix = encodeURIComponent(parameter) + '=';
      const pars = urlparts[1].split(/[&;]/g);
      for (let i = pars.length; i-- > 0;) {
        if (pars[i].lastIndexOf(prefix, 0) !== -1) {
          pars.splice(i, 1);
        }
      }
      url = urlparts[0] + '?' + pars.join('&');
      const lastChar = url[url.length - 1];
      if (lastChar === '?') { url = url.substring(0, url.length - 1); }
      return url;
    } else {
      return url;
    }
  }

  public checkCognitoAuthorization(navigateUrl: any, target: any, queryParam: any): any {
    this.restService.validateCognitoToken(this.userId, this.tokenId).subscribe(
      (data: any) => {
        this.loadingService.toggleLoadingIndicator(false);
        const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
        if (resultQueryParam) {
          this.userId = resultQueryParam.userId;
          this.tokenId = resultQueryParam.tokenId;
        }
        if (data && data.content[0].userId.toLowerCase() === this.userId.toLowerCase()) {
          let url = navigateUrl + queryParam + this.userId.toUpperCase() + '&tokenId=' + this.tokenId;
          if (this.checkActivitiesUrl(url)) {
            url += '&memberId=' + this.userId.toUpperCase();
          }
          window.open(
            url,
            target
          );
        } else {
          this.restService.logoutUser();
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.alertService.error('You do not have permission to view this page.');
      }
    );
  }

  checkAuthorization(navigateUrl: any, target: any, queryParam: any): any {
    this.restService.validateToken(this.tokenId).subscribe(
      (data) => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data && data.valid && data.uid === this.userId.toLowerCase()) {
          let url = navigateUrl + queryParam + this.userId.toUpperCase() + '&tokenId=' + this.tokenId;
          if (this.checkActivitiesUrl(url)) {
            url += '&memberId=' + this.userId.toUpperCase();
          }
          window.open(
            url,
            target
          );
        } else {
          this.restService.logoutUser();
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.alertService.error('You do not have permission to view this page.');
      }
    );
  }
  navigateToUrl(navigateUrl?: string, target?: string, checkAuthentication = true) {

    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    if (resultQueryParam) {
      this.userId = resultQueryParam.userId;
      this.tokenId = resultQueryParam.tokenId;
    }
    if (checkAuthentication) {
      let queryParam: string;
      if (navigateUrl?.split('?').length > 1) {
        queryParam = '&userId=';
      } else {
        queryParam = '?userId=';
      }

      if (this.authUrls.loginMethod === 'SSO') {
        if (this.userId) {
          let url = navigateUrl + queryParam + this.userId.toUpperCase();
          if (this.checkActivitiesUrl(url)) {
            url += '&memberId=' + this.userId.toUpperCase();
          }
          window.open(
            url,
            target
          );
        } else {
          this.restService.logoutSsoUser();
        }
      } else if (this.authUrls.loginMethod === 'Forgerock') {
        if (this.userId && this.tokenId) {
          this.checkAuthorization(navigateUrl, target, queryParam);
        } else {
          this.restService.logoutUser();
        }
      } else if (this.authUrls.loginMethod === 'Cognito') {
        if (this.userId && this.tokenId) {
          this.checkCognitoAuthorization(navigateUrl, target, queryParam);
        } else {
          this.restService.logoutUser();
        }
      } else {
        window.open(
          navigateUrl,
          target
        );
      }
    }
  }

  private checkActivitiesUrl(url: any) {
    if (url) {
      return url.includes('ent-activities') ? true : false;
    }
  }
  public clearSecurityConfig(): void {
    if (!this.securityEngineService.roleSequenceId) { return; }
    this.securityEngineService.roleSequenceId = undefined;
    this.securityEngineService.selectedRole = undefined;
    this.securityEngineService.getComponentConfig(undefined);
    this.router.navigate([], {
      queryParams: {
        roleSequenceId: null
      },
      queryParamsHandling: 'merge'
    });
  }
  public tablePaginationHandler(paginator?: any, responseData?: any): any {
    const data = responseData ? responseData.content : null;
    if (!paginator) { return data; }
    if ((!responseData && Number(paginator.currentPage) > 1) || (responseData.content && responseData.content.length == 0 && Number(paginator.currentPage) > 1)) {
      responseData = new Array();
      paginator.navigateToPage(paginator.pageObject.previousPageUrl);
      return responseData;
    }
    const _links = responseData ? responseData.links : null;
    paginator.pageObject = {
      firstPageUrl: this.halProcessor.linkParser('first', _links),
      previousPageUrl: this.halProcessor.linkParser('previous', _links),
      nextPageUrl: this.halProcessor.linkParser('next', _links),
      lastPageUrl: this.halProcessor.linkParser('last', _links),
      currentPageUrl: this.halProcessor.linkParser('current', _links)
    };
    paginator.getPageNumber();
    return data;
  }

  public checkIsDisable(type: any, data: any): any {
    if (data) {
      for (const sd of data.links) {
        if (sd.rel === type) {
          return true;
        }
      }
    }
  }

  public switchStyle(theme: any): void {
    let themeConfigData;
    const url = 'assets/configuration/theme.json';
    this.loadingService.toggleLoadingIndicator(true);
    this.getCommonMetadata(url).subscribe(
      data => {
        themeConfigData = data ? data : null;
        if (theme === 'default-theme' || themeConfigData['themes-list'][theme].active === 'false') {
          theme = themeConfigData['default-theme'];
        }
        this.loadingService.toggleLoadingIndicator(false);
        this.themeChange(theme);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

  public themeChange(theme: any): void {
    localStorage.setItem('DefaultTheme', theme);
    const oldLink = document.getElementById('theme-link');
    const newLink = document.createElement('link');
    newLink.setAttribute('id', 'theme-link');
    newLink.setAttribute('rel', 'stylesheet');
    newLink.setAttribute('type', 'text/css');
    newLink.setAttribute('href', theme + '.css');
    newLink.onload = () => {
      if (oldLink) { oldLink.parentElement.removeChild(oldLink); }
    };
    document.getElementsByTagName('head')[0].appendChild(newLink);
  }

  public getCommonMetadata(url?: any): any {
    return this.restService.getByUrlWithoutOptions(url);
  }

  public getDatafromDetailLink(
    data?: any,
    linkName?: any,
    method?: any,
    requestBody?: any
  ): any {
    if (data && data.links) {
      const link = data.links.find((item) => item.rel === linkName);
      const url = link.href;
      if (method === 'GET') {
        return this.restService.getByUrl(url);
      } else if (method === 'POST') {
        return this.restService.postByUrl(url, requestBody);
      } else if (method === 'PATCH') {
        return this.restService.patchByUrl(url, requestBody);
      } else if (method === 'DELETE') {
        return this.restService.deleteByUrl(url);
      } else if (method === 'OPTION') {
        return this.restService.optionsByUrl(url);
      }
    }
  }

  public handleSearchQuery(currentQueryParam?: any, query?: any, navigateCommands?: any): any{
    const paramObject: any = {};
    for (const key in currentQueryParam) {
      if (key !== 'query' && key !== 'page' && key !== 'size') {
        if (key === 'filters') {
          let filters;
          if (currentQueryParam[key] instanceof Array)  {
            filters = currentQueryParam[key].filter(item => !item.key);
            paramObject[key] = this.getFilterParams(filters);
          } else {
            paramObject[key] = this.getFilterParams(currentQueryParam[key]);
          }
        } else {
          paramObject[key] = currentQueryParam[key];
        }
      }
    }
    paramObject.query = query !== '' ? query : undefined;
    this.router.navigate(navigateCommands, {
      queryParams: paramObject
    });
    return paramObject;
  }

  public buildSearchQueryUrl(url?: any, page?: any, size?: any, query?: string, filters?: any, since?: any, until?: any, sort?: any): string {
    const queryParams: any = [];
    query && queryParams.push({ key: 'query', value: encodeURIComponent(query) });
    if (sort)  {
      if(sort instanceof Array){
        for (const item of sort) {
          queryParams.push({ key: 'sort', value: item });
        }
      }else{
        queryParams.push({ key: 'sort', value: sort });
      }
    }
    since && queryParams.push({ key: 'since', value: since });
    until && queryParams.push({ key: 'until', value: until });
    page && queryParams.push({ key: 'page', value: page });
    if(size){
      queryParams.push({ key: 'size', value: size });
    }else{
      queryParams.push({ key: 'size', value: this.getComponentConfiguration('paginationOptions', 'itemsPerPage') });
    }
    filters && queryParams.push({ key: 'filters', value: filters });
    let queryParamsString = '';
    for (let i = 0; i < queryParams.length; i++) {
      const param = queryParams[i];
      if (param.value instanceof Array) {
        queryParamsString += param.key + '=';
        for (let j = 0; j < param.value.length; j++) {
          const item = param.value[j];
          if (item instanceof Object) {
            queryParamsString += item.key + ':' + item.value;
          } else {
            queryParamsString += item;
          }

          if (j < param.value.length - 1) {
            queryParamsString += ',';
          }
        }
      } else if (param.value instanceof Object) {
        queryParamsString += param.key + '=' + param.value.key + ':' + param.value.value;
      } else {
        queryParamsString += param.key + '=' + param.value;
      }
      if (i < queryParams.length - 1) {
        queryParamsString += '&';
      }
    }
    if (queryParamsString) {
      if(url.indexOf('?') != -1){
        return url + '&' + queryParamsString;
      }else{
        return url + '?' + queryParamsString;
      }
    }
    return url;
  }

  public clearQueryParam(removeQueryParam?: any): void {
    if(removeQueryParam.length > 0) {
      const queryParams = {};
      removeQueryParam.forEach(key => {
        queryParams[key] = null;
      });
      this.router.navigate([], {
        queryParams: queryParams,
        queryParamsHandling: 'merge'
      });
    }
  }

  public keyPressIdentify(eventKey?: string): string{
    return EVENT_KEYS_MAP[eventKey] || eventKey;
  }

  public setSessionStorageData(resultQueryParam?: any, sessionStorageData?: any, replaceCurrentSession = false, ...sessionData: any): any {
    if (sessionData && sessionData.length){
      for (const item of sessionData){
        if (resultQueryParam[item] && (!replaceCurrentSession || (replaceCurrentSession && !sessionStorageData[item]))) { sessionStorageData[item] = resultQueryParam[item]; }
      }
    }
    return sessionStorageData;
  }

  public getMultiselectItems(component?: any, propertyName?: string, supportData?: any){
    for (const sd of supportData.supportData) {
      if (sd.propertyName === propertyName) {
        const dropDownItems = sd.propertyValues;
        const multiItems: Array<MultiselectItem> = [];
        for (const [index, sd] of dropDownItems.entries()){
          multiItems.push(new MultiselectItem(dropDownItems[index]));
        }
        component[propertyName + 'Dropdown'] = multiItems;
      }
    }
  }

  public setMultiselectItems(data: any) {
    const multiItems: Array<MultiselectItem> = [];
    for (const [index, sd] of data.entries()) {
      multiItems.push(new MultiselectItem(data[index]));
    }
    return multiItems;
  }

  public removeEmptyObjectEntries(objectEntries?: any) : any{
    for (const [key, value] of Object.entries(objectEntries)) {
       if(objectEntries[key] == null) delete objectEntries[key];
    }
    return objectEntries;
  }
  public isComponentOverlayLeaving(): boolean {
    const msgElements = Array.from(document.querySelectorAll('.alert-dismissable, .component-overlay-leave'));
    const multiSelectElements = Array.from(document.querySelectorAll('.dropdown-list'));
    let isMultipleSelectHidden: boolean = true;
    if(multiSelectElements?.length > 0){
      multiSelectElements.forEach(element => {
        if(!element.hasAttribute("hidden")){
          isMultipleSelectHidden = false;
        }
      });
    }
    if(msgElements?.length <= 0 && isMultipleSelectHidden){
      return false;
    }else{
      msgElements.forEach(element => {
        element.classList?.remove('component-overlay-leave');
      });
      return true;
    };
  }

  public getDropdownData(propertyName: string, supportDataList: Array<SupportDataModel>): SupportDataModel{
    return new SupportDataModel(supportDataList.find(item => item.propertyName === propertyName));
  }
}
