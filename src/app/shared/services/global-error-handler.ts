import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandler implements ErrorHandler {
  constructor() { }

  handleError(error: Error | HttpErrorResponse): void {

    const chunkFailedMessage = /Loading chunk [\d]+ failed/;
    if (chunkFailedMessage.test(error.message)) {
      window.location.reload();
    } else if (!navigator.onLine) {
      throw new Error('No Internet Connection');
    } 
    // else if (error instanceof HttpErrorResponse) {
    //   this.getServerMessage(error);
    // } else {
    //   this.getClientStack(error);
    // }
  }

  // getClientStack(error: Error): string {
  //   throw new Error(error.stack ? error.stack : (error?.message ? error.message : error.toString()));
  // }

  // getServerMessage(error: HttpErrorResponse): string {
  //   const msg = error.error.Message;
  //   if (!!msg) {
  //     throw new Error(msg + ' : ' + error.error.ExceptionMessage);
  //   }
  //   throw new Error(`Application can not execute because API hasn\\'t been started`);
  // }

  // getServerStack(error: HttpErrorResponse): string {
  //   throw new Error(error.error.StackTrace);
  // }
}
