import { Injectable } from '@angular/core';

import { RestService } from './rest.service';
import { AppConfiguration } from './app-config.service';
import { DynamicHostUrlService } from './dynamic-host-url.service';
import { PaymentusSecureTokenModel } from '../models/paymentus.model';

@Injectable({
    providedIn: 'root'
})
export class PaymentusService {
    constructor(
        private restService: RestService,
        private hostUrlService: DynamicHostUrlService,
        private appConfig: AppConfiguration
    ) { }

    public generateSecureToken(paymentusSecureToken: PaymentusSecureTokenModel): any {
        const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments)}v1/generateToken`
        return this.restService.postByUrl(url, paymentusSecureToken);
    }

    public getSecureTokenizationIframeUrl(authToken: string): string {
        return `${this.appConfig.multiHostUrls.secureTokenizationIframeUrl}${authToken}`;
    }

    public getSecureTokenizationWebUrl(authToken: string): string {
        return `${this.appConfig.multiHostUrls.secureTokenizationWebUrl}${authToken}`;
    }

    public getPaymentusPostBackURL(): string {
        return `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments)}v1/iframeToken`;
    }

    public isPaymentusMethodCreated(msg: any): boolean {
        if (msg?.data) {
            const cpMessage = msg?.data?.split(':');
            if (cpMessage?.length > 1) {
                return (cpMessage[0] === "paymentusCreated" && cpMessage[1] === 'true');
            }
        }
        return false;
    }
}
