import { Injectable } from '@angular/core';
import { RemoteHostURLService } from './remote-host-url.service';
import {
  RemoteHostModel,
  ApiUrlsModel,
  UiUrlsModel,
  MultiHostUrlsModel,
  GridAppsModel,
  UxUrlsModel,
  AuthUrlsModel,
  SecurityConfigUrlsModel
} from '../interfaces/host-url.interface';

@Injectable()
export class AppConfiguration {
  hostUrlConfig: RemoteHostModel;
  apiUrls: ApiUrlsModel;
  uiUrls: UiUrlsModel;
  uxUrls: UxUrlsModel;
  gridApps: GridAppsModel;
  multiHostUrls: MultiHostUrlsModel;
  authUrls: AuthUrlsModel;
  securityConfigUrls: SecurityConfigUrlsModel;
  landingPageLink: any;
  errorPageLink: any;
  uaaCommonLoginURL: string;
  uaaCommonLogoutURL: string;
  timeInterval: number;
  isShowSecurity: any;
  isComponentSecurity: any;

  constructor() {
    const landingPageLink = '/ent-payments';
    this.landingPageLink = [landingPageLink];
    this.errorPageLink = [landingPageLink + '/errors'];
    this.hostUrlConfig = RemoteHostURLService.hostUrlConfig;
    this.timeInterval = 60000;
    this.apiUrls = this.hostUrlConfig.apiUrls;
    this.uiUrls = this.hostUrlConfig.uiUrls;
    this.uxUrls = this.hostUrlConfig.uxUrls;
    this.gridApps = this.hostUrlConfig.gridApps;
    this.multiHostUrls = this.hostUrlConfig.multiHostUrls;
    this.authUrls = this.hostUrlConfig.authUrls;
    this.securityConfigUrls = this.hostUrlConfig.securityConfigUrls;
    if (this.authUrls.uaaCommonLogin) {
      this.uaaCommonLoginURL = this.authUrls.uaaCommonLogin + 'login';
      this.uaaCommonLogoutURL = this.authUrls.uaaCommonLogin + 'logout';
    }
    if (this.securityConfigUrls) { this.isShowSecurity = this.securityConfigUrls.isShowSecurity; }
    if (this.securityConfigUrls) { this.isComponentSecurity = this.securityConfigUrls.isComponentSecurity; }
  }
}
