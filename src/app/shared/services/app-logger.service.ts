import { Injectable } from '@angular/core';
import { Observable ,  BehaviorSubject } from 'rxjs';
import { Error } from '../models/error.model';

@Injectable({
  providedIn: 'root'
})
export class AppLogger {
    private _error: BehaviorSubject<Error> = new BehaviorSubject<Error>(new Error());
    private _warning: BehaviorSubject<Error> = new BehaviorSubject<Error>(new Error());
    private updateErrorLogger: BehaviorSubject<any> = new BehaviorSubject<any>('');

    updateError$: Observable<any> = this.updateErrorLogger.asObservable();

    updateMessage(logger: any) {
        this.updateErrorLogger.next(logger);
    }

    catchWarning(data: any, message?: string) {
        if (data !== undefined) {
            if (data instanceof Response) {
                const warning: Error = new Error();
                warning.isWarning = true;
                warning.statusCode = data.status;

                if (warning.statusCode.toString() !== '200' ||
                    warning.statusCode.toString() !== '201' ||
                    warning.statusCode.toString() !== '202' ||
                    warning.statusCode.toString() !== '204') {

                    if (warning.statusCode === 404) {
                        warning.message = message;
                    } else if (warning.statusCode === 401 || warning.statusCode === 403) {
                        warning.message = 'You are not authorized to access this function. If you have any questions just give us a call at 1-800-555-1212 or email us at support@csc.com.';
                    } else {
                        warning.message = 'Sorry, looks like we’ve had a technical issue on our end. Please try again and if the issue persists, just give us a call at 1-800-555-1212 or email us at support@csc.com.';
                    }

                    warning.hasError = true;
                } else {
                    warning.hasError = false;
                }

                this._warning.next(warning);
            }
        }
        return this._warning;
    }

    catchError(data: any) {
        if (data !== undefined) {
            if (data instanceof Response) {
                const error: Error = new Error();
                error.isWarning = false;
                error.statusCode = data.status;

                if (error.statusCode.toString() !== '200' ||
                    error.statusCode.toString() !== '201' ||
                    error.statusCode.toString() !== '202' ||
                    error.statusCode.toString() !== '204') {

                    if (error.statusCode === 404) {
                        error.message = data.statusText + '. If you have any questions just give us a call at 1-800-555-1212 or email us at support@csc.com.';
                    } else if (error.statusCode === 401 || error.statusCode === 403) {
                        error.message = 'You are not authorized to access this function. If you have any questions just give us a call at 1-800-555-1212 or email us at support@csc.com.';
                    } else {
                        error.message = 'techicalIssue';
                    }

                    error.hasError = true;
                } else {
                    error.hasError = false;
                }
                this._error.next(error);
            }
        }
    }
}