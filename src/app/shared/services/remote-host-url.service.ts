import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RemoteHostModel } from '../interfaces/host-url.interface';

/**
 * This service allos to get configuration file from remote server or
 * return the file if there is no any configurationBaseURL
 */

@Injectable({
  providedIn: 'root'
})

export class RemoteHostURLService {
  static hostUrlConfig: RemoteHostModel;
  constructor(private http: HttpClient) { }

  load() {
    return new Promise((resolve) => {
      this.http
        .get('./assets/host-url.json')
        .subscribe((response) => {
          RemoteHostURLService.hostUrlConfig = response as RemoteHostModel;
          resolve(true);
        });
    });
  }
}
