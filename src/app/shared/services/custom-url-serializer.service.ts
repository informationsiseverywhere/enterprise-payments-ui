import { UrlSerializer, DefaultUrlSerializer, UrlTree } from '@angular/router';

export class CustomUrlSerializer implements UrlSerializer {
    defaultUrlSerializer: DefaultUrlSerializer = new DefaultUrlSerializer();
    constructor() {

    }
    parse(url: string): UrlTree {
        // Change comma signs to encoded values
        url = url.replace(/\,/g, '%2C');
        // Change plus signs to encoded spaces
        url = url.replace(/\+/g, '%2B');
        // Use the default serializer that you can import to just do the 
        // default parsing now that you have fixed the url.
        return this.defaultUrlSerializer.parse(url);
    }

    serialize(tree: UrlTree): string {
        // Use the default serializer to create a url and replace any spaces with plus signs
        // Use the default serializer to create a url and replace any values with comma signs
        let mapObj = {
            '%2C': ',',
            '%20': '+',
            '%2B': '+'
        };

        let reg = new RegExp(Object.keys(mapObj).join('|'), 'g');

        return this.defaultUrlSerializer.serialize(tree).replace(reg, function (matched) {
            return mapObj[matched];
        });
    }
}
