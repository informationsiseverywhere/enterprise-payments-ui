import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { IHeardersConfig } from '../interfaces/component-config.interface';

@Injectable({
  providedIn: 'root'
})
export class RemoteHeadersService {
  static headersConfig: IHeardersConfig;

  constructor(private http: HttpClient) { }

  load() {
    const jsonFile = 'assets/headers.json';
    return new Promise((resolve) => {
      this.http
        .get(jsonFile)
        .subscribe((response) => {
          RemoteHeadersService.headersConfig = response as IHeardersConfig;
          resolve(true);
        });
    });
  }
}
