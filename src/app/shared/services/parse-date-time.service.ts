
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ParseDateTimeService {

  constructor(
    private datepipe: DatePipe) { }

  // Input yyyy-mm-dd
  // Output MMM d, y
  parseDateTimeToDate(datetime: any) {
    if (datetime) {
      return this.datepipe.transform(datetime, 'MMM d, y');
    }
  }

  // Input yyyymmddhhmmss
  // Output MMM dd, yyyy hh:mm:ss
  parseDateTimeStrToTimeStamp(inputDatetime: any) {
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    if (inputDatetime.length > 0) {
      const datetime = inputDatetime[0];
      const year = datetime.substring(0, 4);
      const monthIndex = datetime.substring(4, 6);
      const day = datetime.substring(6, 8);
      const hour = datetime.substring(8, 10);
      const min = datetime.substring(10, 12);
      const sec = datetime.substring(12, 14);
      const previousMonthIndex = monthIndex - 1;
      const formateddatetime = monthNames[previousMonthIndex] + ' ' + day + ', ' + year + ' ' + hour + ':' + min + ':' + sec;
      return formateddatetime;
    }
    return inputDatetime;
  }

  validateDuration(data: any) {
    const startDateTimeStr = data.startTime;
    const startDateTimeArray = startDateTimeStr.split(' ');
    const startDateArray = startDateTimeArray[0].split('-');
    const startTimeArray = startDateTimeArray[1].split(':');
    let startSecTime = 0;
    let startMsTime = 0;
    if (startTimeArray[2]) {
      const startTime = startTimeArray[2].split('.');
      startSecTime = startTime[0];
      startMsTime = startTime[1];
    }

    const startDateTime = new Date(startDateArray[0], (startDateArray[1] - 1),
      startDateArray[2], startTimeArray[0], startTimeArray[1], startSecTime, startMsTime).getTime();

    if (data.endTime) {
      const endDateTimeStr = data.endTime;
      const endDateTimeArray = endDateTimeStr.split(' ');
      const endDateArray = endDateTimeArray[0].split('-');
      const endTimeArray = endDateTimeArray[1].split(':');
      let endSecTime = 0;
      let endMsTime = 0;
      if (endTimeArray[2]) {
        const endTime = endTimeArray[2].split('.');
        endSecTime = endTime[0];
        endMsTime = endTime[1];
      }
      const endDateTime = new Date(endDateArray[0], (endDateArray[1] - 1), endDateArray[2], endTimeArray[0], endTimeArray[1], endSecTime, endMsTime).getTime();
      return (endDateTime - startDateTime) / 1000;
    } else {
      return 0;
    }
  }
}
