import { throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { HttpEvent } from '@angular/common/http';
import { HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';

import { LoadingService } from './loading.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { AppConfiguration } from './app-config.service';
import { RemoteHeadersService } from './remote-headers.service';
import { DynamicHostUrlService } from './dynamic-host-url.service';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  authUrls: any;
  apiUrls: any;

  constructor(
    private http: HttpClient,
    public appConfig: AppConfiguration,
    private hostUrlService: DynamicHostUrlService,
    private loadingService: LoadingService,
    private alertService: AlertService) {
    this.authUrls = this.appConfig.authUrls;
  }

  getHeaders() {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    const headers = RemoteHeadersService.headersConfig.headers.defaultHeader;
    headers['X-CSC-User-Id'] = (resultQueryParam ? resultQueryParam.userId : null);
    return new HttpHeaders(headers);
  }

  optionsByUrl(url: string) {
    return this.http.request(new HttpRequest('OPTIONS', url, { headers: this.getHeaders(), reportProgress: false })).pipe(
      map((event: HttpEvent<any>) => {
        if (event.type === HttpEventType.Response) {
          return event.body;
        }
      }),
      catchError(this.handleError));
  }

  getByUrl(url: string, headers?: HttpHeaders) {
    return this.http.get(url, { headers: headers || this.getHeaders() }).pipe(catchError(this.handleError));
  }

  getByUrlWithoutOptions(url: string) {
    return this.http.get(url).pipe(catchError(this.handleError));
  }

  getByUrlWithoutParsingToJson(url: string) {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    const getHeaders = new HttpHeaders({
      'X-CSC-User-Id': (resultQueryParam ? resultQueryParam.userId : null)
    });

    return this.http.get(url, { headers: getHeaders, responseType: 'blob' }).pipe(catchError(this.handleError));
  }

  public getByUrlWithoutParsingToJsonObserved(url: string): any {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    const headers = RemoteHeadersService.headersConfig.headers.fileHeaders;
    headers['X-CSC-User-Id'] = (resultQueryParam ? resultQueryParam.userId : null);
    const header = new HttpHeaders(headers);
    return this.http.get(url, { headers: header, responseType: 'blob'}).pipe(catchError(this.handleError));
  }

  getPDFByUrlWithoutParsingToJson(url: string) {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    const headers = RemoteHeadersService.headersConfig.headers.pdfHeaders;
    headers['X-CSC-User-Id'] = (resultQueryParam ? resultQueryParam.userId : null);
    const pdfHeaders = new HttpHeaders(headers);
    return this.http.get(url, { headers: pdfHeaders, responseType: 'blob' }).pipe(catchError(this.handleError));
  }

  getByPdfUrl(url: string) {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    const headers = RemoteHeadersService.headersConfig.headers.contentHeaders;
    headers['X-CSC-User-Id'] = (resultQueryParam ? resultQueryParam.userId : null);
    const contentHeaders = new HttpHeaders(headers);

    return this.http.get(url, { headers: contentHeaders }).pipe(catchError(this.handleError));
  }
  getByProdUrlWithoutParsingToJson(url: string) {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    const headers = RemoteHeadersService.headersConfig.headers.contentHeaders;
    headers['X-CSC-User-Id'] = (resultQueryParam ? resultQueryParam.userId : null);
    const contentHeaders = new HttpHeaders(headers);

    return this.http.get(url, { headers: contentHeaders, responseType: 'blob' }).pipe(catchError(this.handleError));
  }
  getByProdUrl(url: string) {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    const headers = RemoteHeadersService.headersConfig.headers.contentHeaders;
    headers['X-CSC-User-Id'] = (resultQueryParam ? resultQueryParam.userId : null);
    const contentHeaders = new HttpHeaders(headers);

    return this.http.get(url, { headers: contentHeaders }).pipe(catchError(this.handleError));
  }
  patchByUrl(url: string, obj: any, removeEmpty = false) {
    const body = JSON.stringify(this.removeRootElement({ obj }, removeEmpty));
    return this.http.patch(url, body, { headers: this.getHeaders() }).pipe(catchError(this.handleError));
  }

  putByUrl(url: string, obj: any, headers?: HttpHeaders, removeEmpty = false) {
    const body = JSON.stringify(this.removeRootElement({ obj }, removeEmpty));
    return this.http.put(url, body, { headers: headers || this.getHeaders() }).pipe(catchError(this.handleError));
  }

  postByUrl(url: string, obj: any, headers?: HttpHeaders, removeEmpty = false) {
    const body = JSON.stringify(this.removeRootElement({ obj }, removeEmpty));
    return this.http.post(url, body, { headers: headers || this.getHeaders() }).pipe(catchError(this.handleError));
  }

  postByUrlWithoutOptions(url: string, obj: any, removeEmpty = false) {
    const body = JSON.stringify(this.removeRootElement({ obj }, removeEmpty));
    return this.http.post(url, body).pipe(catchError(this.handleError));
  }

  postByUrlWithoutParsingToJson(url: string, obj: any, removeEmpty = false) {
    const body = JSON.stringify(this.removeRootElement({ obj }, removeEmpty));
    return this.http.post(url, body, { headers: this.getHeaders() }).pipe(catchError(this.handleError));
  }

  postFormDataByUrl(url: string, obj: any) {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    let defaultHeader = RemoteHeadersService.headersConfig.headers.defaultHeader;
    defaultHeader = Object.keys(defaultHeader).reduce((c, k) => (c[k.toLowerCase().trim()] = defaultHeader[k], c), {});
    const postHeaders = {
      'X-CSC-User-Id': (resultQueryParam ? resultQueryParam.userId : null)
    };
    if (defaultHeader['x-api-key']) {
      postHeaders['x-api-key'] = defaultHeader['x-api-key'];
    }
    const requestHeaders = new HttpHeaders(postHeaders);
    return this.http.post(url, obj, { headers: requestHeaders }).pipe(catchError(this.handleError));
  }

  deleteByUrl(url: string, headers?: HttpHeaders, removeEmpty = false) {
    return this.http.delete(url, { headers: headers || this.getHeaders() }).pipe(catchError(this.handleError));
  }

  public logoutUser(): void {
    if (this.authUrls.loginMethod === 'Cognito') {
      this.logoutAmazonCognitoUser();
    } else {
      const logoutUrl = this.authUrls.forgeRockAuthUrl + '/sessions?_action=logout';
      const headers = RemoteHeadersService.headersConfig.headers.forgerockHeaders;
      const forgerockHeaders = new HttpHeaders(headers);

      const logoutData = {};

      this.loadingService.toggleLoadingIndicator(true);
      this.postByUrl(logoutUrl, logoutData, forgerockHeaders).subscribe(
        () => {
          this.loadingService.toggleLoadingIndicator(false);
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
        }
      );

      sessionStorage.clear();
      this.alertService.error('Token expired');
      window.location.href = this.appConfig.uaaCommonLogoutURL +
        '?appid=pb360&returnUrl=' + encodeURIComponent(window.location.href);
    }

  }

  logoutAmazonCognitoUser() {
    sessionStorage.clear();
    window.location.href = this.appConfig.uaaCommonLogoutURL +
      '?appid=pb360&returnUrl=' + encodeURIComponent(window.location.href);
  }

  logoutSsoUser() {
    sessionStorage.clear();
    window.location.href = this.authUrls.ssoUrl;
  }

  validateToken(tokenId: string) {
    const authUrl = this.authUrls.forgeRockAuthUrl + '/sessions?_action=validate';
    const headers = RemoteHeadersService.headersConfig.headers.forgerockHeaders;
    const forgerockHeaders = new HttpHeaders(headers);
    this.loadingService.toggleLoadingIndicator(true);
    return this.postByUrl(authUrl, null, forgerockHeaders);
  }

  validateCognitoToken(userId: string, tokenId: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities) + 'v1/utils/users?filters=userid:' + userId;
    return this.getByUrl(url);
  }

  private handleError(error: Response | any): Observable<any> {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    if ((error && error.status === 401 && error.errorSubCode === 'Token') || !resultQueryParam.userId) {
      if ((this.authUrls.loginMethod === 'Forgerock' ||
        this.authUrls.loginMethod === 'Cognito') && this.authUrls.uaaCommonLogin) {
        this.logoutUser();
      } else {
        this.logoutSsoUser();
      }
    }
    return observableThrowError(error);
  }

  removeRootElement(obj: any, removeEmpty: boolean) {
    let numKeys = 0;
    let rootKey;

    for (const item in obj) {
      if (!obj.hasOwnProperty(item)) { continue; }
      rootKey = item;
      numKeys++;
      if (numKeys === 2) { break; }
    }

    if (numKeys === 1) {
      const newObj = {};
      const rootObj = obj[rootKey];

      if (typeof rootObj === 'object') {
        for (const key in rootObj) {
          if (rootObj.hasOwnProperty(key)) {
            if (removeEmpty && rootObj[key] !== undefined && rootObj[key] !== '') {
              if (typeof rootObj[key] === 'object') {
                newObj[key] = this.removeRootElement(rootObj[key], false);
              } else {
                newObj[key] = rootObj[key];
              }

            } else if (!removeEmpty) {
              newObj[key] = rootObj[key];
            }
          }
        }
        return newObj;
      }
    }
    return obj;
  }
}
