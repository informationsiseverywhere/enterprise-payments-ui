import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  headerText: BehaviorSubject<any> = new BehaviorSubject<any>('');
  childRouteData: BehaviorSubject<any> = new BehaviorSubject<any>('');

  constructor() { }

  setHeaderText(newHeaderText: any) {
    this.headerText.next(newHeaderText);
  }

  setChildRouteData(childRoute: any) {
    this.childRouteData.next(childRoute);
  }
}
