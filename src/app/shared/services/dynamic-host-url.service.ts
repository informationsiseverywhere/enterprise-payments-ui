import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { RemoteHostURLService } from './remote-host-url.service';

@Injectable({
  providedIn: 'root'
})
export class DynamicHostUrlService {
  hostUrlConfig: any;

  constructor(
    @Inject(DOCUMENT) private document: any) {
    this.hostUrlConfig = RemoteHostURLService.hostUrlConfig;
  }

  getHostUrl(url?: any) {
    if (this.hostUrlConfig.hostUrl) {
      return `${this.document.location.origin}${url}`;
    } else {
      return `${url}`;
    }
  }
}
