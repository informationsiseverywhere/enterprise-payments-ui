
import {share} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable ,  Observer } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoadingService {
    loading: Observable<boolean>;
    private _observer: Observer<boolean>;

    constructor() {
        this.loading = new Observable<boolean>(observer => this._observer = observer).pipe(share());
    }
    toggleLoadingIndicator(value: boolean) {

        if (this._observer) {
            this._observer.next(value);
        }
    }
}
