
import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { AppConfiguration } from './app-config.service';
import { AppLogger } from './app-logger.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { DynamicHostUrlService } from './dynamic-host-url.service';

@Injectable({
  providedIn: 'root'
})
export class DateService {
  _systemDate = new BehaviorSubject<any>('');
  readonly savedSystemDate = this._systemDate.asObservable();
  apiUrls: any;

  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    private appLoggerService: AppLogger) {  }

  get systemDate(): Observable<any> {
    return this._systemDate.asObservable();
  }

  getDate() {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/date`;
    this.restService.getByUrl(url).subscribe(
      data => {
        this._systemDate.next(data.date);
      },
      err => {
        this.appLoggerService.catchError(err);
      }
    );
  }

}
