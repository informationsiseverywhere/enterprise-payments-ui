import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HalProcessor {

    constructor() { }

    public linkParser(key: string, links: any[]): string {
        for (const k in links) {
            if (links.hasOwnProperty(k)) {
                const link = links[k];
                if (link.rel === key) {
                    return link.href;
                }
            }
        }
        return null;
    }

    public convertFromObjectToArray(object: any): any {
        const arr = [];
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                arr.push(object[key]);
            }
        }
        return arr;
    }

    public replacePlusSignsBySpaces(text: string): any {
        if (text) {
            return text.replace(/\+/g, ' ');
        }
    }

    public isFunction(value): boolean {
        return typeof value === 'function';
    }

    public isPlainObject(value): boolean {
        if (Object.prototype.toString.call(value) !== '[object Object]') {
            return false;
        }
        else {
            const prototype = Object.getPrototypeOf(value);
            return prototype === null || prototype === Object.prototype;
        }
    }

    public isString(value): boolean {
        return value && typeof value === 'string';
    }

    public isBoolean(value): boolean {
        return typeof value === 'boolean';
    }

    public isPromise(promise): boolean {
        return Object.prototype.toString.call(promise) === '[object Promise]';
    }

    public notEmptyValue(value): any {
        if (Array.isArray(value)) {
            return value.length > 0;
        }
        return value;
    }

    public transformStringToArray(value): any {
        if (this.isString(value)) {
            return [value];
        }
        return value;
    }
}
