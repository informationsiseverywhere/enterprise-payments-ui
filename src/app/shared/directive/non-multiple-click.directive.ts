import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appNonMultipleClick], a:not([appMultipleClick])'
})
export class NonMultipleClickDirective {
  constructor(private elem: ElementRef) { }

  @HostListener('click', ['$event'])
  clickEvent(event) {
    this.elem.nativeElement.classList.add('non-active-component');
    setTimeout(this.activeComponent, 500, this.elem.nativeElement);
  }

  private activeComponent(comp?: any): void {
    if (comp && comp.classList) { comp.classList.remove('non-active-component'); }
  }
}
