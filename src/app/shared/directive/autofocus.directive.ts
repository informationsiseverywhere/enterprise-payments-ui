import { Directive, ElementRef, Input } from '@angular/core';
import { AppConfiguration } from '../services/app-config.service';
import { SecurityEngineService } from '../services/security-engine.service';

@Directive({
  selector: '[appAutoFocus]'
})
export class AutoFocusDirective {
  constructor(private elem: ElementRef,
    private securityEngineService: SecurityEngineService,
    private appConfig: AppConfiguration) { }

  @Input()
  set autoFocus(val: boolean) {
    if (val) {
      this.focusComponentHandle();
    }
  }

  private focusComponentHandle(): void {
    if (this.appConfig.isComponentSecurity) {
      if (this.securityEngineService.isSecurityDataLoading) {
        this.securityEngineService.securityUpdated.subscribe(
          () => {
            this.focusToComponent();
          }
        );
      } else {
        this.focusToComponent();
      }
    } else {
      this.focusToComponent();
    }
  }

  private focusToComponent(): void {
    setTimeout(() => {
      this.elem?.nativeElement?.focus();
    },100);
  }
}
