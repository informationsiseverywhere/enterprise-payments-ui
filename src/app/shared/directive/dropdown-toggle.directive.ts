import { Directive, ElementRef, OnInit, AfterViewInit } from "@angular/core";
declare var $:any;

@Directive({
	selector: 'button(.dropdown-toggle), a(.dropdown-toggle)'
})
export class DropdownToggleAccessibleDirective implements OnInit, AfterViewInit {
  constructor(private elementRef: ElementRef) { }

  public ngAfterViewInit(): void{
    const element = this.elementRef.nativeElement;
    if(element?.classList.contains('dropdown-submenu-toggle')){
      this.bsSubmenuKeyboardAccessibility();
    }
  }

  public ngOnInit(): void {
    // On dropdown close
    $(document).on('hidden.bs.dropdown', function(event) {
      let dropdown = $(event.target);
      dropdown.find('.dropdown-submenu-section').removeClass('d-block');
      dropdown.find('.dropdown-menu').attr('aria-expanded', false);
      dropdown.find('.dropdown-toggle').focus();
    });
  }

  public bsSubmenuKeyboardAccessibility(): void{
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
      if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
      }
      let $subMenu = $(this).next('.dropdown-submenu-section');
      $subMenu.toggleClass('d-block');
      $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
        $('.dropdown-submenu .show').removeClass('show');
      });
      return false;
    });
    $('.dropdown-submenu').on('mouseover', function(e) {
      let $subMenu = $(this).find('.dropdown-submenu-section');
      $subMenu.addClass('d-block');
    });
    $('.dropdown-submenu').on('mouseout', function(e) {
      let $subMenu = $(this).find('.dropdown-submenu-section');
      $subMenu.removeClass('d-block');
    });
  }

}