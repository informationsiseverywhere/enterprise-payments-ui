import { Directive, ElementRef, Input, DoCheck } from "@angular/core";
import { FocusableElements } from 'src/app/shared/enum/common.enum';
declare var $:any;

@Directive({
	selector: 'a:not([href]):not([role]):not([tabindex]):not([x-no-tabbing]), [appTabbing]',
	host: {
		'tabindex': '0',
		'role': 'button',
		'(keydown.enter)': '$event.preventDefault(); $event.target.click();',
		'(keydown.space)': '$event.preventDefault(); $event.target.click();'
	}
})
export class TabbingClickDirective implements DoCheck  {
  private disableClassNames = ['disable-security-options', 'disable-dropdown-options', 'non-active-component'];
  private focusableElementsString: string = FocusableElements.layoutElements;
  private isTabbingFlag: boolean = true;

  constructor(private elementRef: ElementRef) { }

  @Input()
  set isTabbing(val: boolean) {
    this.isTabbingFlag = val;
    const element = this.elementRef?.nativeElement;
    if(!val){
      this.clearTabbingAccessibility(element);
    }else{
      this.setTabbingAccessibility(element);
    }
  }

  public ngDoCheck(): void {
    const element = this.elementRef.nativeElement;
    if (this.disableClassNames.some(className => element?.classList.contains(className))){
      this.clearTabbingAccessibility(element);
      this.setDisabledClass(element);
      return;
    }else if(element?.closest('.disable-security-options')){
      this.clearTabbingAccessibility(element);
      this.setDisabledClass(element);
      return;
    }else if(this.isTabbingFlag){
      this.setTabbingAccessibility(element);
      this.setDisabledClass(element, true);
    }
  }

  public clearTabbingAccessibility(element: any): void{
    if(element){
      element.removeAttribute('role');
      element.setAttribute('tabindex', '-1');
    }
  }

  public setTabbingAccessibility(element: any): void{
    if(element){
      element.setAttribute('tabindex', '0');
      element.setAttribute('role', 'button');
    }
  }

  public setDisabledClass(element: any, clearDisabled = false): void{
     if(element){
       if(clearDisabled){
        element?.classList?.remove('disabled');
       }else{
         if(!element?.classList?.contains('disabled') && element?.classList?.contains('dropdown-item')){
           element?.classList?.add('disabled');
         }
       }
     }
  }

  public focusFirstItem(pageContent: any, exceptClass?: string): void {
    if(pageContent){
      const focusableElements = this.getFocusableElements(pageContent, exceptClass);
      (focusableElements?.length > 0) && (focusableElements[0].focus());
    }
  }

  public getFocusableElements(pageContent: any, exceptClass?: string): Array<HTMLElement>{
    let focusableElements = this.focusableElementsString;
    let focusableElementsArray = Array.from(pageContent.querySelectorAll(focusableElements)) as Array<HTMLElement>;
    if(exceptClass && focusableElementsArray?.length > 0){
      focusableElementsArray = focusableElementsArray.filter(element => {
        return !element.classList?.contains(exceptClass);
      });
    }
    return focusableElementsArray;
  }
}