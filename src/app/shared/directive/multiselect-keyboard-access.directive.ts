import { Directive, ElementRef, HostListener } from "@angular/core";
import { CommonService } from 'src/app/shared/services/common.service';
import { EVENT_KEYS_MAP } from 'src/app/shared/enum/common.enum';

@Directive({
	selector: 'angular2-multiselect',
	host: {
		'(keydown.enter)': '$event.preventDefault(); $event.target.click();',
		'(keydown.space)': '$event.preventDefault(); $event.target.click();'
	}
})
export class MultiselectKeyboardAccessDirective {
	constructor(private elementRef: ElementRef,
		private commonService: CommonService) { }

	@HostListener('keydown', ['$event'])
    public onKeyDown(event: any): void {
        const keyPressName = this.commonService.keyPressIdentify(event.key);
        if(keyPressName === EVENT_KEYS_MAP.Tab){
			const currentFocusCheckBox = document.activeElement?.parentElement;
			if(currentFocusCheckBox.classList.contains('pure-checkbox')){
				currentFocusCheckBox.scrollIntoView({block: "center", inline: "nearest"});
			}  
        }else if(keyPressName === EVENT_KEYS_MAP.Esc){
			this.focusElement(this.elementRef.nativeElement.getElementsByClassName("c-btn")[0], event);
		}
		this.focusRemoveIcon();
    }

	public focusRemoveIcon(): void{
		if(this.elementRef?.nativeElement?.getElementsByClassName("c-remove").length > 0){
			const clearAllElements = this.elementRef.nativeElement.getElementsByClassName("c-remove");
			for (const el of clearAllElements) {
				el.tabIndex = 0;
				const currentEle = this;
				el.addEventListener('click', function(){ 
					currentEle.focusElement(currentEle.elementRef.nativeElement.getElementsByClassName("c-btn")[0]);
				}, false);
			}
		}
	}

	public focusElement(element: any, event?: any): void {
		setTimeout(() => {
			element?.focus();
			event && (event?.preventDefault());
		},);
	}
}