import { Directive, ElementRef, HostListener } from "@angular/core";
import { TAB } from '@angular/cdk/keycodes';

@Directive({
  selector: 'input, select, textarea, button, a, [appTabbing]'
})
export class FocusScrollDirective {
  private headerClassName = ['ent-navbar', 'ent-header', 'common-breadscrumb', 'sections-header'];
  constructor(private elementRef: ElementRef) { }

  @HostListener('keyup', ['$event'])
  public onKeyUp(event: any): void {
    if (event.keyCode === TAB) {
      if(document.querySelector('body').classList?.contains('modal-open')) return;
      this.scrollScreenArea(event.target);
    }
  }

  private findPosFromTop(ele: HTMLElement): number {
   return ele.getBoundingClientRect().top + window.scrollX;
  }

  private getHeaderHeight(): number{
    let headerHeight: number = 0;
    for (const name of this.headerClassName) {
      const headerEle = document.querySelector(`.${name}`);
      headerEle && (headerHeight += headerEle.clientHeight);
    }
    return headerHeight;
  }

  public scrollScreenArea(el: any): void {
    const topOffset = this.getHeaderHeight();
    const position = this.findPosFromTop(el);
    if (position < topOffset) {
      el.scrollIntoView({ block: "center", inline: "nearest" });
    }
  }
}