import { Directive, ElementRef, AfterContentInit } from "@angular/core";
import { AppConfiguration } from '../services/app-config.service';
import { SecurityEngineService } from '../services/security-engine.service';
import { FocusableElements } from 'src/app/shared/enum/common.enum';

@Directive({
	selector: '[appFocusFirst]'
})

export class FocusFirstDirective implements AfterContentInit{
  private focusableElementsString: string = FocusableElements.formElements;

  constructor(private elementRef: ElementRef,
    private securityEngineService: SecurityEngineService,
    private appConfig: AppConfiguration) { }

  public ngAfterContentInit(): void{
    const element = this.elementRef.nativeElement;
    if (this.appConfig.isComponentSecurity) {
      if (this.securityEngineService.isSecurityDataLoading) {
        this.securityEngineService.securityUpdated.subscribe(
          () => {
            this.focusFirstItem(element);
          }
        );
      } else {
        this.focusFirstItem(element);
      }
    } else {
      this.focusFirstItem(element);
    }
  }

  public focusFirstItem(pageContent: any): void {
    if(pageContent){
      setTimeout(() => {
        const focusableElements = this.getFocusableElements(pageContent);
        (focusableElements?.length > 0) && (focusableElements[0].focus());
      },);
    }
  }

  public getFocusableElements(pageContent: any): Array<HTMLElement>{
    return Array.from(pageContent.querySelectorAll(this.focusableElementsString)) as Array<HTMLElement>;
  }
}