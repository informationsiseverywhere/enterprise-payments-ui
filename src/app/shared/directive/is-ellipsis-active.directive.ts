import { Directive, ElementRef, HostListener } from '@angular/core';
declare var $:any;
$('html').on('click', function(e) {
  if (typeof $(e.target).data('original-title') == 'undefined') {
    $('[data-toggle="tooltip"]').tooltip('hide');
    $('[data-toggle-second="tooltip"]').tooltip('hide');
  }
});

@Directive({
  selector: '[isEllipsisActive]'
})
export class IsEllipsisActiveDirective {
  constructor(private elementRef: ElementRef) {}

  @HostListener('focusin')
  @HostListener('mouseenter')
  public onMouseEnter(): void {
    const element = this.elementRef.nativeElement;
    if(element?.type === 'button' && element?.children[0]){
      //IE11 Fix
      this.setTitleContent(element.children[0], element);
    }else{
      this.setTitleContent(element);
    }
  }

  public setTitleContent(element: any, parentElement?: any): void{
    this.resetTooltip(element, parentElement);
    if(element.offsetWidth < element.scrollWidth) {
      if(parentElement){
        parentElement.setAttribute('data-original-title', element.textContent);
        $(parentElement).tooltip('show');
        $(parentElement).tooltip('update');
      }else{
        element.setAttribute('data-original-title', element.textContent);
        $(element).tooltip('show');
        $(element).tooltip('update');
      }
    }  
  }

  private resetTooltip(element?: any, parentElement?: any): void{
    $('[data-toggle="tooltip"]').tooltip('hide');
    $('[data-toggle-second="tooltip"]').tooltip('hide');
    element&& element.removeAttribute('data-original-title');
    parentElement && parentElement.removeAttribute('data-original-title');
  }
}
