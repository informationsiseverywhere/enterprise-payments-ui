import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'dateFilter',
    pure: true
})

/*
* if we use | dateFilter only and don't pass any format there => format will be MMM-dd
* if we use | dateFilter:'MMM dd, yyyy',  => format will be MMM dd, yyyy
*/

export class DateFilter implements PipeTransform{

    private datePipe:DatePipe = new DatePipe('en-US');

    transform(value: any, format:string='MMM-dd'): any {
        if(value){
            value = this.convertDateToUTCFormat(value);
            let inputDate = new Date(value);
	    
	     return this.datePipe.transform(inputDate, format);
        }
    }

    convertDateToUTCFormat(dateString: string) {
        if (dateString.length === 10) {
            return dateString;
        }

        if (dateString.indexOf('T') >= 0) {
            return dateString;
        }

        const splitSpace = dateString.split(' ');
        const utcDate = splitSpace[0] + 'T' + splitSpace[1]; // + 'Z';
        return utcDate;
    }
}


