import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'timestampPipe',
    pure: true
})
export class TimestampPipe implements PipeTransform {

    transform(value: string, args?: any): any {
        if (value) {
            const year = value.substr(0, 4);
            const month = value.substr(4, 2);
            const date = value.substr(6, 2);
            const hour = value.substr(8, 2);
            const minute = value.substr(10, 2);
            const second = value.substr(12, 2);

            return year + '-' + month + '-' + date + ' ' + hour + ':' + minute + ':' + second;
        }
    }

}
