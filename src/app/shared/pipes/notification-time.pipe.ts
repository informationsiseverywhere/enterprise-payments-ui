
import {interval as observableInterval,  Observable } from 'rxjs';

import {map, startWith} from 'rxjs/operators';
import { Pipe, ChangeDetectorRef } from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'notificationTime',
    pure: false
})
export class NotificationTimePipe extends AsyncPipe {
    value:Date;
    timer:Observable<string>;
    private datePipe:DatePipe = new DatePipe('en-US');

    constructor(ref:ChangeDetectorRef) {
        super(ref);
    }

    transform(obj:any):any {
        if(obj) {
            obj = obj.replace(/-/g,'/');
            this.value = new Date(obj);

            if(!this.timer) {
                this.timer = this.getObservable("2017/09/10");
            }

            return super.transform(this.timer);
        }
    }

    private getObservable(systemDate:string) {
        return observableInterval(1000).pipe(startWith(0),map((time) => {
            var result:string;
            // current time
            systemDate = systemDate.replace(/-/g,'/');
            let now = new Date(systemDate).getTime() + (time * 1000);

            // time since message was sent in seconds
            let delta = (now - this.value.getTime()) / 1000;

            // format string
            if (delta < 10) {
                result = 'Just a moment ago';
            } else if (delta < 60) { // sent in last minute
                result = Math.floor(delta) > 1 ? Math.floor(delta) + ' seconds ago' : Math.floor(delta) + ' second ago';
            } else if (delta < 3600) { // sent in last hour
                result = Math.floor(delta / 60) > 1 ? Math.floor(delta / 60) + ' minutes ago' : Math.floor(delta / 60) + ' minute ago';
            } else if (delta < 86400) { // sent on last day
                result = Math.floor(delta / 3600) > 1 ? Math.floor(delta / 3600) + ' hours ago' : Math.floor(delta / 3600) + ' hour ago';
            } else { // sent more than one day ago
                result = Math.floor(delta / 86400) > 1 ? this.datePipe.transform(this.value, 'MMM dd, yyyy') : 'Yesterday';
                result += " at " + this.datePipe.transform(this.value, 'jm');
            }
            return result;
        }),);
    };
}