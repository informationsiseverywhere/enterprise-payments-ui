import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SidebarModule } from 'ng-sidebar';
import { AppLoggerComponent } from './components/app-logger/app-logger.component';
import { AppConfiguration } from './services/app-config.service';
import { MapToIterable } from './pipes/mapToIterable.pipe';
import { DateFilter } from './pipes/dateFilter.pipe';
import { LibraryModule } from '../../app/libs/libs.module';
import { HelperComponent } from './components/helper/helper.component';
import { TimestampPipe } from './pipes/timestamp.pipe';
import { NotificationTimePipe } from './pipes/notification-time.pipe';
import { SecuritySettingsComponent } from './components/security-settings/security-settings.component';
import { SecurityConfigComponent } from './components/security-settings/security-config/security-config.component';
import { SecuritySidebarComponent } from '../shared/components/security-settings/security-sidebar/security-sidebar.component';
import { SecuritySidebarDetailComponent } from '../shared/components/security-settings/security-sidebar-detail/security-sidebar-detail.component';
import { NavigationComponent } from '../ent-payments/navigation/navigation.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AlternatePaymentMethodComponent } from './components/alternate-payment-method/alternate-payment-method.component';
import { PaymentReceivedComponent } from './components/payment-received/payment-received.component';
import { SelectAgentComponent } from './components/select-agent/select-agent.component';
import { CommonComponent } from './components/common/common.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    HttpClientModule,
    RouterModule,
    LibraryModule,
    SidebarModule.forRoot()
  ],
  declarations: [
    NavigationComponent,
    AppLoggerComponent,
    DateFilter,
    MapToIterable,
    HelperComponent,
    TimestampPipe,
    NotificationTimePipe,
    SecuritySettingsComponent,
    SecurityConfigComponent,
    SecuritySidebarComponent,
    SecuritySidebarDetailComponent,
    NavbarComponent,
    AlternatePaymentMethodComponent,
    PaymentReceivedComponent,
    SelectAgentComponent,
    CommonComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    AppLoggerComponent,
    SidebarModule,
    DateFilter,
    MapToIterable,
    HelperComponent,
    TimestampPipe,
    NotificationTimePipe,
    SecuritySettingsComponent,
    SecuritySidebarComponent,
    NavigationComponent,
    NavbarComponent,
    AlternatePaymentMethodComponent,
    PaymentReceivedComponent,
    SelectAgentComponent,
    CommonComponent
  ],
  entryComponents: [
  	SecurityConfigComponent,
    PaymentReceivedComponent,
    SelectAgentComponent
  ],
  providers: [
    AppConfiguration
  ]
})
export class SharedModule { }
