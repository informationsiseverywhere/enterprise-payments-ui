import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { QueryParam } from 'src/app/libs/select/models/select.model';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { AppConfiguration } from '../../services/app-config.service';
import { DynamicHostUrlService } from '../../services/dynamic-host-url.service';
import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';

@Component({
  selector: 'app-select-agent',
  templateUrl: './select-agent.component.html'
})

export class SelectAgentComponent implements OnInit {
  public agentForm: FormGroup;
  public apiUrl: string = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties';
  public agent: any;
  public queryParam: QueryParam = new QueryParam('', null, null, 1, 10);
  private agencies: any;

  constructor(
    private fb: FormBuilder,
    private modal: ModalComponent,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    private paymentService: PaymentService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.agentForm = this.fb.group({
      agentNumber: ['', Validators.required],
    });
    let QUERYPARAMS = '';
    this.agencies = this.paymentService.agencies;
    const isAgencySweepPayorChange = this.paymentService.isAgencySweepPayorChange;
    const isUserAgent = this.paymentService.isUserAgent;
    const agentType = this.paymentService.agentType;
    if (isUserAgent === 'false' && !this.agencies && (agentType === 'Select' || agentType === 'Change')) {
      this.getAllAgenices();
    } else {
      if (isUserAgent === 'false' && this.agencies && isAgencySweepPayorChange === 'true' && agentType === 'Change') {
        this.getAllAgenices();
      } else {
        if (this.agencies) { 
          QUERYPARAMS += 'type:Agent,'; 
          for (const [i, agency] of this.agencies.entries()) {
            QUERYPARAMS += 'agentNumber:' + (agency.agentNumber || agency) + ((i === this.agencies.length - 1) ? '' : ',');
          }
        }
        this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
        if (isUserAgent === 'false') {
          this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties';
        } else {
          this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/userAgencies';
        }
      }
    }
  }

  getAllAgenices() {
    const QUERYPARAMS = 'type:Agent,isAgency:true';
    this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties';
  }
  public getAgentNumber(data?: any): void {
    if (data.agentNumber) {
      this.agent = data;
    } else {
      this.agent = null;
      this.agentForm.reset();
      setTimeout(() => {
        this.alertService.error('Selected Agent does not have agency number associated. Select agency number associated with Agent.');
      }, 50);
    }
  }

  public selectAgent(): void {
    this.modal.close(this.agent);
  }
}
