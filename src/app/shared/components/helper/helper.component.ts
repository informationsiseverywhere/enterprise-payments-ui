import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ModalComponent } from 'src/app/libs/modal/modal.component';


@Component({
  selector: 'app-helper',
  templateUrl: './helper.component.html',
  styleUrls: ['./helper.component.scss']
})
export class HelperComponent implements OnInit {
  @Input() message = '';
  @Input() iconClass: string;
  @ViewChild('infoModal', { static: false }) infoModal: ModalComponent;

  constructor() {
  }

  ngOnInit() {}

  showInfoModal() {
    this.infoModal.modalTitle = 'Information';
    this.infoModal.modalFooter = true;
    this.infoModal.cancelButton = false;
    this.infoModal.okButtonText = 'CLOSE';
    this.infoModal.modalMessage = true;
    this.infoModal.message = this.message;
    this.infoModal.open();
  }
}
