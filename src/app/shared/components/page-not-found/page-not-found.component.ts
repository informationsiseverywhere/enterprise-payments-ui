import { OnInit, Component } from '@angular/core';

import { AppConfiguration } from '../../services/app-config.service';


@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html'
})

export class PageNotFoundComponent implements OnInit {

  message = '';
  messageHeader = 'Page Not Found';
  messageContent = '';
  errorDescription = 'Hint :  Page Not Found';
  supportEmail: any;
  landingPageLink: any;
  multiHostUrls: any;

  constructor(
    private appConfig: AppConfiguration) {
      this.multiHostUrls = this.appConfig.multiHostUrls;
     }

  ngOnInit() {
    this.landingPageLink = this.appConfig.landingPageLink;
    this.supportEmail = this.multiHostUrls.supportEmail;
    this.messageContent = 'Page you are looking is not available.';
    this.message = 'Please contact your System Administrator';
  }
}

