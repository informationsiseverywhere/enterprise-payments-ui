import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { AppConfiguration } from '../../services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { environment } from 'src/environments/environment';
import { PaymentModel } from 'src/app/ent-payments/batch-payments/models/batch.model';
import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';
import { SecurityEngineService } from '../../services/security-engine.service';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';
import { PropertyName } from '../../enum/common.enum';

declare var Accept: any;
@Component({
  selector: 'app-alternate-payment-method',
  templateUrl: './alternate-payment-method.component.html',
  styleUrls: ['./alternate-payment-method.component.scss']
})
export class AlternatePaymentMethodComponent implements OnInit, OnDestroy {

  makeBankAccountForm: FormGroup;
  makeCreditCardForm: FormGroup;
  payment: PaymentModel = new PaymentModel();
  supportData: any;
  accountTypeDropdown: any = [];
  @ViewChild('acceptForm') acceptForm: ElementRef;
  accountId: any;
  isCustomerProfileId: any;
  type: any;
  subType: any;
  returnAppId: any;
  policyId: any;
  polEffDt: any;
  multiHostUrls: any;
  public billingType: any;
  public statementDate: any;
  public oneTimePaymentIntegrationDropdown: any;
  public oneTimePaymentIntegration = false;
  public accountTypeDefaultValue: any;
  paymentProviderDropdown: any;
  paymentProvider: any;

  constructor(
    private _fb: FormBuilder,
    private loadingService: LoadingService,
    private router: Router,
    private alertService: AlertService,
    private appConfig: AppConfiguration,
    private paymentService: PaymentService,
    private route: ActivatedRoute,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    public commonService: CommonService) {
    this.multiHostUrls = this.appConfig.multiHostUrls;
  }


  ngOnInit() {
    this.getPaymentsSupportData();
    this.route.queryParams.subscribe(
      queryParams => {
        this.accountId = queryParams.accountId;
        this.type = queryParams.type;
        this.subType = queryParams.subType;
        this.isCustomerProfileId = queryParams.isCustomerProfileId;
        this.returnAppId = queryParams.returnAppId;
        this.policyId = queryParams.policyId;
        this.polEffDt = queryParams.polEffDt;
        this.billingType = queryParams.billingType;
        this.statementDate = queryParams.statementDate;
      });
    this.makeBankAccountForm = this._fb.group({
      storeAccount: [''],
      fullName: ['', Validators.compose([Validators.required, Validators.maxLength(22)])],
      routingNumber: ['', Validators.compose([Validators.required, ValidationService.numericValidator, Validators.maxLength(9), Validators.minLength(9)])],
      accountNumber: ['', Validators.compose([Validators.required, ValidationService.numericValidator, Validators.maxLength(17)])],
      accountType: ['', Validators.required]
    });
    this.makeCreditCardForm = this._fb.group({
      storeCard: [''],
      fullName: ['', Validators.required],
      creditCardNumber: ['', [Validators.required, ValidationService.creditCardValidator]],
      creditCardExpiryDate: ['', [Validators.required, ValidationService.cardExpireDateValidator]],
      cvvNumber: ['', [Validators.required, ValidationService.ccvCodeValidator]]
    });
    this.checkSecurityOption();
  }

  private loadAcceptJSScript(): void {
    const imported = document.createElement('script');
    imported.src = environment.acceptJS.acceptUrl;
    imported.type = environment.acceptJS.type;
    imported.charset = environment.acceptJS.charset;
    document.head.appendChild(imported);
  }

  ngOnDestroy() {
    if (this.oneTimePaymentIntegration) {
      this.removeJSFile(environment.acceptJS.acceptUrl);
      this.removeJSFile(environment.acceptJS.acceptCoreUrl);
    }
  }

  private removeJSFile(fileName: string): void {
    const allsuspects = document.getElementsByTagName('script');
    for (let i = allsuspects.length; i >= 0; i--) {
      if (allsuspects[i] && allsuspects[i].getAttribute('src') != null &&
        allsuspects[i].getAttribute('src').indexOf(fileName) !== -1) {
        allsuspects[i].parentNode.removeChild(allsuspects[i]);
      }
    }
  }

  private getPaymentsSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentsSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.accountType);
          this.getDropdownItems(PropertyName.paymentProvider);
          this.getDropdownItems(PropertyName.oneTimePaymentIntegration);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

  public restrictInitialSpace(event) {
    this.payment.fullName = this.payment.fullName.replace(/[^A-Za-z0-9-,.;'()_@/#&+! ]|^ /g, '');
  }

  public showHideControls(selectedValue: string): void {
    this.payment = new PaymentModel();
    this.payment.paymentMethod = selectedValue;
    if (selectedValue === PropertyName.BankAccount) {
      this.makeBankAccountForm.reset();
      if (this.accountTypeDefaultValue) {
        this.payment.accountType = this.accountTypeDefaultValue;
        this.makeBankAccountForm.patchValue(this.payment);
      }
    } else {
      this.makeCreditCardForm.reset();
    }
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (propertyName === PropertyName.accountType && sd.defaultValue) {
          this.accountTypeDefaultValue = sd.defaultValue;
          this.checkSecurityOption();
        }
      }
    }
    this.paymentProviderDropdown && (this.paymentProvider = this.paymentProviderDropdown[0]);
    let oneTimePaymentIntegration = 'false';
    this.oneTimePaymentIntegrationDropdown && (oneTimePaymentIntegration = this.oneTimePaymentIntegrationDropdown[0]);
    if (oneTimePaymentIntegration === 'true') {
      this.oneTimePaymentIntegration = true;
      if ((this.paymentProvider === PropertyName.authorizeNet && this.oneTimePaymentIntegration)) {
        this.loadAcceptJSScript();
      }
    }
    else if (propertyName === PropertyName.oneTimePaymentIntegration && oneTimePaymentIntegration === 'false') {
      this.oneTimePaymentIntegration = false;
      this.makeBankAccountForm.get("fullName").setValidators(null);
      this.makeBankAccountForm.get("fullName").updateValueAndValidity();
    }
  }

  public setSelectedCardValue(): void {
    const visaPattern = /^4[0-9]{12}(?:[0-9]{3})?$/;
    const masterPattern = /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/;
    const americanPattern = /^3[47][0-9]{13}$/;
    const discoverPattern = /^6(?:011|5[0-9]{2})[0-9]{12}$/;

    if (this.payment.creditCardNumber.match(visaPattern)) {
      this.payment.creditCardType = PropertyName.Visa;
    } else if (this.payment.creditCardNumber.match(masterPattern)) {
      this.payment.creditCardType = PropertyName.MasterCard;
    } else if (this.payment.creditCardNumber.match(americanPattern)) {
      this.payment.creditCardType = PropertyName.AmericanExpress;
    } else if (this.payment.creditCardNumber.match(discoverPattern)) {
      this.payment.creditCardType = PropertyName.Discover;
    } else {
      this.payment.creditCardType = '';
    }
  }

  public addBankAccount(): void {
    const bankDetails = {
      accountNumber: this.payment.accountNumber,
      routingNumber: this.payment.routingNumber,
      nameOnAccount: this.payment.fullName,
      accountType: this.oneTimePaymentIntegration ? this.payment.accountType.toLowerCase() : this.payment.accountType
    };

    if (this.oneTimePaymentIntegration) {
      const secureData = {
        authData: environment.acceptJS.authData,
        bankData: bankDetails,
      };

      this.handleAddCreditBankAccountAction(secureData, PropertyName.BankAccount);
    } else {
      const validatePaymentMethod = {
        paymentMethod: PropertyName.BankAccount,
        accountType: this.payment.accountType,
        paymentType: PropertyName.WebsiteCheck1xACH,
        routingNumber: this.payment.routingNumber,
        bankAccountNumber: this.payment.accountNumber
      };
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.validatePaymentMethod(validatePaymentMethod).subscribe(
        (data: any) => {
          this.loadingService.toggleLoadingIndicator(false);
          if (data) {
            this.paymentService.newPaymentDetails = bankDetails;
            this.navigateToAddPayment(PropertyName.BankAccount);
          }
        },
        (err: any) => {
          this.loadingService.toggleLoadingIndicator(false);
          this.commonService.handleErrorResponse(err);
        }
      );
    }
  }

  public addCreditCard(): void {
    let cardDetails: any;
    if (this.oneTimePaymentIntegration) {
      cardDetails = {
        cardNumber: this.payment.creditCardNumber,
        month: this.payment.creditCardExpiryDate.substring(0, 2),
        year: this.payment.creditCardExpiryDate.substring(3),
        cardCode: this.payment.cvvNumber,
        fullName: this.payment.fullName
      };
    } else {
      cardDetails = {
        cardNumber: this.payment.creditCardNumber,
        cardType: this.payment.creditCardType,
        expirationDate: this.payment.creditCardExpiryDate,
        cardCode: this.payment.cvvNumber
      };
    }

    if (this.oneTimePaymentIntegration) {
      const secureData = {
        authData: environment.acceptJS.authData,
        cardData: cardDetails
      };
      this.handleAddCreditBankAccountAction(secureData, PropertyName.CreditCard);
    } else {
      const validatePaymentMethod = {
        paymentMethod: PropertyName.CreditCard,
        paymentType: PropertyName.CreditCardPayment,
        creditCardNumber: this.payment.creditCardNumber,
        creditCardExpiryDate: this.commonService.removeSpacesAndFWDash(this.payment.creditCardExpiryDate),
        creditCardType: this.payment.creditCardType
      };
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.validatePaymentMethod(validatePaymentMethod).subscribe(
        (data: any) => {
          this.loadingService.toggleLoadingIndicator(false);
          if (data) {
            this.paymentService.newPaymentDetails = cardDetails;
            this.navigateToAddPayment(PropertyName.CreditCard);
          }
        },
        (err: any) => {
          this.loadingService.toggleLoadingIndicator(false);
          this.commonService.handleErrorResponse(err);
        }
      );
    }
  }

  private handleAddCreditBankAccountAction(secureData?: any, action?: string): void {
    Accept.dispatchData(secureData, response => {
      if (response.messages.resultCode === 'Error') {
        let i = 0;
        setTimeout(() => {
          while (i < response.messages.message.length) {
            this.alertService.error(response.messages.message[i].text);
            i = i + 1;
          }
        }, 500);
      } else {
        this.navigateToAddPayment(action, response);
      }
    });
  }

  private navigateToAddPayment(action?: string, response?: any): void {
    const sendBody: any = {
      paymentNonce: response ? response.opaqueData.dataValue : null,
      paymentMethod: this.payment.paymentMethod,
      fullName: this.payment.fullName,
    };
    if (action === PropertyName.CreditCard) {
      sendBody.creditCardNumber = this.payment.creditCardNumber;
      sendBody.creditCardType = this.payment.creditCardType;
      sendBody.storeCard = this.payment.storeCard;
      sendBody.creditCardExpiryDate = this.payment.creditCardExpiryDate;
    } else if (action === PropertyName.BankAccount) {
      sendBody.accountNumber = this.payment.accountNumber;
      sendBody.accountType = this.payment.accountType;
      sendBody.storeAccount = this.payment.storeAccount;
      sendBody.routingNumber = this.payment.routingNumber;
    }
    this.paymentService.newPaymentDetails = sendBody;
    let routeName = 'consumer-payment';
    if (this.type === 'Group') {
      routeName = 'group-payment';
    } else if (this.type === 'policy') {
      routeName = 'policy-payment';
    } else if (this.subType === 'RecurringPayment') {
      routeName = 'recurring-payment';
    } else if (this.type === 'Unidentified') {
      routeName = 'unidentified-payment';
    }
    this.router.navigate(['/', routeName], {
      queryParams: {
        accountId: this.accountId,
        policyId: this.policyId,
        polEffDt: this.polEffDt,
        isCustomerProfileId: false,
        returnAppId: this.returnAppId,
        billingType: this.billingType,
        statementDate: this.statementDate,
        type: this.type
      }
    });
  }

  public navigateToSetUpPayment(): void {
    let routeName = 'consumer-payment';
    if (this.type === 'Group') {
      routeName = 'group-payment';
    } else if (this.type === 'policy') {
      routeName = 'policy-payment';
    } else if (this.subType === 'RecurringPayment') {
      routeName = 'recurring-payment';
    } else if (this.type === 'Unidentified') {
      routeName = 'unidentified-payment';
    }
    this.paymentService.accountDetails = null;
    this.paymentService.newPaymentDetails = null;
    this.router.navigate(['/', routeName], {
      queryParams: {
        accountId: this.accountId,
        policyId: this.policyId,
        polEffDt: this.polEffDt,
        isCustomerProfileId: false,
        returnAppId: this.returnAppId,
        billingType: this.billingType,
        statementDate: this.statementDate,
        type: this.type
      }
    });
  }

  public checkAmexCard(): any {
    if (this.makeCreditCardForm.controls.creditCardNumber.valid && this.payment.creditCardNumber[0] === '3') {
      this.makeCreditCardForm.controls.cvvNumber.setValidators([Validators.required, ValidationService.ccvCodeAmexValidator]);
    } else {
      this.makeCreditCardForm.controls.cvvNumber.setValidators([Validators.required, ValidationService.ccvCodeValidator]);
    }
    this.makeCreditCardForm.controls.cvvNumber.updateValueAndValidity();
    return false;
  }

  public openSecurity(event?: any, configType?: any, excludeField?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, configType, excludeField);
  }

  public checkSecurityOption(): void {
    if (this.securityEngineService.checkVisibility('paymentMethodBankAccount', PropertyName.Show)) {
      this.showHideControls(PropertyName.BankAccount);
    } else if (this.securityEngineService.checkVisibility('paymentMethodCreditCard', PropertyName.Show)) {
      this.showHideControls(PropertyName.CreditCard);
    }
  }
}
