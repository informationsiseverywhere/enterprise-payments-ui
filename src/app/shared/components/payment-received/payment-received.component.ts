import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { DateService } from 'src/app/shared/services/date.service';
import { AppConfiguration } from '../../services/app-config.service';
import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';
import { CurrencyPipe } from '@angular/common';
import { SecurityEngineService } from '../../services/security-engine.service';
import { PropertyName } from '../../enum/common.enum';

@Component({
  selector: 'app-payment-received',
  templateUrl: './payment-received.component.html',
  styleUrls: ['./payment-received.component.scss'],
  providers: [CurrencyPipe]
})

export class PaymentReceivedComponent implements OnInit {
  public propertyName = PropertyName;
  paymentReceived: any;
  systemDate: any;
  accountDetails: any;
  payorName: any;
  payorEmail: any;
  isEmail = false;
  payorPhone: any;
  isEmailSent = false;
  private returnAppId: any;
  multiHostUrls: any;
  public currency: any;
  public type: any;
  public identifier: any;
  public policyNumber: any;
  public policySymbol: any;
  public isPolicyUnidentifiedPayment: boolean = false;

  constructor(
    private paymentService: PaymentService,
    private modal: ModalComponent,
    private loadingService: LoadingService,
    private commonService: CommonService,
    private route: ActivatedRoute,
    private appConfig: AppConfiguration,
    private router: Router,
    private currencyPipe: CurrencyPipe,
    public securityEngineService: SecurityEngineService,
    private dateService: DateService) {
    this.multiHostUrls = this.appConfig.multiHostUrls;
  }

  ngOnInit() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.route.queryParams.subscribe(
      queryParams => {
        this.returnAppId = queryParams.returnAppId;
      });
    if (this.paymentService.objectToModal) {
      this.paymentReceived = this.paymentService.objectToModal;
    } else {
      this.paymentReceived = undefined;
    }
    if (this.paymentService.accountDetails) {
      this.accountDetails = this.paymentService.accountDetails;
      this.type = this.accountDetails ? this.accountDetails.type : '';
      this.identifier = this.accountDetails ? this.accountDetails.identifier : '';
      this.policyNumber = this.accountDetails ? this.accountDetails.policyNumber : '';
      this.policySymbol = this.accountDetails ? this.accountDetails.policySymbol : '';
      this.isPolicyUnidentifiedPayment = this.accountDetails ? this.accountDetails.isPolicyUnidentifiedPayment : false;
      
      if (this.accountDetails.currency) { this.currency = this.accountDetails.currency; }
      if (this.accountDetails.type !== PropertyName.Agent) {
        this.payorName = this.accountDetails ? this.accountDetails.payorName : '';
        this.payorEmail = this.accountDetails ? (this.accountDetails.payorEmail ?
          this.accountDetails.payorEmail : this.accountDetails.emailId) : '';
        this.payorPhone = this.accountDetails ? (this.accountDetails.payorPhone ?
          this.accountDetails.payorPhone : this.accountDetails.phoneNumber) : '';
      } else {
        this.payorName = this.accountDetails ? this.accountDetails.name : '';
        this.payorEmail = this.accountDetails ? (this.accountDetails.emailId ?
          this.accountDetails.emailId : this.accountDetails.emailId) : '';
        this.payorPhone = this.accountDetails ? (this.accountDetails.phoneNumber ?
          this.accountDetails.phoneNumber : this.accountDetails.phoneNumber) : '';
      }
      this.smsNotification();
    }
  }
  public dismiss(): void {
    this.modal.close(false);
  }

  public print(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentPdf(this.paymentReceived.paymentSequenceNumber, this.paymentReceived.postedDate,
      this.paymentReceived.userId, this.paymentReceived.batchId).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);

          const filename = 'PaymentReceipt_' + this.paymentReceived.postedDate + '_' +
            this.paymentReceived.userId + '_' + this.paymentReceived.paymentSequenceNumber + '_' + this.paymentReceived.batchId + '.pdf';

          if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(data, filename);
          } else {
            const url = window.URL.createObjectURL(data);
            const link = document.createElement('a');
            document.body.appendChild(link);
            link.download = filename;
            link.href = url;
            link.click();
          }
        },
        err => {
          this.commonService.handleErrorResponse(err);
        });
  }
  public email(): void {
    if (this.multiHostUrls.notifications.turnOnEmail) {
      this.isEmail = false;
      if (!this.payorEmail) {
        this.isEmail = true;
      }
    }
    const template = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html> <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> </head> <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="margin: 0pt auto; padding: 0px; background:#F4F7FA; margin: 20px 0;"> <table id="main" width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#F4F7FA"> <tbody> <tr> <td valign="top"> <table class="innermain" cellpadding="0" width="580" cellspacing="0" border="0" bgcolor="#F4F7FA" align="center" style="margin:0 auto; table-layout: fixed;"> <tbody> <tr> <td colspan="4"> <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-radius: 4px; box-shadow: 0 2px 8px rgba(0,0,0,0.05);"> <tbody> <tr> <td style=" padding-top: 30px;" align="center" valign="bottom" colspan="2" cellpadding="3"> <img alt="Success" width="80" src="https://www.coinbase.com/assets/app/succeed-green-dcb087e9c6e5265b4c49f75c9c2e1d08bc894bc54816d9a5a476611f631b2929.png" /> </td> </tr> <tr> <td height="20"></td> </tr> <tr style="font-family: -apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif; color:#4E5C6E; font-size:14px; line-height:20px; margin-top:20px;"> <td class="content" colspan="2" valign="top" align="center" style="padding-left:60px; padding-right:60px;"> <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff"> <tbody> <tr> <td valign="top" align="center"> <div style="font-size: 18px; line-height: 32px; font-weight: 500; margin-left: 20px; margin-right: 20px;color: black;">PAYMENT RECEIVED</div> </td> </tr> <tr> <td height="10" &nbsp;=""></td> </tr> <tr> <td align="center"> <div style="font-size: 12px; line-height: 20px; font-weight: 500; margin-left: 10px; margin-right: 10px; margin-bottom: 10px; color:#90949c">Your payment of ' + this.currencyPipe.transform(this.paymentReceived.paymentAmount, this.currency) + ' is received . It may take up to 24 hours of your online balance to display this payment.</div> </td> </tr> <tr> <td height="14" &nbsp;=""></td> </tr> <tr> <td height="1" bgcolor="#DAE1E9"></td> </tr> <tr> <td height="14" &nbsp;=""></td> </tr> <tr> <td align="center"> <table style="width: 100%; border-collapse:collapse;"> <tbody style="border: 0; padding: 0; margin-top:20px;"> <tr> <td width="50%" valign="top" style=" color:#90949c">Transaction ID:</td> <td style="padding: 5px 0;">' + (this.paymentReceived.transactionId) + '</td> </tr> <tr> <td width="50%" valign="top" style=" color:#90949c">Payor:</td> <td style="padding: 5px 0;">' + (this.payorName) + '</td> </tr> <tr> <td style=" color:#90949c">Account: </td> <td style="padding: 5px 0;">' + (this.paymentReceived.identifier) + ' </td> </tr> <tr> <td style="color:#90949c">Payment Method:</td> <td style="padding: 5px 0;">' + (this.paymentReceived.paymentMethod) + '</td> </tr> <tr> <td style="color:#90949c">' + (this.paymentReceived.paymentMethod === 'Credit Card' ? 'Credit Card:' : 'Bank Account:') + '</td> <td style="padding: 5px 0;">' + (this.paymentReceived.paymentMethod === 'Credit Card' ? this.paymentReceived.creditCardNumber : this.paymentReceived.bankAccountNumber) + '</td> </tr> <tr> <td style=" color:#90949c">Paid Amount: <br/> </td> <td style="padding: 5px 0;"> <strong>' + this.currencyPipe.transform(this.paymentReceived.paymentAmount, this.currency) + '</strong> <br/> </td> </tr> <tr> <td style="color:#90949c">Payment Date:</td> <td style="padding: 5px 0;">' + this.systemDate + '</td> </tr> </tbody> </table> </td> </tr> <tr> <td height="40" &nbsp;=""></td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </body></html>';
    if (!this.isEmail && this.multiHostUrls.notifications.turnOnEmail) {
      const requestBody = {
        sendTo: this.payorEmail,
        subject: PropertyName.PaymentReceived,
        content: template,
        sendFrom: this.multiHostUrls.notifications.sendFrom
      };
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.emailsNotifications(requestBody).subscribe(
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.isEmailSent = true;
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
        });
    }
  }
  private smsNotification(): void {
    if (this.payorPhone && this.multiHostUrls.notifications.turnOnSMS) {
      const requestBody = {
        message: 'Your payment of ' + this.currencyPipe.transform(this.paymentReceived.paymentAmount, this.currency)
          + ' is received . It may take up to 24 hours of your online balance to display this payment.',
        phoneNumber: '+1' + this.payorPhone.replace(/[^0-9.]/g, ''),
        senderId: this.multiHostUrls.notifications.senderId,
        smsType: this.multiHostUrls.notifications.smsType
      };
      this.paymentService.smsNotification(requestBody).subscribe(
        () => {
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
        });
    }
  }
  public onSuccessNavigate(status?: any): void {
    if (this.returnAppId) {
      const resultQueryParam = JSON.parse(sessionStorage.getItem(PropertyName.resultQueryParam));
      const cancelUrl = resultQueryParam.cancelUrl;
      const returnUrl = resultQueryParam.returnUrl;
      const url = (status === PropertyName.cancel ? cancelUrl : returnUrl);
      this.commonService.navigateToUrl(url, PropertyName.self, true);
    } else {
      setTimeout(() => {
        this.router.navigate(['/ent-payments', 'express-payments']);
      }, 100);
      this.dismiss();
    }
  }
}
