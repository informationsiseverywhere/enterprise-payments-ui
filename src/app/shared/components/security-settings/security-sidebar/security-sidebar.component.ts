import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SecurityEngineService } from '../../../services/security-engine.service';
import { AppConfiguration } from './../../../../shared/services/app-config.service';
import { SecurityConfigGuard } from '../../../../ent-auth/security-config.guard';

@Component({
  selector: 'app-security-sidebar',
  templateUrl: './security-sidebar.component.html',
  styleUrls: ['./security-sidebar.component.scss']

})
export class SecuritySidebarComponent implements OnInit {

  public opened = false;
  public modeNum = 0;
  public positionNum = 0;
  public dock = false;
  public closeOnClickOutside = false;
  public closeOnClickBackdrop = false;
  public showBackdrop = true;
  public animate = true;
  public trapFocus = true;
  public autoFocus = true;
  public keyClose = true;
  public autoCollapseHeight: number = null;
  public autoCollapseWidth: number = null;
  public MODES: Array<string> = ['over'];
  public POSITIONS: Array<string> = ['right'];
  public isOpening: boolean = false;
  @Input() sectionName: any;
  @Output() refreshSummary: EventEmitter<any> = new EventEmitter();
  @Output() toggleClose: EventEmitter<any> = new EventEmitter();
  @ViewChild('securitySidebarDetail', { static: false }) securitySidebarDetail: any;
  @ViewChild('sidebar', { static: false }) sidebar: any;
  
  constructor(
    public securityEngineService: SecurityEngineService,
    private route: ActivatedRoute,
    private appConfig: AppConfiguration,
    private securityConfigGuard: SecurityConfigGuard) { }

  ngOnInit() {
    this.securityEngineService.roleSequenceId = undefined;
    this.route.queryParams.subscribe(
      queryParams => {
        this.securityEngineService.roleSequenceId = queryParams.roleSequenceId;
      });
    if (this.route.snapshot.data) {
      const snapshotData = this.route.snapshot.data;
      this.securityEngineService.screenName = snapshotData.screenName ? snapshotData.screenName : null;
      this.securityEngineService.filterValue = snapshotData.filterValue ? snapshotData.filterValue : null;
      this.securityEngineService.additionalSecurityName = snapshotData.additionalSecurityName ? snapshotData.additionalSecurityName : null;
    }
    if (this.appConfig.isComponentSecurity) { this.refreshSecurityData(); }
  }

  public closeSidebar(): void {
    this.sidebar?.close();
  }

  public async refreshSecurityData(): Promise<any> {
    this.securityEngineService.securityOpen = false;
    let roleSequenceId = '';
    this.route.queryParams.subscribe(
      queryParams => {
        roleSequenceId = queryParams.roleSequenceId;
      });
    this.securityEngineService.roleSequenceId = roleSequenceId;
    await this.securityEngineService.getComponentConfig(roleSequenceId);
    this.refreshSummary.emit(true);
  }

  public getTitleText() {
    let screenName = this.sectionName;
    if (this.securityEngineService.currentFieldName &&
      this.securityEngineService.currentFieldName === this.securityEngineService.additionalSecurityName) { screenName = 'Account Options'; }
    return 'FIELD VISIBILITY - ' + screenName + ' Section';
  }

  public async securityConfigClosed(): Promise<any> {
    if(this.isOpening){
      this.toggleClose.emit(true);
      this.securityEngineService.securityOpen = false;
      this.isOpening = false;
      if (this.securitySidebarDetail) {
        await this.securitySidebarDetail.checkHideByChildren();
        if(this.securityEngineService.supplementalFieldsSettings) { this.securitySidebarDetail.checkHideByChildrenDynamicFields(); }
        await this.refreshSecurityData();
        this.securityConfigGuard.routePermissionSecurityConfigGuard(this.route);
      }
    }
  }

  public securityConfigOpened(): void{
    this.isOpening = true;
  }
}
