import {
  Directive, Input, ElementRef, OnInit,
  ViewContainerRef, TemplateRef
} from '@angular/core';
import { NgIfContext } from '@angular/common';

import { SecurityEngineService } from '../../services/security-engine.service';
import { AppConfiguration } from '../../../shared/services/app-config.service';

@Directive({
  selector: '[checkPermission]'
})

export class CheckPermissionDirective implements OnInit {
  @Input() name: string;
  templateName: string;
  selector: any;

  constructor(
    private elem: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private securityEngineService: SecurityEngineService,
    private elseTemplateRef: TemplateRef<NgIfContext>,
    private appConfig: AppConfiguration) {}

  @Input()
  set checkPermission(val: any) {
    this.elseTemplateRef = undefined;
    this.name = val;
    this.updateView();
  }

  @Input()
  set checkPermissionElse(templateRef: TemplateRef<any>) {
    if (templateRef) { this.elseTemplateRef = templateRef; }

    const fieldProperty = this.securityEngineService.checkPermission(this.name);
    if (fieldProperty === 'Hide') { this.viewContainer.createEmbeddedView(this.elseTemplateRef); }


  }

  ngOnInit() {
    this.securityEngineService.securityUpdated.subscribe(
      () => {
        this.updateView();
      }
    );

  }
  public updateView() {
    this.viewContainer.clear();
    if (!this.appConfig.isComponentSecurity) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      return;
    }
    // const currentSelector = this.templateRef['_def']['element']['template']['lastRenderRootNode']['element']['name'];
    const currentSelector = this.templateRef.elementRef.nativeElement.parentElement ?
      this.templateRef.elementRef.nativeElement.parentElement.localName : null;
    const fieldProperty = this.securityEngineService.checkPermission(this.name);
    if (fieldProperty) {
      if (fieldProperty === 'Hide') {
        if (this.elseTemplateRef) {
          this.viewContainer.createEmbeddedView(this.elseTemplateRef);
        } else { this.viewContainer.clear(); }
      } else {
        this.viewContainer.createEmbeddedView(this.templateRef);
        if (fieldProperty === 'Disable' && this.elem.nativeElement.previousSibling){
          this.elem.nativeElement.previousSibling.classList.add('disable-security-options');
        }
      }
    }
  }
}

