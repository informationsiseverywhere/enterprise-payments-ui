import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { TypeaheadComponent } from 'src/app/libs/typeahead/typeahead.component';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { ModelObjects, PropertyName } from 'src/app/shared/enum/common.enum';
import { HeaderService } from 'src/app/shared/services/header.service';
import { Role } from 'src/app/shared/models/role.model';
import { RoleService } from 'src/app/shared/services/role.service';
import { SecurityConfigGuard } from 'src/app/ent-auth/security-config.guard';

@Component({
  selector: 'app-security-config',
  templateUrl: './security-config.component.html',
  styleUrls: ['./security-config.component.scss']
})
export class SecurityConfigComponent implements OnInit {
  public roles: Role[] = [];
  public userId: string;
  public selectedRole: Role = null;
  public userInput: any = null;
  parentRouteName: any;
  modelObjects = ModelObjects;
  configurationType: any;
  returnAppId: any;
  @ViewChild('typeahead', { static: false }) apptypeahead: TypeaheadComponent;
  typeData = ['System Level', 'Role Level'];
  selectedLevel: any;
  screenName: any;
  constructor(
    private roleService: RoleService,
    private modal: ModalComponent,
    private loadingService: LoadingService,
    private router: Router,
    private headerService: HeaderService,
    private route: ActivatedRoute,
    private securityEngineService: SecurityEngineService,
    private securityConfigGuard: SecurityConfigGuard) { }

  ngOnInit() {
    this.configurationType = this.roleService.objectToModal;
    this.screenName = (this.securityEngineService?.screenName ? this.securityEngineService?.screenName : this.screenName);
    const resultQueryParam = JSON.parse(sessionStorage.getItem(PropertyName.resultQueryParam));
    this.userId = resultQueryParam.userId;
    this.getRoleList();
    this.parentRouteName = this.headerService.headerText.getValue();
    if (this.configurationType === ModelObjects.ConfigureDefaultValues) {
      this.route.queryParams.subscribe(
        queryParams => {
          this.screenName = queryParams.screenName;
          if (this.screenName) {
            this.securityEngineService.screenName = this.screenName;
          } else {
            this.securityEngineService.selectedRole = null;
          }
          this.setCurrentSelectedRole();
        }
      );
    } else {
      this.securityEngineService.selectedRole = null;
    }
  }

  public setCurrentSelectedRole(): void {
    const currentSelectedRole = this.securityEngineService.selectedRole;
    if (currentSelectedRole) {
      if (!currentSelectedRole?.roleName) {
        //System Level
        this.selectedLevel = this.typeData[0];
      } else {
        //Role Level
        this.selectedLevel = this.typeData[1];
        this.selectedRole = currentSelectedRole;
      }
    } else {
      //Role Level
      this.selectedLevel = this.typeData[1];
    }
  }

  public updateRolelist(event: KeyboardEvent): void {
    if (this.userInput || this.userInput === '') {
      this.loadingService.toggleLoadingIndicator(true);
      this.roleService.getRoleList(this.userInput).subscribe(
        res => {
          this.loadingService.toggleLoadingIndicator(false);
          if (res) {
            this.roles = res ? res.content : null;
            if (event && (event.which === 40 || event.keyCode === 40)) {//If Key down
              return;
            } else if (event && (event.which === 38 || event.keyCode === 38)) {//If Key up
              return;
            } else {
              this.apptypeahead.list = this.roles;
              this.apptypeahead.populateSuggestions();
              this.apptypeahead.populateTypeahead();
            }
          }
        }
      );
    }
  }
  public userText(userInput?: any): void {
    if (userInput || userInput === '') {
      this.userInput = userInput;
    }
  }
  public clearRoleInput(event?: any): void {
    this.userText(event);
    this.updateRolelist(undefined);
  }
  private getRoleList(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.roleService.getRoleList().subscribe(
      res => {
        this.loadingService.toggleLoadingIndicator(false);
        this.roles = res ? res.content : null;
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }
  public roleSelected(role?: any): void {
    if (role) {
      this.selectedRole = role;
      this.securityEngineService.selectedRole = role;
    }
  }

  public showActiveControls(selectedLevel: any) {
    this.selectedLevel = selectedLevel;
    if (this.configurationType === ModelObjects.ConfigureDefaultValues) {
      if (selectedLevel === this.typeData[0]) {
        this.selectedRole = new Role({
          roleSequenceId: '',
          roleName: '',
          description: '',
          numberofUsers: '',
          numberofApplications: ''
        });
      } else {
        this.selectedRole = null;
      }
    }
  }
  public processRole(): void {
    if (this.configurationType === ModelObjects.SecurityConfiguration) {
      this.securityEngineService.getComponentConfig(this.selectedRole.roleSequenceId);
      this.securityConfigGuard.routePermissionSecurityConfigGuard(this.securityEngineService.activatedRoute, this.selectedRole.roleSequenceId);
    } else {
      if (this.selectedLevel = this.typeData[1]) {
        this.router.navigate(['ent-payments', this.selectedRole.roleSequenceId, 'configure-default-values'],
          {
            queryParams: {
              screenName: this.securityEngineService.screenName,
              returnAppId: this.returnAppId
            }, skipLocationChange: true
          });
      } else {
        this.router.navigate(['ent-payments', null, 'configure-default-values'],
          {
            queryParams: {
              screenName: this.securityEngineService.screenName,
              returnAppId: this.returnAppId
            }, skipLocationChange: true
          });
      }
    }
    this.modal.close(false);
  }
}
