import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { LoadingService } from '../../services/loading.service';
import { CommonService } from '../../services/common.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-security-settings',
  templateUrl: './security-settings.component.html',
  styleUrls: ['./security-settings.component.scss']
})
export class SecuritySettingsComponent implements OnInit, OnChanges {
  @Input() securityData: any;
  @Input() roleSequenceId: any;
  @Output() reloadSecuritySettings: EventEmitter<any> = new EventEmitter();
  public selectedItem: any;
  public securitySettingsDropdown: any;
  constructor(
    private loadingService: LoadingService,
    private commonService: CommonService,
    private securityEngineService: SecurityEngineService) { }

  ngOnInit() {
    this.securitySettingsDropdown = ['Show', 'Hide'];
  }
  ngOnChanges(changes: any) {
    if (this.securityData) {
      this.selectedItem = this.securityData.fieldProperty;
    }
  }
  private changeAuthFieldsProperty(body?: any, data?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.securityEngineService.updateSetting(this.roleSequenceId, body, data, this.securityEngineService.screenName).subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.securityEngineService.getComponentConfig(this.roleSequenceId);
        this.reloadSecuritySettings.emit(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }
  public setSecuritySettings(selectedItem?: any): void {
    this.selectedItem = selectedItem;
    const body = {
      fieldProperty: selectedItem
    };
    this.changeAuthFieldsProperty(body, this.securityData);
  }
}
