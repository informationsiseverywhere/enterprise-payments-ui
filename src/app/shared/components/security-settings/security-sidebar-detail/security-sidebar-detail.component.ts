import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-security-sidebar-detail',
  templateUrl: './security-sidebar-detail.component.html'
})
export class SecuritySidebarDetailComponent implements OnInit, OnChanges {
  @Input() roleSequenceId: any;
  @Input() securityFieldData: any;
  @Input() securitySectionList: any = [];
  @Input() securitySectionData: any;
  @Input() securityConfigList: any;
  @Input() supplementalFieldsSettings: any;

  constructor(
    private loadingService: LoadingService,
    private commonService: CommonService,
    private securityEngineService: SecurityEngineService) { }

  ngOnInit() {}

  ngOnChanges(changes: any) {
    if (this.securityFieldData && this.securityFieldData.checkHideByChildren) { this.checkHideByChildren(); }
  }

  public change(event: any, data: any): void {
    this.securitySectionData[data.fieldName].fieldProperty = event.target.value;
    const body = {
      fieldProperty: event.target.value,
      filterValue: this.securityEngineService.filterValue
    };
    this.changeAuthFieldsProperty(body, data);
  }

  public click(event: any, data: any): void {
    this.securitySectionData[data.fieldName].fieldProperty = event;
    const body = {
      fieldProperty: event,
      filterValue: this.securityEngineService.filterValue
    };
    this.changeAuthFieldsProperty(body, data);
  }

  private async changeAuthFieldsProperty(body: any, securityData: any): Promise<any> {
    this.loadingService.toggleLoadingIndicator(true);
    let currentScreenName = this.securityEngineService.screenName;
    if (this.securityEngineService.currentFieldName && this.securityEngineService.currentFieldName === this.securityEngineService.additionalSecurityName) { currentScreenName = this.securityEngineService.additionalSecurityName; }
    await this.securityEngineService.updateSetting(this.roleSequenceId, body, securityData, currentScreenName).toPromise().then(
      () => {
        if (!this.getItemOfConfig(securityData.fieldName).checkHideByChildren && !this.getItemOfConfig(securityData.fieldName).visibilityVariable) { this.checkHideByChildren(); }
        this.loadingService.toggleLoadingIndicator(false);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  public getSecuritySection(fieldName?: any): any {
    return this.securitySectionList.filter(item => item.fieldName === fieldName);
  }
  public getItemOfConfig(fieldName?: any, type?: any, checkInChild = true): any {
    for (const i of this.securityConfigList) {
      if (type && i[type]) { return i; }
      if (i.fieldName === fieldName) {
        return i;
      } else if (i.values && checkInChild) {
        for (const item of i.values) {
          if (item.fieldName === fieldName) { return item; }
        }
      }
    }
  }

  public async checkHideByChildren(): Promise<any> {
    const listOfCheckParent: Array<any> = [];
    for (const i of this.securityConfigList) {
      i.checkHideByChildren && listOfCheckParent.push(i);
      i.visibilityVariable && listOfCheckParent.push(i);
      if(i.values && i.values.length > 0){
        const visibilityVariableList = i.values.filter(item => item.visibilityVariable);
        if(visibilityVariableList && visibilityVariableList.length > 0){
          for (const visibilityItem of visibilityVariableList) {
            listOfCheckParent.push(visibilityItem);
          }
        }
      }
    }

    if (listOfCheckParent.length > 0) {
      for (const item of listOfCheckParent) {
        if ((item.values && item.values.length > 0) || item.visibilityVariable) {
          let countOfValues = 0;
          let countOfDisabledValue = 0;
          let countOfHiddenValue = 0;

          if(item.values && item.values.length > 0){
            for (const childItem of item.values) {
              if (!this.securityEngineService.securitySectionData[childItem.fieldName]) { return; }
              if (!childItem.notDependOnParent && this.securityEngineService.securitySectionData[childItem.fieldName]) {
                countOfValues++;
                if (this.securityEngineService.securitySectionData[childItem.fieldName].fieldProperty === 'Disable' && childItem.notDependOnDisable != true) {
                  countOfDisabledValue++;
                } else if (this.securityEngineService.securitySectionData[childItem.fieldName].fieldProperty === 'Hide') { countOfHiddenValue++; }
              }
            }
          }
          
          if (item.visibilityVariable) {
            const visibilityVariables = item.visibilityVariable.split(';');
            for (const visibilityItem of visibilityVariables) {
              countOfValues++;
              if (this.securitySectionData[visibilityItem.trim()].fieldProperty === 'Disable' ) { countOfDisabledValue++; }
              else if (this.securitySectionData[visibilityItem.trim()].fieldProperty === 'Hide') { countOfHiddenValue++; }
            }
          }

          let fieldProperty = 'Hide';
          if (item.type === 'BTN' && countOfDisabledValue == countOfValues || countOfHiddenValue == countOfValues) {
            fieldProperty = (countOfDisabledValue == countOfValues && this.securityEngineService.securitySectionData[item.fieldName].fieldProperty === 'Disable') ? 'Disable' : 'Hide';
          } else if (item.type === 'BTN' && countOfDisabledValue > 0 && countOfValues == (countOfDisabledValue + countOfHiddenValue)) {
            fieldProperty = this.securityEngineService.securitySectionData[item.fieldName].fieldProperty === 'Disable' ? 'Disable' : 'Hide';
          } else if (item.type === 'RBT' && countOfValues == (countOfDisabledValue + countOfHiddenValue)) {
            fieldProperty = 'Hide';
          } else { continue; }

          const body = {
            fieldProperty
          };
          if(this.securitySectionData[item.fieldName].fieldProperty != fieldProperty){
            this.securitySectionData[item.fieldName].fieldProperty = fieldProperty;
            await this.changeAuthFieldsProperty(body, item);
          }
        }
      }
    }
  }

  public checkRelatedField(field: any, isCheckInChildren = false): boolean {
    const itemConfig = this.getItemOfConfig(field.fieldName, undefined, isCheckInChildren);
    if (itemConfig.dependField) {
      const dependFields = itemConfig.dependField.split(';');
      for (const dependField of dependFields) {
        if (this.securitySectionData[dependField.trim()].fieldProperty === 'Hide' || (this.securitySectionData[dependField.trim()].fieldProperty === 'Disable' && itemConfig.notDependOnDisable != true)) { return true; }
      }
    }
    return false;
  }

  public changeChevron(event?: any): void {
    if (event && event.currentTarget) {
      const chevron = event.currentTarget.firstElementChild;
      if (chevron.classList.contains('fa-chevron-down')) {
        chevron.classList.remove('fa-chevron-down');
        chevron.classList.add('fa-chevron-up');
      } else {
        chevron.classList.remove('fa-chevron-up');
        chevron.classList.add('fa-chevron-down');
      }
    }
  }

  public changeDynamic(event: any, data: any): void {
    this.securitySectionData[data.fieldName].fieldProperty = event.target.value;
    const body = {
      fieldProperty: event.target.value,
      filterValue: this.securityEngineService.filterValue
    };
    this.changeAuthDynamicFieldsProperty(body, data);
  }

  public clickDynamic(event: any, data: any): void {
    this.securitySectionData[data.fieldName].fieldProperty = event;
    const body = {
      fieldProperty: event,
      filterValue: this.securityEngineService.filterValue
    };
    this.changeAuthDynamicFieldsProperty(body, data);
  }

  private async changeAuthDynamicFieldsProperty(body: any, securityData: any): Promise<any> {
    this.loadingService.toggleLoadingIndicator(true);
    const currentScreenName = this.securityEngineService.screenName;
    await this.securityEngineService.updateSetting(this.roleSequenceId, body, securityData, currentScreenName).toPromise().then(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }
  public isGroupListSectionDisabled(field: any, item: any): boolean{
    return (field?.fieldProperty != 'Show' || this.checkRelatedField(field) || (item.isCollapse && field?.fieldProperty != 'Show'));
  }

  public isChildListSectionDisabled(): boolean{
    return (this.securityFieldData && this.securityFieldData?.fieldProperty != 'Show');
  }
}
