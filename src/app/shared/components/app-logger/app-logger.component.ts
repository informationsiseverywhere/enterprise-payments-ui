import { OnInit, Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AppConfiguration } from './../../services/app-config.service';
import { AppLogger } from '../../services/app-logger.service';
import { ErrorStatusCode } from '../../models/error.model';

export class LoggerModel {
  context = '';
  errorCode: any;
  details: any;

  constructor(values: any = {}) {
    Object.assign(this, values);
  }
}

@Component({
  selector: 'app-logger',
  templateUrl: './app-logger.component.html',
  styleUrls: ['./app-logger.component.scss']
})
export class AppLoggerComponent implements OnInit, OnDestroy {

  message = 'If this problem persists, please contact your system administrator.';
  messageHeader = 'Internal Server Error';
  errorContext: LoggerModel = new LoggerModel();
  subscription: Subscription;
  messageContent = 'Looks like something went wrong!';
  errorDescription = 'Hint :  Internal Server Error';
  supportEmail: any;
  landingPageLink: any;
  multiHostUrls: any;
  errorStatusCode: any;

  constructor(
    private appLogger: AppLogger,
    private appConfig: AppConfiguration) {
    this.multiHostUrls = this.appConfig.multiHostUrls;
    this.errorStatusCode = ErrorStatusCode;
    this.subscription = this.appLogger.updateError$.subscribe(
      errorContext => {
        this.errorContext = errorContext;
        if (this.errorContext) {
          this.errorDescription = `Hint : ${(this.errorContext.errorCode ?
            this.errorContext.errorCode.message : '')}`;
          this.getMessageHeader(this.errorContext.errorCode.status);
        }
      });
  }

  ngOnInit() {
    this.landingPageLink = this.appConfig.landingPageLink;
    this.supportEmail = this.multiHostUrls.supportEmail;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getMessageHeader(errorCode: string) {
    const errorMessage = this.errorStatusCode.find((err: { status: any; }) => {
      return err.status === errorCode;
    });
    this.messageHeader = errorMessage.header;
    this.messageContent = errorMessage.description;
  }
}

