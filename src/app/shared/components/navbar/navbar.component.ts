import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ViewportScroller } from '@angular/common';
import { SecurityConfigComponent } from 'src/app/shared/components/security-settings/security-config/security-config.component';
import { AuthenticationService } from 'src/app/ent-auth/authentication.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppMenuItem, AppItemDetail } from 'src/app/libs/menu-items/models/app-menu-item.model';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { HeaderService } from '../../services/header.service';
import { AppConfiguration } from '../../services/app-config.service';
import { UserService } from '../../services/user-details.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { LoadingService } from '../../services/loading.service';
import { CommonService } from '../../services/common.service';
import { DynamicHostUrlService } from '../../services/dynamic-host-url.service';

import { UtilitiesApps } from 'src/app/libs/menu-items/enums/app-menu-items.enums';
import { AppsService } from 'src/app/shared/services/apps.service';
import { User } from 'src/app/shared/models/user.model';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import { RoleService } from '../../services/role.service';
import { IconName, LoginMethodModel, ModelObjects } from '../../enum/common.enum';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'ent-navbar',
  templateUrl: './navbar.component.html'
})

export class NavbarComponent implements OnInit {
  collapseOut: string = null;
  isCollapsed = false;
  headerTag: any;
  logoPath: string;
  landingPageLink: any;
  public appMenuItems: Array<AppMenuItem> = [];
  orgId: string;
  userName: string;
  userId: string;
  enableAuthentication: boolean;
  tokenId: string;

  @ViewChild('selectRoleModel', { static: false }) selectRoleModel: ModalComponent;
  public returnAppId: any;
  navParam: any;
  public profileSettings: any;
  public isShowSecurity: boolean;
  type: any;
  authUrls: any;
  multiHostUrls: any;
  gridApps: any;
  uiUrls: any;
  public currentTheme: any;
  public themeList: any[] = [];
  public applicationsLoaded: boolean = true;
  public applicationList: any;
  public isGridAppsShow: boolean;
  public modelObjects = ModelObjects;
  public userProfileImageUrl: string;

  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private router: Router,
    private _headerService: HeaderService,
    public appConfig: AppConfiguration,
    private route: ActivatedRoute,
    private roleService: RoleService,
    private userDetailsService: UserService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private authService: AuthenticationService,
    private appsService: AppsService,
    public commonService: CommonService,
    private viewportScroller: ViewportScroller,
    public securityEngineService: SecurityEngineService) {
    this.authUrls = this.appConfig.authUrls;
    this.multiHostUrls = this.appConfig.multiHostUrls;
    this.isGridAppsShow = this.appConfig.gridApps.show;
    this.uiUrls = this.appConfig.uiUrls;
    this.applicationList = ComponentConfiguration.settings.applicationList;
  }

  ngOnInit() {
    this.currentTheme = localStorage.getItem('DefaultTheme');
    this.getThemeList();
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    this.isShowSecurity = this.appConfig.isShowSecurity;
    this.route.queryParams.subscribe(
      queryParams => {
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
        this.type = queryParams.type;
      });
    this.enableAuthentication = (this.authUrls.loginMethod === LoginMethodModel.Forgerock ||
      this.authUrls.loginMethod === LoginMethodModel.Cognito ? true : false);
    this.logoPath = this.multiHostUrls.companyLogo;
    this.landingPageLink = this.multiHostUrls.landingRoute;
    this.userId = resultQueryParam.userId;
    this.tokenId = resultQueryParam.tokenId;
    this.router.events.subscribe(() => {
      this.isCollapsed = !this.isCollapsed;
      this.collapseOut = (this.isCollapsed ? 'out' : '');
    });

    this.headerTag = this._headerService.headerText.getValue();
    this.userDetailsService.currentUserChanged.subscribe(
      (currentUser: User) => {
        if (currentUser) {
          (currentUser.name) && (this.userName = currentUser.name);
          (this.isGridAppsShow && currentUser.userSequenceId) && this.getAllApplications(currentUser.userSequenceId);
          (currentUser?.userId) && (this.userProfileImageUrl = this.commonService.getAWSs3ImageUrl(currentUser?.userId));
        }
      });
      this.userDetailsService.userProfileImageUrl.subscribe(
      data => {
        data && (this.userProfileImageUrl = data);
      });
  }

  logout() {
    if (this.authUrls.loginMethod === 'Forgerock' || this.authUrls.loginMethod === 'Cognito') {
      this.restService.logoutUser();
    } else {
      this.restService.logoutSsoUser();
    }
  }

  securityConfig(value: any) {
    this.roleService.objectToModal = value;
    this.selectRoleModel.modalTitle = 'Select Role';
    this.selectRoleModel.modalFooter = false;
    this.selectRoleModel.subModalTitle = 'Select the role which you would like to configure';
    this.selectRoleModel.icon = IconName.selectedRole;
    this.selectRoleModel.open(SecurityConfigComponent);
  }

  selectLevel(value: any) {
    this.roleService.objectToModal = value;
    this.selectRoleModel.modalTitle = 'Select Configuration ';
    this.selectRoleModel.modalFooter = false;
    this.selectRoleModel.subModalTitle = 'Select the configuration which you would like to use for default configuration setup';
    this.selectRoleModel.icon = IconName.selectedRole;
    this.selectRoleModel.open(SecurityConfigComponent);
  }

  goToHome() {
    if (this.headerTag.parentRouteName === '/ent-payments') {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          type: this.type,
          returnAppId: this.returnAppId,
          nav: this.navParam
        }
      };
      this.router.navigate([this.headerTag.parentRouteName], navigationExtras);
    }
  }

  public openProfileDetail(): void {
    const uaaProfileURL = this.authUrls.uaaCommonLogin + 'settings/profile-detail';
    this.commonService.navigateToUrl(uaaProfileURL, '_blank', true);
  }


  checkAuthorization() {
    this.loadingService.toggleLoadingIndicator(true);
    this.authService.getUserList().subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.openAdminPage();
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.alertService.error('You do not have permission to view this page.');
      }
    );
  }

  public openAdminPage(): void {
    const uaaAdminURL = this.authUrls.uaaCommonLogin + 'settings/admin/users';
    this.commonService.navigateToUrl(uaaAdminURL, '_blank', true);
  }

  public redirectToDashboard(): void {
    const url = this.uiUrls.dashboard;
    this.commonService.navigateToUrl(url, '_self');
  }

  public setTheme(theme: string): void {
    this.commonService.switchStyle(theme);
    localStorage.setItem('DefaultTheme', theme);
    this.currentTheme = theme;
  }

  public getThemeList(): void {
    const url = 'assets/configuration/theme.json';
    this.loadingService.toggleLoadingIndicator(true);
    let themeConfigData;
    this.commonService.getCommonMetadata(url).subscribe(
      data => {
        themeConfigData = data ? data['themes-list'] : null;
        let themeKeys = Object.keys(themeConfigData);
        for (const sd of themeKeys) {
          let item = themeConfigData[sd];
          item.themeKey = sd;
          this.themeList.push(item);
        }
        this.loadingService.toggleLoadingIndicator(false);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );

  }

  public checkCurrentTheme(themeKey: any): boolean {
    if (localStorage.getItem('DefaultTheme') && themeKey === localStorage.getItem('DefaultTheme')) return true;
    return false;
  }

  public getMenuItemDetail(menuItem: AppMenuItem, appList?: Array<AppItemDetail>): AppItemDetail {
    return appList.find((item: AppItemDetail) => item.applicationId == menuItem.applicationId);
  }

  public appMenuItemsLoad(availableAppList?: Array<AppItemDetail>, unAvailableAppList?: Array<AppItemDetail>): void {
    const UTILITIES_SECTION = 'Utilities';
    const APPS_SECTION = 'Apps';
    const applicationMetadataUrl = 'assets/configuration/gridApps.json';
    const menuItemList: Array<AppMenuItem> = [];

    this.commonService.getCommonMetadata(applicationMetadataUrl).subscribe(
      (data: any) => {
        this.gridApps = data ? data.gridApps : null;
        Object.keys(this.gridApps).forEach((item) => {
          if ((this.gridApps[item]?.applicationId != this.applicationList?.applicationId) && this.uiUrls[item]) {
            const menuItem: AppMenuItem = new AppMenuItem(this.gridApps[item]);
            const menuItemDetail: AppItemDetail = this.getMenuItemDetail(menuItem, availableAppList);
            if (menuItemDetail && !this.getMenuItemDetail(menuItem, unAvailableAppList)) {
              menuItem.routeUrl = this.uiUrls[item];
              menuItem.displayText = this.gridApps[item].name;
              menuItem.positionId = menuItemDetail.positionId;
              menuItem.sectionName = Object.values(UtilitiesApps).includes(item) ? UTILITIES_SECTION : APPS_SECTION;
              menuItemList.push(menuItem);
            }
          }
        });
        this.appMenuItems = menuItemList;
      }
    );
  }

  public getApplicationList(isAvailable = true, appsList: Array<AppItemDetail>): Array<AppItemDetail> {
    return appsList.filter((appItem: AppItemDetail) => appItem.isAvailable === isAvailable);
  }

  public getAllApplications(userSequenceId: string): void {
    this.applicationsLoaded = false;
    this.appsService.getAllApplications(userSequenceId).subscribe(
      (data: any) => {
        if (data) {
          const appsList: Array<AppItemDetail> = data.content ? data.content : null;
          const unAvailableAppList: Array<AppItemDetail> = this.getApplicationList(false, appsList);
          const availableAppList: Array<AppItemDetail> = this.getApplicationList(true, appsList);
          this.appMenuItemsLoad(availableAppList, unAvailableAppList);
          this.applicationsLoaded = true;
        }
      }
    );
  }

  public skipToContent($event: any, anchorName: string): void{
    this.viewportScroller.setOffset([0, 204]);
    (anchorName) && (this.viewportScroller.scrollToAnchor(anchorName));
  }
}
