
export interface RemoteHostModel {
  apiUrls: ApiUrlsModel;
  authnUrl: string;
  uiUrls: UiUrlsModel;
  uxUrls: UxUrlsModel;
  multiHostUrls: MultiHostUrlsModel;
  authUrls: AuthUrlsModel;
  securityConfigUrls: SecurityConfigUrlsModel;
  gridApps: GridAppsModel;
}

export interface ApiUrlsModel {
  utilities: any;
  batch: any;
  directBilling: any;
  groupBilling: any;
  agencyBilling: any;
  payments: any;
  makePayments: any;
  client: any;
  comments: any;
  bussinessConfig: any;
  activities: any;
  contractsCommissions: any;
  disbursements: any;
  adminNotifications: any;
}

export interface UiUrlsModel {
  show: boolean;
  dashboard: any;
  utilities: any;
  securities: any;
  batch: any;
  activities: any;
  client: any;
  directBilling: any;
  groupBilling: any;
  groupBill: any;
  agencyBilling: any;
  agencyBill: any;
  payments: any;
  bussinessConfig: any;
  contractsCommissions: any;
  disbursements: any;
  adminNotifications: any;
}

export interface GridAppsModel {
  show: boolean;
  utilities: GridAppsEntriesModel;
  securities: GridAppsEntriesModel;
  batch: GridAppsEntriesModel;
  activities: GridAppsEntriesModel;
  client: GridAppsEntriesModel;
  directBilling: GridAppsEntriesModel;
  groupBilling: GridAppsEntriesModel;
  agencyBilling: GridAppsEntriesModel;
  payments: GridAppsEntriesModel;
  contractsCommissions: GridAppsEntriesModel;
  disbursements: GridAppsEntriesModel;
  adminNotifications: GridAppsEntriesModel;
}

export interface GridAppsEntriesModel {
  show: boolean;
  name: string;
  imageUrl: string;
}

export interface UxUrlsModel {
  show: boolean;
  groupBilling: any;
  agencyBilling: any;
  directBilling: any;
  uxAccountSearch: any;
}

export interface MultiHostUrlsModel {
  companyLogo: any;
  imageUrl: any;
  docProdUrl: any;
  supportEmail: any;
  notifications: any;
  paymentusUrl: string;
  secureTokenizationIframeUrl: string;
  secureTokenizationWebUrl: string;
}

export interface AuthUrlsModel {
  loginMethod: string;
  ssoUrl: any;
  uaaCommonLogin: any;
  forgeRockAuthUrl: string;
  userPoolId: any;
  appClientId: any;
  nonCognitoDomains: any;
}

export interface SecurityConfigUrlsModel {
  isShowSecurity: boolean;
  isComponentSecurity: boolean;
}
