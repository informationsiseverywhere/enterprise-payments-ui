import { IMyOptions } from 'src/app/libs/date-picker/interfaces/my-options.interface';
import { IInputOptions } from 'src/app/libs/input/interfaces/input-options.interface';

export interface IComponentConfig {
    datePickerOptions: IMyOptions;
    inputAmountOptions: IInputOptions;
    inputNumberOptions: IInputOptions;
    inputPercentageNumberOptions: IInputOptions;
    breadCrumbOptions: any;
    applicationList: any;
    paginationOptions: any;
}
export interface IHeardersConfig {
  headers: any;
}
