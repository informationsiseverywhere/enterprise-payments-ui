import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable, BehaviorSubject, from } from 'rxjs';
import { catchError, filter, take, switchMap, finalize } from 'rxjs/operators';

import { RemoteHostURLService } from './../services/remote-host-url.service';

const isIFrame = (input: HTMLElement | null): input is HTMLIFrameElement => input !== null && input.tagName === 'IFRAME';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (RemoteHostURLService.hostUrlConfig) {
      if (RemoteHostURLService.hostUrlConfig.authUrls.loginMethod === 'Forgerock') {
        return next.handle(this.addAuthenticationToken(request));
      } else if (RemoteHostURLService.hostUrlConfig.authUrls.loginMethod === 'Cognito') {
        request = this.addAuthenticationToken(request);

        return next.handle(request).pipe(
          catchError((error: HttpErrorResponse) => {
            if (error && error.status === 401 && (error.error.errorSubCode === 'Token' || (error.error.message && error.error.message === 'Unauthorized'))) {
              // 401 errors are most likely going to be because we have an expired token that we need to refresh.
              if (this.refreshTokenInProgress) {
                // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
                // which means the new token is ready and we can retry the request again
                return this.refreshTokenSubject.pipe(
                  filter(result => result !== null),
                  take(1),
                  switchMap((response) => next.handle(this.addAuthenticationToken(request, response.data.value)))
                );
              } else {
                this.refreshTokenInProgress = true;
                // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
                this.refreshTokenSubject.next(null);

                return from(this.asyncRefreshAccessToken()).pipe(
                  switchMap((response: any) => {
                    this.refreshTokenSubject.next(response);
                    return next.handle(this.addAuthenticationToken(request, response.data.value));
                  }),
                  // When the call to refreshToken completes we reset the refreshTokenInProgress to false
                  // for the next time the token needs to be refreshed
                  finalize(() => this.refreshTokenInProgress = false)
                );
              }
            } else {
              return throwError(error);
            }
          })
        ) as Observable<HttpEvent<any>>;
      }
    }
    return next.handle(request);
  }

  async asyncRefreshAccessToken() {
    return await this.refreshAccessToken();
  }

  private refreshAccessToken(): Promise<any> {
    return new Promise((resolve) => {
      const channel = new MessageChannel();
      // this will fire when iframe will answer
      channel.port1.onmessage = e => resolve(e);
      // let iframe know we're expecting an answer
      // send it its own port
      const uaaFrame = document.getElementById('uaaFrame');
      if (isIFrame(uaaFrame) && uaaFrame.contentWindow) {
        if(uaaFrame.getAttribute('uaa-loaded')){
          uaaFrame.contentWindow.postMessage(
            {
              action: 'checkRefreshToken'
            }, '*', [channel.port2]);
        }else{
          sessionStorage.clear();
          window.location.href = RemoteHostURLService.hostUrlConfig.authUrls.uaaCommonLogin + 'logout?appid=pb360&returnUrl=' + encodeURIComponent(window.location.href);
        }
      }
    });
  }

  private addAuthenticationToken(request: HttpRequest<any>, accessToken?: string): HttpRequest<any> {
	  // If you are calling an outside domain then do not add the token.
    if (this.isNonCognitoDomains(request.url)) {
        return request;
    }
    // If we do not have a token yet then we should not set the header.
    // Here we could first retrieve the token from where we store it.
    if (RemoteHostURLService.hostUrlConfig.authUrls.loginMethod === 'Forgerock') {
      if (sessionStorage.getItem('resultQueryParam')) {
        const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
        request = request.clone(
          { headers: request.headers.set('iPlanetDirectoryPro', resultQueryParam ? resultQueryParam.tokenId : null) });
      }
    }
    if (RemoteHostURLService.hostUrlConfig.authUrls.loginMethod === 'Cognito') {
      if (accessToken) {
        const resultQueryParam: any = {};
        const sessionStorageData = JSON.parse(sessionStorage.getItem('resultQueryParam') || '{}');
        if (sessionStorageData) {
          if (!resultQueryParam.userId) {
            resultQueryParam.userId = sessionStorageData.userId;
          }
          if (!resultQueryParam.tokenId) {
            resultQueryParam.tokenId = accessToken;
          }
        }
        sessionStorage.clear();
        sessionStorage.setItem('resultQueryParam', JSON.stringify(resultQueryParam));
        request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + accessToken) });
      } else {
        if (sessionStorage.getItem('resultQueryParam')) {
          const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
          request = request.clone(
            { headers: request.headers.set('Authorization', resultQueryParam ? 'Bearer ' + resultQueryParam.tokenId : null) });
        }
      }
    }
    return request;
  }
  
  private isNonCognitoDomains(url: string): boolean{
      if (RemoteHostURLService.hostUrlConfig && RemoteHostURLService.hostUrlConfig.authUrls.nonCognitoDomains) {
          const nonCognitoDomains = RemoteHostURLService.hostUrlConfig.authUrls.nonCognitoDomains.split(',');
          for (const domain of nonCognitoDomains) {
              if (url.match(new RegExp(domain.trim(), 'g'))) {
                  return true;
              }
          }
          return false;
      }else{
          return false;
      }
  }
}