import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './ent-auth/auth.guard';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

const appRoutes: Routes = [
  {
    path: 'ent-payments',
    loadChildren: () => import('./ent-payments/ent-payments.module').then(mod => mod.EntPaymentsModule),
    data: {
      title: 'Assure Payments',
      breadcrumb: 'Dashboard',
      headerTitle: 'Dashboard',
      parentRouteName: '/ent-payments'
    },
    canLoad: [AuthGuard]
  },
  {
    path: 'consumer-payment',
    loadChildren: () => import('./ent-payments/consumer-payment/consumer-payment.module').then(mod => mod.ConsumerPaymentModule),
    data: {
      title: 'Assure Payments',
      breadcrumb: 'Consumer Payment',
      headerTitle: 'Consumer Payment',
      parentRouteName: '/consumer-payment',
      screenName: 'epConsumerPayments'
    },
    canLoad: [AuthGuard]
  },
  {
    path: 'group-payment',
    loadChildren: () => import('./ent-payments/group-pay/group-pay.module').then(mod => mod.GroupPayModule),
    data: {
      title: 'Assure Payments',
      breadcrumb: 'Group Payment',
      headerTitle: 'Group Payment',
      parentRouteName: '/group-payment',
      screenName: 'epGroupPayments'
    },
    canLoad: [AuthGuard]
  },
  {
    path: 'policy-payment',
    loadChildren: () => import('./ent-payments/policy-pay/policy-pay.module').then(mod => mod.PolicyPayModule),
    data: {
      title: 'Assure Payments',
      breadcrumb: 'Policy Payment',
      headerTitle: 'Policy Payment',
      parentRouteName: '/policy-payment',
      screenName: 'epPolicyPayments'
    },
    canLoad: [AuthGuard]
  },
  {
    path: 'recurring-payment',
    loadChildren: () => import('./ent-payments/recurring-payment/recurring-payment.module').then(mod => mod.RecurringPaymentModule),
    data: {
      title: 'Assure Payments',
      breadcrumb: 'Recurring Payment',
      headerTitle: 'Recurring Payment',
      parentRouteName: '/recurring-payment',
      screenName: 'epRecurringPayments'
    },
    canLoad: [AuthGuard]
  },
  {
    path: 'commission-payment',
    loadChildren: () => import('./ent-payments/commission-payment/commission-payment.module').then(mod => mod.CommissionPaymentModule),
    data: {
      title: 'Assure Payments',
      breadcrumb: 'Commission Payment',
      headerTitle: 'Commission Payment',
      parentRouteName: '/commission-payment'
    },
    canLoad: [AuthGuard]
  },
  {
    path: 'unidentified-payment',
    loadChildren: () => import('./ent-payments/unidentified-payment/unidentified-payment.module').then(mod => mod.UnidentifiedPaymentModule),
    data: {
      title: 'Assure Payments',
      breadcrumb: 'Unidentified Payment',
      headerTitle: 'Unidentified Payment',
      parentRouteName: '/unidentified-payment',
      screenName: 'epUnidentifiedPayments'
    },
    canLoad: [AuthGuard]
  },
  { path: '', redirectTo: '/ent-payments', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'enabled' })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
