import { NgModule, APP_INITIALIZER, ErrorHandler, DEFAULT_CURRENCY_CODE, LOCALE_ID } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { Router, UrlSerializer } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './ent-auth/auth.module';
import { SharedModule } from './shared/shared.module';
import { ComponentConfiguration } from './shared/services/component-config.service';
import { CustomUrlSerializer } from './shared/services/custom-url-serializer.service';
import { LibraryModule } from './libs/libs.module';
import { RestService } from './shared/services/rest.service';
import { HeaderService } from './shared/services/header.service';
import { SecurityEngineService } from './shared/services/security-engine.service';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { RemoteHeadersService } from './shared/services/remote-headers.service';
import { RemoteHostURLService } from './shared/services/remote-host-url.service';
import { JwtInterceptor } from './shared/interceptor/jwt.interceptor';
import { GlobalErrorHandler } from './shared/services/global-error-handler';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    SharedModule,
    LibraryModule,
    AuthModule,
    AppRoutingModule,
    FontAwesomeModule
  ],
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  providers: [
    RemoteHostURLService,
    RemoteHeadersService,
    ComponentConfiguration,
    {
      provide: APP_INITIALIZER,
      useFactory: (remoteHostURLService: RemoteHostURLService) => () => remoteHostURLService.load(),
      deps: [RemoteHostURLService], multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (remoteHeadersService: RemoteHeadersService) => () => remoteHeadersService.load(),
      deps: [RemoteHeadersService], multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (componentConfiguration: ComponentConfiguration) => () => componentConfiguration.load(),
      deps: [ComponentConfiguration], multi: true
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: UrlSerializer, useClass: CustomUrlSerializer },
    RestService,
    HeaderService,
    SecurityEngineService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      deps: [RemoteHostURLService, RemoteHeadersService],
      multi: true
    },
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    { provide: LOCALE_ID, useValue: 'en-US' },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'USD' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  // Diagnostic only: inspect router configuration
  constructor(router: Router) {
    // Use a custom replacer to display function names in the route configs
    // const replacer = (key, value) => (typeof value === 'function') ? value.name : value;

    // console.log('Routes: ', JSON.stringify(router.config, replacer, 2));
  }
}
