import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ent-vertical-nav',
  templateUrl: './vertical-nav.component.html',
  host: {
    'class': 'vertical-nav'
  }
})
export class VerticalNavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
