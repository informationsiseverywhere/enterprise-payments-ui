import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

import { AlertService } from '../alert/services/alert.service';

@Component({
  selector: 'ent-copy-to-clipboard',
  templateUrl: './copy-to-clipboard.html',
})
export class CopyToClipBoardComponent implements OnInit {
  @Input() value: string;
  @Output() copied: EventEmitter<any> = new EventEmitter<any>();

  constructor(private alertSevice: AlertService) {}
  public text: any;
  public status: string;

  ngOnInit(): void {}

  public notify(event: string): void {
    const listener = (e: ClipboardEvent) => {
      const clipboard = e.clipboardData || window['clipboardData'];
      clipboard.setData('text', this.value.toString());
      e.preventDefault();
      this.copied.emit(this.value);
    };
    setTimeout(() => {
      this.alertSevice.success('Copied to clipboard');
    }, 1000);

    document.addEventListener('copy', listener, false);
    document.execCommand('copy');
    document.removeEventListener('copy', listener, false);
  }
}
