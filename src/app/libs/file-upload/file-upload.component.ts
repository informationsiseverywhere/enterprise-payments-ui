import { Component, ElementRef, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { AlertService } from '../alert/services/alert.service';

@Component({
  selector: 'ent-file-upload',
  templateUrl: './file-upload.component.html'
})
export class FileUploadComponent {
  @Input() multiple = false;
  @Input() isUploadButton = false;
  @Input() indexValue: any;
  @Output() uploadOutput: EventEmitter<any> = new EventEmitter();
  @Output() selectFileOutput: EventEmitter<any> = new EventEmitter();
  @ViewChild('fileInput', { static: false }) inputEl: ElementRef;
  isSupportedFile = false;
  filename = '';
  filenameDisplay = '';

  constructor(private alertService: AlertService,) { }

  upload() {
    const inputEl: HTMLInputElement = this.inputEl.nativeElement;
    if (inputEl.files.length === 0) { return; }

    const files: FileList = inputEl.files;
    const formData = new FormData();
    for (var i = 0; i < files.length; i++) {
      formData.append('file', files[i], files[i].name);
    }
    this.uploadOutput.emit(formData);
  }

  selectFiles() {
    const inputEl: HTMLInputElement = this.inputEl.nativeElement;
    if (inputEl.files.length == 0) {
      return;
    } else {
      this.filename = '';
      this.filenameDisplay = '';
      const files: FileList = inputEl.files;
      console.log(files);
      if ((files[0].type === 'application/vnd.ms-excel' || files[0].type === 'application/csv'
        || files[0].type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')) {
        this.fileOperate(files);
      } else
      if (files[0]?.name?.length > 0) {
        const fileExt = files[0]?.name.substring(files[0]?.name.length - 3);
        if (fileExt.toLowerCase() === 'csv') {
          this.fileOperate(files);
        } else {
          this.alertService.error('CSV file is required');
          this.isSupportedFile = false;
        }
      } else {
        this.alertService.error('CSV file is required');
        this.isSupportedFile = false;
      }
      this.selectFileOutput.emit(true);
    }
  }

  fileOperate(files: any) {
    for (var i = 0; i < files.length; i++) {
      if (files.length === 1) {
        this.filename += files[i].name;
        this.filenameDisplay += files[i].name;
        this.filename = this.filename.replace(/\\/g, '/').replace(/.*\//, '').substring(0, 100);
        this.isSupportedFile = true;
      } else {
        this.filename = files.length + ' Files Selected';
      }
    }
  }
}
