
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { LoadingService } from './../../shared/services/loading.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'ent-spinner',
  templateUrl: './spinner.component.html'
})
export class SpinnerComponent implements OnInit, OnDestroy {
  public isShow: boolean = false;
  private sub: any;

  constructor(private _loadingService: LoadingService) {

  }

  ngOnInit() {
    this.sub = this._loadingService.loading.pipe(
      debounceTime(200),
      distinctUntilChanged())
      .subscribe((data: boolean) => {
        this.isShow = data;
      });
  }

  ngOnDestroy(): any {
    this.sub.unsubscribe();
  }
}
