import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IconName } from 'src/app/shared/enum/common.enum';

import { AppConfiguration } from 'src/app/shared/services/app-config.service';

@Component({
  selector: 'app-name-display',
  templateUrl: './name-display.component.html'
})
export class NameDisplayComponent implements OnInit {
  @Input() imageId: any;
  @Input() name: any;
  @Input() nameAvathar = false;
  @Input() nameOnly = false;
  @Input() nameEdit = false;
  @Input() showEdit = true;
  @Input() nameLink = false;
  @Input() query: any;
  @Input() popoverPlacement = 'top';
  @Input() popoverWidth = 'fit-content';
  @Input() length = 50;
  @Input() titleName = false;
  @Input() icon: any;
  @Input() maxWidth = 290;
  @Input() number: any;
  @Input() roleSequenceId: any;
  @Input() securityEditName: any;
  imageUrl: any;
  @Output() linkEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() editEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() openSetting: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    public appConfig: AppConfiguration) {
    this.imageUrl = IconName.userProfilePath;
  }

  ngOnInit(): void { }

  public linkEventTrigger(): void {
    this.linkEvent.emit(true);
  }

  public editEventTrigger(): void {
    this.editEvent.emit(true);
  }

  public openSecurity(): void {
    this.openSetting.emit(true);
  }
}
