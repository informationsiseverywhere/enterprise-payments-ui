import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ent-tab',
  templateUrl: './tab.component.html'
})
export class TabComponent implements OnInit, OnChanges {
  @Input() title: string;
  @Input() active = false;
  @Output() optionChanges: EventEmitter<any> = new EventEmitter<any>();
  public _hidden = false;
  public _disabled = false;

  constructor() { }

  ngOnChanges(changes: any) {
    this.optionChanges.emit(true);
  }

  ngOnInit() {
  }

  @Input()
  public set hidden(value: boolean) {
    this._hidden = value;
    this.optionChanges.emit(true);
  }

  public get hidden(): boolean {
    return this._hidden;
  }

  @Input()
  public set disabled(value: boolean) {
    this._disabled = value;
    this.optionChanges.emit(true);
  }

  public get disabled(): boolean {
    return this._disabled;
  }

}
