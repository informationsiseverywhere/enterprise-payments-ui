import {
  Component,
  OnInit,
  ContentChildren,
  QueryList,
  AfterContentInit,
  EventEmitter,
  Output,
  Input
} from '@angular/core';
import { TabComponent } from './tab/tab.component';

@Component({
  selector: 'ent-tabs',
  templateUrl: './tabs.component.html'
})
export class TabsComponent implements OnInit, AfterContentInit {
  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;
  private isActive: boolean = false;

  @Output()
  onTabChanged: EventEmitter<TabComponent> = new EventEmitter<TabComponent>();
  @Input() customClass: boolean;
  @Output() tabClick: any = new EventEmitter<any>();
  constructor() { }

  ngOnInit() { }

  ngAfterContentInit() {
    this.tabs.forEach(tab => {
      tab.optionChanges.subscribe(() => this.refreshTabs())
    });

    this.refreshTabs();
  }

  private refreshTabs(): void {
    this.isActive = false;
    if (this.tabs.length > 0) {
      this.tabs.forEach(tab => {
        if (!this.isActive && !tab.hidden) {
          this.selectTab(tab);
          this.isActive = true;
        }
        if (this.isActive == false) tab.active = false;
      });
    }
  }

  selectTab(tab: TabComponent) {
    if (!tab.hidden) {
      // deactivate all tabs
      this.tabs.toArray().forEach(tab => (tab.active = false));
      this.onTabChanged.emit(tab);
      // activate the tab the user has clicked on.
      tab.active = true;
      this.tabClick.emit(tab);
    }
  }
}
