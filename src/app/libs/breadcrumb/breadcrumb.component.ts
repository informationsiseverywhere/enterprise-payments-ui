
import { filter } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET } from '@angular/router';

import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';

interface Breadcrumb {
  label: string;
  params?: Params;
  queryParams: string;
  url: string;
}

@Component({
  selector: 'ent-breadcrumb',
  templateUrl: './breadcrumb.component.html'
})

export class BreadcrumbComponent implements OnInit {
  public breadcrumbs: Breadcrumb[];
  public _breadcrumbSub: any;
  private defaultQueryParamConfig: any;
  private resultQueryParam = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.breadcrumbs = [];

    // subscribe to the NavigationEnd event
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(event => {
      const root: ActivatedRoute = this.activatedRoute.root;
      this.breadcrumbs = this.getBreadcrumbs(root);
    });
  }

  ngOnInit() {
    this.defaultQueryParamConfig = ComponentConfiguration.settings.breadCrumbOptions;
    this.resultQueryParam = {};
    if (this.defaultQueryParamConfig.commonQueryParams) {
      const commonQueryParams = this.defaultQueryParamConfig.commonQueryParams.split(',');
      for (const param of commonQueryParams) {
        this.activatedRoute.queryParams.subscribe(queryParams => {
          if (queryParams[param]) { this.resultQueryParam[param] = queryParams[param]; }
        });
      }
    }

  }

  private getBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: Breadcrumb[] = []): Breadcrumb[] {
    const ROUTE_DATA_BREADCRUMB = 'breadcrumb';
    const ROUTE_DATA_BREADCRUMB_QUERYPARAMS = 'breadcrumbQueryParams';

    // get the child routes
    const children: ActivatedRoute[] = route.children;

    // return if there are no more children
    if (children.length === 0) {
      return breadcrumbs;
    }

    // iterate over each children
    for (const child of children) {
      // verify primary route
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }

      // verify the custom data property "breadcrumb" is specified on the route
      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        return this.getBreadcrumbs(child, url, breadcrumbs);
      }

      if (child.snapshot.url.length === 0) {
        return this.getBreadcrumbs(child, url, breadcrumbs);
      }

      // get the route's URL segment
      const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');

      // append route URL to URL
      url += `/${routeURL}`;

      // add breadcrumb
      if (child.snapshot.data[ROUTE_DATA_BREADCRUMB] !== '') {
        const breadcrumb: Breadcrumb = {
          label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
          params: child.snapshot.params,
          queryParams: this.getQueryParams(child.snapshot.data[ROUTE_DATA_BREADCRUMB_QUERYPARAMS], child.snapshot.queryParams),
          url: url
        };
        breadcrumbs.push(breadcrumb);
      }

      // recursive
      return this.getBreadcrumbs(child, url, breadcrumbs);
    }

    // we should never get here, but just in case
    return breadcrumbs;
  }

  private getQueryParams(breadcrumbQueryParams?: any, currentQueryParams?: any): any {
    let resultQueryParam = {};
    if (breadcrumbQueryParams && breadcrumbQueryParams.length > 0 && currentQueryParams) {
      for(const params of breadcrumbQueryParams) {
        if(currentQueryParams[params]) { resultQueryParam[params] = currentQueryParams[params]; }
      }
    }
    return resultQueryParam;
  }

  public navigateToUrl(url?: any, queryParams?: any): void {
    this.router.navigate([url], {
      queryParams: Object.assign(this.resultQueryParam, queryParams)
    });
  }

}
