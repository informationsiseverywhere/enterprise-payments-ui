import { WizardStepComponent } from './../wizard-step/wizard-step.component';
import { Component, OnInit, ContentChildren, QueryList, Output, EventEmitter, AfterContentInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'ent-wizard',
  templateUrl: './wizard.component.html'
})
export class WizardComponent implements AfterContentInit {
  @Input() title: string;
  @Input() closeText: string = 'Close';
  @Input() previousText: string = 'Previous';
  @Input() nextText: string = 'Next';
  @Input() doneText: string = 'FINISH SET-UP';
  @ContentChildren(WizardStepComponent)
  wizardSteps: QueryList<WizardStepComponent>;
  private _steps: Array<WizardStepComponent> = [];
  private _isCompleted: boolean = false;
  showDone: boolean;
  showNext: boolean;
  showPrev: boolean;

  stepWidth: any;
  currentSlide = 0;

  @ViewChild('wizardContent', { static: false }) wizardContent: ElementRef;

  @Output()
  onStepChanged: EventEmitter<WizardStepComponent> = new EventEmitter<WizardStepComponent>();

  @Output() onClose: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngAfterContentInit() {
    this.wizardSteps.forEach(step => {
      this._steps.push(step);
    });
    this.steps[0].isActive = true;
    this.showDone = this.steps[0].showDone;
    this.showNext = this.steps[0].showNext;
    this.showPrev = this.steps[0].showPrev;

    this.stepWidth = this.wizardContent.nativeElement.offsetWidth;

    this.steps.forEach(step => {
      step.stepWidth = this.stepWidth;
    })

    setTimeout(() => {
      this.steps.forEach(step => {
        step.wizardStep.nativeElement.style.transition = 'all 0.5s ease-in-out';
      })
    }, 1000);
  }

  get steps(): Array<WizardStepComponent> {
    return this._steps.filter(step => !step.hidden);
  }

  get isCompleted(): boolean {
    return this._isCompleted;
  }

  get activeStep(): WizardStepComponent {
    return this.steps.find(step => step.isActive);
  }

  set activeStep(step: WizardStepComponent) {
    if (step !== this.activeStep && !step.isDisabled) {
      this.activeStep.isActive = false;
      step.isActive = true;
      this.onStepChanged.emit(step);
    }
  }

  public get activeStepIndex(): number {
    return this.steps.indexOf(this.activeStep);
  }

  get hasNextStep(): boolean {
    return this.activeStepIndex < this.steps.length - 1;
  }

  get hasPrevStep(): boolean {
    return this.activeStepIndex > 0;
  }

  next() {
    if (this.hasNextStep) {
      const nextStep: WizardStepComponent = this.steps[this.activeStepIndex + 1];
      this.activeStep.onNext.emit();
      nextStep.isDisabled = false;
      this.activeStep = nextStep;
      this.showDone = this.activeStep.showDone;

      const offset = -(++this.currentSlide * this.stepWidth);

      this.steps.forEach(step => {
        step.wizardStep.nativeElement.style.transform = `translateX(${offset}px)`;
      });
    }
  }

  previous() {
    if (this.hasPrevStep) {
      const prevStep: WizardStepComponent = this.steps[this.activeStepIndex - 1];
      this.activeStep.onPrev.emit();
      prevStep.isDisabled = false;
      this.activeStep = prevStep;
      this.showDone = this.activeStep.showDone;

      const offset = -(--this.currentSlide * this.stepWidth);

      this.steps.forEach(step => {
        step.wizardStep.nativeElement.style.transform = `translateX(${offset}px)`;
      });
    }
  }

  complete() {
    this.activeStep.onComplete.emit();
    this._isCompleted = true;
  }

  close() {
    this.onClose.emit();
  }

  allStepsValid() {
    for (const step of this.steps) {
      if (!step.isValid) {
        return false;
      }
    }
    return true;
  }

}
