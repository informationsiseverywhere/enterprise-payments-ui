import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ent-image',
  templateUrl: './image.component.html'
})
export class ImageComponent implements OnInit {
  @Input() imageUrl: string;
  @Input() alt: string;
  @Input() width: number;
  @Input() errorImageUrl: string;
  @Input() stretch:boolean = false;
  @Input() svgIcon:boolean = false;
  @Input() height: number;
  @Input() skeletonAppearance: string;


  constructor() { }

  ngOnInit() { }

  public errorHandler(event: any): void {
    if(this.errorImageUrl){ event.target.src = this.errorImageUrl; }
  }
}
