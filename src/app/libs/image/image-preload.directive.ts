import {Directive, Input, HostBinding, OnInit} from '@angular/core'
@Directive({
    selector: '[imgPreload]',
    host: {
      '(error)':'updateUrl()',
      '(load)': 'load()',
      '[src]':'src'
     }
  })
  
 export class ImagePreloadDirective implements OnInit {
    @Input() src: string;
    @Input() default: string;
    @Input() skeletonAppearance: string;
    @HostBinding('class') className;

    constructor() {
    }

    ngOnInit() {
      if(this.skeletonAppearance) this.className = this.skeletonAppearance;
     }
  
    public updateUrl(): void {
      this.src = this.default;
    }

    public load(): void{
        this.className = 'image-loaded';
    }
  }