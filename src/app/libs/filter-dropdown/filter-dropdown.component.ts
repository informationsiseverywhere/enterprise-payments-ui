import { Component, Input, Output, EventEmitter, forwardRef, ViewChild, HostListener, ElementRef } from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';

export const DROPDOWN_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FilterDropdownComponent),
  multi: true
};

export const DROPDOWN_CONTROL_VALIDATOR: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => FilterDropdownComponent),
  multi: true
};

@Component({
  selector: 'ent-filter-dropdown',
  templateUrl: './filter-dropdown.component.html',
  providers: [DROPDOWN_CONTROL_VALUE_ACCESSOR, DROPDOWN_CONTROL_VALIDATOR, CurrencyPipe]
})
export class FilterDropdownComponent implements ControlValueAccessor {
  @Input() dropdownItems: Array<any> = [];
  @Input() fasName: string;
  @Input() prefix: string;
  @Output() selectedValue: EventEmitter<any> = new EventEmitter<any>();
  @Output() closeClicked: EventEmitter<any> = new EventEmitter<any>();
  @Input() displayField: string;
  @Input('value') _value: any;
  @Input() showOthers: boolean;
  validateFn: any = () => { };
  @ViewChild("dropdown", { static: false }) dropdown: ElementRef;
  @ViewChild("menu", { static: false }) menu: ElementRef;
  currentIndex = -1;

  @HostListener('document:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dropdown.nativeElement.className.indexOf('open') > 0) {
      if (this.dropdownItems.length > 0) {
        if (event.keyCode === 40 || event.keyCode === 38) {
          if (event.keyCode === 40) { // Down Arrow
            this.currentIndex++;
            if (this.currentIndex === this.dropdownItems.length) {
              this.currentIndex = this.dropdownItems.length - 1;
            }
          } else if (event.keyCode === 38) { // Up Arrow
            this.currentIndex--;
            if (this.currentIndex < 0) {
              this.currentIndex = 0;
            }
          }

          if (this.displayField) {
            this.value = this.dropdownItems[this.currentIndex][this.displayField];
          } else {
            this.value = this.dropdownItems[this.currentIndex];
          }
        } else {
          if (event.keyCode === 13) { // Enter
            if (this._value) {
              this.selectedValue.emit(this._value);
            }
          }
        }
      }
    }
  }

  /**
 * Creates and initializes a new dropdown component.
 */
  constructor(private currencyPipe: CurrencyPipe) { }

  /**
 * Get accessor.
 */
  get value(): any { return this._value; };

  /**
 * Set accessor including call the onchange callback.
 */
  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }
  /**
   * From ControlValueAccessor interface.
   */
  writeValue(val: any) {
    this._value = val;
    this.onChange(val);
  }

  onChange = (_) => { };
  onTouched = () => { };
  /**
 * From ControlValueAccessor interface.
 */
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  /**
 * From ControlValueAccessor interface.
 */
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  validate(c: FormControl) {
    return this.validateFn(c);
  }

  itemSelected(item: any, i) {
    if (this.displayField) {
      this.value = item[this.displayField];
    } else {
      this.value = item;
    }
    this.currentIndex = i;
    this.selectedValue.emit(item);
  }

  setOthers(input: any) {
    if (input && input.value) {
      this.value = "Other - " + this.currencyPipe.transform(input.value, "USD", 'symbol', '1.2-2');
      this.selectedValue.emit(input);
    }
  }

  resetFilter() {
    this.value = null;
    this.currentIndex = -1;
    this.selectedValue.emit();
  }

  close() {
    this.closeClicked.emit(true);
  }
}
