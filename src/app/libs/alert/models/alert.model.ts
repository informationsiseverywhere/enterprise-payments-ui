export class Alert {
    type: string;
    message: string;
    messages: any[];
}
