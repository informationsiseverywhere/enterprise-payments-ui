import { Component, OnInit, HostListener, ElementRef, Renderer2, Input } from '@angular/core';
import { AlertService } from './services/alert.service';
import { Alert } from './models/alert.model';
import { CommonService } from './../../shared/services/common.service';
import { EVENT_KEYS_MAP } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'ent-alert',
  templateUrl: './alert.component.html'
})
export class AlertComponent implements OnInit {

  alert: Alert = null;
  icon: string;
  @Input() closeOnEscape: boolean = true;
  public documentEscapeListener: Function;

  constructor(
    private alertService: AlertService,
    private commonService: CommonService,
    public el: ElementRef,
    public renderer: Renderer2) { }

  ngOnInit() {
    this.alertService.getAlert().subscribe((alert: Alert) => {
      if (!alert) {
        this.alert = null;
        return;
      }
      this.alert = alert;
      if (this.alert) {
        this.alert.message && this.commonService.checkTokenIsExpired(this.alert.message);
      }
      if (this.closeOnEscape) {
        this.bindDocumentEscapeListener();
      } else {
        this.unbindDocumentEscapeListener();
      }
    });
  }

  removeAlert(alert: Alert) {
    this.alert = null;
    this.unbindDocumentEscapeListener();
  }

  @HostListener('document:click', ['$event']) clickedOutside($event) {
    if (this.alert) {
      this.removeAlert(this.alert);
    }
  }

  private bindDocumentEscapeListener(): void {
    const documentTarget: any = this.el ? this.el.nativeElement.ownerDocument : 'document';
    this.documentEscapeListener = this.renderer.listen(documentTarget, 'keydown', (event) => {
      const keyPressName = this.commonService.keyPressIdentify(event.key);  
      if(keyPressName === EVENT_KEYS_MAP.Esc){
        this.removeAlert(this.alert);
      }
    });
  }

  private unbindDocumentEscapeListener(): void {
    if (this.documentEscapeListener) {
      this.documentEscapeListener();
      this.documentEscapeListener = null;
    }
  }
}
