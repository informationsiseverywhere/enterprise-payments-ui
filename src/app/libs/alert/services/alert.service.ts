import { Injectable } from '@angular/core';
import { Observable ,  Subject } from 'rxjs';
import { Alert } from '../models/alert.model';

@Injectable()
export class AlertService {
    private subject = new Subject<Alert>();

    constructor() {
    }

    getAlert(): Observable<any> {
        return this.subject.asObservable();
    }

    success(message: string) {
        this.alert('success', message);
    }

    error(message?: string, messages?: any[]) {
        this.alert('error', message, messages);
    }

    wait(message: string) {
        this.alert('wait', message);
    }

    alert(type: string, message: string, messages?: any[]) {
        this.subject.next(<Alert>{ type: type, message: message, messages: messages });
    }

    clear() {
        this.subject.next();
    }
}
