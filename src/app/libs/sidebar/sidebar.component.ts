import { Component, OnInit, Input } from '@angular/core';

import { CommonService } from 'src/app/shared/services/common.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';

@Component({
  selector: 'ent-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {

  @Input() sidebarObject: SidebarObject[] = [];

  constructor(
    private commonService: CommonService,
    private appConfig: AppConfiguration
    ) { }

  ngOnInit() {}

  navigate(linkUrl: any): void {
    const url = this.appConfig.hostUrlConfig.uiUrls[linkUrl];
    this.commonService.navigateToUrl(url, '_self');
  }
}
export class SidebarObject {
  name: string;
  routeUrl: string;
  queryParams: any;
  imageUrl: string;
  displayText: string;
  activeOption: any;
  noLink: boolean;
  linkUrl: any;
  active: boolean;

  constructor(values: any = {}) {
    Object.assign(this, values);
  }
}
