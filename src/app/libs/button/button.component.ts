import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ent-button',
  templateUrl: './button.component.html'
})
export class ButtonComponent implements OnInit {
  @Input() buttonText = 'Button';
  @Input() buttonType = 'button';
  @Input() inversedButton = false;
  @Input() disabled = false;
  @Output() buttonClick: any = new EventEmitter<any>();
  @Input() imageUrl: string;
  @Input() imagePosition: string;
  @Input() fasName: string;

  constructor() {
  }

  ngOnInit() {
  }

  public eventClick(event: any): void {
    this.buttonClick.emit(event);
  }
}
