import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ent-empty-view',
  templateUrl: './empty-view.component.html'
})
export class EmptyViewComponent implements OnInit {
  @Input() message: string;
  @Input() iconUrl: string;
  @Input() subMessage: string;
  constructor() { }

  ngOnInit() {
  }

}
