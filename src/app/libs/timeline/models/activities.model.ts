export class TimeLineModel {
  title: string;
  effectiveDate: string;
  activityDate: any;
  description: any;
  activityDescription: any;
  userName: any;
  amount: any;
}
