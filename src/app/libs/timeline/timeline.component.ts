import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { ParseDateTimeService } from './../../shared/services/parse-date-time.service';
import { TimeLineModel } from './models/activities.model';

@Component({
  selector: 'ent-timeline',
  templateUrl: './timeline.component.html'
})
export class TimelineComponent implements OnInit, OnChanges {

  @Input() timeLineData: TimeLineModel[] = [];
  @Input() showAmount: false;
  @Input() currency: any;
  @Input() maxWidth = 175;
  @Input() length = 20;
  badgeColors: any[] = [];
  index = 0;

  constructor(private parseDateTimeService: ParseDateTimeService) { }

  ngOnInit() { }
  ngOnChanges() {
    if (this.timeLineData) {
      for (const item of this.timeLineData) {
        if (this.index > 4) {
          this.index = 0;
        }
        const color = 'badge-' + this.index;
        this.badgeColors.push(color);
        this.index++;
      }
    }
  }
  parseDateTime(datetime: any) {
    let data = datetime.activityDate;
    if (datetime.activityDate) {
      data = datetime.activityDate;
    } else {
      data = datetime.effectiveDate;
    }
    return this.parseDateTimeService.parseDateTimeToDate(data);
  }
}
