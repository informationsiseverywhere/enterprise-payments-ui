import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ent-label',
  templateUrl: './label.component.html'
})
export class LabelComponent implements OnInit {
  @Input() data: string;
  @Input() requiredField: string;
  @Input() className: string;
  constructor() { }

  ngOnInit() {}
}
