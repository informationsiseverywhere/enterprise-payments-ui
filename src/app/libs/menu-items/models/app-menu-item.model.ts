export class AppMenuItem {
    applicationId: string;
    name: string;
    displayText: string;
    imageUrl: string;
    sectionName: string;
    positionId: number;
    routeUrl: string;
    
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class AppItemDetail {
    applicationId: string;
    applicationName: string;
    positionId: number;
    isAvailable: boolean;
    
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
