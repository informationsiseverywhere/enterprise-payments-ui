import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { CommonService } from "src/app/shared/services/common.service";
import { AppMenuItem } from "./models/app-menu-item.model"

@Component({
  selector: 'ent-menu-items',
  templateUrl: './menu-items.component.html'
})
export class MenuItemsComponent implements OnInit, OnChanges {
  @Input() appMenuItems: Array<AppMenuItem> = [];
  public appsSectionList: any = [];
  constructor(public commonService: CommonService) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['appMenuItems'].currentValue && changes['appMenuItems'].currentValue.length > 0){
      const appsGroups = new Set(this.appMenuItems.map(item => item.sectionName));
      appsGroups.forEach(g =>
        this.appsSectionList.push({
          name: g,
          values: this.orderMenuItemsByPosition(g)
        }
        ));
    }
  }

  ngOnInit() {}

  private orderMenuItemsByPosition(groupName: string): Array<AppMenuItem>{
    const groupItems = this.appMenuItems.filter((i: AppMenuItem) => i.sectionName === groupName);
    return groupItems.sort((obj1: AppMenuItem, obj2: AppMenuItem) => obj1.positionId < obj2.positionId ? -1 : obj1.positionId > obj2.positionId ? 1 : 0);
  }

  public navigateToApp(url?: string): void {
    this.commonService.navigateToUrl(url, '_blank', true);
  }

}
