import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, FormControl } from '@angular/forms';

@Component({
  selector: 'ent-number-incrementer',
  templateUrl: './number-incrementer.component.html',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => NumberIncrementerComponent), multi: true },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => NumberIncrementerComponent), multi: true }
  ]
})
export class NumberIncrementerComponent implements ControlValueAccessor {
  @Input('value') _value: number = 0;
  @Input() incrementalValue: number = 1;
  @Input() min: number = 0;
  @Input() max: number;
  @Input() control: FormControl;
  @Input() requireDecimals: boolean = false;

  @Output() valueChanged: EventEmitter<any> = new EventEmitter();
  @Output() valueChangeBlur: EventEmitter<any> = new EventEmitter();
  constructor() { }

  isError() {
    if (this.control) {
      return (this.control.invalid && this.control.touched && this.control.dirty) ? true : false;
    }
  }

  restrictDecimals(event: any) {
    if (this.requireDecimals) return event.charCode >= 48 && event.charCode <= 57
  }

  decrease() {
    if ((this.value - Number(this.incrementalValue)) >= Number(this.min)) {
      this.value -= Number(this.incrementalValue);
    } else {
      this.value = Number(this.min);
    }
    this.valueChanged.emit(this.value);
  }

  increase() {
    if (this.max) {
      if ((this.value + Number(this.incrementalValue)) <= Number(this.max)) {
        this.value += Number(this.incrementalValue);
      } else {
        this.value = Number(this.max);
      }
    } else {
      this.value += Number(this.incrementalValue);
    }
    this.valueChanged.emit(this.value);
  }

  onInputChange() {
    if (this.value < Number(this.min)) {
      this.value = Number(this.min);
    } else if (this.max) {
      if (this.value > Number(this.max)) {
        this.value = Number(this.max);
      }
    } else if (!this.value) {
      this.value = 0;
    }
    this.valueChanged.emit(this.value);
  }

  get value(): number { return this._value; }

  set value(v: number) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }

  writeValue(val: any) {
    this._value = val;
    this.onChange(val);
  }

  validateFn: any = () => { };

  validate(c: FormControl) {
    return this.validateFn(c);
  }

  onChange = (_) => { };
  onTouched = () => { };
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }
  onInputBlur($event?: any) {
    this.valueChangeBlur.emit($event.target.value);
  }
}
