import {
  Component,
  Input, Output, EventEmitter, OnChanges, SimpleChanges, ElementRef, Renderer2, forwardRef, ViewChild, OnInit, HostListener
} from '@angular/core';
import * as moment from 'moment';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { IMyDate, IMyDateRange, IMyMonth, IMyWeek, IMyDayLabels, IMyMonthLabels, IMyOptions } from './interfaces/index';
import { ValidatorService } from './services/datepicker.validator.service';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import { LocaleService } from './services/datepicker.locale.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { EVENT_KEYS_MAP } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'ent-date-picker',
  templateUrl: './date-picker.component.html',
  providers: [
    LocaleService,
    ValidatorService,
    DatePipe,
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DatePickerComponent), multi: true },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => DatePickerComponent), multi: true }
  ]
})
export class DatePickerComponent implements OnInit, OnChanges, ControlValueAccessor {
  @Input() _value: string;
  @Input() options: any = {
    todayBtnTxt: 'Today',
    showDateFormatPlaceholder: 'true',
    selectionTxtFontSize: '1rem',
    width: '100%'
  };
  @Input() locale: string;
  @Input() defaultMonth: string;
  @Input() selDate: string;
  @Input() currentDate: string;
  @Input() control: FormControl;
  @Input() openOnTop = false;
  @Input() isAutoFocus: boolean = false;
  @Input() closeOnEscape: boolean = true;
  @ViewChild('inputDate', { static: false }) inputDate: ElementRef;
  @ViewChild('prevMonthSelector', { static: false }) prevMonthSelector: ElementRef;
  @ViewChild('inputEditMonth', { static: false }) inputEditMonth: ElementRef;
  @ViewChild('buttonEditMonth', { static: false }) buttonEditMonth: ElementRef;
  @ViewChild('inputEditYear', { static: false }) inputEditYear: ElementRef;
  @ViewChild('buttonEditYear', { static: false }) buttonEditYear: ElementRef;
  @ViewChild('selectorButton', { static: false }) selectorButton: ElementRef;
  public documentEscapeListener: Function;
  
  @Output() dateChanged: EventEmitter<any> = new EventEmitter();
  @Output() inputFieldChanged: EventEmitter<any> = new EventEmitter();
  @Output() calendarViewChanged: EventEmitter<any> = new EventEmitter();
  @Output() clearDateValue: EventEmitter<any> = new EventEmitter();

  showSelector = false;
  visibleMonth: IMyMonth = { monthTxt: '', monthNbr: 0, year: 0 };
  selectedMonth: IMyMonth = { monthTxt: '', monthNbr: 0, year: 0 };
  selectedDate: IMyDate = { year: 0, month: 0, day: 0 };
  weekDays: Array<string> = [];
  dates: Array<any> = [];
  selectionDayTxt = '';
  invalidDate = false;
  disableTodayBtn = false;
  dayIdx = 0;
  weekDayOpts: Array<string> = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'];

  editMonth = false;
  invalidMonth = false;
  editYear = false;
  invalidYear = false;

  PREV_MONTH = 1;
  CURR_MONTH = 2;
  NEXT_MONTH = 3;

  MIN_YEAR = 1000;
  MAX_YEAR = 9999;

  private defaultDatePickerConfig: IMyOptions;
  private _disabledField: any;

  // Default options
  opts: IMyOptions = {
    dayLabels: {} as IMyDayLabels,
    monthLabels: {} as IMyMonthLabels,
    dateFormat: '',
    parseDateFormat: '',
    showTodayBtn: true as boolean,
    todayBtnTxt: '',
    firstDayOfWeek: '',
    sunHighlight: true as boolean,
    markCurrentDay: true as boolean,
    disableUntil: { year: 0, month: 0, day: 0 } as IMyDate,
    disableSince: { year: 0, month: 0, day: 0 } as IMyDate,
    disableDays: [] as Array<IMyDate>,
    disableDateRange: { begin: { year: 0, month: 0, day: 0 } as IMyDate, end: { year: 0, month: 0, day: 0 } as IMyDate } as IMyDateRange,
    disableWeekends: false as boolean,
    height: '30px',
    width: '100%',
    selectionTxtFontSize: '18px',
    inline: false as boolean,
    showClearDateBtn: true as boolean,
    alignSelectorRight: false as boolean,
    indicateInvalidDate: true as boolean,
    showDateFormatPlaceholder: false as boolean,
    customPlaceholderTxt: '',
    editableDateField: true as boolean,
    editableMonthAndYear: true as boolean,
    minYear: this.MIN_YEAR,
    maxYear: this.MAX_YEAR,
    componentDisabled: false as boolean,
    inputValueRequired: false as boolean,
    showSelectorArrow: true as boolean,
    showInputField: true as boolean
  };

  validateFn: any = () => { };

  @Input()
  public set disabledField(value: any) {
    this._disabledField = value;
    (this.opts) && (this.opts.componentDisabled = this._disabledField);
  }

  public get disabledField(): any {
    return this._disabledField;
  }

  ngOnInit() {
    this.defaultDatePickerConfig = ComponentConfiguration.settings.datePickerOptions;

    Object.keys(this.defaultDatePickerConfig).forEach((k) => {
      (this.opts)[k] = this.defaultDatePickerConfig[k];
    });
  }

  constructor(
    public elem: ElementRef, private renderer: Renderer2,
    private localeService: LocaleService, private validatorService: ValidatorService, private datePipe: DatePipe, private commonService: CommonService, public el: ElementRef) {
    this.setLocaleOptions();
    renderer.listen('document', 'click', (event: any) => {
      if (this.showSelector && event.target &&
        this.elem.nativeElement !== event.target && !this.elem.nativeElement.contains(event.target)) {
        this.showSelector = false;
        this.unbindDocumentEscapeListener();
      }
      if (this.opts.editableMonthAndYear && event.target && this.elem.nativeElement.contains(event.target)) {
        this.resetMonthYearEdit();
      }
    });
  }

  isError() {
    if (this.control) {
      return (this.control.invalid && this.control.touched && this.control.dirty) ? true : false;
    }
  }

  public get value(): any {
    return this._value;
  }

  public set value(v: any) {
    let val = v;
    if (val) { val = moment(v, [this.opts.parseDateFormat, this.opts.dateFormat]).format(this.opts.parseDateFormat); }
    if (val !== this._value) {
      this._value = val;
      if (val) { this.selectionDayTxt = moment(val, [this.opts.parseDateFormat, this.opts.dateFormat]).format(this.opts.dateFormat); }
      this.onChange(val);
    }
  }

  public writeValue(val: any): void {
    if (val) {
      this.invalidDate = false;
      this._value = moment(val, [this.opts.parseDateFormat, this.opts.dateFormat]).format(this.opts.parseDateFormat);
      this.selectionDayTxt = moment(val, [this.opts.parseDateFormat, this.opts.dateFormat]).format(this.opts.dateFormat);
    }
    if (val === null || val === undefined) { this.clearDate(); }
    this.onChange(val);
  }

  validate(c: FormControl) {
    return this.validateFn(c);
  }

  onChange = (_) => { };
  onTouched = () => { };
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  setLocaleOptions(): void {
    const opts: IMyOptions = this.localeService.getLocaleOptions(this.locale);
    Object.keys(opts).forEach((k) => {
      (this.opts)[k] = opts[k];
    });
  }

  setOptions(): void {
    if (this.options !== undefined) {
      Object.keys(this.options).forEach((k) => {
        (this.opts)[k] = this.options[k];
      });
    }
    if (this.opts.minYear < this.MIN_YEAR) {
      this.opts.minYear = this.MIN_YEAR;
    }
    if (this.opts.maxYear > this.MAX_YEAR) {
      this.opts.maxYear = this.MAX_YEAR;
    }
  }

  getComponentWidth(): string {
    if (this.opts.showInputField) {
      return this.opts.width;
    } else if (this.selectionDayTxt.length > 0 && this.opts.showClearDateBtn) {
      return '60px';
    } else {
      return '30px';
    }
  }

  resetMonthYearEdit(): void {
    this.editMonth = false;
    this.editYear = false;
    this.invalidMonth = false;
    this.invalidYear = false;
  }

  editMonthClicked(event: any): void {
    event.stopPropagation();
    if (this.opts.editableMonthAndYear) {
      this.editMonth = true;
      this.inputMonthYearFocusing(this.inputEditMonth);
    }
  }

  editYearClicked(event: any): void {
    event.stopPropagation();
    if (this.opts.editableMonthAndYear) {
      this.editYear = true;
      this.inputMonthYearFocusing(this.inputEditYear);
    }
  }

  public userDateInput(event: any): void {
    this.invalidDate = false;
    if (event.target.value.length === 0) {
      const keyPressName = this.commonService.keyPressIdentify(event.key);
      if(keyPressName !== EVENT_KEYS_MAP.Tab){
        this.clearDate();
      }
    } else {
      const date: IMyDate = this.validatorService.isDateValid(event.target.value,
        this.opts.dateFormat, this.opts.minYear, this.opts.maxYear,
        this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends,
        this.opts.disableDays, this.opts.disableDateRange, this.opts.monthLabels);
      if (date.day !== 0 && date.month !== 0 && date.year !== 0) {
        this.selectDate({ day: date.day, month: date.month, year: date.year });
      } else {
        this.invalidDate = true;
      }
    }
    if (this.invalidDate) {
      this.inputFieldChanged.emit({
        value: event.target.value,
        dateFormat: this.opts.dateFormat,
        valid: !(event.target.value.length === 0 || this.invalidDate)
      });
    }
    const pattern = /^(\d{2})(\d{2})(\d{4})$/;
    if (event.target.value.match(pattern)) {
      const tempVal = event.target.value.replace(pattern, '$3-$1-$2');
      if (moment(tempVal, this.opts.parseDateFormat, true).isValid()) {
        this.invalidDate = false;
        this.setValueByDateInRange(tempVal);
      }
    } else {
      if (moment(event.target.value, this.opts.parseDateFormat, true).isValid()
        || moment(event.target.value, this.opts.dateFormat, true).isValid()) {
        const dateVal = moment(event.target.value, [this.opts.parseDateFormat, this.opts.dateFormat]).format(this.opts.parseDateFormat);
        this.invalidDate = false;
        this.setValueByDateInRange(dateVal);
      } else {
        this.onChange(event.target.value);
      }
    }
  }

  private setValueByDateInRange(dateVal?: any): void {
    const date: IMyDate = this.validatorService.isDateValid(dateVal,
      this.opts.parseDateFormat, this.opts.minYear, this.opts.maxYear,
      this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends,
      this.opts.disableDays, this.opts.disableDateRange, this.opts.monthLabels);
    if (date.day !== 0 && date.month !== 0 && date.year !== 0) {
      this.selectDate({ day: date.day, month: date.month, year: date.year });
    } else {
      this.selectionDayTxt = moment(dateVal).format(this.opts.dateFormat);
      this.inputDate.nativeElement.value = this.selectionDayTxt;
      this.invalidDate = true;
    }
  }

  lostFocusInput(event: any): void {
    this.selectionDayTxt = event.target.value;
    this.onTouched();
  }

  userMonthInput(event: any): void {
    if (event.keyCode === 13 || event.keyCode === 37 || event.keyCode === 39) {
      return;
    }

    this.invalidMonth = false;

    const m: number = this.validatorService.isMonthLabelValid(event.target.value, this.opts.monthLabels);
    if (m !== -1) {
      this.editMonth = false;
      if (m !== this.visibleMonth.monthNbr) {
        this.visibleMonth = { monthTxt: this.monthText(m), monthNbr: m, year: this.visibleMonth.year };
        this.generateCalendar(m, this.visibleMonth.year);
      }
      this.inputMonthYearFocusing(this.buttonEditMonth);
    } else {
      this.invalidMonth = true;
    }
  }

  userYearInput(event: any): void {
    if (event.keyCode === 13 || event.keyCode === 37 || event.keyCode === 39) {
      return;
    }

    this.invalidYear = false;

    const y: number = this.validatorService.isYearLabelValid(Number(event.target.value), this.opts.minYear, this.opts.maxYear);
    if (y !== -1) {
      this.editYear = false;
      if (y !== this.visibleMonth.year) {
        this.visibleMonth = { monthTxt: this.visibleMonth.monthTxt, monthNbr: this.visibleMonth.monthNbr, year: y };
        this.generateCalendar(this.visibleMonth.monthNbr, y);
      }
      this.inputMonthYearFocusing(this.buttonEditYear);
    } else {
      this.invalidYear = true;
    }
  }

  public isTodayDisabled(): void {
    this.disableTodayBtn = this.validatorService.isDisabledDay(this.getTodayDate(),
      this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends,
      this.opts.disableDays, this.opts.disableDateRange);
  }

  public getTodayDate(): IMyDate{
    return this.currentDate ? this.setToday(this.currentDate) : this.getToday();
  }

  public parseOptions(): void {
    this.setOptions();
    if (this.locale) {
      this.setLocaleOptions();
    }
    this.isTodayDisabled();
    this.dayIdx = this.weekDayOpts.indexOf(this.opts.firstDayOfWeek);
    if (this.dayIdx !== -1) {
      let idx: number = this.dayIdx;
      for (let i = 0; i < this.weekDayOpts.length; i++) {
        this.weekDays.push(this.opts.dayLabels[this.weekDayOpts[idx]]);
        idx = this.weekDayOpts[idx] === 'sa' ? 0 : idx + 1;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('locale')) {
      this.locale = changes.locale.currentValue;
    }

    if (changes.hasOwnProperty('options')) {
      this.options = changes.options.currentValue;
    }

    this.weekDays.length = 0;
    this.parseOptions();

    if (changes.hasOwnProperty('defaultMonth')) {
      const dm: string = changes.defaultMonth.currentValue;
      if (dm !== null && dm !== undefined && dm !== '') {
        this.selectedMonth = this.parseSelectedMonth(dm);
      } else {
        this.selectedMonth = { monthTxt: '', monthNbr: 0, year: 0 };
      }
    }

    if (changes.hasOwnProperty('selDate')) {
      const sd: any = changes.selDate;
      if (sd.currentValue !== null && sd.currentValue !== undefined &&
        sd.currentValue !== '' && Object.keys(sd.currentValue).length !== 0) {
        this.selectedDate = this.parseSelectedDate(sd.currentValue);
      } else {
        // Do not clear on init
        if (!sd.isFirstChange()) {
          this.clearDate();
        }
      }
    }
    if (this.opts.inline) {
      this.setVisibleMonth();
    }
  }

  removeBtnClicked(): void {
    this.onTouched();
    // Remove date button clicked
    this.clearDate();
    this.inputDate?.nativeElement?.focus();
  }

  openBtnClicked(): void {
    // Open selector button clicked
    this.showSelector = !this.showSelector;
    if (this.showSelector) {
      this.setVisibleMonth();
      if(this.closeOnEscape){
        this.bindDocumentEscapeListener();
      }else{
        this.unbindDocumentEscapeListener();
      }
    }else{
      this.unbindDocumentEscapeListener();
    }
  }

  public openBtnKeydown(event: any): void{
    const keyPressName = this.commonService.keyPressIdentify(event.key);
    if(keyPressName === EVENT_KEYS_MAP.Tab && this.showSelector){
      this.prevMonthSelector.nativeElement.focus();
      event.preventDefault();
    }
  }

  private setVisibleMonth(): void {
    // Sets visible month of calendar
    let date = null;
    if (this._value) {
      const validDate: IMyDate = this.validatorService.isDateValid(this._value, this.opts.parseDateFormat,
        this.opts.minYear, this.opts.maxYear, this.opts.disableUntil,
        this.opts.disableSince, this.opts.disableWeekends, this.opts.disableDays,
        this.opts.disableDateRange, this.opts.monthLabels);
      if (validDate.day !== 0 && validDate.month !== 0 && validDate.year !== 0) {
        const dateVal = moment(this._value).format('YYYY-MM-DD');
        date = dateVal.split('-');
      } else {
        date = null;
        this.selectedDate.day = 0;
      }
    }

    if (date) {
      this.selectedDate.year = Number(date[0]);
      this.selectedDate.month = Number(date[1]);
      this.selectedDate.day = Number(date[2]);
    }
    let y = 0;
    let m = 0;
    if (this.selectedDate.year === 0 && this.selectedDate.month === 0 && this.selectedDate.day === 0) {
      if (this.selectedMonth.year === 0 && this.selectedMonth.monthNbr === 0) {
        const today: IMyDate = this.getTodayDate();
        y = today.year;
        m = today.month;
      } else {
        y = this.selectedMonth.year;
        m = this.selectedMonth.monthNbr;
      }
    } else {
      y = this.selectedDate.year;
      m = this.selectedDate.month;
    }
    this.visibleMonth = { monthTxt: this.opts.monthLabels[m], monthNbr: m, year: y };

    // Create current month
    this.generateCalendar(m, y);
  }

  public clearDate(): void {
    // Clears the date and notifies parent using callbacks
    this.selectionDayTxt = '';
    this._value = '';
    this.selectedDate = { year: 0, month: 0, day: 0 };
    this.dateChanged.emit({ date: {}, jsdate: null, formatted: this._value, epoc: 0 });
    this.inputFieldChanged.emit({ value: '', dateFormat: this.opts.dateFormat, valid: false });
    this.invalidDate = false;
    this.onChange(this.selectionDayTxt);
    this.clearDateValue.emit('');
  }

  prevMonth(): void {
    // Previous month from calendar
    const d: Date = this.getDate(this.visibleMonth.year, this.visibleMonth.monthNbr, 1);
    d.setMonth(d.getMonth() - 1);

    const y: number = d.getFullYear();
    const m: number = d.getMonth() + 1;

    this.visibleMonth = { monthTxt: this.monthText(m), monthNbr: m, year: y };
    this.generateCalendar(m, y);
  }

  nextMonth(): void {
    // Next month from calendar
    const d: Date = this.getDate(this.visibleMonth.year, this.visibleMonth.monthNbr, 1);
    d.setMonth(d.getMonth() + 1);

    const y: number = d.getFullYear();
    const m: number = d.getMonth() + 1;

    this.visibleMonth = { monthTxt: this.monthText(m), monthNbr: m, year: y };
    this.generateCalendar(m, y);
  }

  prevYear(): void {
    // Previous year from calendar
    if (this.visibleMonth.year - 1 < this.opts.minYear) {
      return;
    }
    this.visibleMonth.year--;
    this.generateCalendar(this.visibleMonth.monthNbr, this.visibleMonth.year);
  }

  nextYear(): void {
    // Next year from calendar
    if (this.visibleMonth.year + 1 > this.opts.maxYear) {
      return;
    }
    this.visibleMonth.year++;
    this.generateCalendar(this.visibleMonth.monthNbr, this.visibleMonth.year);
  }

  public todayClicked(): void {
    // Today button clicked
    const today: IMyDate = this.getTodayDate();
    this.selectDate({ day: today.day, month: today.month, year: today.year });
    if (this.opts.inline && today.year !== this.visibleMonth.year || today.month !== this.visibleMonth.monthNbr) {
      this.visibleMonth = { monthTxt: this.opts.monthLabels[today.month], monthNbr: today.month, year: today.year };
      this.generateCalendar(today.month, today.year);
    }
  }

  cellClicked(cell: any): void {

    // Cell clicked on the calendar
    if (cell.cmo === this.PREV_MONTH) {
      // Previous month of day
      this.prevMonth();
    } else if (cell.cmo === this.CURR_MONTH) {
      // Current month of day
      this.selectDate(cell.dateObj);
    } else if (cell.cmo === this.NEXT_MONTH) {
      // Next month of day
      this.nextMonth();
    }
    this.resetMonthYearEdit();
  }

  cellKeyDown(event: any, cell: any) {
    if ((event.keyCode === 13 || event.keyCode === 32) && !cell.disabled) {
      event.preventDefault();
      this.cellClicked(cell);
    }
  }

  private selectDate(date: any): void {
    // Date selected, notifies parent using callbacks
    this.selectedDate = { day: date.day, month: date.month, year: date.year };
    this.selectionDayTxt = moment(date.year + '-' + date.month + '-' + date.day, 'YYYY-MM-DD').format(this.opts.dateFormat);
    this.inputDate.nativeElement.value = this.selectionDayTxt;
    this._value = moment(this.selectionDayTxt, this.opts.dateFormat).format(this.opts.parseDateFormat);
    this.onChange(this._value);
    this.showSelector = false;
    this.unbindDocumentEscapeListener();
    this.dateChanged.emit({
      date: this.selectedDate,
      jsdate: this.getDate(date.year, date.month, date.day),
      formatted: this._value, epoc: Math.round(this.getTimeInMilliseconds(this.selectedDate) / 1000.0)
    });
    this.inputFieldChanged.emit({ value: this.selectionDayTxt, dateFormat: this.opts.dateFormat, valid: true });
    this.invalidDate = false;
    this.inputDate?.nativeElement?.focus();
  }

  preZero(val: string): string {
    // Prepend zero if smaller than 10
    return Number(val) < 10 ? '0' + val : val;
  }

  monthText(m: number): string {
    // Returns month as a text
    return this.opts.monthLabels[m];
  }

  monthStartIdx(y: number, m: number): number {
    // Month start index
    const d = new Date();
    d.setDate(1);
    d.setMonth(m - 1);
    d.setFullYear(y);
    const idx = d.getDay() + this.sundayIdx();
    return idx >= 7 ? idx - 7 : idx;
  }

  daysInMonth(m: number, y: number): number {
    // Return number of days of current month
    return new Date(y, m, 0).getDate();
  }

  daysInPrevMonth(m: number, y: number): number {
    // Return number of days of the previous month
    const d: Date = this.getDate(y, m, 1);
    d.setMonth(d.getMonth() - 1);
    return this.daysInMonth(d.getMonth() + 1, d.getFullYear());
  }

  isCurrDay(d: number, m: number, y: number, cmo: number, today: IMyDate): boolean {
    // Check is a given date the today
    return d === today.day && m === today.month && y === today.year && cmo === this.CURR_MONTH;
  }

  getToday(): IMyDate {
    const date: Date = new Date();
    return { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
  }

  setToday(currentDate: string): IMyDate {
    let date = [];
    date = currentDate.split('-');
    return { year: +date[0], month: +date[1], day: +date[2] };
  }

  getTimeInMilliseconds(date: IMyDate): number {
    return this.getDate(date.year, date.month, date.day).getTime();
  }

  getDayNumber(date: IMyDate): number {
    // Get day number: su=0, mo=1, tu=2, we=3 ...
    const d: Date = this.getDate(date.year, date.month, date.day);
    return d.getDay();
  }

  getWeekday(date: IMyDate): string {
    // Get weekday: su, mo, tu, we ...
    return this.weekDayOpts[this.getDayNumber(date)];
  }

  getDate(year: number, month: number, day: number): Date {
    // Creates a date object from given year, month and day
    return new Date(year, month - 1, day, 0, 0, 0, 0);
  }

  sundayIdx(): number {
    // Index of Sunday day
    return this.dayIdx > 0 ? 7 - this.dayIdx : 0;
  }

  public generateCalendar(m: number, y: number): void {
    this.dates.length = 0;
    const today: IMyDate = this.getTodayDate();
    const monthStart: number = this.monthStartIdx(y, m);
    const dInThisM: number = this.daysInMonth(m, y);
    const dInPrevM: number = this.daysInPrevMonth(m, y);

    let dayNbr = 1;
    let cmo: number = this.PREV_MONTH;
    for (let i = 1; i < 7; i++) {
      const week: IMyWeek[] = [];
      if (i === 1) {
        // First week
        const pm = dInPrevM - monthStart + 1;
        // Previous month
        for (let j = pm; j <= dInPrevM; j++) {
          const date: IMyDate = { year: y, month: m - 1, day: j };
          week.push({
            dateObj: date,
            cmo,
            currDay: this.isCurrDay(j, m, y, cmo, today),
            dayNbr: this.getDayNumber(date),
            disabled: this.validatorService.isDisabledDay(date,
              this.opts.disableUntil,
              this.opts.disableSince,
              this.opts.disableWeekends,
              this.opts.disableDays, this.opts.disableDateRange)
          });
        }

        cmo = this.CURR_MONTH;
        // Current month
        const daysLeft: number = 7 - week.length;
        for (let j = 0; j < daysLeft; j++) {
          const date: IMyDate = { year: y, month: m, day: dayNbr };
          week.push({
            dateObj: date,
            cmo,
            currDay: this.isCurrDay(dayNbr, m, y, cmo, today),
            dayNbr: this.getDayNumber(date),
            disabled: this.validatorService.isDisabledDay(date,
              this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends, this.opts.disableDays, this.opts.disableDateRange)
          });
          dayNbr++;
        }
      } else {
        // Rest of the weeks
        for (let j = 1; j < 8; j++) {
          if (dayNbr > dInThisM) {
            // Next month
            dayNbr = 1;
            cmo = this.NEXT_MONTH;
          }
          const date: IMyDate = { year: y, month: cmo === this.CURR_MONTH ? m : m + 1, day: dayNbr };
          week.push({
            dateObj: date,
            cmo,
            currDay: this.isCurrDay(dayNbr, m, y, cmo, today),
            dayNbr: this.getDayNumber(date),
            disabled: this.validatorService.isDisabledDay(date,
              this.opts.disableUntil, this.opts.disableSince,
              this.opts.disableWeekends, this.opts.disableDays, this.opts.disableDateRange)
          });
          dayNbr++;
        }
      }
      this.dates.push(week);
    }
    // Notify parent
    this.calendarViewChanged.emit({
      year: y,
      month: m,
      first: {
        number: 1,
        weekday: this.getWeekday({ year: y, month: m, day: 1 })
      },
      last: { number: dInThisM, weekday: this.getWeekday({ year: y, month: m, day: dInThisM }) }
    });
  }

  private parseSelectedDate(selDate: any): IMyDate {
    // Parse selDate value - it can be string or IMyDate object
    let date: IMyDate = { day: 0, month: 0, year: 0 };
    if (typeof selDate === 'string') {
      const sd: string = selDate;
      date.day = this.validatorService.parseDatePartNumber(this.opts.dateFormat, sd, 'DD');

      date.month = this.opts.dateFormat.indexOf('MMM') !== -1
        ? this.validatorService.parseDatePartMonthName(this.opts.dateFormat, sd, 'MMM', this.opts.monthLabels)
        : this.validatorService.parseDatePartNumber(this.opts.dateFormat, sd, 'MM');

      date.year = this.validatorService.parseDatePartNumber(this.opts.dateFormat, sd, 'YYYY');
    } else if (typeof selDate === 'object') {
      date = selDate;
    }
    this.selectionDayTxt = moment(date.year + '-' + date.month + '-' + date.day, 'YYYY-MM-DD').format(this.opts.dateFormat);
    return date;
  }

  parseSelectedMonth(ms: string): IMyMonth {
    return this.validatorService.parseDefaultMonth(ms);
  }

  public inputMonthYearFocusing(element: ElementRef): void{
    if(element?.nativeElement){
      setTimeout(() => {
        element.nativeElement.focus();
      }, 100);
    }
  }

  public userMonthInputFocusout(): void{
    this.editMonth = false;
  }

  public userYearInputFocusout(): void{
    this.editYear = false;
  }

  private bindDocumentEscapeListener(): void {
    const documentTarget: any = this.el ? this.el.nativeElement.ownerDocument : 'document';
    this.documentEscapeListener = this.renderer.listen(documentTarget, 'keydown', (event) => {
      const keyPressName = this.commonService.keyPressIdentify(event.key);  
      if(keyPressName === EVENT_KEYS_MAP.Esc){
        this.showSelector = false;
        this.selectorButton.nativeElement.focus();
        this.unbindDocumentEscapeListener();
      }
    });
  }

  private unbindDocumentEscapeListener(): void {
    if (this.documentEscapeListener) {
        this.documentEscapeListener();
        this.documentEscapeListener = null;
    }
  }
}
