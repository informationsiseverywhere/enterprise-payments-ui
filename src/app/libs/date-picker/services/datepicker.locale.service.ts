import { Injectable } from '@angular/core';

import { IMyOptions } from '../interfaces';
import { DatePickerLocaleModel } from '../interfaces/datepicker-locale.model';

@Injectable()
export class LocaleService {

  getLocaleOptions(locale: string): IMyOptions {
    if (locale && DatePickerLocaleModel.hasOwnProperty(locale)) {
      // User given locale
      return DatePickerLocaleModel[locale];
    }
    // Default: en
    return DatePickerLocaleModel.en;
  }
}
