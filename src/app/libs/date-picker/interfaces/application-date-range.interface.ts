import { IMyDate } from './application-date.interface';

export interface IMyDateRange {
    begin: IMyDate;
    end: IMyDate;
}
