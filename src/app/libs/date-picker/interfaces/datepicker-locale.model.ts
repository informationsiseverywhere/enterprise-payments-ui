export const DatePickerLocaleModel = {
  en: {
    dayLabels: {
      su: 'Sun',
      mo: 'Mon',
      tu: 'Tue',
      we: 'Wed',
      th: 'Thu',
      fr: 'Fri',
      sa: 'Sat'
    },
    monthLabels: {
      1: 'Jan',
      2: 'Feb',
      3: 'Mar',
      4: 'Apr',
      5: 'May',
      6: 'Jun',
      7: 'Jul',
      8: 'Aug',
      9: 'Sep',
      10: 'Oct',
      11: 'Nov',
      12: 'Dec'
    },
    dateFormat: 'yyyy-mm-dd',
    todayBtnTxt: 'Today',
    firstDayOfWeek: 'mo',
    sunHighlight: true
  }
};
