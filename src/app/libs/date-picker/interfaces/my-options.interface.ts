import { IMyDayLabels } from './my-day-labels.interface';
import { IMyMonthLabels } from './my-month-labels.interface';
import { IMyDate } from './application-date.interface';
import { IMyDateRange } from './application-date-range.interface';

export interface IMyOptions {
    dayLabels?: IMyDayLabels;
    monthLabels?: IMyMonthLabels;
    dateFormat?: string;
    parseDateFormat?: string;
    showTodayBtn?: boolean;
    todayBtnTxt?: string;
    firstDayOfWeek?: string;
    sunHighlight?: boolean;
    markCurrentDay?: boolean;
    disableUntil?: IMyDate;
    disableSince?: IMyDate;
    disableDays?: Array<IMyDate>;
    disableDateRange?: IMyDateRange;
    disableWeekends?: boolean;
    height?: string;
    width?: string;
    selectionTxtFontSize?: string;
    inline?: boolean;
    showClearDateBtn?: boolean;
    alignSelectorRight?: boolean;
    indicateInvalidDate?: boolean;
    showDateFormatPlaceholder?: boolean;
    customPlaceholderTxt?: string;
    editableDateField?: boolean;
    editableMonthAndYear?: boolean;
    minYear?: number;
    maxYear?: number;
    componentDisabled?: boolean;
    inputValueRequired?: boolean;
    showSelectorArrow?: boolean;
    showInputField?: boolean;
}
