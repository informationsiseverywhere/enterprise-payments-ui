import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ent-export-to-excel',
  templateUrl: './export-to-excel.component.html'
})
export class ExportToExcelComponent implements OnInit {
  @Input('reportTitle') reportTitle: string;
  @Input('reportDate') reportDate: string;
  @Input('tableId') tableId: string;
  @Input('fileName') fileName: string;
  @Input('reportHeader') reportHeader: string;
  @Input('jobId') jobId: string;

  constructor() { }

  ngOnInit() { }

  exportToExcel(table?: any, fileName?: any, reportHeader?: any) {
    if (!table.nodeType) {
      table = document.getElementById(table).nextSibling ? document.getElementById(table).nextSibling : document.getElementById(table);
    }
    let template: any;
    let header = document.getElementById(reportHeader);

    //clone table
    let tableToExport = table.cloneNode(true);
    let headerToExport = <HTMLElement>header.children[0].cloneNode(true);

    //Remove Breadcumb
    if (header.children[0]) {
      let headerChildren = header.children[0];
      if (headerChildren.children && headerToExport.childNodes[1]) {
          headerToExport.removeChild(headerToExport.childNodes[1]);
      }
    }
    headerToExport.appendChild(document.createElement("br"));
    //Remove unnessecary data
    if (tableToExport.children) {
      let tableChildren = tableToExport.children;
      if (tableChildren[0] && tableChildren[0].id === 'export-to-excel-button') {
          tableToExport.removeChild(tableToExport.childNodes[0]);
      }
    }

    //build up excel content/format
    template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>' + fileName + '</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body>' + headerToExport.innerHTML + tableToExport.innerHTML + '</body></html>';

    let myBlob = new Blob([template], {
      type: 'application/vnd.ms-excel'
    });

    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(myBlob, this.fileName + '.xls');
    } else {
      const url = window.URL.createObjectURL(myBlob);

      const link = document.createElement('a');
      document.body.appendChild(link);
      link.download = this.fileName;
      link.href = url;
      link.click();
    }
  }
}
