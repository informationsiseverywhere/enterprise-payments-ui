import { Component, OnInit, Input, Output, ElementRef, EventEmitter, ViewChild, Inject, OnChanges, HostListener } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { LoadingService } from '../../shared/services/loading.service';
import { AlertService } from '../alert/services/alert.service';
import { PaginationService } from './services/pagination.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { EVENT_KEYS_MAP } from 'src/app/shared/enum/common.enum';

export class PageObject {
    firstPageUrl: string;
    previousPageUrl: string;
    nextPageUrl: string;
    lastPageUrl: string;
    currentPageUrl: string;

    constructor(values: any = {}) {
        Object.assign(this, values);
    }
}

@Component({
    selector: 'ent-pagination',
    templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnInit, OnChanges {
    @Input() pageObject: PageObject;
    @Input() listFormatter: (arg: any) => string;
    @Input() valuePropertyName = 'id';
    @Input() displayPropertyName = 'value';
    @Output() response: EventEmitter<any> = new EventEmitter<any>();
    @Output() isLoading: EventEmitter<any> = new EventEmitter<any>();
    private data: any;
    currentPage: any;
    totalPage: string;
    public _size;
    @Input() showSizeDropdown = false;
    @Output() sizeSet: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('toggle', { static: false }) toggle: any;
    @ViewChild('dropdown', { static: false }) dropdown: ElementRef;
    @ViewChild('numberInput', { static: false }) numberInput: any;
    @ViewChild('numberPageInput', { static: false }) numberPageInput: ElementRef;
    @ViewChild('menu', { static: false }) menu: ElementRef;
    @ViewChild('clearNumberInput', { static: false }) clearNumberInput: ElementRef;
    @ViewChild('otherPageInput', { static: false }) otherPageInput: ElementRef;
    
    @Input() position = 'bottom';
    @Input() viewPagination = false;
    @Input() applyLinks = false;
    @Input() nonPageSizeSet = false;
    public itemsPerPageList: any;
    public openPageSize = false;
    public otherPageActive = false;
    public closeAllExpand = false;

   /* Apply pagination on click outside */
    @HostListener('document:click', ['$event.target'])
    onClick(targetElement) {
        if ((this.toggle && this.toggle.nativeElement.contains(targetElement)) || targetElement.classList.contains('close-addon')) {
            return;
        }
        if (this.menu) {
            const clickedInside = this.menu.nativeElement.contains(targetElement);
            if (!clickedInside && this.openPageSize) {
                if(this.otherPageActive){
                    this.setSizeValue(this.numberInput.nativeElement.value);
                }else{
                    this.openPageSize = false;
                }
            }
        }
    }

    constructor(
        private paginationService: PaginationService,
        private loadingService: LoadingService,
        private alertService: AlertService,
        @Inject(DOCUMENT) private document: any,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public commonService: CommonService) { }

    ngOnInit() {
        if(this.commonService.getComponentConfiguration('paginationOptions', 'itemsPerPageTemplate')){ 
            this.itemsPerPageList = this.commonService.getComponentConfiguration('paginationOptions', 'itemsPerPageTemplate').split(',');
        }
    }

    ngOnChanges(changes: any) {
        this.getPageNumber();
    }

    public reloadItems(): void {
        this.reloadCurrentPage();
    }

    @Input()
    public set size(value: number) {
        if (value) {
            this._size = value;
        } else {
            this._size = this.commonService.getComponentConfiguration('paginationOptions', 'itemsPerPage');
        }
    }
  
    public get size(): number {
        if (this._size) {
            return this._size;
        } else {
            return this.commonService.getComponentConfiguration('paginationOptions', 'itemsPerPage');
        }
    }

    public getPageNumber(): void {
        if (!this.pageObject) {
            return;
        }
        if (this.pageObject.lastPageUrl) {
            this.totalPage = this.getQueryString('page', this.pageObject.lastPageUrl)[1];
        } else {
            this.totalPage = '1';
        }
        if (this.pageObject.currentPageUrl) {
            this.currentPage = this.getQueryString('page', this.pageObject.currentPageUrl)[1];
        } else {
            this.currentPage = '1';
        }
        if (this.numberPageInput) { this.numberPageInput.nativeElement.value = this.currentPage; }
    }

    public setDataResponse(res?: any): void {
        this.loadingService.toggleLoadingIndicator(false);
        this.data = res;
        this.response.emit(this.data);
    }

    public navigateToPage(pageUrl?: string): void {
    	this.closeAllExpand = true;
        if (pageUrl) {
            if (this.viewPagination) {
                this.isLoading.emit(true);
                const executePageUrl = this.setSizeQueryParam(pageUrl);
                this.paginationService.getData(executePageUrl).subscribe(
                    res => {
                        this.setDataResponse(res);
                    },
                    () => {
                        this.alertService.error('Page you are looking is not available. Please contact your system administrator.');
                    }
                );
            } else {
                const queryParam = this.getQueryString('page', pageUrl)[1];
                if (queryParam) {
                    this.updateRouteUrl(queryParam);
                }
            }
        }
    }

    private reloadCurrentPage(): void {
    	this.closeAllExpand = true;
        if (this.pageObject.currentPageUrl) {
            if (this.viewPagination) {
                const currentPageUrl = this.setSizeQueryParam(this.pageObject.currentPageUrl);
                this.loadingService.toggleLoadingIndicator(true);
                this.paginationService.getData(currentPageUrl).subscribe(
                    res => {
                        this.loadingService.toggleLoadingIndicator(false);
                        this.data = res;
                        if (this.data) {
                            this.response.emit(this.data);
                        } else {
                            this.navigateToPage(this.pageObject.firstPageUrl);
                        }
                    },
                    () => {
                        this.loadingService.toggleLoadingIndicator(false);
                        this.alertService.error('Cannot reload current page. Please contact your system administrator.');
                    }
                );
            } else {
                this.updateRouteUrl(this.currentPage);
            }
        }
    }

    public getQueryString(field, url): any {
        const href = url ? url : window.location.href;
        const reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        const string = reg.exec(href);
        return string;
    }

    public setSizeQueryParam(url: string): any {
        if (this.applyLinks) {
            return url;
        } else {
            const queryParam = this.getQueryString('size', url)[0];
            const updatedUrl = url.replace(queryParam, '&size=' + this.size);
            return updatedUrl;
        }
    }

    private setPageQueryParam(url: string): any {
        const queryParam = this.getQueryString('page', url)[0];
        if(queryParam){
           let updatedUrl =  this.commonService.removeQueryParam('page', url);
           if(updatedUrl.indexOf('?') != -1){
            return updatedUrl + '&page=' + this.currentPage;
          }else{
            return updatedUrl + '?page=' + this.currentPage;
          }
        }
        return url;
    }

    public setSizeValue(value?: any): void {
        this.openPageSize = !this.openPageSize;
        if (value) {
            if (value > 0) {
                this.size = value;
                this.currentPage = '1';
                this.toggle.nativeElement.setAttribute('aria-expanded', 'false');
                this.dropdown.nativeElement.classList.remove('open');
                this.menu.nativeElement.classList.remove('show');
                this.numberInput.nativeElement.blur();
                this.reloadItems();
            } else {
                this.alertService.error('Please give the number greater than 0.');
            }
        } else {
            this.numberInput.value = '';
            this.menu.nativeElement.classList.remove('show');
            this.commonService.clearQueryParam(['size', 'page']);
        }
    }

    public updateRouteUrl(pageQueryParam: any): void {
        if (pageQueryParam) {
            let navigateUrl: any;
            if (this.document.location.hash.substring(0, this.document.location.hash.indexOf('?'))) {
                navigateUrl = this.document.location.hash.substring(0, this.document.location.hash.indexOf('?'));
            } else {
                navigateUrl = this.document.location.hash;
            }
            navigateUrl = navigateUrl.slice(1);
            const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
            if (queryParams.page && queryParams.size) {
                queryParams.page = pageQueryParam;
                queryParams.size = this.size;
                this.router.navigate([navigateUrl], {
                    queryParams
                });
            } else if (queryParams.size) {
                queryParams.size = this.size;
                this.router.navigate([navigateUrl], { queryParams: { page: pageQueryParam }, queryParamsHandling: 'merge' });
            } else {
                if(this.nonPageSizeSet){
                    this.router.navigate([navigateUrl], { queryParams: { page: pageQueryParam }, queryParamsHandling: 'merge' });
                }else{
                    this.router.navigate([navigateUrl], { queryParams: { page: pageQueryParam, size: this.size }, queryParamsHandling: 'merge' });
                }
               
            }
        }
    }

    public setPageValue(value: any): void {
        if (value && value != this.currentPage) {
            if (value > 0) {
                this.isLoading.emit(true);
                this.currentPage = value;
                this.numberPageInput.nativeElement.blur();
                this.pageObject.currentPageUrl = this.setPageQueryParam(this.pageObject.currentPageUrl);
                this.reloadItems();
            }
        } else { this.numberPageInput.nativeElement.value = this.currentPage; }
    }

    public checkValidation(event?: any, value?: any, type?: any): void {
        let result: any = value;
        if (value.startsWith('0', 0)) { result = value.substr(1, value.length); }
        if (type === 'Page') {
            if (Number(value) > Number(this.totalPage)) {
                let str;
                if (value.startsWith(event.key)) {
                    str = value.substring(0, value.length - 1);
                } else { str = value.replace(event.key, ''); }
                if (Number(str) > Number(this.totalPage)) {
                    result = '1';
                } else { result = str; }
            } else { result = result; }
            this.numberPageInput.nativeElement.value = result;
        } else if (type === 'Size') {
            this.numberInput.value = result;
        }
    }

    public isPageExisting(size: any): boolean{
        for (const param of this.itemsPerPageList) {
            if(Number(param) == size) return true;
        }
       return false;
    }

    public togglePageSize(): any {
        this.openPageSize = !this.openPageSize;
        if(this.size && !this.isPageExisting(this.size)){
            this.numberInput.nativeElement.value = this.size;
            this.otherPageActive = true;
        }else{
            this.numberInput.nativeElement.value = '';
            this.otherPageActive = false;
        }
    }

    public clearValue(event: any): void{
        this.numberInput.nativeElement.value = '';
        setTimeout(() => {
            this.numberInput?.nativeElement.focus();
        }, 100);
        event.stopPropagation();
    }

    public otherPageFocused(event: any): void {
        if(this.numberInput?.nativeElement && (!this.clearNumberInput || document.activeElement !== this.clearNumberInput?.nativeElement)){
            setTimeout(() => {
                const selectionEnd = this.numberInput.nativeElement.value.length;
                this.numberInput?.nativeElement.focus();
                this.numberInput?.nativeElement.setSelectionRange(selectionEnd, selectionEnd);
            },);
          }
    }

    public otherPageInputKeyUp(event: any): void {
        const keyPressName = this.commonService.keyPressIdentify(event.key);
        switch (keyPressName) {
            case EVENT_KEYS_MAP.Up:
                setTimeout(() => {
                    const lastElement = document.getElementById(`dropdown-page-${this.itemsPerPageList.length - 1}`);
                    lastElement.focus();
                    event.preventDefault();
                },);
                break;
            case EVENT_KEYS_MAP.Down:
                this.otherPageFocused(event);
                break;
            default: break;
        }
    }

}
