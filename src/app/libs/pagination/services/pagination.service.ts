
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { RestService } from '../../../shared/services/rest.service';
import { AppLogger } from '../../../shared/services/app-logger.service';

@Injectable()
export class PaginationService {

  constructor(private restService: RestService, private appLoggerService: AppLogger) { }

  getData(url: string) {
    return this.restService.getByUrl(url).pipe(map(
      data => {
        return data;
      },
      err => {
        this.appLoggerService.catchError(err);
      }
    ));
  }
}
