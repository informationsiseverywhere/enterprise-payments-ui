import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ent-icon',
  templateUrl: './icon.component.html'
})
export class IconComponent implements OnInit {
  @Input() fasName: string;
  constructor() { }

  ngOnInit() {
  }

}
