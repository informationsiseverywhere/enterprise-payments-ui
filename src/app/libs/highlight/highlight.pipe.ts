import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({ name: 'highlightText' })
export class HighlightPipe implements PipeTransform {

    /* use this for single match search */
    static SINGLE_MATCH: string = 'Single-Match';
    /* use this for single match search with a restriction that target should start with search string */
    static SINGLE_AND_STARTS_WITH_MATCH: string = 'Single-And-StartsWith-Match';
    /* use this for global search */
    static MULTI_MATCH: string = 'Multi-Match';

    constructor(private domSanitizer: DomSanitizer) {
    }

    transform(data: string,
        highlightText: string,
        option: string = 'Multi-Match',
        caseSensitive: boolean = false,
        highlightStyleName: string = 'search-highlight'): SafeHtml {

        if (highlightText && data && option) {

            highlightText = highlightText.trim();
            const highlightTokenArr = highlightText.trim().split(/\s+/);
            const caseFlag: string = !caseSensitive ? 'i' : '';
            let result = '';

            // Multi words search
            if (highlightTokenArr.length > 1) {

                // init an indexArray acording to data.length
                const indexArray = new Array(data.length).fill(0);
                for (const token of highlightTokenArr) {
                    let tokenIndex = -1;
                    // if token is existed in data, mark index of token with 1
                    while (true) {
                        tokenIndex = data.toLowerCase().indexOf(token.toLowerCase(), tokenIndex + 1);
                        if (tokenIndex > -1) {
                            indexArray.fill(1, tokenIndex, tokenIndex + token.length);
                        } else {
                            break;
                        }
                    }
                }

                // build result string base on indexArray
                let index = 0;
                let oldIndex = index;
                while (index < indexArray.length) {
                    if (indexArray[index] === 1) {
                        result += data.slice(oldIndex, index);
                        let length = 1;
                        while ((index + length) < indexArray.length) {
                            if (indexArray[index + length] === 1) {
                                length++;
                            } else {
                                break;
                            }
                        }
                        const highlightWord = data.substr(index, length);
                        index += length;
                        oldIndex = index;
                        result += `<span class="${highlightStyleName}" style="background-color:#26c281; color:#ffffff; font-weight:bold">${highlightWord}</span>`;
                    }
                    index++;
                }
                if (oldIndex < index) {
                    result += data.slice(oldIndex, index + 1);
                }
            } else { // Single word search

                let regex: any = '';

                // handle special characters
                let pattern = highlightText.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                pattern = pattern.split(' ').filter((t) => {
                    return t.length > 0;
                }).join('|');

                switch (option) {
                    case 'Single-Match': {
                        regex = new RegExp(pattern, caseFlag);
                        break;
                    }
                    case 'Single-And-StartsWith-Match': {
                        regex = new RegExp('^' + pattern, caseFlag);
                        break;
                    }
                    case 'Multi-Match': {
                        regex = new RegExp(pattern, 'g' + caseFlag);
                        break;
                    }
                    default: {
                        // default will be a global case-insensitive match
                        regex = new RegExp(pattern, 'gi');
                    }
                }
                result = data.replace(regex, (match) => `<span class="${highlightStyleName}" style="background-color:#26c281; color:#ffffff; font-weight:bold">${match}</span>`);
            }
            return this.domSanitizer.bypassSecurityTrustHtml(result);
        } else {
            return data;
        }
    }
}
