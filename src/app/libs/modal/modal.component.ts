import { Component, ComponentFactoryResolver, ViewChild, ViewContainerRef, ComponentRef, EventEmitter, Output, ElementRef, Input, Renderer2, OnDestroy } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common.service';
import { EVENT_KEYS_MAP } from 'src/app/shared/enum/common.enum';
declare var $:any;

@Component({
  selector: 'ent-modal',
  templateUrl: './modal.component.html'
})
/**
  * API to an open modal window.
  */
export class ModalComponent implements OnDestroy {
  /**
     * Caption for the title.
     */
  public modalTitle: string;
  public subModalTitle: string;
  /**
   *  input to dynamic component
   */
  public componentInput: string;
  /**
    * component which is to be loaded dynamically.
    */
  public component: any;
  /**
     * Describes if the modal contains Ok Button.
     * The default Ok button will close the modal and emit the callback.
     * Defaults to true.
     */
  public okButton: boolean = true;
  /**
     * Caption for the OK button.
     * Default: Ok
     */
  public okButtonText: string = 'Ok';
  /**
     * Describes if the modal contains cancel Button.
     * The default Cancelbutton will close the modal.
     * Defaults to true.
     */
  public cancelButton: boolean = true;
  /**
     * Caption for the Cancel button.
     * Default: Cancel
     */
  public cancelButtonText: string = 'Cancel';
  /**
     * if the modalMessage is true it will show the message inside modal body.
     */
  public modalMessage: boolean = true;
  /**
     * Some message/content can be set in message which will be shown in modal body.
     */
  public message: string;
  /**
    * if the value is true modal footer will be visible or else it will be hidden.
    */
  public modalFooter: boolean = true;
  /**
    * shows modal header if the value is true.
    */
  public modalHeader: boolean = true;
  /**
    * if the value is true modal will be visible or else it will be hidden.
    */
  public isOpen: boolean = false;
  /**
    * Emitted when a ok button was clicked
    * or when close method is called.
    */
  public borderColor: boolean = true;
  public icon: string;
  public hideClose: boolean = true;
  public documentEscapeListener: Function;
  private focusedBeforeOpenEle: HTMLElement;
  cmpRef: ComponentRef<any>;
  @Input() closeOnEscape: boolean = true;
  @ViewChild('child', { read: ViewContainerRef, static: false }) target;
  @ViewChild('modalRoot', { static: false }) modalRoot: ElementRef;
  @Output() public modalOutput: EventEmitter<any> = new EventEmitter();
  dragEventTarget: MouseEvent | TouchEvent;
  constructor(private element: ElementRef, public componentFactoryResolver: ComponentFactoryResolver, public viewContainer: ViewContainerRef, public commonService: CommonService, public renderer: Renderer2) {
  }

  public ngOnDestroy(): void {
    if(this.modalRoot?.nativeElement?.classList?.contains('show')){
      this.unbindDocumentEscapeListener();
      this.removeBackDrop();
    }
  }

  /**
       * Opens a modal window creating backdrop.
       * @param component The angular Component that is to be loaded dynamically(optional).
       */
  open(component?) {
    this.getFocusedBeforeOpen();
    this.closeModalHandle(this);
    if (this.closeOnEscape) {
      this.bindDocumentEscapeListener();
    }else{
      this.unbindDocumentEscapeListener();
    }
    this.isOpen = true;
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
    if (component) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
      this.cmpRef = this.target.createComponent(componentFactory);
    }
    $(this.modalRoot?.nativeElement).modal('show');
  }
  /**
     *  close method dispose the component, closes the modal and optionally emits modalOutput value.
     */
  close(data?: any) {
    this.dispose();
    if (data) {
      this.modalOutput.emit(data);
    } else {
      this.modalOutput.emit(false);
    }
    this.modalRoot.nativeElement.style.top = '0px';
    this.modalRoot.nativeElement.style.left = '0px';
  }
  /**
     *  ok method dispose the component, closes the modal and emits true.
     */
  submit() {
    this.dispose();
    this.modalOutput.emit(true);
  }
  /**
     *  dispose method dispose the loaded component.
     */
  dispose() {
    $(this.modalRoot.nativeElement).modal('hide');
    this.isOpen = false;
    this.unbindDocumentEscapeListener();
    if (this.component !== undefined) {
      this.component.then((componentRef: ComponentRef<any>) => {
        componentRef.destroy();
        return componentRef;
      });
    }
  }
  initDrag(event: MouseEvent | TouchEvent) {
    this.dragEventTarget = event;
  }
  onCloseIcon(event: Event) {
    event.stopPropagation();
  }

  private closeModalHandle(currentModal: any): void{
    if(currentModal && this.modalRoot?.nativeElement){
      $(this.modalRoot.nativeElement).on('hidden.bs.modal', function (e) {
        currentModal.setFocused();
      })
    }
  }

  private getFocusedBeforeOpen(): void{
    this.focusedBeforeOpenEle = document.activeElement as HTMLElement;
    if(this.focusedBeforeOpenEle.closest('.dropdown-menu')){
      const dropdownItem = this.focusedBeforeOpenEle.closest('.dropdown-menu');
      if(dropdownItem){
        const dropdownToggle = $(dropdownItem).parent().find('.dropdown-toggle');
        dropdownToggle && (this.focusedBeforeOpenEle = dropdownToggle);
      }
    }
  }

  public setFocused(): void {
    //Set focus back to element before the modal was opened
    if (this.focusedBeforeOpenEle) {
      this.focusedBeforeOpenEle.focus();
      this.focusedBeforeOpenEle = null;
    }
  }

  private bindDocumentEscapeListener(): void {
    const documentTarget: any = this.modalRoot.nativeElement ? this.modalRoot.nativeElement.ownerDocument : 'document';
    this.documentEscapeListener = this.renderer.listen(documentTarget, 'keydown', (event) => {
      const keyPressName = this.commonService.keyPressIdentify(event.key);
      if(this.closeOnEscape && keyPressName === EVENT_KEYS_MAP.Esc && this.isOpen){
        if(!this.commonService.isComponentOverlayLeaving()){
          this.close();
        }
      }
    });
  }

  private unbindDocumentEscapeListener(): void {
    if (this.documentEscapeListener) {
      this.documentEscapeListener();
      this.documentEscapeListener = null;
    }
  }

  private removeBackDrop(): void {
    document.querySelector('body').classList.remove('modal-open');
    const backDrop = document.getElementsByClassName('modal-backdrop');
    if (backDrop.length > 0) {
      document.body.removeChild(backDrop[0]);
    }
  }
}
