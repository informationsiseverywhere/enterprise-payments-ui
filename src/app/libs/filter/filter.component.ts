import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, EventEmitter, Output, forwardRef } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl } from '@angular/forms';

export const FILTER_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FilterComponent),
  multi: true
};

export const FILTER_CONTROL_VALIDATOR: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => FilterComponent),
  multi: true
};

@Component({
  selector: 'ent-filter',
  templateUrl: './filter.component.html',
  providers: [FILTER_CONTROL_VALUE_ACCESSOR, FILTER_CONTROL_VALIDATOR]
})
export class FilterComponent implements OnInit {
  showFilter = false;

  searchedList: Array<any> = [];

  tempSelectedItems: Array<any> = [];
  selectedItems: Array<any> = [];

  tempCheckboxList: Array<any> = [];
  checkboxList: Array<any> = [];
  searchCheckBoxList: Array<any> = [];

  @Input('value') _value: any;
  tempValue = '';
  selectedIndex: any;
  optionSelected = false;

  debouncer: Subject<string> = new Subject<string>();

  @Input() dropdownItems: Array<any> = [];
  @Input() displayField: string;
  @Input() selected: string = 'single'; /* single | multiple */

  @Output() applyClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() resetClick: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('filter', { static: false }) filter: ElementRef;
  @ViewChild('checkAll', { static: false }) checkAll: ElementRef;

  validateFn: any = () => { };

  constructor() {
    this.debouncer.pipe(debounceTime(1000)).subscribe(
      filterWord => {
        this.tempCheckboxList.fill(1);
        this.searchCheckBoxList = [];
        this.searchedList = [];
        for (const index in this.dropdownItems) {
          if (this.displayField) {
            if (this.dropdownItems[index][this.displayField].toLowerCase().indexOf(filterWord.toLowerCase()) > -1) {
              this.searchCheckBoxList.push(index);
              this.searchedList.push(this.dropdownItems[index]);
            }
          } else {
            if (this.dropdownItems[index].toLowerCase().indexOf(filterWord.toLowerCase()) > -1) {
              this.searchCheckBoxList.push(index);
              this.searchedList.push(this.dropdownItems[index]);
            }
          }
        }
        if (this.selected === 'multiple') {
          if (this.searchedList.length > 0) {
            this.checkAll.nativeElement.checked = true;
          } else {
            this.checkAll.nativeElement.checked = false;
          }
        }
        this.tempSelectedItems = this.cloneItemArray(this.searchedList);
      });
  }

  ngOnInit() {
  }

  /* Use ngDoCheck in case we push new element to array, ngOnChanges does not fire */
  // Initialize the filter
  ngDoCheck() {
    if (this.dropdownItems.length > 0 && this.checkboxList.length == 0) {

      /* remove duplicated value from dropdownItems */
      // do not remove this snippet
      /*if (this.displayField) {
          for (const item of this.dropdownItems) {
              const index = this.searchedList.map(e => {
                  return e[this.displayField];
              }).indexOf(item[this.displayField]);
              if (index === -1) {
                  this.searchedList.push(item);
              }
          }
          this.dropdownItems = this.cloneItemArray(this.searchedList);
      } else {
          this.dropdownItems = Array.from(new Set(this.dropdownItems));
      }*/

      this.checkboxList = new Array(this.dropdownItems.length).fill(1);
      this.tempCheckboxList = new Array(this.dropdownItems.length).fill(1);
      this.searchCheckBoxList = new Array(this.dropdownItems.length).fill(1);

      this.selectedItems = this.cloneItemArray(this.dropdownItems);

    }
  }

  toggleFilter() {
    this.showFilter = !this.showFilter;
    this.searchedList = this.cloneItemArray(this.dropdownItems);
    if (this.selected === 'single') {
      this.tempValue = this.value;
    } else {
      this.tempValue = this.value = '';
      this.tempCheckboxList = this.checkboxList.slice();
      this.tempSelectedItems = this.cloneItemArray(this.selectedItems);
      this.searchCheckBoxList = this.checkboxList.slice();
      this.searchedList = this.cloneItemArray(this.dropdownItems);
    }

    if (this.showFilter) {
      this.optionSelected = false;
    }
  }

  search(event) {
    if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
      return;
    } else if (event.key.length === 1 || event.keyCode === 8 || event.keyCode === 32) {
      this.debouncer.next(event.target.value);
    }
  }

  selectAll(checked) {
    if (checked) {
      this.tempSelectedItems = this.cloneItemArray(this.dropdownItems);
      this.tempCheckboxList.fill(1);
    } else {
      this.tempCheckboxList.fill(0);
      this.tempSelectedItems = [];
    }
  }

  check(checked, index) {
    if (this.tempValue) {
      index = this.searchCheckBoxList[index];
    }
    if (checked) {
      this.tempCheckboxList[index] = 1;
      this.tempSelectedItems.push(this.dropdownItems[index]);
      if (this.tempSelectedItems.length === this.dropdownItems.length) {
        this.checkAll.nativeElement.checked = true;
      }
    } else {
      this.tempCheckboxList[index] = 0;
      let deletedIndex;
      if (this.displayField) {
        deletedIndex = this.tempSelectedItems.findIndex(x => x[this.displayField] == this.dropdownItems[index][this.displayField]);
      } else {
        deletedIndex = this.tempSelectedItems.indexOf(this.dropdownItems[index]);
      }
      this.tempSelectedItems.splice(deletedIndex, 1);
      if (this.tempSelectedItems.length !== this.dropdownItems.length) {
        this.checkAll.nativeElement.checked = false;
      }
    }
  }

  itemSelected(item, index) {
    this.displayField ? this.tempValue = item[this.displayField] : this.tempValue = item;
    this.selectedIndex = index;
    this.optionSelected = true;
  }

  applySingle() {
    this.value = this.tempValue;
    if (this.value !== '') {
      this.applyClick.emit(this.searchedList[this.selectedIndex]);
    }
    this.toggleFilter();
  }

  applyMultiple() {
    if (this.tempSelectedItems.length === 0) {
      this.checkAll.nativeElement.checked = true;
      this.selectAll(true);
      this.selectedItems = this.cloneItemArray(this.tempSelectedItems);
      this.checkboxList = this.tempCheckboxList.slice();
      this.toggleFilter();
      this.applyClick.emit(this.selectedItems);
      return;
    }
    if (this.tempValue) {
      this.selectedItems = this.cloneItemArray(this.tempSelectedItems);
      this.checkboxList.fill(0);
      for (const index of this.searchCheckBoxList) {
        if (this.tempCheckboxList[index] === 1) {
          this.checkboxList[index] = 1;
        }
      }
      this.toggleFilter();
      this.applyClick.emit(this.selectedItems);
    } else {
      this.selectedItems = this.cloneItemArray(this.tempSelectedItems);
      this.checkboxList = this.tempCheckboxList.slice();
      this.toggleFilter();
      this.applyClick.emit(this.selectedItems);
    }
  }

  cancel() {
    this.toggleFilter();
  }

  reset() {
    if (this.selected === 'single') {
      this.selectedIndex = undefined;
      this.value = '';
      this.toggleFilter();
      this.resetClick.emit();
    } else {
      this.checkAll.nativeElement.checked = true;
      this.selectAll(true);
      this.selectedItems = this.cloneItemArray(this.tempSelectedItems);
      this.checkboxList = this.tempCheckboxList.slice();
      this.searchCheckBoxList = this.tempCheckboxList.slice();
      this.toggleFilter();
      this.resetClick.emit();
    }
  }

  clearInput() {
    this.tempValue = '';
    this.debouncer.next('');
  }

  cloneItemArray(src: Array<any>) {
    if (this.displayField) {
      //Clone an object array
      return src.map(a => Object.assign({}, a));
    } else {
      //Clone a string array
      return src.slice();
    }
  }

  /* Close on click outside */
  @HostListener('document:click', ['$event.target'])
  onClick(targetElement) {
    const clickedInside = this.filter.nativeElement.contains(targetElement);
    if (!clickedInside) {
      targetElement.classList.contains('close-icon') ? this.showFilter = true : this.showFilter = false;
      this.tempCheckboxList = this.checkboxList.slice();
      this.tempSelectedItems = this.cloneItemArray(this.selectedItems);
    }
  }

  get value(): any { return this._value; }

  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
    }
  }

  /**
   * From ControlValueAccessor interface.
   */
  writeValue(val: any) {
    this._value = val;
    this.onChange(val);
  }

  onChange = (_) => { };
  onTouched = () => { };
  /**
 * From ControlValueAccessor interface.
 */
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  /**
 * From ControlValueAccessor interface.
 */
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  validate(c: FormControl) {
    return this.validateFn(c);
  }
}
