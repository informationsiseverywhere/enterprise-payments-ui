import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'orderBy', pure: false })
export class OrderBy implements PipeTransform {
    transform(input: any, [config = '+'], sort): any {
        if(!Array.isArray(input) || !sort) return input; //value isn't even an array can't sort

        if (!Array.isArray(config) || (Array.isArray(config) && config.length == 1)) {
            //Single property to sort on
            const propertyToCheck: string = !Array.isArray(config) ? config : config[0];
            const desc = propertyToCheck.substr(0, 1) == '-';

            if (!propertyToCheck || propertyToCheck == '-' || propertyToCheck == '+') {
                //is a basic array that is sorting on the array's object itself
                return !desc ? input.sort() : input.sort().reverse();
            } else {
                //is a complex array that is sorting on a single property
                return input.sort((a, b) => a[propertyToCheck] < b[propertyToCheck] ? -1 : a[propertyToCheck] > b[propertyToCheck] ? 1 : 0);
            }
        } else {
            //is a complex array with multiple properties to sort on
        }
    }
}
