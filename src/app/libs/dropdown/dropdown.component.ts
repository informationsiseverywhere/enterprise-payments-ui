import { Component, OnInit, Input, Output, EventEmitter, forwardRef, ViewChild, HostListener, ElementRef } from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';
import { CurrencyPipe, getCurrencySymbol } from '@angular/common';
import { Subject } from 'rxjs';
import { tap, debounceTime } from 'rxjs/operators';
import { CommonService } from 'src/app/shared/services/common.service';
import { InputComponent } from 'src/app/libs/input/input.component';
import { IsEllipsisActiveDirective } from 'src/app/shared/directive/is-ellipsis-active.directive';
import { PropertyName } from 'src/app/shared/enum/common.enum';

export const DROPDOWN_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DropdownComponent),
  multi: true
};

export const DROPDOWN_CONTROL_VALIDATOR: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => DropdownComponent),
  multi: true
};

@Component({
  selector: 'ent-dropdown',
  templateUrl: './dropdown.component.html',
  providers: [DROPDOWN_CONTROL_VALUE_ACCESSOR, DROPDOWN_CONTROL_VALIDATOR, CurrencyPipe]
})
export class DropdownComponent implements ControlValueAccessor, OnInit {
  @Input() type: string;
  @Input() fasName: string;
  @Input() prefix: string;
  @Output() selectedValue: EventEmitter<any> = new EventEmitter<any>();
  @Output() clearInputValue: EventEmitter<any> = new EventEmitter();
  @Input() displayField: string;
  private _value: any;
  @Input() currency: any;
  get value(): any { return this._value; }

  @Input()
  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }
  @Input() showOthers: boolean;
  @ViewChild('dropdown', { static: true }) dropdown: ElementRef;
  @ViewChild('menu', { static: false }) menu: ElementRef;
  @ViewChild('numberInput', { static: false }) numberInput: InputComponent;
  @ViewChild('buttonDropdown', { static: false }) buttonDropdown: ElementRef;
  @ViewChild(IsEllipsisActiveDirective, { static: false }) isEllipsisActiveDirective: IsEllipsisActiveDirective;
  currentIndex = -1;
  @Input() disabledField: string;
  @Input() control: FormControl;
  @Input() sort = true;
  @Input() showClose = true;
  @Input() isAutoSelected = true;
  @Input() filterMsg: string;
  @Input() isAutoFocus: boolean = false;
  public _dropdownItems: Array<any> = [];
  private pressedKeys: string[] = [];
  private keyPress = new Subject<string>();
  public isOtherSelected = false;

  @Input()
  public set dropdownItems(value: Array<any>) {
    this._dropdownItems = value;
    (this.dropdownItems?.length > 0) && this.focusCurrentSelectedItem(this._dropdownItems);
  }

  public get dropdownItems(): Array<any> {
    return this._dropdownItems;
  }

  ngOnInit() {
    this.handleKeyPresses();
  }

  @HostListener('document:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.menu && this.menu.nativeElement.className.indexOf('show') > 0) {
      if (this.dropdownItems.length > 0) {
        const keyPressName = this.commonService.keyPressIdentify(event.key);
        if (keyPressName === 'ArrowDown' || keyPressName === 'ArrowUp') {
          if (keyPressName === 'ArrowDown') { // Down Arrow
            if (this.showOthers) {
              if (!this.isOtherSelected) { this.currentIndex++; }
              if ((this.currentIndex === this.dropdownItems.length) && !this.isOtherSelected) {
                if (this.showOthers) {
                  this.isOtherSelected = true;
                  this.numberInput.elementRef.nativeElement.focus();
                  this.value = '';
                  this.currentIndex = -1;

                } else {
                  this.currentIndex = this.dropdownItems.length - 1;
                }
              }
            } else {
              this.currentIndex++;
              if (this.currentIndex === this.dropdownItems.length) {
                this.currentIndex = this.dropdownItems.length - 1;
              }
            }
            this.dropdownItemSelected(this.currentIndex, false, false);
            event.preventDefault();
          } else if (keyPressName === 'ArrowUp') { // Up Arrow
            if (this.showOthers) {
              if (this.isOtherSelected) {
                this.currentIndex = this.dropdownItems.length - 1;
              } else {
                this.currentIndex--;
              }
              this.isOtherSelected = false;
            } else {
              this.currentIndex--;
            }
            if (this.currentIndex < 0) {
              this.currentIndex = 0;
            }
            this.dropdownItemSelected(this.currentIndex, false, false);
            event.preventDefault();
          }
          if (this.isAutoSelected && !this.isOtherSelected) {
            if (this.displayField) {
              this.value = this.dropdownItems[this.currentIndex][this.displayField];
            } else {
              this.value = this.dropdownItems[this.currentIndex];
            }
          }
        } else {
          if (keyPressName === 'Enter' || keyPressName === 'Tab') { // Enter Or Tab
            if(this.isOtherSelected) this.closeDropdownMenu();
            this.buttonDropdown?.nativeElement?.focus();
            this.dropdownItemSelected(this.currentIndex, !this.isAutoSelected);
            event.preventDefault();
          } else if (event.key.length === 1) { // catching unicode character only
            this.keyPress.next(event.key.toLocaleLowerCase());
          }
        }
      }
    } else {
      setTimeout(() => {
        this.menu?.nativeElement?.classList?.remove('component-overlay-leave');
      }, 100);
    }
  }

  private displayEllipsisTooltip(item?: any): void{
    if(item.querySelector('.dropdown-item-label')){
      const itemLabel = item.querySelector('.dropdown-item-label');
      this.isEllipsisActiveDirective.setTitleContent(itemLabel);
    }
  }

  handleKeyPresses() {
    this.keyPress
      .pipe(
        tap(letter => this.pressedKeys.push(letter)),
        debounceTime(200))
      .subscribe(letter => {
        const filterWord = this.pressedKeys.join('');
        const filteredList = this.dropdownItems.filter(data => {
          if (this.displayField) {
            return data[this.displayField].toLowerCase().indexOf(filterWord.toLowerCase()) > -1;
          }
          return data.toLowerCase().indexOf(filterWord.toLowerCase()) > -1;
        });
        if (filteredList.length > 0) {
          let minIndex = 100;
          let tempValue = filteredList[0];
          for (const item of filteredList) {
            let index = this.displayField ? item[this.displayField].indexOf(filterWord) : item.indexOf(filterWord); // check lower case
            if (index !== -1 && index < minIndex) {
              minIndex = index;
              tempValue = item;
            }
            // indexOf 'e' in 'Engineer' is 4. Therefore, we need to check upper case.
            index = this.displayField ? item[this.displayField].indexOf(filterWord.toUpperCase()) : item.indexOf(filterWord.toUpperCase()); // check upper case
            if (index !== -1 && index < minIndex) {
              minIndex = index;
              tempValue = item;
            }
          }
          if (!this.showOthers || !this.isOtherSelected) { 
            this.dropdownItemSelected(this.currentIndexValue(tempValue, this.dropdownItems), false, false);
          }
        }
        this.pressedKeys = [];
      });
  }

  private dropdownItemSelected(indexItem: number, isClicked = false, isCloseDropdown = true): void{
    this.currentIndex = indexItem;
    if(!this.isOtherSelected) this.itemSelected(this.dropdownItems[this.currentIndex], this.currentIndex, isClicked, isCloseDropdown);
    !isCloseDropdown && this.focusDropdownItem(this.currentIndex);
  }

  private focusDropdownItem(index: number): void{
    const item = this.menu?.nativeElement?.querySelector('#menuDropdownItem-' + index);
    item?.focus();
    item && this.displayEllipsisTooltip(item);
  }

  private currentIndexValue(value: any, dropdownItems: Array<any>): number{
    return dropdownItems?.findIndex(element => this.currentSelectedValue(element) === this.currentSelectedValue(value));
  }

  private currentSelectedValue(value: any): any{
    if(typeof value === 'object' && this.displayField){
      return value[this.displayField];
    }else{
      return value;
    }
  }

  private focusCurrentSelectedItem(dropdownItems: Array<any>): void{
    setTimeout(() => {
      this.currentIndex = this.currentIndexValue(this.value, dropdownItems);
      this.value && this.focusDropdownItem(this.currentIndex);
    }, 100);  
  }

  private closeDropdownMenu(): void{
    this.menu?.nativeElement?.classList?.remove('show');
    this.dropdown?.nativeElement?.classList?.remove('show');
    this.buttonDropdown?.nativeElement?.setAttribute('aria-expanded', 'false');
  }

  public currencySymbol(currency: any) {
    if (currency) {
      return getCurrencySymbol(currency, 'wide', 'en');
    }
  }

  validateFn: any = () => { };

  /*** Creates and initializes a new dropdown component.*/
  constructor(
    private currencyPipe: CurrencyPipe,
    public commonService: CommonService) { }

  isError() {
    if (this.control) {
      return (this.control.invalid && this.control.touched && this.control.dirty) ? true : false;
    }
  }


  /**
   * From ControlValueAccessor interface.
   */
  writeValue(val: any) {
    this._value = val;
    this.onChange(val);
  }

  onChange = (_) => { };
  onTouched = () => { };
  /*** From ControlValueAccessor interface.*/
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }

  /*** From ControlValueAccessor interface.*/
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  validate(c: FormControl) {
    return this.validateFn(c);
  }

  public openDropdown(dropdownItems?: Array<any>): void {
    (dropdownItems?.length > 0) && this.focusCurrentSelectedItem(dropdownItems);
    if (this.showOthers){
      if(!(this.value && this.value.substring(0, 5) === 'Other') || this.numberInput.elementRef.nativeElement.value === ''){
        this.isOtherSelected = false;
      }else{
        this.isOtherSelected = true;
      }
    }
  }

  public itemSelected(item: any, i: any, isClicked: boolean, isCloseDropdown: boolean = true): void {
    if (this.showOthers) { this.isOtherSelected = false; }
    this.currentIndex = i;
    if (this.isAutoSelected || (!this.isAutoSelected && isClicked)) {
      if (this.displayField) {
        this.value = item[this.displayField];
      } else {
        this.value = item;
      }
      this.selectedValue.emit(item);
      if(isCloseDropdown) this.closeDropdownMenu();
    }
  }

  public othersValueInput(input: any) {
    if (this.isOtherSelected) {
      this.updateOthersValue(input);
    }
  }

  public othersValueSelected(input: any) {
    this.updateOthersValue(input);
    this.isOtherSelected = true;
    this.numberInput.elementRef.nativeElement.focus();
    this.currentIndex = -1;
    if (!(input && input.value)) { this.value = ''; }
  }

  public updateOthersValue(input: any) {
    if (input && input.value) {
      this.value = PropertyName.otherLabel + this.currencyPipe.transform(input.value, this.currency, 'symbol', '1.2-2');
      this.selectedValue.emit(input);
    }
  }

  clearValues($event?: any) {
    this.clearInputValue.emit($event.target.value);
    this.selectedValue.emit($event.target.value);
    if (this.control) { this.control.markAsTouched(); }
    this.buttonDropdown?.nativeElement?.focus();
  }

  public dropdownMenuClosingFocus(): void{
    if (this.menu && this.menu.nativeElement.className.indexOf('show') > 0){
      this.menu?.nativeElement?.classList?.add('component-overlay-leave');
    }
  }
}
