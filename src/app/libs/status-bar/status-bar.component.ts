import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ent-status-bar',
  templateUrl: './status-bar.component.html'
})
export class StatusBarComponent implements OnInit {
  @Input() labelList: any;
  @Input() activatedValue: string;

  constructor() { }

  ngOnInit() {
  }

  indexOf(object, value) {
    return object.indexOf(value);
  }

}
