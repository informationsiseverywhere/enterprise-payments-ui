import { Component, forwardRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, FormControl } from '@angular/forms';

@Component({
    selector: 'ent-text-area',
    templateUrl: './text-area.component.html',
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => TextAreaComponent), multi: true },
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => TextAreaComponent), multi: true }
    ]
})
export class TextAreaComponent implements ControlValueAccessor {
    @Input() placeholder: string;
    @Input() disabledField: string;
    @Output() valueChanged: EventEmitter<any> = new EventEmitter();
    @Output() enterClickOutput: EventEmitter<any> = new EventEmitter();
    @Output() escClickOutput: EventEmitter<any> = new EventEmitter();
    @Output() clearInputValue: EventEmitter<any> = new EventEmitter();
    @Input() fasName: string;
    @Input() rowSize = 2;
    @ViewChild('textarea', { static: false }) textarea: any;
    @Input() control: FormControl;
    @Input() isScroll = false;
    @Input() showClose = true;
    @Input('value') _value: any = '';

    validateFn: any = () => { };
    constructor() { }

    isError() {
        if (this.control) {
            return (this.control.invalid && this.control.touched && this.control.dirty) ? true : false;
        }
    }

    get value(): any { return this._value; };

    set value(v: any) {
        if (v !== this._value) {
            this._value = v;
            this.onChange(v);
        }
    }

    writeValue(val: any) {
        this._value = val;
        this.onChange(val);
    }

    validate(c: FormControl) {
        return this.validateFn(c);
    }

    setDisabledState(isDisabled: boolean) {

    }

    onChange = (_) => { };
    onTouched = () => { };
    registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
    registerOnTouched(fn: () => void): void { this.onTouched = fn; }

    onInputChange($event) {
        this.valueChanged.emit($event.target.value);
    }

    confirmText($event: any) {
        this.enterClickOutput.emit($event);
        $event.target.blur();
    }

    focus() {
        this.textarea.nativeElement.focus();
    }

    blur() {
        this.textarea.nativeElement.blur();
    }

    resetText() {
        this.escClickOutput.emit();
    }

    clearValues($event?: any) {
        this.clearInputValue.emit($event.target.value);
        this.valueChanged.emit($event.target.value);
        if (this.control) { this.control.markAsTouched(); }
        this.textarea?.nativeElement.focus();
    }
}
