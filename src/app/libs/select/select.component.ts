
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SelectService } from './services/select.service';
import { Component, Input, Output, ElementRef, ViewChild, EventEmitter, OnInit, forwardRef, HostListener } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';
import { QueryParam, Link, PageObject } from './models/select.model';
import { HalProcessor } from '../../shared/services/utilities.service';
import { CommonService } from '../../shared/services/common.service';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import * as moment from 'moment';

const NO_RECORDS_ARE_AVAILABLE = 'No records are available';
const LOADING = 'Loading...';

export const SELECT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectComponent),
  multi: true
};

@Component({
  selector: 'ent-select',
  templateUrl: './select.component.html',
  providers: [SELECT_CONTROL_VALUE_ACCESSOR]
})
export class SelectComponent implements ControlValueAccessor, OnInit {
  list: any[] = [];
  dropdownItems: Array<any> = [];
  links: Link[] = [];
  pageObject: PageObject;
  hasNext = false;
  @Input('value') _value: any;
  showClose = true;
  firstQuery = '';
  currentIndex = -1;
  isSelected = false;
  debouncer: Subject<string> = new Subject<string>();
  private defaultDateConfig: any;
  private previousValue: any;

  @Input() fasName: string;
  @Input() displayField: string;
  @Input() disabledField: string;
  @Input() other: string;
  @Input() url: string;
  @Input() size = 10;
  @Input() queryParam: QueryParam = new QueryParam('', null, null, 1, 10);
  @Input() control: FormControl;
  @Input() placeholder: string;
  @Input() forceSelection: boolean;
  @Input() selectRequired = false;
  @Input() applyQueryParamOnSearch: string;
  @Input() isLoadApi = true;
  @Input() isDate = false;
  @Input() isAutoFocus: boolean = false;
  @Output() selectedValue: EventEmitter<any> = new EventEmitter<any>();
  @Output() valueChanged: EventEmitter<any> = new EventEmitter();
  @Output() clearInputValue: EventEmitter<any> = new EventEmitter();
  @Output() dropdownClick: any = new EventEmitter<any>();
  @ViewChild('input', { static: false }) input: ElementRef;
  @ViewChild('moreBt', { static: false }) moreBt: ElementRef;
  @ViewChild('toggle', { static: false }) toggle: ElementRef;
  @ViewChild('content', { static: false }) content: ElementRef;
  @ViewChild('dropdown', { static: false }) dropdown: ElementRef;

  constructor(
    private selectService: SelectService,
    private halProcessor: HalProcessor,
    private commonService: CommonService) {
    this.debouncer.pipe(debounceTime(1000)).subscribe(
      () => {
        if(this.previousValue !== this.value){
          this.previousValue = this.value;
          this.currentIndex = -1;
        }
        this.dropdownItems = this.list = [];
        if (this.value && this.value.trim() !== '') {
          this.dropdownItems.push(LOADING);
          this.queryParam.query = this.value;
          this.valueChanged.emit(this.value);
          if (this.applyQueryParamOnSearch) {
            const queryParam = new QueryParam(this.queryParam.query, this.queryParam.filters, this.applyQueryParamOnSearch, 1, 10);
            this.getItemsByUrl(this.url, queryParam);
          } else {
            this.getItemsByUrl(this.url, this.queryParam);
          }
        }
      });
  }
  isError() {
    if (this.control) {
      return (this.control.invalid && this.control.touched && this.control.dirty) ? true : false;
    }
  }

  search(event) {
    if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 13) {
      return;
    } else if (event.key.length === 1 || event.keyCode === 8 || event.keyCode === 32) {
      this.toggle.nativeElement.classList.add('show');
      this.dropdown.nativeElement.classList.add('show');
      this.debouncer.next(event.target.value);
    }
  }

  textChanged(event) {
    if (this.selectRequired) {
      if ((this.isSelected && this._value != event) || (this.selectRequired && !this._value)) {
        this.isSelected = false;
        if (this.selectRequired && this.control) { this.control.setErrors({ notSelected: true }); }
        this.selectedValue.emit(undefined);
        this._value = event;
        return;
      }
    }
    this._value = event;
    if (this.control && !this.control.getError('notSelected')) { this.onChange(this._value); }
  }

  ngOnInit(): void {
    this.previousValue = '';
    this.defaultDateConfig = ComponentConfiguration.settings;
    this.firstQuery = this.queryParam?.query;
    if (this.isLoadApi) {
      this.getItemsByUrl(this.url, this.queryParam);
    }
  }

  fillInDropdown() {
    for (let i = 0; i < this.list.length; i++) {
      if (i < this.size) {
        this.dropdownItems.push(this.list[i]);
      }
    }
    this.list.splice(0, this.size);
    if (this.dropdownItems[0] === LOADING) {
      this.dropdownItems.splice(0, 1);
    }
  }

  public focusToCurrentSelectionIndex(): void{
    setTimeout(() => {
      const item = this.dropdown.nativeElement.querySelector('#dropdownItem-' + this.currentIndex);
      item.focus();
      event.preventDefault();
    }, 100);
  }

  getItemsByUrl(url: string, param?: QueryParam) {
    this.hasNext = false;
    this.selectService.getItems(url, param).subscribe(
      res => {
        this.list = res ? res.content : null;
        this.links = res ? res.links : null;
        if (this.list.length > 0) {
          (this.list.length < this.size) ? this.hasNext = false : this.hasNext = true;
          this.fillInDropdown();
        } else {
          this.dropdownItems = [];
          this.dropdownItems.push(NO_RECORDS_ARE_AVAILABLE);
        }
        const item = this.dropdown.nativeElement.querySelector('#dropdownItem-' + this.currentIndex);
        this.focusElement(item, null, true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  getMoreItems(event: any) {
    event.stopPropagation();
    if (this.list.length > 0) {
      this.fillInDropdown();
      const item = this.dropdown.nativeElement.querySelector('#dropdownItem-' + this.currentIndex);
      this.focusElement(item, event);
      return;
    }

    const nextPageUrl = this.halProcessor.linkParser('next', this.links);
    if (nextPageUrl) {
      this.getItemsByUrl(nextPageUrl);
    } else {
      this.hasNext = false;
    }
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    this.cleanActiveItem();
    if (this.dropdown.nativeElement.className.indexOf('show') > 0) {
      if (this.dropdownItems.length > 0) {
        if (event.keyCode === 40 || event.keyCode === 38) {
          if (event.keyCode === 40) { // Down Arrow
            this.currentIndex++;
            if (this.currentIndex >= this.dropdownItems.length) {
              this.currentIndex = this.dropdownItems.length - 1;
              if(this.hasNext){
                this.focusElement(this.moreBt.nativeElement, event);
                return;
              }
            }
            const item = this.dropdown.nativeElement.querySelector('#dropdownItem-' + this.currentIndex);
            item.focus();
            event.preventDefault();
          } else if (event.keyCode === 38) { // Up Arrow
            this.currentIndex--;
            if (this.currentIndex < 0) {
              this.currentIndex = 0;
            }
            if(document.activeElement === this.moreBt.nativeElement){
              this.currentIndex = this.dropdownItems.length - 1;
            }
            const item = this.dropdown.nativeElement.querySelector('#dropdownItem-' + this.currentIndex);
            item.focus();
            event.preventDefault();
          }
        } else {
          if (event.keyCode === 13 || event.keyCode === 9) { //Enter, Tab
            if(document.activeElement !== this.moreBt.nativeElement){
              if (this.currentIndex > -1) {
                this.itemSelected(this.dropdownItems[this.currentIndex], this.currentIndex);
              }
              this.toggle.nativeElement.classList.remove('show');
              this.dropdown.nativeElement.classList.remove('show');
              return false;
            }
          }
        }
      }
    } else if (event.keyCode === 13) {
      this.toggle.nativeElement.classList.add('show');
      this.dropdown.nativeElement.classList.add('show');
      this.focusToCurrentSelectionIndex();
    } else {
      setTimeout(() => {
        this.dropdown?.nativeElement?.classList?.remove('component-overlay-leave');
      }, 100);
    }
  }

  public itemSelected(item: any, i: any): void {
    if (item === NO_RECORDS_ARE_AVAILABLE || item === LOADING) {
      return;
    }
    if (this.displayField) {
      if (this.other) {
        this.value = item[this.displayField] + ' (' + item[this.other] + ')';
      } else {
        if (this.isDate) {
          this.value = moment(item[this.displayField], this.defaultDateConfig.parseDateFormat).format(this.defaultDateConfig.dateFormat);
        } else {
          this.value = item[this.displayField];
        }
      }
    } else {
      if (item != NO_RECORDS_ARE_AVAILABLE && item != LOADING) {
        this.value = item;
        this.selectedValue.emit(this.value);
        return;
      }
    }
    this.currentIndex = i;
    this.input.nativeElement.focus();
    this.isSelected = true;
    if (this.control && this.control.hasError('notSelected')) { delete this.control.errors.notSelected; }
    this.onChange(this._value);
    this.selectedValue.emit(item);
    this.dropdown.nativeElement.classList.remove('show');
    this.toggle.nativeElement.classList.remove('show');
  }

  clear() {
    this.value = '';
    this.previousValue = '';
    this.currentIndex = -1;
    this.focusElement(this.input?.nativeElement);
    event.stopPropagation();
    this.toggle.nativeElement.classList.add('show');
    this.dropdown.nativeElement.classList.add('show');
    this.dropdownItems = [];
    this.queryParam.query = this.firstQuery;
    this.clearInputValue.emit('');
    this.valueChanged.emit('');
    if (this.selectRequired && this.control) { this.control.setErrors({ notSelected: true }); }
    this.isSelected = false;
    this.selectedValue.emit(undefined);
  }

  focus() {
    if (this.currentIndex >= 0) {
      /* Focus to previous element when opening the search
         Need timer to make it work */
      setTimeout(() => {
        this.cleanActiveItem();
        const item = document.getElementById('dropdownItem-' + this.currentIndex);
        if (item && item.classList) {
          item.classList.add('active');
        }
        this.input.nativeElement.focus();
      }, 100);
    }
  }

  cleanActiveItem() {
    const dropDowns = Array.from(document.querySelectorAll('[id^="dropdownItem"]'));
    dropDowns.forEach(node => {
      node.classList.remove('active');
    });
  }

  get value(): any { return this._value; }

  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }

  writeValue(val: any) {
    this._value = val;
    this.onChange(val);
  }

  onChange = (_) => { };
  onTouched = () => { };

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  validateFn: any = () => { };
  validate(c: FormControl) {
    return this.validateFn(c);
  }

  eventClick(event: any) {
    this.dropdownClick.emit(event);
  }

  public dropdownMenuClosingFocus(): void{
    if (this.dropdown && this.dropdown.nativeElement.className.indexOf('show') > 0){
      this.dropdown?.nativeElement?.classList?.add('component-overlay-leave');
    }
  }

  public focusElement(element: any, event?: any, isPreventScroll = false): void {
    setTimeout(() => {
      element?.focus({ preventScroll: isPreventScroll });
      event && (event.preventDefault());
    },);
  }
}
