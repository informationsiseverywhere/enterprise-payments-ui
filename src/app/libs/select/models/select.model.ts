export class QueryParam {
    query: string;
    filters: any;
    sort: any;
    page: number;
    size: number;

    constructor(query?: string, filters?: any, sort?: any, page?: number, size?: number) {
        this.query = query;
        this.filters = filters;
        this.sort = sort;
        this.page = page;
        this.size = size;
    }
}

export class PageObject {
    firstPageUrl: string;
    previousPageUrl: string;
    nextPageUrl: string;
    lastPageUrl: string;
    currentPageUrl: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class Link {
    rel: string = '';
    href: string = '';

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
