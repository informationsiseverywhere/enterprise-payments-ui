import { Injectable } from '@angular/core';
import { RestService } from '../../../shared/services/rest.service';
import { QueryParam } from '../models/select.model';

@Injectable()
export class SelectService {

    constructor(private restService: RestService) { }

    getItems(requestURL: string, queryParam?: QueryParam) {
        let url = requestURL;
        if(!!url) { 
            url = this.buildUrl(url, queryParam);
            return this.restService.getByUrl(url);
        }
    }

    private buildUrl(url: string, queryParam: QueryParam): string {
        const paramList = [];
        if (queryParam) {
            queryParam.query && paramList.push({ key: 'query', value: encodeURIComponent(queryParam.query) });
            queryParam.filters && paramList.push({ key: 'filters', value: queryParam.filters });
            queryParam.sort && paramList.push({ key: 'sort', value: queryParam.sort });
            queryParam.page && paramList.push({ key: 'page', value: queryParam.page });
            queryParam.size && paramList.push({ key: 'size', value: queryParam.size });
        }
        let queryString = '';
        for (const param of paramList) {
            queryString += param.key + '=' + param.value;
            if (param !== paramList[paramList.length - 1]) {
                queryString += '&';
            }
        }
        return url + (queryString.length ? ('?' + queryString) : '');
    }
}
