
import { debounceTime } from 'rxjs/operators';
import { Component, forwardRef, Input, Output, EventEmitter, HostListener, ViewChild, ElementRef, Renderer2, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { QueryParam } from '../select/models/select.model';
import { AppConfiguration } from '../../shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { EVENT_KEYS_MAP } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'ent-search-input',
  templateUrl: './search-input.component.html',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SearchInputComponent), multi: true },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => SearchInputComponent), multi: true }
  ]
})
export class SearchInputComponent implements ControlValueAccessor, OnInit {
  filters: any;
  query: any;
  @Input() placeholder: string;
  @Input('value') _value: any = '';
  @Input() advancedSearch = false;
  @Input() advancedSearchTitle: string;
  @Input() title = 'Advanced Search';
  @Input() clearSearchValue: string;
  @Input() disabled: boolean = false;
  @Input() isAutoFocus: boolean = false;
  @Input() closeAdvancedSearchOnEscape: boolean = true;
  @Output() openToggleAdvancedSearch: EventEmitter<any> = new EventEmitter<any>();
  @Output() valueChanged: EventEmitter<string> = new EventEmitter<string>();
  openSearch = false;
  @ViewChild('advancedContent', { static: false }) advancedContent: ElementRef;
  @ViewChild('toggleButton', { static: false }) toggleButton: ElementRef;
  @ViewChild('closeAdvancedContent', { static: false }) closeAdvancedContent: ElementRef;
  @ViewChild('searchInputVal', { static: false }) searchInputVal: ElementRef;
  validateFn: any = () => { };
  debouncer: Subject<string> = new Subject<string>();
  public paramObject = new QueryParam;
  public documentEscapeListener: Function;
  private previousValue: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public commonService: CommonService,
    public appConfig: AppConfiguration,
    public renderer: Renderer2) {
    this.debouncer.pipe(debounceTime(1000)).subscribe(
      val => {
	 if(this.previousValue === val){
          return;
        }else{
          this.previousValue = val;
        }
        this.valueChanged.emit(val);
      });
  }

  ngOnInit(): void {
    this.previousValue = '';
    if (this.closeAdvancedSearchOnEscape) {
      this.bindDocumentEscapeListener();
    }else{
      this.unbindDocumentEscapeListener();
    }
  }

  get value(): any { return this._value; }

  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }

  writeValue(val: any) {
    this._value = val;
    (!val) && (this.previousValue = '');
    this.onChange(val);
  }

  validate(c: FormControl) {
    return this.validateFn(c);
  }

  setDisabledState(isDisabled: boolean) {

  }

  onChange = (_) => { };
  onTouched = () => { };
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  onInputChange($event) {
    this.debouncer.next($event.target.value);
  }

  clearValue() {
    this._value = null;
  }

  clearSearch() {
    const currentQueryParam = this.route.snapshot.queryParams;
    if(!this.paramObject) {
      for (const key in currentQueryParam) {
        if (key !== 'query' && key !== 'page' && key !== 'size') {
          this.paramObject[key] = currentQueryParam[key];
        }
      }
    }
    this.valueChanged.emit('');
    this.paramObject.query = undefined;
    this.previousValue = '';
    this.focusElement(this.searchInputVal?.nativeElement);
    this.router.navigate(['/' + this.clearSearchValue], {
      queryParams: this.paramObject
    });
  }

  toggleAdvancedSearch() {
    this.openSearch = !this.openSearch;
    if (this.closeAdvancedSearchOnEscape) {
      if(this.openSearch){
        this.bindDocumentEscapeListener();
      }else{
        this.unbindDocumentEscapeListener();
      }
    }else{
      this.unbindDocumentEscapeListener();
    }
    this.openToggleAdvancedSearch.emit(this.openSearch);
  }

  /*Close on click outside*/
  @HostListener('document:click', ['$event.target'])
  onClick(targetElement) {
    if (this.toggleButton && this.toggleButton.nativeElement.contains(targetElement)) {
      return;
    }
    if (this.advancedContent) {
      const clickedInside = this.advancedContent.nativeElement.contains(targetElement);
      if (!clickedInside) {
        this.openSearch = false;
	      this.unbindDocumentEscapeListener();
      }
    }
  }

  private bindDocumentEscapeListener(): void {
    const documentTarget: any = this.advancedContent.nativeElement ? this.advancedContent.nativeElement.ownerDocument : 'document';
    this.documentEscapeListener = this.renderer.listen(documentTarget, 'keydown', (event) => {
      const keyPressName = this.commonService.keyPressIdentify(event.key);
      if(this.closeAdvancedSearchOnEscape && keyPressName === EVENT_KEYS_MAP.Esc && this.openSearch){
        if(!this.commonService.isComponentOverlayLeaving()){
          this.openSearch = false;
          this.unbindDocumentEscapeListener();
	        this.focusElement(this.toggleButton?.nativeElement);
        }
      }
    });
  }

  @HostListener('document:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    const keyPressName = this.commonService.keyPressIdentify(event.key);
    if(keyPressName === EVENT_KEYS_MAP.Tab && this.openSearch){
      const clickedInside = this.advancedContent.nativeElement.contains(event.target);
      if (!clickedInside) {
        this.openSearch = false;
        this.unbindDocumentEscapeListener();
      }
    }
  }

  public unbindDocumentEscapeListener(): void {
    if (this.documentEscapeListener) {
      this.documentEscapeListener();
      this.documentEscapeListener = null;
    }
  }

  public advancedSearchCloseHandle(): void {
    this.focusElement(this.toggleButton?.nativeElement);
  }

  public focusCloseAdvancedContent(): void {
    this.focusElement(this.closeAdvancedContent?.nativeElement);
  }

  public focusSearchInput(): void {
    this.focusElement(this.searchInputVal?.nativeElement);
  }

  public focusElement(element: any): void {
    setTimeout(() => {
      element?.focus();
    },);
  }

}
