import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { A11yModule } from '@angular/cdk/a11y';
import { TextComponent } from './text/text.component';
import { IconComponent } from './icon/icon.component';
import { ImageComponent } from './image/image.component';
import { LabelComponent } from './label/label.component';
import { LinkComponent } from './link/link.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { InputCheckboxComponent } from './input-checkbox/input-checkbox.component';
import { CardComponent } from './card/card.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ButtonComponent } from './button/button.component';
import { InputComponent } from './input/input.component';
import { UserInitialsComponent } from './user-initials/user-initials.component';
import { ExportToExcelComponent } from './export-to-excel/export-to-excel.component';
import { TitleBarComponent } from './title-bar/title-bar.component';
import { HeaderComponent } from './header/header.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { EmptyViewComponent } from './empty-view/empty-view.component';
import { TypeaheadComponent } from './typeahead/typeahead.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { ControlMessagesComponent } from './form-validation-control/control-message.component';
import { BadgeComponent } from './badge/badge.component';
import { DatagridComponent } from './datagrid/datagrid.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { StatusBarComponent } from './status-bar/status-bar.component';
import { TabComponent } from './tabs/tab/tab.component';
import { TabsComponent } from './tabs/tabs.component';
import { TextAreaComponent } from './text-area/text-area.component';
import { VerticalNavComponent } from './vertical-nav/vertical-nav.component';
import { WizardComponent } from './wizard/wizard.component';
import { WizardStepComponent } from './wizard-step/wizard-step.component';
import { PaginationComponent } from './pagination/pagination.component';
import { PaginationService } from './pagination/services/pagination.service';
import { MenuItemsComponent } from './menu-items/menu-items.component';
import { AlertComponent } from './alert/alert.component';
import { AlertService } from './alert/services/alert.service';
import { Popover } from './signpost/popover-content/popover.component';
import { PopoverContentComponent } from './signpost/popover-content/popover-content.component';
import { SignpostComponent } from './signpost/signpost.component';
import { SelectService } from './select/services/select.service';
import { SelectComponent } from './select/select.component';
import { TimelineComponent } from './timeline/timeline.component';
import { OrderBy } from './dropdown/sort.pipe';
import { NumberIncrementerComponent } from './number-incrementer/number-incrementer.component';
import { FilterDropdownComponent } from './filter-dropdown/filter-dropdown.component';
import { HighlightPipe } from './highlight/highlight.pipe';
import { FilterComponent } from './filter/filter.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { CheckPermissionDirective } from '../shared/components/security-settings/check-permission.directive';
import { NonMultipleClickDirective } from '../shared/directive/non-multiple-click.directive';
import { ModalComponent } from './modal/modal.component';
import { ModalOpen } from './modal/modal-open.component';
import { DraggableDirective } from './modal/draggable.directive';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { ImagePreloadDirective } from './image/image-preload.directive';
import { CopyToClipBoardComponent } from './copy-to-clipboard/copy-to-clipboard';
import { TextMaskModule } from 'angular2-text-mask';
import { CtrlClickDirective } from './link/ctrl-click.directive';
import { NameDisplayComponent } from './name-display/name-display.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { IsEllipsisActiveDirective } from '../shared/directive/is-ellipsis-active.directive';
import { TabbingClickDirective } from '../shared/directive/tabbing-click.directive';
import { AutoFocusDirective } from '../shared/directive/autofocus.directive';
import { MultiselectKeyboardAccessDirective } from '../shared/directive/multiselect-keyboard-access.directive';
import { DropdownToggleAccessibleDirective } from '../shared/directive/dropdown-toggle.directive';
import { FocusFirstDirective } from '../shared/directive/focus-first.directive';
import { FocusScrollDirective } from '../shared/directive/focus-scroll.directive';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    AngularSvgIconModule,
    TextMaskModule,
    AngularMultiSelectModule,
    A11yModule
  ],
  declarations: [
    TextComponent,
    IconComponent,
    ImageComponent,
    LabelComponent,
    LinkComponent,
    DropdownComponent,
    DatePickerComponent,
    InputCheckboxComponent,
    CardComponent,
    BreadcrumbComponent,
    ButtonComponent,
    InputComponent,
    UserInitialsComponent,
    ExportToExcelComponent,
    TitleBarComponent,
    HeaderComponent,
    FileUploadComponent,
    SpinnerComponent,
    EmptyViewComponent,
    TypeaheadComponent,
    SearchInputComponent,
    ControlMessagesComponent,
    BadgeComponent,
    DatagridComponent,
    SidebarComponent,
    StatusBarComponent,
    TabComponent,
    TabsComponent,
    TextAreaComponent,
    VerticalNavComponent,
    WizardComponent,
    WizardStepComponent,
    PaginationComponent,
    MenuItemsComponent,
    AlertComponent,
    Popover,
    PopoverContentComponent,
    SignpostComponent,
    SelectComponent,
    TimelineComponent,
    OrderBy,
    FilterDropdownComponent,
    NumberIncrementerComponent,
    HighlightPipe,
    FilterComponent,
    ModalComponent,
    ModalOpen,
    DraggableDirective,
    CheckPermissionDirective,
    NonMultipleClickDirective,
    SkeletonComponent,
    ImagePreloadDirective,
    CopyToClipBoardComponent,
    CtrlClickDirective,
    NameDisplayComponent,
    IsEllipsisActiveDirective,
    TabbingClickDirective,
    AutoFocusDirective,
    MultiselectKeyboardAccessDirective,
    DropdownToggleAccessibleDirective,
    FocusFirstDirective,
    FocusScrollDirective
    ],
  exports: [
    TextMaskModule,
    DropdownComponent,
    LabelComponent,
    LinkComponent,
    ImageComponent,
    TextComponent,
    InputCheckboxComponent,
    CardComponent,
    BreadcrumbComponent,
    ButtonComponent,
    InputComponent,
    UserInitialsComponent,
    ExportToExcelComponent,
    TitleBarComponent,
    HeaderComponent,
    DatePickerComponent,
    FileUploadComponent,
    SpinnerComponent,
    EmptyViewComponent,
    TypeaheadComponent,
    SearchInputComponent,
    ControlMessagesComponent,
    BadgeComponent,
    DatagridComponent,
    SidebarComponent,
    StatusBarComponent,
    TabComponent,
    TabsComponent,
    TextAreaComponent,
    VerticalNavComponent,
    WizardComponent,
    WizardStepComponent,
    PaginationComponent,
    MenuItemsComponent,
    AlertComponent,
    Popover,
    PopoverContentComponent,
    SignpostComponent,
    SelectComponent,
    TimelineComponent,
    OrderBy,
    FilterDropdownComponent,
    NumberIncrementerComponent,
    HighlightPipe,
    FilterComponent,
    ModalComponent,
    CheckPermissionDirective,
    NonMultipleClickDirective,
    SkeletonComponent,
    CopyToClipBoardComponent,
    CtrlClickDirective,
    NameDisplayComponent,
    AngularMultiSelectModule,
    IsEllipsisActiveDirective,
    TabbingClickDirective,
    AutoFocusDirective,
    MultiselectKeyboardAccessDirective,
    DropdownToggleAccessibleDirective,
    FocusFirstDirective,
    FocusScrollDirective
  ],
  providers: [
    PaginationService,
    AlertService,
    SelectService
  ],
  entryComponents: [
    PopoverContentComponent
  ],
})
export class LibraryModule { }
