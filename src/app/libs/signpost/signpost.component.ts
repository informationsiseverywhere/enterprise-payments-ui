import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ent-signpost',
  templateUrl: './signpost.component.html'
})
export class SignpostComponent implements OnInit {

  @Input() message: string = '';
  @Input() title: string;
  @Input() icon: boolean = true;
  @Input() imageUrl: string = 'assets/images/icons/question-mark1.png';
  @Input() iconClass: string = '';
  @Input() placement: string = 'right';
  @Input() popoverOnHover: boolean = false;
  @Input() closeOnClickOutside: boolean = true;
  @Input() name: string;
  @Input() popoverWidth = 'fit-content';
  ngOnInit(): void {
  }

}
