import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';

let colorIndex = 0;

@Component({
  selector: 'ent-user-initials',
  templateUrl: './user-initials.component.html'
})
export class UserInitialsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() name: string = '';
  @Input() randomBackgroundColor: boolean;
  index: number = 0;

  constructor() {
  }

  ngOnInit() { }

  ngOnDestroy() {
    colorIndex = 0;
  }

  ngOnChanges(changes: any) {
    if (this.randomBackgroundColor) {
      this.getRandomBackgroundColor();
    }
  }

  getFirstTwoLetters(name: string) {
    let nameSplitList = name.split(' ');
    return nameSplitList.length > 1 ? nameSplitList[0][0] +
      (nameSplitList[nameSplitList.length - 1][0] ? nameSplitList[nameSplitList.length - 1][0] :
        '') : nameSplitList[0][0];
  }

  getRandomBackgroundColor() {
    this.index = colorIndex++;
    if (colorIndex > 4) {
      colorIndex = 0;
    }
  }
}
