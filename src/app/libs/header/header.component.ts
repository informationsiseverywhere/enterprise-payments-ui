import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { AppConfiguration } from '../../shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { TabbingClickDirective } from 'src/app/shared/directive/tabbing-click.directive';

@Component({
  selector: 'ent-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  @Input() name: string;
  @Input() originalName: string;
  @Input() showBreadcrumb = true;
  @ViewChild('commonHeader', { static: false }) commonHeader: ElementRef;
  @ViewChild(TabbingClickDirective, { static: false }) tabbingClickDirective: TabbingClickDirective;

  returnAppId: string;
  partyId: any;
  cancelUrl: string;
  navParam: any;
  isSummary = false;
  landingPageLink: any;
  constructor(
    private route: ActivatedRoute, 
    private commonService: CommonService,
    public appConfiguration: AppConfiguration,
    public securityEngineService: SecurityEngineService) {

  }

  ngOnInit() {
    this.landingPageLink = this.appConfiguration.landingPageLink;
    this.route.parent.params.subscribe(
      params => {
        this.partyId = params.partyId;
      });
    this.route.parent.queryParams.subscribe(
      queryParams => {
        this.returnAppId = queryParams.returnAppId;
        if (this.landingPageLink[0].includes('client')) { this.isSummary = true; }
        this.navParam = queryParams.nav;
        const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
        const cancelUrl = (resultQueryParam ? resultQueryParam.cancelUrl : null);
        this.cancelUrl = cancelUrl;
      });
    this.route.queryParams.subscribe(
      queryParams => {
        if (this.appConfiguration.isComponentSecurity && queryParams.roleSequenceId) {
          this.securityEngineService.roleSequenceId = queryParams.roleSequenceId;
          this.securityEngineService.getRoleDetail(queryParams.roleSequenceId);
        }
      });
  }
  public goBack(): void {
    const target = this.navParam ? this.navParam : '_self';
    if (this.returnAppId === 'pointinj') {
      window.open(
        this.cancelUrl,
        '_self'
      );
    } else {
      this.commonService.navigateToUrl(this.cancelUrl, target);
    }
  }
  public clearRole(): void {
    this.commonService.clearSecurityConfig();
  }

  public mainContentFocused(): void{
    const commonHeaderEle = this.commonHeader.nativeElement;
    const exceptClass = this.showBreadcrumb ? null : 'breadcrumb-link';
    if(this.tabbingClickDirective?.getFocusableElements(commonHeaderEle, exceptClass)?.length > 0){
      this.tabbingClickDirective?.focusFirstItem(commonHeaderEle, exceptClass);
    }else{
      this.tabbingClickDirective?.focusFirstItem(commonHeaderEle.parentElement?.nextSibling);
    }
  }
}
