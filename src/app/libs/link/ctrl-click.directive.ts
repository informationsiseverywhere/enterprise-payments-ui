import { Directive, EventEmitter, ElementRef, HostListener, Output } from '@angular/core';

@Directive({ selector: '[entCtrlClick]' })
export class CtrlClickDirective {

  @Output() ctrlClick = new EventEmitter<MouseEvent>();

  constructor() { }

  @HostListener('click', ['$event'])
  public onDocumentClick(event: MouseEvent): void {
    this.ctrlClick.emit(event);
  }
}
