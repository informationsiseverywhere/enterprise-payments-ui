import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'ent-link',
  templateUrl: './link.component.html'
})
export class LinkComponent implements OnInit {
  @Input() linkText: string;
  @Input() linkUrl: any;
  @Input() href: string;
  @Input() queryParams: any;
  @Input() allowEdit = false;
  @Input() customLink = false;
  @Input() query: any;
  @Input() textHighlight = false;
  @Output() editClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() linkClickOutput: EventEmitter<any> = new EventEmitter<any>();
  @Input() title: any;
  @Input() ellipsis = false;
  @Input() isAutoFocus: boolean = false;
  
  constructor(
    private commonService: CommonService,
    @Inject(DOCUMENT) private document: any,
    private router: Router) { }

  ngOnInit() { }

  showEditMode() {
    this.editClick.emit();
  }

  linkClick() {
    this.linkClickOutput.emit();
    this.editClick.emit();
  }

  ctrlClick(event: MouseEvent) {
    if (event.ctrlKey) {
      console.log(this.document.location);
      let url = `${this.document.location.origin}${this.document.location.pathname}#`;
      url += this.linkUrl.join('/');
      if (this.queryParams) {
        for (const i in this.queryParams) {
          if (url.split('?').length > 1) {
            url += `&${i}=${this.queryParams[i]}`;
          } else {
            url += `?${i}=${this.queryParams[i]}`;
          }
        }
      }
      this.commonService.navigateToUrl(url, '_blank', true);
      console.log(url);
    } else {
      this.router.navigate(this.linkUrl, { queryParams: this.queryParams });
    }
  }
}
