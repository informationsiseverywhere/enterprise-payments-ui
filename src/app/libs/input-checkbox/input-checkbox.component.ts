import { Component, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, FormControl } from '@angular/forms';

let nextUniqueId = 0;

@Component({
    selector: 'ent-input-checkbox',
    templateUrl: './input-checkbox.component.html',
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => InputCheckboxComponent), multi: true },
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => InputCheckboxComponent), multi: true }
    ]
})
export class InputCheckboxComponent implements ControlValueAccessor {

    @Input() requiredField: string;
    @Input('value') _value: string;
    @Input() text: string;
    @Input() disabled: string;
    @Input() isAutoFocus: boolean = false;
    validateFn: any = () => { };
    @Output() valueChanged: EventEmitter<any> = new EventEmitter();
    _uniqueId: string = `checkbox-${++nextUniqueId}`;

    constructor() { }

    get value(): any { return this._value; };

    set value(v: any) {
        if (v !== this._value) {
            this._value = v;
            this.onChange(v);
        }
    }

    writeValue(val: any) {
        this._value = val;
        this.onChange(val);
    }

    validate(c: FormControl) {
        return this.validateFn(c);
    }

    onChange = (_) => { };
    onTouched = () => { };
    registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
    registerOnTouched(fn: () => void): void { this.onTouched = fn; }

    onInputChange($event) {
        this.valueChanged.emit($event);
    }
}
