export interface IInputOptions {
    maxValue: string;
    minValue: string;
    decimalPlaces: number;
    decimalSeparator : string;
    thousandSeparator : string;
}
