import { Component, forwardRef, Input, Output, EventEmitter, ElementRef, ViewChild, OnChanges, Inject, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, FormControl } from '@angular/forms';
import { DOCUMENT } from '@angular/common';

import { ValidationService } from './../../libs/form-validation-control/services/validation.service';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import { IInputOptions } from 'src/app/libs/input/interfaces/input-options.interface';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';

@Component({
  selector: 'ent-input',
  templateUrl: './input.component.html',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => InputComponent), multi: true },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => InputComponent), multi: true }
  ]
})
export class InputComponent implements ControlValueAccessor, OnInit, OnChanges {
  @Input() placeholder: string;
  @Input() fasName: string;
  @Input() decimalPlaces: number;
  @Input() focus: any;
  @ViewChild('input', { static: false }) elementRef: ElementRef;
  @ViewChild('numberInput', { static: false }) numberInput: ElementRef;
  @Input() disabledField: string;
  @Output() valueChanged: EventEmitter<any> = new EventEmitter();
  @Output() valueChangeBlur: EventEmitter<any> = new EventEmitter();
  @Output() valueChangeKey: EventEmitter<any> = new EventEmitter();
  @Output() clearInputValue: EventEmitter<any> = new EventEmitter();
  @Input() type = 'text';
  @Input() showClose = true;
  @Input() control: FormControl;
  @Input() icon;
  @Input() maxlength: number;
  @Input() maxValue: string;
  @Input() minValue: string;
  @Input() decimalSeparator: string;
  @Input() thousandSeparator: string;
  @Input() options: IInputOptions;
  public inputNumberTxt = '';
  private delDirection = false;
  validateFn: any = () => { };
  @Input('value') _value: any = '';
  @Input() isAutoFocus: boolean = false;

  constructor(@Inject(DOCUMENT) private document: any,
   public appConfig: AppConfiguration) { }

  ngOnInit() {
    if ((this.type === 'number' || this.type === 'negativeNumber') && ComponentConfiguration.settings.inputNumberOptions) {
      if (this.options) { this.setOptions(); }
      this.maxValue = this.maxValue ? this.maxValue : ComponentConfiguration.settings.inputNumberOptions.maxValue;
      this.minValue = this.minValue ? this.minValue : ComponentConfiguration.settings.inputNumberOptions.minValue;
      this.decimalPlaces = this.decimalPlaces ? this.decimalPlaces : ComponentConfiguration.settings.inputNumberOptions.decimalPlaces;
      this.decimalSeparator = this.decimalSeparator ? this.decimalSeparator : ComponentConfiguration.settings.inputNumberOptions.decimalSeparator;
      this.thousandSeparator = this.thousandSeparator ? this.thousandSeparator : ComponentConfiguration.settings.inputNumberOptions.thousandSeparator;
    }
    if (this.decimalPlaces && !this.decimalSeparator) { this.decimalSeparator = '.'; }
    if (this.thousandSeparator && this.decimalSeparator && this.thousandSeparator === this.decimalSeparator) { this.thousandSeparator = ''; }
  }

  ngOnChanges() {
    if (this.focus === 'true' || this.focus === true) {
      setTimeout(() => { // this will make the execution after the above boolean has changed
        this.elementRef.nativeElement.focus();
      }, 50);
    }
  }
  isError() {
    if (this.control) {
      return (this.control.invalid && this.control.touched && this.control.dirty) ? true : false;
    }
  }
  public get value(): any { return this._value; }

  public set value(v: any) {
    if (this.type === 'number' || this.type === 'negativeNumber') {
      if (v !== this._value) {
        this._value = this.getInputValue(v);
        this.onChange(this._value);
        this.inputNumberTxt = this.getInputNumberTxt(v);
      }
    } else {
      if (v !== this._value) {
        this._value = v;
        this.onChange(v);
      }
    }

  }

  public writeValue(val?: any): void {
    if ((this.type === 'negativeNumber') || (this.type === 'number')) {
      if (val || val == 0) { val = parseFloat(val).toFixed(this.decimalPlaces); }
      if (isNaN(val)) { val = ''; }
      this.inputNumberTxt = this.getInputNumberTxt(val);
    }
    if (val === null || val === undefined) { val = ''; }
    this._value = val;
    this.onChange(val);
  }

  validate(c?: FormControl) {
    return this.validateFn(c);
  }

  setDisabledState(isDisabled?: boolean) {

  }

  onChange = (_) => { };
  onTouched = () => { };
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  public onInputChange($event?: any): void {
    this.valueChanged.emit(this.getInputValue($event.target.value));
  }
  public onInputBlur($event?: any): void {
    this.valueChangeBlur.emit(this.getInputValue($event.target.value));
  }
  public onInputKey($event?: any): void {
    this.valueChangeKey.emit(this.getInputValue($event.target.value));
  }
  public clearValues($event?: any): void {
    this.value = '';
    event.stopPropagation();
    this.clearInputValue.emit(this.getInputValue($event.target.value));
    this.valueChanged.emit(this.getInputValue($event.target.value));
    if (this.type === 'number' || this.type === 'negativeNumber') {
      this.numberInput?.nativeElement?.focus();
    }else{
      this.elementRef?.nativeElement?.focus();
    }
    this.control.markAsTouched();
  }

  public inputKeyDown($event?: any) {
    if ($event.keyCode === 46) { this.delDirection = true; }
  }

  public focusOutHandling($event?: any): void {
    if (this._value) {
      let tempVal;
      if ($event && $event.target.value === '-') { this._value = ''; }
      this._value = this.getInputValue(this._value);
      tempVal = this._value;
      this.onChange(this._value);
      this.valueChanged.emit(this._value);
      if (tempVal === '0.') { tempVal = this._value; }
      this.inputNumberTxt = this.getInputNumberTxt(tempVal);
      this.numberInput.nativeElement.value = this.getInputNumberTxt(tempVal);
    } else {
      this.inputNumberTxt = '';
      this.numberInput.nativeElement.value = '';
    }
  }

  private insertText(val?: any, $el?: any, text?: any, focusLastPosition = true): void {
    let start = $el.selectionStart;
    const end = $el.selectionEnd;
    $el.value = val.slice(0, start) + text + val.slice(end);
    $el.focus();
    let caretPos = start + text.length;
    if (!focusLastPosition) { caretPos = start++; }
    $el.setSelectionRange(caretPos, caretPos);
  }


  public isKeyNumber($event?: any, $el?: any): boolean {
    // Fix firefox backspace, tab, ctrl combination
    if ($event.code === 'Backspace' || $event.code === 'Tab' || $event.ctrlKey) {
      return;
    }

    if (this.decimalPlaces == 0 && this.isDecimalSeparatorPress($event)) {
      return false;
    }

    let targetValue;
    let caratPos = 0;
    let dotPos = 0;

    if ((this.type === 'negativeNumber') || (this.type === 'number')) {
      if (this.thousandSeparator) {
        targetValue = $event.target.value.split(this.thousandSeparator).join('');
        caratPos = $el.selectionStart - ($event.target.value.substring(0, $el.selectionStart).split(this.thousandSeparator).length - 1);
        dotPos = $el.value.indexOf(this.decimalSeparator) - ($event.target.value.split(this.decimalSeparator)[0].split(this.thousandSeparator).length - 1);
      } else {
        targetValue = $event.target.value;
        caratPos = $el.selectionStart;
        dotPos = $el.value.indexOf(this.decimalSeparator);
      }
    } else {
      targetValue = $event.target.value;
    }

    const number = targetValue.split(this.decimalSeparator);

    if (this.type === 'number' || this.type === 'bank') {
      if (((!this.isDecimalSeparatorPress($event) || (this.isDecimalSeparatorPress($event) && targetValue === '')) ||
        targetValue.indexOf(this.decimalSeparator) != -1) && (!ValidationService.numberTypeValidator($event.key))) {
        if ((this.isDecimalSeparatorPress($event) && targetValue.indexOf(this.decimalSeparator) == -1) || (this.isDecimalSeparatorPress($event) && caratPos < dotPos && dotPos > -1)) {
          if (caratPos < dotPos && dotPos > -1) {
            this._value = '0' + this.decimalSeparator;
            this.insertText(targetValue, $el, '0' + this.decimalSeparator);
          } else {
            this._value = '0';
          }
          this.onChange(this._value);
          this.inputNumberTxt = this.getInputNumberTxt(this._value);
        } else {
          $event.preventDefault();
        }
      }
    }

    if (this.type === 'negativeNumber') {
      if ((($event.which != 45) || targetValue.indexOf('-') != -1) &&
        ((!this.isDecimalSeparatorPress($event) || (this.isDecimalSeparatorPress($event) && targetValue === '')) ||
          targetValue.indexOf(this.decimalSeparator) != -1) && (!ValidationService.numberTypeValidator($event.key))) {
        if ((this.isDecimalSeparatorPress($event) && targetValue.indexOf(this.decimalSeparator) == -1) || (this.isDecimalSeparatorPress($event) && caratPos < dotPos && dotPos > -1)) {
          if (caratPos < dotPos && dotPos > -1) {
            this._value = '0' + this.decimalSeparator;
            this.insertText(targetValue, $el, '0' + this.decimalSeparator);
          } else {
            this._value = '0';
          }
          this.onChange(this._value);
          this.inputNumberTxt = this.getInputNumberTxt(this._value);
        } else if (targetValue.indexOf('-') != -1 && $event.which == 45) {
          const tempVal = targetValue.replace('-', '');
          if (tempVal && this.isValueInRange(tempVal)) {
            this._value = tempVal;
            this.onChange(this._value);
            this.inputNumberTxt = this.getInputNumberTxt(this._value);
          } else {
            $event.preventDefault();
          }
        } else {
          $event.preventDefault();
        }
      } else {
        if ($event.which == 45 && targetValue.indexOf('-') == -1 && (!ValidationService.numberTypeValidator($event.key))) {
          const tempVal = '-' + targetValue;
          if ((tempVal && this.isValueInRange(tempVal)) || tempVal === '-') {
            targetValue = tempVal;
            if (tempVal != '-') {
              this._value = targetValue;
              this.onChange(targetValue);
              this.inputNumberTxt = this.getInputNumberTxt(this._value);
            } else {
              this.inputNumberTxt = tempVal;
              return;
            }
            $event.preventDefault();
          } else {
            $event.preventDefault();
          }
        }
      }
    }
    if ((this.type === 'negativeNumber') || (this.type === 'number')) {
      if (this.isDecimalSeparatorPress($event) && targetValue.indexOf(this.decimalSeparator) == -1) {
        if (caratPos == 0 && dotPos == -1) {
          this._value = '0';
          this.onChange(this._value);
          this.inputNumberTxt = this.getInputNumberTxt(this._value);
        }
      }
      if (!this.isDecimalSeparatorPress($event) && $event.which > 31 && ($event.which < 48 || $event.which > 57)) {
        return false;
      }
      if (number.length > 1 && this.isDecimalSeparatorPress($event)) {
        return false;
      }
      if (caratPos > dotPos && dotPos > -1 && (caratPos - dotPos) >= (this.decimalPlaces + 1) && (number[1].length > this.decimalPlaces - 1)) {
        return false;
      }
      return true;
    }
  }

  private isDecimalSeparatorPress(event?: any): boolean {
    return event.key === this.decimalSeparator ? true : false;
  }

  private isValueInRange(value?: any): boolean {
    value = this.getInputValue(value);
    const changedValue = parseFloat(value).toFixed(this.decimalPlaces);
    const maxValue = parseFloat(this.maxValue).toFixed(this.decimalPlaces);
    const minValue = parseFloat(this.minValue).toFixed(this.decimalPlaces);
    if (parseFloat(changedValue) <= parseFloat(maxValue) && parseFloat(changedValue) >= parseFloat(minValue)) {
      return true;
    }
    return false;
  }

  private setOptions(): void {
    if (this.options !== undefined) {
      Object.keys(this.options).forEach((k) => {
        if (!this[k]) { this[k] = this.options[k]; }
      });
    }
  }

  public numberInputChanged(value?: any): void {
    if (value === this._value) { return; }
    if (this.type === 'number' || this.type === 'negativeNumber') {
      if (value && !this.isNumeric(value)) {
        if (value === '-') { return; }
        this.numberInput.nativeElement.value = this.getInputNumberTxt(this.inputNumberTxt);
        return;
      }
      const currentSelectionStart = this.numberInput.nativeElement.selectionStart;
      let isDelDigit = false;
      if (this.thousandSeparator && this.inputNumberTxt.toString().charAt(currentSelectionStart) === this.thousandSeparator && this.delDirection == true) { isDelDigit = true; }
      if (value !== this._value) {
        if (value && !this.isValueInRange(value) && value != '-') {
          const parts = this.inputNumberTxt.toString().split(this.decimalSeparator);
          let currentTxt;
          if (parts.length > 1) {
            currentTxt = this.inputNumberTxt;
          } else {
            currentTxt = parts[0];
          }
          this.numberInput.nativeElement.value = this.getInputNumberTxt(currentTxt);
          this.numberInput.nativeElement.focus();
          this.numberInput.nativeElement.setSelectionRange(currentSelectionStart - 1, currentSelectionStart - 1);
          this.inputNumberTxt = currentTxt;
          return;
        } else {
          if (value.indexOf(this.decimalSeparator) != -1) {
            const number = value.split(this.decimalSeparator);
            if (number[1].length > this.decimalPlaces) {
              const caratPos = this.numberInput.nativeElement.selectionStart;
              const val = value.substr(0, caratPos) + value.substr(caratPos + 1);
              this.insertText('', this.numberInput.nativeElement, val, false);
              this._value = this.getInputValue(val);
              this.onChange(this._value);
              this.inputNumberTxt = this.getInputNumberTxt(val);
              return;
            }
          }
          let digitNum;
          if (this.thousandSeparator) {
            digitNum = value.substring(0, this.numberInput.nativeElement.selectionStart).split(this.thousandSeparator).join('').length;
          } else {
            digitNum = value.substring(0, this.numberInput.nativeElement.selectionStart).length;
          }

          let thousandSeparatorCount = 0;
          let digitCount = 0;
          const inputNumTxt = this.getInputNumberTxt(value);
          for (const c of inputNumTxt) {
            if (digitCount == digitNum) {
              break;
            } else if (this.thousandSeparator && c === this.thousandSeparator) {
              thousandSeparatorCount++;
            } else {
              digitCount++;
            }
          }
          this.numberInput.nativeElement.value = inputNumTxt;
          this.numberInput.nativeElement.focus();
          if (!isDelDigit) {
            this.numberInput.nativeElement.setSelectionRange(digitCount + thousandSeparatorCount, digitCount + thousandSeparatorCount);
          } else {
            this.numberInput.nativeElement.setSelectionRange(digitCount + thousandSeparatorCount + 1, digitCount + thousandSeparatorCount + 1);
          }
        }
      } else {
        if (this.inputNumberTxt === '-') { return; }
        this.numberInput.nativeElement.value = this.getInputNumberTxt(this.inputNumberTxt);
        this.numberInput.nativeElement.focus();
        if (!isDelDigit) {
          this.numberInput.nativeElement.setSelectionRange(currentSelectionStart, currentSelectionStart);
        } else {
          this.numberInput.nativeElement.setSelectionRange(currentSelectionStart + 1, currentSelectionStart + 1);
        }
      }
    }
    if (value !== this._value) {
      this._value = this.getInputValue(value);
      this.onChange(this._value);
      this.inputNumberTxt = this.getInputNumberTxt(value);
    }
    this.delDirection = false;
  }

  private getInputValue(value?: any): string {
    if (!value) { return ''; }
    if ((this.type === 'negativeNumber') || (this.type === 'number')) {
      let numericVal;
      if (this.thousandSeparator) {
        numericVal = value.split(this.thousandSeparator).join('');
      } else {
        numericVal = value;
      }
      return parseFloat(numericVal).toFixed(this.decimalPlaces);
    }
    return value;
  }

  private getInputNumberTxt(value?: any): string {
    if (!value) { return ''; }
    if ((this.type === 'negativeNumber') || (this.type === 'number')) {
      let numerictxt;
      if (this.thousandSeparator) {
        numerictxt = value.split(this.thousandSeparator).join('');
      } else {
        numerictxt = value;
      }
      const parts = numerictxt.toString().split(this.decimalSeparator);
      if (this.thousandSeparator) { parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, this.thousandSeparator); }
      return parts.join(this.decimalSeparator);
    }
    return value;
  }

  private isNumeric(value?: any): boolean {
    const numericPartern = /^[0-9]+$/;
    let tempVal;
    if (this.thousandSeparator) {
      tempVal = value.split(this.thousandSeparator).join('');
    } else {
      tempVal = value;
    }
    if (this.type === 'negativeNumber') { tempVal = tempVal.split('-').join(''); }
    tempVal = tempVal.split(this.decimalSeparator).join('');
    if (tempVal.match(numericPartern)) { return true; }
    return false;
  }

}
