import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ent-title-bar',
  templateUrl: './title-bar.component.html'
})
export class TitleBarComponent implements OnInit {
  @Input() titleText: string = '';
  @Input() prefixText: string = '';
  @Input() suffixText: string = '';
  @Input() link: any;
  @Input() params: any;
  @Input() iconUrl: any;
  @Input() dropdownMenuObject: any;
  @Output('optionSelected') optionSelected: any = new EventEmitter<any>();
  @Input() displayField: string;
  @Input() border: boolean;

  constructor() {
  }

  ngOnInit() {

  }

  reflectTitle(dropdownItem) {
    this.optionSelected.emit(dropdownItem);
  }
}
