import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ent-text',
  templateUrl: './text.component.html'
})
export class TextComponent implements OnInit {
  @Input() text: string;
  @Input() className: string;
  constructor() { }

  ngOnInit() { }
}
