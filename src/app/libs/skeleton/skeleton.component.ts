import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';

@Component({
    selector: 'ent-skeleton',
    templateUrl: './skeleton.component.html'
})
export class SkeletonComponent implements OnInit {
    @Input() loading: any;
    @Input() appearance: String;
    @Input() skeletonTemplate: TemplateRef<any>;
    @Input() securityName: any;

    constructor(public securityEngineService: SecurityEngineService,
        private appConfig: AppConfiguration) {}

    ngOnInit() {
        
    }

    public isLoadingCheck(): boolean{
        if(this.securityName && this.appConfig.isComponentSecurity){
            if(this.loading || this.loading === null){
                if(this.securityEngineService.checkVisibility(this.securityName, 'Show') || this.securityEngineService.checkVisibility(this.securityName, 'Disable')){ 
                    return true;
                }
                else if(!this.securityEngineService.isSecurityDataLoading){
                    return true;
                }
            }
        }else if(this.loading || this.loading === null){
            return true;
        }
        return false;
    }
}
