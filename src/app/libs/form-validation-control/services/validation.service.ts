import { FormGroup, AbstractControl } from '@angular/forms';
import * as moment from 'moment';

import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';

export class ValidationService {

  static getValidatorErrorMessage(validatorName: string, labelName: string, validatorValue?: any, amountValue?: number) {
    const config = {
      required: `${labelName} is required.`,
      invalidCreditCard: 'Please enter a valid credit card #.',
      invalidEmailAddress: 'Please enter a valid email address.',
      invalidPhoneNumber: 'Please enter a valid phone number.',
      invalidNumeric: 'Please enter a valid number',
      invalidPassword: 'Invalid password. Password must be at least 6 characters long, and contain a number.',
      minlength: `Minimum ${validatorValue.requiredLength} characters.`,
      maxlength: `Maximum ${validatorValue.requiredLength} characters.`,
      duplicated: 'Email already taken.',
      notDuplicated: 'Passwords entered do not match.',
      invalidDate: 'Please enter a valid date.',
      invalidCardExpireDate: 'Please enter a valid date.',
      mismatchedPhase: 'Bank Account Number not match',
      invalidUserId: 'Please enter a valid user id.',
      invalidGroupId: 'Please enter a valid group id.',
      invalidBatchId: 'Please enter a valid batch id',
      invalidEmployeeId: 'Please enter a valid employee id.',
      invalidApplicationName: 'Invalid application name.',
      invalidDateTime: 'Invalid format for activity time - Format:YYYY-MM-DD HH:MM:SS',
      invalidName: 'Please enter a valid name.',
      invalidSocialSecurity: 'Invalid social security number.',
      invalidFederalIdentification: 'Invalid federal identification number.',
      invalidPostalCode: 'Please enter a valid postal code.',
      invalidCCV: 'Invalid CCV Code Format',
      nonAmountZero: 'Please enter a valid amount',
      selectAmountValidation: `Transfer amount should be less than or equal to $${amountValue}.`,
      max: `Max value is ${validatorValue.max}`,
      min: `Min value is ${validatorValue.min}`,
      invalidAmount: 'Amount should be max 12 digit before the decimal and 2 positions after the decimal.',
      invalidPercentage: 'Maximum Percentage is 100 and 2 positions after the decimal.',
      validPaymentAmount: `Payment Amount must be greater than $${amountValue}.`,
      notSelected: `A valid ${labelName} should be selected from the list.`,
      lessOrEqualValue: `${labelName} must be less than or equal to ${validatorValue}`,
      greaterOrEqualValue: `${labelName} must be greater than or equal to ${validatorValue}`,
      minValue: `${labelName} can not be less than or equal to ${validatorValue}`,
      maxValue: `${labelName} can not be greater than or equal to ${validatorValue}`,
      zeroValue: `${labelName} must be greater than 0`,
      invalidlength: 'Maximum Length is 255.',
    };

    return config[validatorName];
  }

  static creditCardValidator(control: FormGroup) {
    // Visa, MasterCard, American Express, Diners Club, Discover, JCB
    // you get valid testing credit card from http://www.getcreditcardnumbers.com/
    if (control.value) {
      if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
        return null;
      } else {
        return { invalidCreditCard: true };
      }
    }
  }

  static emailValidator(control: FormGroup) {
    // RFC 2822 compliant regex
    if (control.value) {
      if (control.value.match(/[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/)) {
        return null;
      } else {
        return { invalidEmailAddress: true };
      }
    }
  }

  static appNameValidator(control: FormGroup) {
    // RFC 2822 compliant regex
    if (control.value) {
      if (control.value.match(/^[a-zA-Z][a-zA-Z0-9]*$/)) {
        return null;
      } else {
        return { invalidApplicationName: true };
      }
    }
  }

  static batchIdValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[A-Za-z0-9]*$/)) {
        return null;
      } else {
        return { invalidBatchId: true };
      }
    }
  }

  static userIdValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[^]*[a-zA-Z0-9_\.]+$/)) {
        return null;
      } else {
        return { invalidUserId: true };
      }
    }
  }
  static groupIdValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[ A-Za-z0-9]*$/)) {
        return null;
      } else {
        return { invalidGroupId: true };
      }
    }
  }

  static IdValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[a-zA-Z0-9-_]+$/)) {
        return null;
      } else {
        return { invalidId: true };
      }
    }
  }

  static phoneValidator(control: FormGroup) {
    // RFC 2822 compliant regex
    if (control.value) {
      if (control.value.match(/\\d{10}/)) {
        return null;
      } else {
        return { invalidPhoneNumber: true };
      }
    }
  }

  static numericValidator(control: FormGroup) {
    // RFC 2822 compliant regex
    if (control.value) {
      if (control.value.toString().match(/^[0-9]*$/)) {
        return null;
      } else {
        return { invalidNumeric: true };
      }
    }
  }

  static passwordValidator(control: FormGroup) {
    // {6,100}           - Assert password is between 6 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    if (control.value) {
      if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
        return null;
      } else {
        return { invalidPassword: true };
      }
    }
  }

  static confirmPasswordValidator(control: FormGroup) {
    // match Password
    if (control.value) {
      if (control.value === control.parent.value.password_fb) {
        return null;
      } else {
        return { notDuplicated: true };
      }
    }
  }

  static confirmBankAccountNumberValidator(control: FormGroup) {
    // match Bank Account
    if (control.value) {
      if (control.value.match(control.parent.value.bankAccount_fb)) {
        return null;
      } else {
        return { mismatchedPhase: true };
      }
    }
  }

  static dateValidator(control: FormGroup) {
    if (control.value) {
      if (moment(control.value, ComponentConfiguration.settings.datePickerOptions.parseDateFormat, true).isValid()
        || moment(control.value, ComponentConfiguration.settings.datePickerOptions.dateFormat, true).isValid()) {
        return null;
      } else {
        return { invalidDate: true };
      }
    }
  }

  static dateTimeValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01]) (0[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9]).([0-9][0-9][0-9])$/)) {
        return null;
      } else {
        return { invalidDateTime: true };
      }
    }
  }

  static cardExpireDateValidator(control: FormGroup) {
    const datePatern: any = /^(0[1-9]|1[0-2])\/?([0-9]{2}|[0-9]{2})$/;
    const pattern = /^(\d{2})(\d{2})$/;

    if (control.value) {
      if (control.value.match(pattern)) {
        control.setValue(control.value.replace(pattern, '$1/$2'));
      }
      if (control.value.match(datePatern)) {
        return null;
      } else {
        return { invalidCardExpireDate: true };
      }
    }
  }

  static nameValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[ A-Za-z_'`]*$/)) {
        return null;
      } else {
        return { invalidName: true };
      }
    }
  }

  static clientNameValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[ A-Za-z0-9_'`]*$/)) {
        return null;
      } else {
        return { invalidName: true };
      }
    }
  }

  static businessNameValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[a-zA-Z0-9#$%&'*+/=?^_ `.{|}~-][a-zA-Z0-9#$%&'*+/=?^_ `.{|}~-]*$/)) {
        return null;
      } else {
        return { invalidName: true };
      }
    }
  }

  static USAPhoneValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[(]\d\d\d[)]\d\d\d[-]\d\d\d\d$/)) {
        return null;
      } else {
        return { invalidPhoneNumber: true };
      }
    }
  }

  static socialSecurityValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^\d\d\d[-]\d\d[-]\d\d\d\d$/)) {
        return null;
      } else {
        return { invalidSocialSecurity: true };
      }
    }
  }

  static federalIdentificationValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^\d\d[-]\d\d\d\d\d\d\d$/)) {
        return null;
      } else {
        return { invalidFederalIdentification: true };
      }
    }
  }

  static employeeIdValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[a-zA-Z0-9]*$/)) {
        return null;
      } else {
        return { invalidEmployeeId: true };
      }
    }
  }

  static postalCodeValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/(^\d{5}$)|(^\d{5}-\d{4}$)/)) {
        return null;
      } else {
        return { invalidPostalCode: true };
      }
    }
  }

  static ccvCodeValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[0-9]{3}$/)) {
        return null;
      } else {
        return { invalidCCV: true };
      }
    }
  }

  static ccvCodeAmexValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.match(/^[0-9]{4}$/)) {
        return null;
      } else {
        return { invalidCCV: true };
      }
    }
  }

  static amountValidator(control: FormGroup) {
    if (control.value) {
      if (control.value.toString().match(/^\d{1,12}(\.\d{1,2}){0,1}$/)) {
        return null;
      } else {
        return { invalidAmount: true };
      }
    }
  }

  static percentageValidator(control: FormGroup) {
    if (control.value) {
      if (control.value > 100) {
        return { invalidPercentage: true };
      } else {
        if (control.value.toString().match(/^\d{1,12}(\.\d{1,2}){0,1}$/)) {
          return null;
        } else {
          return { invalidPercentage: true };
        }
      }
    }
  }

  static fieldLengthValidator(control: FormGroup) {
    if (control.value) {
      if (control.value > 255) {
        return { invalidlength: true };
      }
    }
  }

  static numberTypeValidator(key: AbstractControl) {
    if (key) {
      if (key.toString().match(/^\d{1,12}(\.\d{1,2}){0,1}$/)) {
        return true;
      } else {
        return false;
      }
    }
  }

  public static numberValidation(min: any = null, max: any = null, isMinEqual: boolean = false, isMaxEqual: boolean = false): any {
    if (min && max || min === 0 && max) {
      return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (parseFloat(control.value) <= parseFloat(min) && isMinEqual) {
          return { minValue: min };
        } else if (parseFloat(control.value) < parseFloat(min) && !isMinEqual) {
          return { greaterOrEqualValue: min };
        } else if (parseFloat(control.value) >= parseFloat(max) && isMaxEqual) {
          return { maxValue: max };
        } else if (parseFloat(control.value) > parseFloat(max) && !isMaxEqual) {
          return { lessOrEqualValue: max };
        } else if (parseFloat(control.value) === 0) {
          return { zeroValue: min };
        } else {
          return null;
        }
      };
    } else if (!min && max) {
      return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (parseFloat(control.value) >= parseFloat(max) && isMaxEqual) {
          return { maxValue: max };
        } else if (parseFloat(control.value) > parseFloat(max) && !isMaxEqual) {
          return { lessOrEqualValue: max };
        } else {
          return null;
        }
      };
    } else {
      return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (parseFloat(control.value) === 0) {
          return { zeroValue: min };
        } else if (parseFloat(control.value) <= parseFloat(min) && isMinEqual) {
          return { minValue: min };
        } else if (parseFloat(control.value) < parseFloat(min) && !isMinEqual) {
          return { greaterOrEqualValue: min };
        } else {
          return null;
        }
      };
    }
  }
}
