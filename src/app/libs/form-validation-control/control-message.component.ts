import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidationService } from './services/validation.service';

@Component({
  selector: 'ent-control-messages',
  templateUrl: './control-message.component.html'
})

export class ControlMessagesComponent {
  @Input() control: FormControl;
  @Input() message: string;
  @Input() labelName: string;
  @Input() amountValue: number;
  constructor() { }

  get errorMessage() {
    if (this.control) {
      for (const propertyName in this.control.errors) {
        if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched && this.control.dirty) {
          if (this.message) {
            return this.message;
          }
          return ValidationService.getValidatorErrorMessage(propertyName, this.labelName,
            this.control.errors[propertyName], this.amountValue);
        }
      }
    }
    return null;
  }
}
