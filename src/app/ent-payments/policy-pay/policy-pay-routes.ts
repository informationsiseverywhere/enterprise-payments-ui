import { Routes } from '@angular/router';

import { AuthGuard } from 'src/app/ent-auth/auth.guard';
import { NavigationComponent } from '../navigation/navigation.component';
import { PolicyPayComponent } from './components/policy-pay.component';
import { AlternatePaymentMethodComponent } from 'src/app/shared/components/alternate-payment-method/alternate-payment-method.component';


export const policyPayRoutes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
      {
        path: '',
        component: PolicyPayComponent
      },
      {
        path: 'add-new-payment',
        component: AlternatePaymentMethodComponent,
        data: {
          breadcrumb: 'Add New Payments',
          headerTitle: 'Add New Payments',
          screenName: 'epAddNewPaymentMethod'
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

