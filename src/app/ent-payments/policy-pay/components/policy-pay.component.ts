import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentReceivedComponent } from 'src/app/shared/components/payment-received/payment-received.component';
import { SelectAgentComponent } from 'src/app/shared/components/select-agent/select-agent.component';
import { PaymentModel } from '../../batch-payments/models/batch.model';
import { PaymentService } from '../../batch-payments/services/payments.service';
import { CurrencyPipe } from '@angular/common';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from '../../security-config/security-config.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { IconName, PropertyName } from 'src/app/shared/enum/common.enum';
import { PaymentusService } from 'src/app/shared/services/paymentus.service';
import { PaymentusTokenModel } from 'src/app/shared/models/paymentus.model';

@Component({
  selector: 'app-policy-pay',
  templateUrl: './policy-pay.component.html',
  styleUrls: ['./policy-pay.component.scss'],
  providers: [CurrencyPipe]
})
export class PolicyPayComponent implements OnInit {
  public propertyName = PropertyName;
  public iconName = IconName;
  public accountId: any;
  public accountBalanceDetail: any;
  public isCurrentDue = false;
  public isAccountBalance = false;
  public isOtherAmount = false;
  public isPaymentMethod = PropertyName.inprogress;
  public isSelectAmount = PropertyName.active;
  public isConfirm = PropertyName.inprogress;
  public amount: any;
  public payment: PaymentModel = new PaymentModel();
  public isBankIndex: any;
  public accountDetails: any;
  public storedAccountDetails: any;
  public accountNumber: any;
  public payorAddress: any;
  public imageId: any;
  public payorName: any;
  public isEdit = false;
  public headerName = PropertyName.ChooseAmount;
  public returnAppId: any;
  public paymentType: any = PropertyName.CP;
  public agent: any;
  private identifier: any;
  public isAgencySweep: any;
  private agencySweepDropdown: any;
  public isAgencySweepPayorChange: any;
  private agencySweepPayorChangeDropdown: any;
  private supportData: any;
  public billingType: any;
  public isAccountDetailLoaded = false;
  public isPolicyDetailLoaded = false;
  public isAccountBalanceDetailLoaded = false;
  public isAccountBalanceDetailError = false;
  public agencies: any;
  bankProfileAttributeCheck: any;
  imageUrl: any;
  currency: any;
  private addNewPayment: any;
  private clientId: any;
  public type = PropertyName.policy;
  private isOtherAmountEntered = false;
  private isCustomerProfileId: boolean;
  policyId: any;
  polEffDt: any;
  policyDetails: any;
  policyNumber: any;
  public isViewAllProfilesClicked = false;
  public isAgencySweepBGRPOverride: any;
  private agencySweepBGRPOverrideDropdown: any;
  public isBusinessGroup: any;
  private businessGroupDropdown: any;
  public businessGroup: any;
  public paymentProfileList: any;
  public isShowPaymentMethodButton = true;
  public isShowAccountDetailSection = true;
  public isPaymentOptions = true;
  userAgentDropdown: any;
  isUserAgent: any;
  navParam: any;
  cancelUrl: any;
  public oneTimePaymentIntegrationDropdown: any;
  public oneTimePaymentIntegration = false;
  paymentProviderDropdown: any;
  paymentProvider: any;
  agencySweepPaymentIntegrationDropdown: any;
  agencySweepPaymentIntegration = false;
  @ViewChild('paymentReceivedModel', { static: false }) paymentReceivedModel: ModalComponent;
  @ViewChild('selectAgentModal', { static: false }) selectAgentModal: ModalComponent;
  @ViewChild('header', { static: false }) header: any;
  summaryData: any;
  public splitIndicator: any;

  constructor(
    private router: Router,
    private loadingService: LoadingService,
    private route: ActivatedRoute,
    public appConfig: AppConfiguration,
    private paymentService: PaymentService,
    private paymentusService: PaymentusService,
    private alertService: AlertService,
    private currencyPipe: CurrencyPipe,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    public commonService: CommonService) {
    this.imageUrl = IconName.userProfilePath;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        if (queryParams.userId) {
          this.onSuccessNavigate(PropertyName.cancel);
        } else {
          this.returnAppId = queryParams.returnAppId;
          this.billingType = queryParams.billingType;
          this.isCustomerProfileId = queryParams.isCustomerProfileId;
          this.navParam = queryParams.nav;
          const resultQueryParam = JSON.parse(sessionStorage.getItem(PropertyName.resultQueryParam));
          const cancelUrl = (resultQueryParam ? resultQueryParam.cancelUrl : null);
          this.cancelUrl = cancelUrl;
          if (this.paymentService.accountId === queryParams.accountId) {
            this.accountId = queryParams.accountId;
            this.policyId = queryParams.policyId;
            this.polEffDt = queryParams.polEffDt;
            this.paymentService.accountId = this.accountId;
          } else {
            this.accountId = queryParams.accountId;
            this.paymentService.accountId = this.accountId;
            this.policyId = queryParams.policyId;
            this.polEffDt = queryParams.polEffDt;
            this.paymentService.accountBalanceDetail = undefined;
            this.paymentService.accountDetails = undefined;
            this.paymentService.storedAccountDetails = undefined;
          }
          this.getAgencySweepPaymentSupportData(this.accountId);
        }
      });
    this.getPartiesSupportdata();
  }

  private getAgencySweepPaymentSupportData(accountId?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getAgencySweepPaymentSupportData(accountId).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.agencySweep);
          this.getDropdownItems(PropertyName.UserAgent);
          this.getDropdownItems(PropertyName.AgencySweepPayorChange);
          this.getDropdownItems(PropertyName.AgencySweepBGRPOverride);
          this.getDropdownItems(PropertyName.BusinessGroup);
          this.getDropdownItems(PropertyName.oneTimePaymentIntegration);
          this.getDropdownItems(PropertyName.agencySweepPaymentIntegration);
          this.getDropdownItems(PropertyName.paymentProvider);
          this.checkData();
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    this.agencySweepDropdown && (this.isAgencySweep = this.agencySweepDropdown[0]);
    this.userAgentDropdown && (this.isUserAgent = this.userAgentDropdown[0]);
    this.agencySweepPayorChangeDropdown && (this.isAgencySweepPayorChange = this.agencySweepPayorChangeDropdown[0]);
    this.agencySweepBGRPOverrideDropdown && (this.isAgencySweepBGRPOverride = this.agencySweepBGRPOverrideDropdown[0]);
    this.businessGroupDropdown && (this.isBusinessGroup = this.businessGroupDropdown[0]);
    let oneTimePaymentIntegration = 'false';
    this.oneTimePaymentIntegrationDropdown && (oneTimePaymentIntegration = this.oneTimePaymentIntegrationDropdown[0]);
    let agencySweepPaymentIntegration = 'false';
    this.agencySweepPaymentIntegrationDropdown && (agencySweepPaymentIntegration = this.agencySweepPaymentIntegrationDropdown[0]);
    this.paymentProviderDropdown && (this.paymentProvider = this.paymentProviderDropdown[0]);
    this.oneTimePaymentIntegration = false;
    this.agencySweepPaymentIntegration = false;
    if (oneTimePaymentIntegration === 'true') {
      this.oneTimePaymentIntegration = true;
    }
    if (agencySweepPaymentIntegration === 'true') {
      this.agencySweepPaymentIntegration = true;
    }
  }
  private checkData(): void {
    this.getAccountDetails(this.accountId, this.type);
    this.getPolicyAccountDetail(this.accountId, this.policyId, this.polEffDt);
    this.getPolicyBalanceDetails(this.accountId, this.policyId, this.polEffDt, this.type);
    if (this.paymentService.newPaymentDetails) {
      this.addNewPayment = this.paymentService.newPaymentDetails;
      this.payment.amount = this.paymentService.amount;
      if (this.isCustomerProfileId) {
        this.makePaymentLoop(PropertyName.isPaymentMethod, true);
      } else {
        this.makePaymentLoop(PropertyName.isSelectAmount, true);
      }
    } else {
      if (this.payment && this.isSelectAmount !== PropertyName.active) {
        this.makePaymentLoop(PropertyName.isPaymentMethod, this.isPaymentMethod === PropertyName.active ? false : true);
      } else {
        this.makePaymentLoop(PropertyName.isSelectAmount, false);
      }
    }
  }
  getPolicyAccountDetail(accountId: string, policyId: any, policyEffectiveDate: any) {
    this.isPolicyDetailLoaded = false;
    if (this.paymentService.policyDetails) {
      this.policyDetails = this.paymentService.policyDetails;
      this.policyNumber = this.policyDetails.policyNumber;
      this.isPolicyDetailLoaded = true;
    } else {
      this.paymentService.getPolicyAccountDetail(accountId, policyId, policyEffectiveDate).subscribe(
        data => {
          this.isPolicyDetailLoaded = true;
          this.policyDetails = data;
          this.paymentService.policyDetails = data;
          this.policyNumber = this.policyDetails.policyNumber;
          this.splitIndicator = this.policyDetails?.splitIndicator;
        },
        (err) => {
          this.commonService.handleErrorAPIResponse(this, 'isPolicyDetailLoaded', 'policyDetails', err);
        });
    }
  }
  private getPolicyBalanceDetails(accountId: any, policyId: any, polEffDt: any, type: any): void {
    this.isAccountBalanceDetailLoaded = false;
    this.isAccountBalanceDetailError = false;
    if (this.paymentService.accountBalanceDetail) {
      this.accountBalanceDetail = this.paymentService.accountBalanceDetail;
      this.isAccountBalanceDetailLoaded = true;
      if (!this.paymentService.newPaymentDetails) {
        this.checkSelectAmount(this.accountBalanceDetail);
      }
      this.checkSecurityOptions();
    } else {
      this.accountBalanceDetail = {};
      this.paymentService.getPolicyBalanceDetails(accountId, policyId, polEffDt, type).subscribe(
        data => {
          this.isAccountBalanceDetailLoaded = true;
          this.accountBalanceDetail = data;
          this.paymentService.accountBalanceDetail = data;
          if (!this.paymentService.newPaymentDetails) {
            this.checkSelectAmount(data);
          }
          this.checkSecurityOptions();
        },
        (err) => {
          this.isAccountBalanceDetailError = true;
          this.commonService.handleErrorAPIResponse(this, 'isAccountBalanceDetailLoaded', 'accountBalanceDetail', err);
        });
    }
  }
  private checkSelectAmount(data?: any): void {
    if (data.currentDue === 0 && data.pastDue === 0) {
      this.isCurrentDue = false;
      if (data.total === 0) {
        this.isAccountBalance = false;
        this.isOtherAmount = true;
        this.selectAmount(null, PropertyName.isOtherAmount);
      } else {
        this.isAccountBalance = true;
        this.selectAmount(data.total, PropertyName.isAccountBalance);
      }
    } else {
      const currentDate = data.currentDue + data.pastDue;
      this.selectAmount(currentDate, PropertyName.isCurrentDue);
      this.isCurrentDue = true;
    }
  }
  public selectAmount(value?: any, radioCheck?: any): void {
    if (radioCheck !== PropertyName.isOtherAmount) { this.payment.amount = null; }
    if (radioCheck === PropertyName.isCurrentDue) {
      this[radioCheck] = true;
      this.isAccountBalance = false;
      this.isOtherAmount = false;
      this.payment.amount = value;
      this.isOtherAmountEntered = false;
    } else if (radioCheck === PropertyName.isAccountBalance) {
      this[radioCheck] = true;
      this.isCurrentDue = false;
      this.isOtherAmount = false;
      this.payment.amount = value;
      this.isOtherAmountEntered = false;
    } else if (radioCheck === PropertyName.isOtherAmount) {
      this[radioCheck] = true;
      this.isAccountBalance = false;
      this.isCurrentDue = false;
      if (!this.isOtherAmountEntered) {
        this.payment.amount = 1;
        this.isOtherAmountEntered = true;
      }
    }
  }
  private getAccountDetails(accountId?: any, type?: any): void {
    this.isAccountDetailLoaded = false;
    if (this.paymentService.accountDetails) {
      this.accountDetails = this.paymentService.accountDetails;
      this.clientId = this.accountDetails.payorId;
      this.payorName = this.accountDetails ? this.accountDetails.payorName : '';
      this.accountNumber = this.accountDetails ? this.accountDetails.accountNumber : '';
      this.payorAddress = this.accountDetails ? this.accountDetails.payorAddress : '';
      this.imageId = this.accountDetails ? this.accountDetails.imageId : undefined;
      this.businessGroup = this.accountDetails ? this.accountDetails.businessGroup : undefined;
      const agencies = this.accountDetails ? this.accountDetails.agencies : undefined;
      if (agencies) {
        this.getUserAgencies(agencies);
      }
      this.currency = this.accountDetails ? this.accountDetails.currency : undefined;
      this.identifier = this.accountNumber;
      this.isAccountDetailLoaded = true;
      this.getStoredCard(this.clientId);
      this.checkSecurityPaymentType();
      if ((this.paymentProvider === PropertyName.paymentus && this.oneTimePaymentIntegration)) {
        this.getSummary(this.clientId);
      }
    } else {
      this.paymentService.getAccountDetails(accountId, type).subscribe(
        (data: any) => {
          this.accountDetails = data ? data.content[0] : null;
          this.paymentService.accountDetails = this.accountDetails;
          this.clientId = this.accountDetails.payorId;
          this.payorName = this.accountDetails ? this.accountDetails.payorName : '';
          this.accountNumber = this.accountDetails ? this.accountDetails.accountNumber : '';
          this.payorAddress = this.accountDetails ? this.accountDetails.payorAddress : '';
          this.imageId = this.accountDetails ? this.accountDetails.imageId : undefined;
          this.businessGroup = this.accountDetails ? this.accountDetails.businessGroup : undefined;
          const agencies = this.accountDetails ? this.accountDetails.agencies : undefined;
          if (agencies) {
            this.getUserAgencies(agencies);
          }
          this.currency = this.accountDetails ? this.accountDetails.currency : undefined;
          this.identifier = this.accountNumber;
          this.isAccountDetailLoaded = true;
          this.getStoredCard(this.clientId);
          this.checkSecurityPaymentType();
          if ((this.paymentProvider === PropertyName.paymentus && this.oneTimePaymentIntegration)) {
            this.getSummary(this.clientId);
          }
        },
        (err: any) => {
          this.commonService.handleErrorAPIResponse(this, 'isAccountDetailLoaded', 'accountDetails', err);
        });
    }
  }

  getUserAgencies(agencies: any) {
    this.isAccountDetailLoaded = false;
    let QUERYPARAMS = '';
    for (const agency of agencies) {
      QUERYPARAMS += 'agentNumber:' + agency + ',';
    }
    this.paymentService.getUserAgencies(QUERYPARAMS, this.isUserAgent).subscribe(
      (data: any) => {
        this.agencies = data ? data.content : null;
        this.isAccountDetailLoaded = true;
      },
      () => {
        this.isAccountDetailLoaded = true;
      });
  }

  private getStoredCard(clientId?: any): void {
    if (this.paymentService.storedAccountDetails) {
      this.storedAccountDetails = this.paymentService.storedAccountDetails;
      if (this.paymentService.newPaymentDetails) {
        if (!this.storedAccountDetails) {
          this.storedAccountDetails = {
            paymentProfiles: []
          };
        }
        this.selectAddNewBank(this.paymentService.newPaymentDetails);
      }
    } else {
      if (this.oneTimePaymentIntegration) {
        this.paymentService.getStoredCard(clientId).subscribe(
          (data: any) => {
            this.storedAccountDetails = data;
            this.paymentService.storedAccountDetails = data;
            if (this.addNewPayment) {
              if (!data) {
                this.storedAccountDetails = {
                  paymentProfiles: []
                };
              }
              this.selectAddNewBank(this.addNewPayment);
            } else {
              if (data) {
                this.selectBank(data.paymentProfiles[0], 0);
              }
            }
          },
          () => {
            this.storedAccountDetails = null;
            this.paymentService.storedAccountDetails = this.storedAccountDetails;
            if (this.addNewPayment) {
              if (!this.storedAccountDetails) {
                this.storedAccountDetails = {
                  paymentProfiles: []
                };
              }
              this.selectAddNewBank(this.addNewPayment);
            }
          });
      } else {
        this.storedAccountDetails = null;
        this.paymentService.storedAccountDetails = this.storedAccountDetails;
        if (this.paymentService.newPaymentDetails) { this.addNewPayment = this.paymentService.newPaymentDetails; }
        if (this.addNewPayment) {
          if (!this.storedAccountDetails) {
            this.storedAccountDetails = {
              paymentProfiles: []
            };
          }
          this.selectAddNewBank(this.addNewPayment);
        }

      }
    }
  }

  private selectAddNewBank(data?: any): void {
    if (data.paymentMethod === PropertyName.BankAccount) {
      this.storedAccountDetails.paymentProfiles.push({
        name: data.fullName,
        bankAccount: {
          accountType: PropertyName.saving,
          routingNumber: data.routingNumber,
          accountNumber: data.accountNumber,
          nameOnAccount: data.fullName
        }
      });
    } else if (data.paymentMethod === PropertyName.CreditCard) {
      this.storedAccountDetails.paymentProfiles.push({
        name: data.fullName,
        creditCard: {
          cardType: data.creditCardType,
          cardNumber: data.creditCardNumber
        }
      });
    }
    const index = this.storedAccountDetails.paymentProfiles.length - 1;
    this.selectBank(this.storedAccountDetails.paymentProfiles[index], index);
  }

  public selectBank(data?: any, index?: any): void {
    this.isBankIndex = index;
    const paymentMethod = this.isObject(data);
    this.payment.address = data && data.address ? data.address : null;
    this.payment.paymentProfileId = data && data.paymentProfileId ? data.paymentProfileId : null;
    this.payment.customerProfileId = this.storedAccountDetails.customerProfileId;
    if (paymentMethod === PropertyName.bankAccount) {
      this.payment.paymentMethod = PropertyName.BankAccount;
      this.payment.fullName = data.name;
      this.payment.accountType = data.bankAccount.accountType;
      this.payment.accountNumber = data.bankAccount.accountNumber;
      this.payment.routingNumber = data.bankAccount.routingNumber;
      this.payment.attributeValue = data.attributeValue;
    } else if (paymentMethod === PropertyName.creditCard) {
      this.payment.paymentMethod = PropertyName.CreditCard;
      this.payment.fullName = data.name;
      this.payment.creditCardType = data.creditCard.cardType;
      this.payment.creditCardNumber = data.creditCard.cardNumber;
      this.payment.creditCardExpiryDate = data.creditCard.expirationDate;
      this.payment.attributeValue = data.attributeValue;
    } else if (paymentMethod === PropertyName.digitalWallet) {
      this.payment.paymentMethod = data?.digitalWallet?.walletType;
      this.payment.fullName = data.name;
      this.payment.walletType = data?.digitalWallet?.walletType;
      this.payment.walletNumber = data.digitalWallet.walletNumber;
      this.payment.walletIdentifier = data.digitalWallet?.walletIdentifier;
      this.payment.attributeValue = data.attributeValue;
    }
  }

  public isObject(value?: any): string {
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.creditCard)) {
      return PropertyName.creditCard;
    }
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.bankAccount)) {
      return PropertyName.bankAccount;
    }
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.digitalWallet)) {
      return PropertyName.digitalWallet;
    }
  }

  public navigateToAddPayment(): void {
    if (!this.storedAccountDetails) {
      this.paymentService.amount = this.payment.amount;
      if (this.paymentType === PropertyName.CP) {
        if ((this.paymentProvider === PropertyName.authorizeNet && this.oneTimePaymentIntegration) ||
          ((this.paymentProvider === '' || this.paymentProvider === PropertyName.authorizeNet) && !this.oneTimePaymentIntegration) ||
          ((this.paymentProvider === PropertyName.authorizeNet || this.paymentProvider === PropertyName.paymentus)
            && !this.oneTimePaymentIntegration)) {
          this.router.navigate(['/policy-payment', 'add-new-payment'], {
            queryParams: {
              accountId: this.accountId,
              type: this.type,
              policyId: this.policyId,
              polEffDt: this.polEffDt,
              isCustomerProfileId: false,
              returnAppId: this.returnAppId,
              billingType: this.billingType
            }
          });
        } else if ((this.paymentProvider === PropertyName.paymentus && this.oneTimePaymentIntegration)) {
          // this.paymentusFlow();
          this.storedAccountDetails = {
            paymentProfiles: []
          };
          this.isConfirm = PropertyName.noProfilesFound;
          this.makePaymentLoop(PropertyName.isSelectAmount, true);
        }
      } else {
        if ((this.paymentProvider === PropertyName.authorizeNet && this.agencySweepPaymentIntegration) ||
          ((this.paymentProvider === '' || this.paymentProvider === PropertyName.authorizeNet) && !this.agencySweepPaymentIntegration) ||
          ((this.paymentProvider === PropertyName.authorizeNet || this.paymentProvider === PropertyName.paymentus)
            && !this.agencySweepPaymentIntegration)) {
          this.makePaymentLoop(PropertyName.isSelectAmount, true);
        } else if ((this.paymentProvider === PropertyName.paymentus && this.agencySweepPaymentIntegration)) {
          // this.paymentusFlow();
          this.storedAccountDetails = {
            paymentProfiles: []
          };
          this.isConfirm = PropertyName.noProfilesFound;
          this.makePaymentLoop(PropertyName.isSelectAmount, true);
        }
      }
    }
  }

  private getSummary(partyId?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getSummary(partyId).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.summaryData = data;
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private generateToken(): void {
    const payload: PaymentusTokenModel = new PaymentusTokenModel({
      accountNumber: this.accountNumber,
      amount: this.payment.amount,
      firstName: this.summaryData?.details?.name?.given,
      lastName: this.summaryData?.details?.name?.family,
      iframe: false,
      emailId: this.accountDetails?.emailId,
      phoneNumber: this.accountDetails?.phoneNumber,
      zipCode: this.accountDetails?.payorAddress?.slice(this.accountDetails?.payorAddress?.length - 5)
    })
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentusService.generateSecureToken(payload).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        setTimeout(() => {
          this.printStatement(PropertyName.cancel);
        }, 100);
        const url = this.paymentusService.getSecureTokenizationWebUrl(data?.token);
        window.open(
          url,
          PropertyName.blank
        );
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  paymentusFlow() {
    this.generateToken();
  }

  public makePaymentLoop(value?: any, activeClass?: any): void {
    this.paymentService.amount = this.payment.amount;
    if (value === PropertyName.isSelectAmount && activeClass) {
      this[value] = PropertyName.complete;
      this.isPaymentMethod = PropertyName.active;
      if (this.isConfirm !== PropertyName.noProfilesFound) { this.isConfirm = PropertyName.inprogress; }
      this.headerName = PropertyName.PaymentMethod;
    } else if (value === PropertyName.isSelectAmount && !activeClass) {
      this[value] = PropertyName.active;
      this.isPaymentMethod = PropertyName.inprogress;
      this.isConfirm = PropertyName.inprogress;
      this.headerName = PropertyName.ChooseAmount;
    } else if (value === PropertyName.isPaymentMethod && activeClass) {
      this[value] = PropertyName.complete;
      this.isConfirm = PropertyName.active;
      this.isSelectAmount = PropertyName.complete;
      this.headerName = PropertyName.ConfirmPay;
    }
  }

  public makePay(): void {
    if (this.addNewPayment) {
      this.makePayWithNewPayments();
    } else {
      this.makePayWithStoredCards();
    }
  }

  private makePayWithStoredCards(): void {
    const payload: any = {
      customerProfileId: this.payment.customerProfileId,
      paymentProfileId: this.payment.paymentProfileId,
      amount: this.payment.amount
    };
    if (this.paymentProvider === PropertyName.paymentus) {
      payload.accountNumber = this.accountNumber
    }
    if (this.oneTimePaymentIntegration) {
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.makePayment(payload).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.callToEnterprisePaymentsAPI(data, this.payment);
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.reset(true, null);
        });
    } else {
      this.callToEnterprisePaymentsAPI(null, this.payment);
    }
  }

  private makePayWithNewPayments(): void {
    const payment: any = {
      paymentNonce: this.addNewPayment.paymentNonce,
      amount: this.payment.amount,
      customer: {
        firstName: '',
        lastName: this.addNewPayment.fullName,
        merchantcustomerid: this.clientId
      },
      storeCard: this.addNewPayment.paymentMethod === PropertyName.BankAccount ? this.addNewPayment.storeAccount : this.addNewPayment.storeCard
    };
    if (this.storedAccountDetails) {
      payment.customerProfileId = this.storedAccountDetails.customerProfileId;
    }
    if (this.oneTimePaymentIntegration) {
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.makeNewPayment(payment).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.callToEnterprisePaymentsAPI(data, this.addNewPayment);
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.reset(true, null);
        });
    } else {
      this.callToEnterprisePaymentsAPI(null, this.addNewPayment);
    }
  }
  private callToEnterprisePaymentsAPI(responseData?: any, payment?: any): any {
    const requestBody: any = {
      paymentAmount: this.payment.amount,
      policyNumber: this.policyDetails.policyNumber,
      policySymbol: this.policyDetails.lineOfBusiness
    };
    if (responseData) {
      requestBody.paymentComments = responseData.transactionMessage;
      requestBody.paymentId = responseData.transactionId;
      requestBody.transactionId = responseData.paymentId;
      if (this.splitIndicator) {
        requestBody.identifier = this.accountNumber;
        requestBody.identifierType = PropertyName.ConsumerDirect;
      }
      if (payment.paymentMethod === PropertyName.BankAccount) {
        requestBody.bankAccountNumber = responseData.accountNumber ? responseData?.accountNumber : payment?.accountNumber;
        requestBody.authorizationResponseType = responseData.avsCode;
      } else {
        requestBody.authorizationResponse = responseData.responseCode;
        requestBody.authorization = responseData.authCode;
        requestBody.authorizationResponseType = responseData.avsCode + responseData.cvvCode + responseData.cavvCode;
        requestBody.creditCardNumber = responseData.accountNumber ? responseData.accountNumber : payment.creditCardNumber;
        requestBody.creditCardType = responseData.accountType ? responseData.accountType : payment.creditCardType;
        if (payment.paymentMethod !== (PropertyName.Paypal && PropertyName.PaypalCredit)) {
          requestBody.creditCardExpiryDate = PropertyName.XXXX;
        }
      }
    } else {
      requestBody.bankAccountNumber = payment.accountNumber;
    }
    if (payment.paymentMethod === PropertyName.BankAccount) {
      requestBody.paymentMethod = PropertyName.BankAccount;
      requestBody.accountType = payment.accountType;
      requestBody.paymentType = (this.oneTimePaymentIntegration) ? PropertyName.Echeck : PropertyName.WebsiteCheck1xACH;
      requestBody.routingNumber = payment.routingNumber;
    } else {
      requestBody.paymentMethod = PropertyName.CreditCard;
      requestBody.paymentType = PropertyName.CreditCardPayment;
      if (!responseData) {
        requestBody.creditCardNumber = payment.creditCardNumber;
        requestBody.creditCardType = payment.creditCardType;
        requestBody.creditCardExpiryDate = this.commonService.removeSpacesAndFWDash(payment.creditCardExpiryDate);
      }
    }
    if (payment.paymentMethod === PropertyName.Paypal) {
      requestBody.paymentMethod = PropertyName.Paypal;
      requestBody.walletNumber = payment.walletNumber;
      requestBody.paymentType = PropertyName.Paypal;
      requestBody.walletType = PropertyName.Paypal;
      requestBody.walletIdentifier = payment.walletIdentifier;
    }
    if (payment.paymentMethod === PropertyName.PaypalCredit) {
      requestBody.paymentMethod = PropertyName.PaypalCredit;
      requestBody.walletNumber = payment.walletNumber;
      requestBody.paymentType = PropertyName.PaypalCredit;
      requestBody.walletType = PropertyName.PaypalCredit;
      requestBody.walletIdentifier = payment.walletIdentifier;
    }
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.callToEnterprisePaymentsAPI(requestBody).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentCreatedResponse: any = data;
        let paymentDetailUrl = '';
        if (paymentCreatedResponse.links && paymentCreatedResponse.links.length > 0) {
          paymentDetailUrl = paymentCreatedResponse.links[0].href;
          this.getPaymentDetail(paymentDetailUrl, requestBody);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset(true, null);
      });
  }

  private getPaymentDetail(url?: any, requestBody?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentDetailByUrl(url).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentDetailInfo: any = data;
        requestBody.paymentSequenceNumber = paymentDetailInfo.paymentSequenceNumber;
        requestBody.batchId = paymentDetailInfo.batchId;
        requestBody.postedDate = paymentDetailInfo.postedDate;
        requestBody.userId = paymentDetailInfo.userId;
        this.reset(true, requestBody);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset(true, null);
      });
  }

  private reset(status?: any, requestBody?: any): void {
    if (status) {
      this.isPaymentMethod = PropertyName.inprogress;
      this.isSelectAmount = PropertyName.active;
      this.isConfirm = PropertyName.inprogress;
      this.isBankIndex = -1;
      this.payment = new PaymentModel();
      this.isCurrentDue = false;
      this.isAccountBalance = false;
      this.isOtherAmount = false;
      this.storedAccountDetails = undefined;
      this.addNewPayment = undefined;
      this.paymentService.amount = undefined;
      this.paymentService.newPaymentDetails = undefined;
      this.paymentService.accountBalanceDetail = undefined;
      this.paymentService.policyDetails = undefined;
      this.paymentService.storedAccountDetails = undefined;
      this.openPaymentReceivedModel(requestBody);
    }
  }

  private openPaymentReceivedModel(data?: any): void {
    if (data) {
      if (data.paymentMethod !== PropertyName.AgencySweep && (this.oneTimePaymentIntegration)) {
        this.paymentReceivedModel.modalTitle = PropertyName.PaymentReceived;
        this.paymentReceivedModel.subModalTitle = 'Your payment of ' +
          this.currencyPipe.transform(data.paymentAmount, this.currency) + ' is received . It may take up to 24 hours of your online balance to display this payment.';
        this.paymentReceivedModel.icon = IconName.success;
      } else {
        this.paymentReceivedModel.modalTitle = PropertyName.PaymentSubmitted;
        this.paymentReceivedModel.subModalTitle = 'Your payment of ' + this.currencyPipe.transform(data.paymentAmount, this.currency) + ' is successfully submitted.';
        this.paymentReceivedModel.icon = IconName.success;
      }
    } else {
      this.paymentReceivedModel.modalTitle = PropertyName.PaymentFailed;
      this.paymentReceivedModel.subModalTitle = 'We can`t process your Payment . Please contact Customer Service for further assistance.';
      this.paymentReceivedModel.icon = IconName.fail;
    }
    this.paymentService.objectToModal = data;
    this.paymentReceivedModel.modalFooter = false;
    this.paymentReceivedModel.open(PaymentReceivedComponent);
  }

  public printStatement(status?: any): void {
    this.paymentService.accountDetails = undefined;
    this.paymentService.objectToModal = undefined;
    if (this.returnAppId) {
      this.onSuccessNavigate(status);
    } else {
      setTimeout(() => {
        this.router.navigate(['/ent-payments', PropertyName.expressPayments]);
      }, 100);
    }
  }

  public onSuccessNavigate(status?: any): void {
    const resultQueryParam = JSON.parse(sessionStorage.getItem(PropertyName.resultQueryParam));
    const cancelUrl = resultQueryParam.cancelUrl;
    const returnUrl = resultQueryParam.returnUrl;
    const url = (status === PropertyName.cancel ? cancelUrl : returnUrl);
    this.commonService.navigateToUrl(url, PropertyName.self, true);
  }

  checkAgencyModalChange() {
    if (this.paymentType === PropertyName.ASP && this.isAgencySweepPayorChange === 'true' && this.isConfirm !== PropertyName.active) {
      return true;
    } else {
      return false;
    }
  }

  public selectPaymentTypeList(event: any): void {
    this.paymentService.accountDetails = null;
    this.paymentService.storedAccountDetails = null;
    if (event === PropertyName.ASP) {
      if (!this.agencies) {
        if (this.isUserAgent === 'false') {
          this.openAgentModal(PropertyName.Select);
        }
      } else {
        if (this.agencies && this.agencies.length > 0) {
          this.agent = this.agencies[0].agentNumber;
          this.getAgentDetails(this.agent);
        }
      }
    } else {
      if (event === PropertyName.CP) {
        this.getAccountDetails(this.accountId, this.type);
      }
    }
    this.paymentType = event;
  }

  public selectAgentEvent(event: any): void {
    if (event) {
      this.paymentService.accountDetails = null;
      this.paymentService.accountBalanceDetail = undefined;
      this.paymentService.storedAccountDetails = undefined;
      this.accountDetails = event;
      this.paymentService.accountDetails = event;
      this.clientId = event.partyId;
      this.payorName = this.accountDetails ? this.accountDetails.name : '';
      this.payorAddress = this.accountDetails.addresses ? this.accountDetails.addresses[0].address : '';
      this.imageId = this.accountDetails ? this.accountDetails.pictureId : undefined;
      this.agent = this.accountDetails ? this.accountDetails.agentNumber : undefined;
      this.isShowAccountDetailSection = true;
      this.isShowPaymentMethodButton = true;
      if (this.businessGroup !== undefined) {
        this.getAgentStoredCard(this.clientId, this.businessGroup, this.bankProfileAttributeCheck);
      } else {
        this.getAgentStoredCard(this.clientId, this.billingType, this.bankProfileAttributeCheck);
      }
    } else {
      const isShowConsumerPayment = this.securityEngineService.checkVisibility(PropertyName.consumerPayment, PropertyName.Show);
      if (this.paymentService.agentType === PropertyName.Select) {
        if (isShowConsumerPayment) {
          this.getConsumerPayment();
        } else {
          this.navigateToBack();
        }
      } else {
        this.paymentType = PropertyName.ASP;
      }
    }
  }

  private getPartiesSupportdata(): void {
    this.paymentService.getPartiesSupportdata().subscribe(
      data => {
        if (data) {
          this.supportData = data;
          this.getBankProfile(PropertyName.bankProfileAttribute);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getBankProfile(propertyName: string): any {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Check'] = sd.propertyValues;
      }
    }
    if (this.bankProfileAttributeCheck) { this.bankProfileAttributeCheck = this.bankProfileAttributeCheck[0]; }
  }

  public getPaymentProfiles(isCheckOverrideBgrp = false): void {
    let list = this.paymentProfileList;
    if (this.bankProfileAttributeCheck === 'true') {
      if (isCheckOverrideBgrp) {
        list = this.paymentProfileList.filter(element => element.isOverrideBgrp);
      }
    }
    this.isViewAllProfilesClicked = !isCheckOverrideBgrp ? true : false;
    this.storedAccountDetails = { paymentProfiles: (list && list.length > 0) ? list : this.paymentProfileList };
    this.selectBank(this.storedAccountDetails.paymentProfiles[0], 0);
  }

  private getAgentStoredCard(clientId?: any, billingType?: any, bankProfileAttribute?: any): void {
    this.paymentService.getAgentStoredCard(clientId, billingType, bankProfileAttribute, this.agencySweepPaymentIntegration).subscribe(
      data => {
        this.paymentProfileList = data ? data.content : null;
        this.getPaymentProfiles(true);
        this.paymentService.storedAccountDetails = this.storedAccountDetails;
        if (this.storedAccountDetails.paymentProfiles && this.storedAccountDetails.paymentProfiles.length <= 0) {
          this.isConfirm = PropertyName.noProfilesFound;
        } else {
          this.isConfirm = PropertyName.inprogress;
        }
      },
      () => {
        this.storedAccountDetails = null;
        this.paymentService.storedAccountDetails = this.storedAccountDetails;
      });
  }

  public getAgentDetails(agent?: any) {
    this.isAccountDetailLoaded = false;
    this.paymentService.getAgentDetails(agent, this.isUserAgent).subscribe(
      (data: any) => {
        this.isAccountDetailLoaded = true;
        if (data) {
          this.accountDetails = data ? data.content[0] : null;
          this.paymentService.accountDetails = this.accountDetails;
          this.clientId = this.accountDetails.partyId;
          this.payorName = this.accountDetails ? this.accountDetails.name : '';
          this.payorAddress = this.accountDetails.addresses ? this.accountDetails.addresses[0].address : '';
          this.imageId = this.accountDetails ? this.accountDetails.pictureId : undefined;
          this.agent = this.accountDetails ? this.accountDetails.agentNumber : undefined;
          if (this.businessGroup !== undefined) {
            this.getAgentStoredCard(this.clientId, this.businessGroup, this.bankProfileAttributeCheck);
          } else {
            this.getAgentStoredCard(this.clientId, this.billingType, this.bankProfileAttributeCheck);
          }
        }
      },
      (err: any) => {
        this.commonService.handleErrorAPIResponse(this, 'isAccountDetailLoaded', 'accountDetails', err);
      });
  }

  public openAgentModal(type?: any): void {
    this.getPartiesSupportdata();
    this.selectAgentModal.modalTitle = `${type} ${PropertyName.Agent}`;
    this.paymentService.agentType = type;
    this.paymentService.isUserAgent = this.isUserAgent;
    this.paymentService.isAgencySweepPayorChange = this.isAgencySweepPayorChange;
    this.paymentService.agencies = this.agencies;
    this.selectAgentModal.modalFooter = false;
    this.selectAgentModal.icon = IconName.agent;
    this.selectAgentModal.open(SelectAgentComponent);
  }

  public goBack(): void {
    this.isPaymentMethod = PropertyName.inprogress;
    this.isSelectAmount = PropertyName.active;
    this.isConfirm = PropertyName.inprogress;
    this.isBankIndex = -1;
    this.payment = new PaymentModel();
    this.isCurrentDue = false;
    this.isAccountBalance = false;
    this.isOtherAmount = false;
    this.addNewPayment = undefined;
    this.paymentService.amount = undefined;
    this.paymentService.accountDetails = null;
    this.paymentService.newPaymentDetails = undefined;
    this.paymentService.accountBalanceDetail = undefined;
    this.paymentService.policyDetails = undefined;
    this.paymentService.storedAccountDetails = undefined;
    this.paymentService.agentType = null;
    this.checkData();
  }

  public makePayASP(): void {
    const bankDetails = this.storedAccountDetails.paymentProfiles[this.isBankIndex].bankAccount;
    const requestBody: any = {
      identifier: this.identifier,
      identifierType: PropertyName.ConsumerDirect,
      paymentAmount: this.payment.amount,
      accountType: bankDetails.accountType,
      routingNumber: bankDetails.routingNumber,
      bankAccountNumber: bankDetails.accountNumber,
      paymentMethod: PropertyName.AgencySweep,
      paymentType: PropertyName.AgencySweepPayment,
      agencyNumber: this.agent,
      paymentProfileId: this.storedAccountDetails.paymentProfiles[this.isBankIndex].paymentProfileId
    };
    if (this.businessGroup !== undefined) {
      requestBody.businessGroup = this.storedAccountDetails.paymentProfiles[this.isBankIndex].attributeValue;
    }
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.callToEnterprisePaymentsAPI(requestBody).subscribe(
      data => {
        requestBody.paymentSequenceNumber = data.paymentSequenceNumber;
        requestBody.postedDate = data.postedDate;
        requestBody.batchId = data.batchId;
        requestBody.userId = data.userId;
        this.loadingService.toggleLoadingIndicator(false);
        const paymentCreatedResponse: any = data;
        let paymentDetailUrl = '';
        if (paymentCreatedResponse.links && paymentCreatedResponse.links.length > 0) {
          paymentDetailUrl = paymentCreatedResponse.links[0].href;
          this.getPaymentDetail(paymentDetailUrl, requestBody);
        }
        this.loadingService.toggleLoadingIndicator(false);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset(true, null);
      });
  }

  public openSecurity(event?: any, configType?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, configType);
  }

  public checkSecurityPaymentType(): void {
    this.isShowAccountDetailSection = true;
    this.isShowPaymentMethodButton = true;
    this.isPaymentOptions = true;
    const isShowConsumerPayment = this.securityEngineService.checkVisibility(PropertyName.consumerPayment, PropertyName.Show);
    const isShowAgencySweepPayment = this.securityEngineService.checkVisibility(PropertyName.agencySweepPayment, PropertyName.Show);
    if (!isShowConsumerPayment && !isShowAgencySweepPayment) {
      this.hideAccountDetailSection();
      this.isPaymentOptions = false;
    } else {
      if (isShowConsumerPayment && isShowAgencySweepPayment) {
        this.getConsumerPayment();
      } else if (isShowConsumerPayment) {
        this.getConsumerPayment();
      } else if (isShowAgencySweepPayment) {
        this.getAgencySweepPayment();
      }
    }
  }

  private getAgencySweepPayment(): void {
    if (this.accountDetails) {
      if (this.isAgencySweep === 'true') {
        this.paymentType = PropertyName.ASP;
        this.selectPaymentTypeList(this.paymentType);
      } else {
        this.navigateToBack();
      }
    }
  }

  public navigateToBack(): void {
    this.alertService.error('Selected account/policy do not have required details for available payment options. You will be re-directed to previous screen.');
    setTimeout(() => {
      this.router.navigate(['/ent-payments', PropertyName.expressPayments]);
      if (this.returnAppId) {
        const target = this.navParam ? this.navParam : PropertyName.self;
        if (this.returnAppId === PropertyName.pointinj) {
          window.open(
            this.cancelUrl,
            PropertyName.self
          );
        } else {
          this.commonService.navigateToUrl(this.cancelUrl, target);
        }
      } else {
        setTimeout(() => {
          this.router.navigate(['/ent-payments', PropertyName.expressPayments]);
        }, 100);
      }
    }, 5000);
  }

  private getConsumerPayment(): void {
    this.paymentType = PropertyName.CP;
    if (!this.accountDetails) {
      this.getAccountDetails(this.accountId, this.type);
    } else {
      if (this.accountDetails && this.accountDetails?.agentNumber) {
        this.paymentService.accountDetails = null;
        this.getAccountDetails(this.accountId, this.type);
      }
    }
  }

  private hideAccountDetailSection(): void {
    this.isShowAccountDetailSection = false;
    this.isShowPaymentMethodButton = false;
    this.paymentType = null;
  }

  public checkSecurityOptions(): any {
    let total = this.accountBalanceDetail && this.accountBalanceDetail.accountBalance ? this.accountBalanceDetail.accountBalance : this.accountBalanceDetail.total;
    if (this.isSelectAmount === PropertyName.active) {
      if (this.securityEngineService.checkVisibility(PropertyName.currentBalance, PropertyName.Show) && this.accountBalanceDetail.currentDue + this.accountBalanceDetail.pastDue !== 0) {
        this.selectAmount(this.accountBalanceDetail.currentDue + this.accountBalanceDetail.pastDue, PropertyName.isCurrentDue);
      } else if (this.securityEngineService.checkVisibility(PropertyName.totalBalance, PropertyName.Show) && total !== 0) {
        this.selectAmount(total, PropertyName.isAccountBalance);
      } else if (this.securityEngineService.checkVisibility(PropertyName.otherAmount, PropertyName.Show)) {
        this.selectAmount(null, PropertyName.isOtherAmount);
      } else {
        this.isAccountBalance = false;
        this.isCurrentDue = false;
        this.isOtherAmount = false;
        this.isShowPaymentMethodButton = false;
      }
    }
  }

  refreshSecurity(event: any) {
    if (event && this.accountBalanceDetail && this.accountDetails) {
      this.checkSecurityOptions();
      this.checkSecurityPaymentType();
    }
  }
}
