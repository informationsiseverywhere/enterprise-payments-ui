import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { policyPayRoutes } from './policy-pay-routes';

@NgModule({
  imports: [
    RouterModule.forChild(policyPayRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PolicyPayRoutingModule {
}
