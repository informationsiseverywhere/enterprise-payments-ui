import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { PolicyPayRoutingModule } from './policy-pay-routing.module';

import { PolicyPayComponent } from './components/policy-pay.component';

@NgModule({
  declarations: [
    PolicyPayComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    PolicyPayRoutingModule
  ]
})
export class PolicyPayModule { }
