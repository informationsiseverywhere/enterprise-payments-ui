import { Link } from 'src/app/shared/models/link.model';

export class PaymentModel {
    customerProfileId: string;
    paymentProfileId: string;
    fullName: string;
    address: string;
    paymentMethod: string;
    accountType: string;
    creditCardType: string;
    creditCardNumber: string;
    creditCardExpiryDate: string;
    cvvNumber: string;
    routingNumber: string;
    accountNumber: string;
    amount: number;
    storeCard: boolean = true;
    storeAccount: boolean = true;
    paymentSequenceNumber: number;
    postedDate:string;
    batchId: string;
    userId: string;
    nameOnAccount: string;
    links: Array<Link>;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
export const PaymentType = [
    'Bank Account',
    'Credit Card'
];
