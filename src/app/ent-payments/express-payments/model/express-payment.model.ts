export const ExpressPaymentOptionsList = [    
  {
    name: 'Account Number',
    value: 'isAccountNumber',
    securityName: 'payOptionByAccountNumber',
    imageUrl: 'assets/images/express-payments/account.svg'
  },
  {
    name: 'Policy Number',
    value: 'isPolicyNumber',
    securityName: 'payOptionByPolicyNumber',
    imageUrl: 'assets/images/express-payments/policy.svg'
  },
  {
    name: 'Email',
    value: 'isEmail',
    securityName: 'payOptionByEmail',
    imageUrl: 'assets/images/express-payments/mail.svg'
  },
  {
    name: 'Phone Number',
    value: 'isPhoneNumber',
    securityName: 'payOptionByPhoneNumber',
    imageUrl: 'assets/images/express-payments/phone.svg'
  }
];
