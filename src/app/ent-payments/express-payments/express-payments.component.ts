import { Component, ElementRef, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { PaymentService } from '../batch-payments/services/payments.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from '../security-config/security-config.service';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import { UnidentifiedPaymentModel } from 'src/app/ent-payments/unidentified-payment/model/unidentified-payment.model';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { SelectAgentComponent } from 'src/app/shared/components/select-agent/select-agent.component';
import { ExpressPaymentOptionsList } from 'src/app/ent-payments/express-payments/model/express-payment.model';
import { PropertyName } from 'src/app/shared/enum/common.enum';
import { PaymentusTokenModel } from 'src/app/shared/models/paymentus.model';
import { PaymentusService } from 'src/app/shared/services/paymentus.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { QueryParam } from 'src/app/libs/select/models/select.model';
import { SelectComponent } from 'src/app/libs/select/select.component';
@Component({
  selector: 'app-express-payments',
  templateUrl: './express-payments.component.html',
  styleUrls: ['./express-payments.component.scss']
})
export class ExpressPaymentsComponent implements OnInit {
  NO_RECORDS_ARE_AVAILABLE = 'No records are available';
  isPhoneNumber = false;
  isEmail = false;
  isPolicyNumber = false;
  isAccountNumber = false;
  accountSelected: any;
  policyAmount: any;
  policyAmountDetail: any;
  labelName: string;
  accountDetails: any;
  clientId: any;
  accountId: any;
  navigateCommands: any;
  policyId: any;
  policyEffectiveDate: any;
  isFocus = true;
  currentOption: string;
  mask: any[] = ['(', /[1-9]/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  expressPaymentForm: FormGroup;
  public billingType: any;
  public isAccountDetailsLoaded: boolean;
  @ViewChild('expressEntries', { static: false }) expressEntries: ElementRef;
  @ViewChild('selectAgentModal', { static: false }) selectAgentModal: ModalComponent;
  @ViewChild('policyPaymentSelect') policyPaymentSelect: SelectComponent;
  public applicationList: any;
  public accountTypeData: any = ['Consumer', PropertyName.AgencySweep];
  public accountType: string;
  public type: any;
  public isUnidentified: boolean = false;
  public isMakeUnidentifiedPaymentLink: boolean = false;
  public PolicySymbolDropdown: any = [];
  public supportData: any;
  public noDataFound: string;
  public unidentifiedPaymentDetails: UnidentifiedPaymentModel = new UnidentifiedPaymentModel();
  public isAgencySweepEnabled: any;
  public agencySweepDropdown: any;
  public isUserAgentEnabled: any;
  public userAgentDropdown: any;
  public isAgencySweepPayorChange: any;
  public agencySweepPayorChangeDropdown: any;
  public agenciesDropdown: any;
  public agencies: any;
  public agent: any;
  public agentDetail: any;
  public expressPaymentOptionsList: any;
  public oneTimePaymentIntegrationDropdown: any;
  public oneTimePaymentIntegration = false;
  paymentProviderDropdown: any;
  paymentProvider: any;
  agencySweepPaymentIntegrationDropdown: any;
  agencySweepPaymentIntegration = false;
  public propertyName = PropertyName;
  public apiUrl: string;
  public queryParam: any;
  public policyNumber: any;
  public isSelectedPolicySymbol: any;
  public policyDetails: any;
  public defaultPolicySymbol: any;
  public policyNumberValue: any;
  public isSelectedDisplayField: string;
  constructor(
    private loadingService: LoadingService,
    private paymentService: PaymentService,
    public securityEngineService: SecurityEngineService,
    public commonService: CommonService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    private fb: FormBuilder,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    @Inject(DOCUMENT) private document: any,
    private paymentusService: PaymentusService,
    private router: Router) {
    this.applicationList = ComponentConfiguration.settings.applicationList;
  }

  ngOnInit() {
    this.accountType = this.accountTypeData[0];
    this.expressPaymentOptionsList = ExpressPaymentOptionsList;
    this.expressPaymentForm = this.fb.group({
      verifyNumber: [''],
      verifyZipCode: [''],
      paymentAmount: [''],
      policySymbol: [''],
      policyAccountNumber: ['']
    });
    this.getExpressPaymentSupportData();
    this.setSelectAccountAPIUrl();
    this.selectMethod('isAccountNumber');
  }

  isError(controlName: string) {
    const control = this.expressPaymentForm.get(controlName);
    if (control) {
      return (control.invalid && control.touched) ? true : false;
    }
  }

  public setSelectAccountAPIUrl(): void {
    this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts';
    const QUERYPARAMS = 'type:DirectBill,type:GroupBill,type:Group';
    this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
  }

  public checkFirstLoad(): void {
    if (!this.policyNumber) {
      this.policyPaymentSelect.dropdownItems = [];
      this.policyPaymentSelect.hasNext = false;
    }
  }

  get form() { return this.expressPaymentForm.controls; }

  public showActiveControls(selectedValue?: string): void {
    this.accountDetails = undefined;
    this.paymentService.isUnidentifiedAgencySweepPayment = false;
    this.accountType = selectedValue;
  }

  public selectMethod(radioCheck: any): void {
    if (this.currentOption !== radioCheck) {
      this.currentOption = radioCheck;
      this.reset();
      this.isFocus = false;
      if (radioCheck === 'isPolicyNumber') {
        this[radioCheck] = true;
        this.isPhoneNumber = false;
        this.isEmail = false;
        this.isAccountNumber = false;
        this.labelName = 'Policy Number';
        this.isSelectedDisplayField = 'policyNumber';
        this.setVerifyNumberFocus(true);
      } else if (radioCheck === 'isPhoneNumber') {
        this[radioCheck] = true;
        this.isPolicyNumber = false;
        this.isEmail = false;
        this.isAccountNumber = false;
        this.labelName = 'Phone Number';
        this.isSelectedDisplayField = 'phoneNumber';
        this.setVerifyNumberFocus(true);
        this.isFocus = false;
      } else if (radioCheck === 'isEmail') {
        this[radioCheck] = true;
        this.isPolicyNumber = false;
        this.isPhoneNumber = false;
        this.isAccountNumber = false;
        this.labelName = 'Email';
        this.isSelectedDisplayField = 'emailId';
        this.setVerifyNumberFocus(true);
      } else if (radioCheck === 'isAccountNumber') {
        this[radioCheck] = true;
        this.isPolicyNumber = false;
        this.isPhoneNumber = false;
        this.isEmail = false;
        this.labelName = 'Account Number';
        this.isSelectedDisplayField = 'accountNumber';
        this.setVerifyNumberFocus(true);
      }
      this.resetControlsValidation();
      this.policyPaymentSelect.dropdownItems = [];
    }
  }

  public setVerifyNumberFocus(value: boolean) {
    setTimeout(() => {
      this.isFocus = value;
    });
  }

  public resetControlsValidation(): void {
    if (this.isPolicyNumber) {
      if (this.isUnidentified) {
        this.expressPaymentForm.controls.paymentAmount.setValidators([Validators.required]);
        this.expressPaymentForm.controls.policySymbol.setValidators(null);
      } else {
        this.expressPaymentForm.controls.policySymbol.setValidators([Validators.required, Validators.maxLength(25)]);
      }
      this.expressPaymentForm.controls.verifyNumber.setValidators([Validators.required, Validators.maxLength(25)]);
    } else if (this.isPhoneNumber) {
      this.expressPaymentForm.controls.verifyNumber.setValidators([Validators.required, ValidationService.USAPhoneValidator]);
    } else if (this.isEmail) {
      this.expressPaymentForm.controls.verifyNumber.setValidators([Validators.required, ValidationService.emailValidator]);
    } else if (this.isAccountNumber) {
      if (this.isUnidentified) this.expressPaymentForm.controls.paymentAmount.setValidators([Validators.required]);
      this.expressPaymentForm.controls.verifyNumber.setValidators([Validators.required, Validators.maxLength(30)]);
    }
    this.expressPaymentForm.controls.paymentAmount.updateValueAndValidity();
    this.expressPaymentForm.controls.verifyNumber.updateValueAndValidity();
    this.expressPaymentForm.controls.policySymbol.updateValueAndValidity();
  }
  public reset(): void {
    this.expressPaymentForm.patchValue({
      verifyNumber: undefined,
      verifyZipCode: undefined,
      paymentAmount: undefined,
      policySymbol: undefined,
      policyAccountNumber: undefined
    });
    this.accountDetails = undefined;
    this.noDataFound = undefined;
    this.isUnidentified = false;
    this.PolicySymbolDropdown = undefined;
    this.policyNumberValue = undefined;
    this.isMakeUnidentifiedPaymentLink = false;
    this.accountType = this.accountTypeData[0];
    this.paymentService.isUnidentifiedAgencySweepPayment = false;
    this.expressPaymentForm.controls.verifyNumber.setValidators(null);
    this.expressPaymentForm.controls.policySymbol.setValidators(null);
    this.expressPaymentForm.controls.paymentAmount.setValidators(null);
    this.expressPaymentForm.controls.verifyZipCode.setValidators(null);
    this.expressPaymentForm.reset();
  }

  public checkPolicyNumber(event: any): void {
    this.accountSelected = undefined;
    this.policyNumber = event && event.trim();
    if (this.policyNumber !== '') {
      this.queryParam.query = '';
      if (this.isAccountNumber) {
        const QUERYPARAMS = `accountNumber:${this.policyNumber},type:DirectBill,type:GroupBill,type:Group`;
        this.queryParam.filters = QUERYPARAMS;
      } else if (this.isPolicyNumber) {
        const QUERYPARAMS = `policyNumber:${this.policyNumber},type:DirectBill,type:GroupBill`;
        this.queryParam.filters = QUERYPARAMS;
      } else if (this.isEmail) {
        const QUERYPARAMS = `emailId&query=${this.policyNumber}`;
        this.queryParam.filters = QUERYPARAMS;
      } else {
        const QUERYPARAMS = `phoneNumber&query=${this.policyNumber}`;
        this.queryParam.filters = QUERYPARAMS;
      }
    }
    this.PolicySymbolDropdown = undefined;
    if (!event) {
      this.expressPaymentForm.controls.policySymbol.setValidators(null);
    }
  }

  public unidentifiedPaymentCancelHandle(): void {
    const currOption = this.currentOption;
    this.currentOption = undefined;
    this.selectMethod(currOption);
  }

  public unidentifiedPaymentSwitch(isUnidentified?: boolean): void {
    if (isUnidentified) {
      if (!this.isEmail && !this.isPhoneNumber) {
        this.isUnidentified = true;
        this.isMakeUnidentifiedPaymentLink = false;
        this.resetControlsValidation();
      }
    } else {
      this.isUnidentified = false;
      this.isMakeUnidentifiedPaymentLink = false;
    }
  }

  public convertToUpperCase(event?: any, property?: any): void {
    if (event && !this.isEmail) { this.form[property].patchValue(event); }
  }

  private getExpressPaymentSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentsSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.agencySweep);
          this.getDropdownItems(PropertyName.UserAgent);
          this.getDropdownItems(PropertyName.agencies);
          this.getDropdownItems(PropertyName.AgencySweepPayorChange);
          this.getDropdownItems(PropertyName.oneTimePaymentIntegration);
          this.getDropdownItems(PropertyName.agencySweepPaymentIntegration);
          this.getDropdownItems(PropertyName.paymentProvider);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getDropdownItems(propertyName?: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    this.agencySweepDropdown && (this.isAgencySweepEnabled = this.agencySweepDropdown[0]);
    this.userAgentDropdown && (this.isUserAgentEnabled = this.userAgentDropdown[0]);
    this.agenciesDropdown && (this.agencies = this.agenciesDropdown);
    this.agencySweepPayorChangeDropdown && (this.isAgencySweepPayorChange = this.agencySweepPayorChangeDropdown[0]);
    let oneTimePaymentIntegration = 'false';
    this.oneTimePaymentIntegrationDropdown && (oneTimePaymentIntegration = this.oneTimePaymentIntegrationDropdown[0]);
    let agencySweepPaymentIntegration = 'false';
    this.agencySweepPaymentIntegrationDropdown && (agencySweepPaymentIntegration = this.agencySweepPaymentIntegrationDropdown[0]);
    this.paymentProviderDropdown && (this.paymentProvider = this.paymentProviderDropdown[0]);
    this.oneTimePaymentIntegration = false;
    this.agencySweepPaymentIntegration = false;
    if (oneTimePaymentIntegration === 'true') {
      this.oneTimePaymentIntegration = true;
    }
    if (agencySweepPaymentIntegration === 'true') {
      this.agencySweepPaymentIntegration = true;
    }
  }

  private getAccountDetails(accountId: any): void {
    this.accountDetails = new Object();
    this.isAccountDetailsLoaded = false;
    this.paymentService.getAccountDetailsWithoutFilter(accountId).subscribe(
      (data: any) => {
        this.isAccountDetailsLoaded = true;
        this.accountDetails = data ? data : null;
        this.clientId = this.accountDetails.payorId;
        this.billingType = this.accountDetails.billingType;
        if (this.isPolicyNumber) {
          this.navigateCommands = ['/policy-payment'];
        }
        if (this.isAccountNumber || this.isEmail || this.isPhoneNumber) {
          this.navigateCommands = ['/consumer-payment'];
        }
        this.paymentService.amount = undefined;
        this.paymentService.accountDetails = null;
        this.paymentService.newPaymentDetails = undefined;
        this.paymentService.accountBalanceDetail = undefined;
        this.paymentService.policyDetails = undefined;
        this.paymentService.storedAccountDetails = undefined;
        this.paymentService.agentType = undefined;
      },
      (err) => {
        this.commonService.handleErrorAPIResponse(this, 'isAccountDetailsLoaded', 'accountDetails', err);
      });
  }

  public openSecurity(event?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event);
  }

  public checkSecurityOptions(event?: any): any {
    if (event) {
      if (this.currentOption) {
        const currentSelectedItem = this.expressPaymentOptionsList.filter(item => item.value === this.currentOption);
        if (currentSelectedItem && currentSelectedItem.length > 0 && this.securityEngineService.checkVisibility(currentSelectedItem[0].securityName, PropertyName.Show)) { return; }
      }
      for (const item of this.expressPaymentOptionsList) {
        if (this.securityEngineService.checkVisibility(item.securityName, PropertyName.Show)) {
          this.selectMethod(item.value);
          break;
        }
      }
    }
  }

  public expressPaymentNavigate(): void {
    const cancelUrl = this.document.location.href;
    const paramObject: any = {};
    paramObject.cancelUrl = cancelUrl;
    paramObject.returnUrl = cancelUrl;
    paramObject.returnAppId = this.applicationList.applicationName;
    if (this.isUnidentified) {
      paramObject.type = this.type;
      paramObject.subType = 'UnidentifiedPayment';
      paramObject.billingType = 'Account Bill';
      this.unidentifiedPaymentDetails.identifierType = this.accountType === 'Consumer' ? PropertyName.ConsumerDirect : PropertyName.AgencySweep;
      this.unidentifiedPaymentDetails.identifier = this.isPolicyNumber ? this.form.policyAccountNumber.value : this.form.verifyNumber.value;
      this.unidentifiedPaymentDetails.paymentAmount = this.form.paymentAmount.value;
      this.unidentifiedPaymentDetails.policySymbol = this.isPolicyNumber ? this.form.policySymbol.value : null;
      this.unidentifiedPaymentDetails.policyNumber = this.isPolicyNumber ? this.form.verifyNumber.value : null;
      this.paymentService.unidentifiedPaymentDetails = this.unidentifiedPaymentDetails;
    } else {
      paramObject.accountId = this.accountId;
      paramObject.policyId = this.policyId;
      paramObject.polEffDt = this.policyEffectiveDate;
      paramObject.billingType = this.billingType;
    }
    this.router.navigate(this.navigateCommands, {
      queryParams: paramObject
    });
  }

  public searchCriteriaValueChange(event?: any, controlName?: string, isUpperCase = false): void {
    if (event) {
      this.isMakeUnidentifiedPaymentLink = false;
      this.noDataFound = undefined;
      this.accountDetails = undefined;
      if (isUpperCase && controlName) { this.convertToUpperCase(event.toUpperCase(), controlName); }
    }
  }

  public selectedAccount(data?: any): void {
    if (data) {
      this.accountSelected = data;
      if (this.accountSelected && (this.isAccountNumber || this.isEmail || this.isPhoneNumber)) {
        this.accountId = this.accountSelected.accountId
        this.getAccountDetails(this.accountId);
        this.unidentifiedPaymentSwitch(false);
      } if (this.accountSelected && this.isPolicyNumber) {
        if (this.accountSelected.accountId && this.accountSelected.policies) {
          this.expressPaymentForm.get('verifyNumber').setValue(this.policyNumber);
          this.getPolicySymbol();
        }
      }
    } else {
      this.accountSelected = data;
      this.PolicySymbolDropdown = undefined;
      this.accountDetails = undefined;
      this.policySymbol.patchValue(null);
    }
  }

  public selectedPolicySymbol(event?: any): void {
    if (event && this.isPolicyNumber) {
      let selectedPolyNumberObj = this.policyDetails.find(item => item.policySymbol === event)
      if (selectedPolyNumberObj) {
        this.policyId = selectedPolyNumberObj.policyId;
        this.policyEffectiveDate = selectedPolyNumberObj.policyEffectiveDate;
      }
      this.isSelectedPolicySymbol = event;
      this.accountId = this.accountSelected.accountId
      this.getAccountDetails(this.accountId);
      this.unidentifiedPaymentSwitch(false);
    }
  }

  private getPolicySymbol(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPolicySymbol(this.accountSelected.accountId, this.policyNumber).subscribe(
      data => {
        this.policyDetails = data?.content;
        this.loadingService.toggleLoadingIndicator(false);
        this.accountId = null;
        if (this.policyDetails && this.policyDetails.length > 0) {
          if (this.policyDetails.length === 1) {
            this.PolicySymbolDropdown = null;
            this.defaultPolicySymbol = data.content[0];
            this.policySymbol.patchValue(this.defaultPolicySymbol.policySymbol);
            this.selectedPolicySymbol(this.defaultPolicySymbol.policySymbol);
          } else {
            if (data?.content?.length > 1) {
              this.policyDetails = this.getUnique(this.policyDetails, 'policySymbol');
              const policySymbolList = [];
              this.policyDetails.forEach(element => {
                if (element.id === this.accountSelected.accountId) {
                  policySymbolList.push(element.policySymbol);
                  this.policySymbol.patchValue(null);
                  this.PolicySymbolDropdown = policySymbolList;
                }
              });
              if (this.policyDetails.length === 1) {
                this.PolicySymbolDropdown = null;
                this.defaultPolicySymbol = this.policyDetails[0];
                this.policySymbol.patchValue(this.defaultPolicySymbol.policySymbol);
                this.selectedPolicySymbol(this.defaultPolicySymbol.policySymbol);
              }
            }
          }
        }
        else {
          this.policySymbol.patchValue(null);
        }
        this.policySymbol.updateValueAndValidity();
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  getUnique(arr, comp) {

    // store the comparison  values in array
    const unique = arr.map(e => e[comp])

      // store the indexes of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the false indexes & return unique objects
      .filter((e) => arr[e]).map(e => arr[e]);

    return unique;
  }


  public expressPaymentAction(): void {
    if (this.handleUnidentifiedPaymentSecurityConfig() && this.isUnidentified) {
      this.type = 'Unidentified';
      if (this.accountType === 'Consumer') {
        this.unidentifiedConsumerPayment();
      } else {
        this.unidentifiedAgencySweepPayment();
      }
      return;
    }
  }

  public unidentifiedAgencySweepPayment(): void {
    this.paymentService.isUnidentifiedAgencySweepPayment = true;
    this.paymentService.isUserAgent = this.isUserAgentEnabled;
    this.paymentService.isAgencySweepPayorChange = this.isAgencySweepPayorChange;
    if (this.agencies && this.agencies.length > 0) {
      if (this.agencies.length > 1) {
        this.openAgentModal('Select');
      } else {
        this.agent = this.agencies[0];
        this.getAgentDetails(this.agent, this.isUserAgentEnabled);
      }
    } else {
      this.openAgentModal('Select');
    }
  }

  public getAgentDetails(agent?: any, isUserAgent?: boolean): void {
    this.paymentService.getAgentDetails(agent, isUserAgent).subscribe(
      (data: any) => {
        if (data) {
          this.agentDetail = data ? data.content : null;
          if (this.agentDetail && this.agentDetail.length > 0) {
            this.selectAgentEvent(this.agentDetail[0]);
          }
        }
      },
      (err: any) => {
        this.commonService.handleErrorResponse(err);
      });
  }


  public unidentifiedConsumerPayment(): void {
    if ((this.paymentProvider === PropertyName.authorizeNet && this.oneTimePaymentIntegration) ||
      ((this.paymentProvider === '' || this.paymentProvider === PropertyName.authorizeNet) && !this.oneTimePaymentIntegration) ||
      ((this.paymentProvider === PropertyName.authorizeNet || this.paymentProvider === PropertyName.paymentus)
        && !this.oneTimePaymentIntegration)) {
      this.navigateCommands = ['/unidentified-payment', 'add-new-payment'];
      this.expressPaymentNavigate();
    } else if ((this.paymentProvider === PropertyName.paymentus && this.oneTimePaymentIntegration)) {
      this.paymentusFlow();
    }
  }

  private generateToken(): void {
    const payload: PaymentusTokenModel = new PaymentusTokenModel({
      accountNumber: this.form.verifyNumber.value,
      amount: this.form.paymentAmount.value,
      iframe: false,
      zipCode: this.form.verifyZipCode.value
    });
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentusService.generateSecureToken(payload).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        setTimeout(() => {
          this.reset();
        }, 100);
        const url = this.paymentusService.getSecureTokenizationWebUrl(data?.token);
        window.open(
          url,
          PropertyName.blank
        );
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  paymentusFlow() {
    this.generateToken();
  }

  public openAgentModal(type?: any): void {
    this.selectAgentModal.modalTitle = type + ' Agent';
    this.paymentService.agentType = type;
    this.paymentService.isUserAgent = this.isUserAgentEnabled;
    this.paymentService.agencies = this.agencies;
    this.selectAgentModal.modalFooter = false;
    this.selectAgentModal.icon = 'assets/images/model-icons/agent.svg';
    this.selectAgentModal.open(SelectAgentComponent);
  }

  public selectAgentEvent(event: any): void {
    if (event) {
      this.paymentService.accountDetails = null;
      this.paymentService.accountBalanceDetail = undefined;
      this.paymentService.storedAccountDetails = undefined;
      this.paymentService.accountDetails = event;
      this.navigateCommands = ['/unidentified-payment'];
      this.expressPaymentNavigate();
    }
  }

  public handleUnidentifiedPaymentSecurityConfig(event?: any): boolean {
    if ((this.isAccountNumber && this.securityEngineService.checkVisibility('unidentifiedBillAccountPayment', PropertyName.Show))
      || this.isPolicyNumber && this.securityEngineService.checkVisibility('unidentifiedPolicyPayment', PropertyName.Show)) {
      this.noDataFound && (this.noDataFound = undefined);
      if (this.accountType && !this.securityEngineService.checkVisibility(this.getFieldNameSecurityConfig(this.accountType), PropertyName.Show)) {
        for (const item of this.accountTypeData) {
          if (this.securityEngineService.checkVisibility(this.getFieldNameSecurityConfig(item), PropertyName.Show)) {
            this.showActiveControls(item);
            return true;
          }
        }
      }
      return true;
    }
    if (event && this.isUnidentified) {
      this.reset();
      this.resetControlsValidation();
    }
    this.isUnidentified = false;
    this.isMakeUnidentifiedPaymentLink = false;

    return false;
  }

  public getFieldNameSecurityConfig(key?: any): any {
    let fieldName: any;
    switch (key) {
      case 'Consumer':
        fieldName = this.isAccountNumber ? 'unidentifiedBillAccountPaymentConsumer' : 'unidentifiedPolicyPaymentConsumer';
        break;
      case PropertyName.AgencySweep:
        fieldName = this.isAccountNumber ? 'unidentifiedBillAccountPaymentAgencySweep' : 'unidentifiedPolicyPaymentAgencySweep';
        break;
    }
    return fieldName;
  }

  public checkExpressOptionsEnable(key?: any): boolean {
    return key ? this[key] : false;
  }

  public checkMultiExpressPaymentOptionsEnable(): boolean {
    if (this.expressPaymentOptionsList) {
      const currentExpressPaymentOptions: any = [];
      for (const item of this.expressPaymentOptionsList) {
        if (this.securityEngineService.checkVisibility(item.securityName, PropertyName.Show)) {
          currentExpressPaymentOptions.push(item);
        }
      }
      if (currentExpressPaymentOptions && currentExpressPaymentOptions.length > 1) { return true; }
    }
    return false;
  }

  get policySymbol() { return this.expressPaymentForm.get('policySymbol'); }

}
