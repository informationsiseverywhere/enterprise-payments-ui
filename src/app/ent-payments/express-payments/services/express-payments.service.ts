import { Injectable } from '@angular/core';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';

@Injectable({
  providedIn: 'root'
})
export class ExpressPaymentsService {
  public objectToModal: any;
  apiUrls: any;

  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration) { }

  verifyDetails(details?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/_verify';
    return this.restService.postByUrl(url, details);
  }
}
