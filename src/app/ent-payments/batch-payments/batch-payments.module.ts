import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { BatchPaymentsRoutingModule } from './batch-payments-routing.module';

import { BatchPaymentsComponent } from './components/batch-payments/batch-payments.component';
import { BatchSearchSectionComponent } from './components/batch-payments/batch-search-section/batch-search-section.component';
import { BatchPaymentsResultsComponent } from './components/batch-payments/batch-payments-results/batch-payments-results.component';
import { BatchPaymentsHeaderComponent } from './components/batch-payments/batch-payments-header/batch-payments-header.component';
import { BatchPaymentsActionsComponent } from './components/batch-payments/batch-payments-actions/batch-payments-actions.component';
import { AddBatchComponent } from './components/batch-payments/add-batch/add-batch.component';
import { BatchActionsComponent } from './components/batch-payments/batch-actions/batch-actions.component';
import { ImportPaymentsComponent } from './components/batch-payments/import-payments/import-payments.component';

@NgModule({
  declarations: [
    BatchPaymentsComponent,
    BatchSearchSectionComponent,
    BatchPaymentsResultsComponent,
    BatchPaymentsHeaderComponent,
    BatchPaymentsActionsComponent,
    AddBatchComponent,
    BatchActionsComponent,
    ImportPaymentsComponent
  ],
  exports: [
    BatchPaymentsActionsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    BatchPaymentsRoutingModule
  ]
})
export class BatchPaymentsModule { }
