import { Routes } from '@angular/router';

import { AuthGuard } from 'src/app/ent-auth/auth.guard';
import { CommonComponent } from 'src/app/shared/components/common/common.component';
import { BatchPaymentsComponent } from './components/batch-payments/batch-payments.component';

export const paymentsRoutes: Routes = [
  {
    path: '',
    component: CommonComponent,
    children: [
      {
        path: '',
        component: BatchPaymentsComponent
      },
      {
        path: ':batchId/payments/:postedDate/:userId',
        canActivate: [AuthGuard],
        loadChildren: () => import('./components/batch-summary/batch-summary.module').then(mod => mod.BatchSummaryModule),
        data: {
          breadcrumb: 'Batch Summary',
          headerTitle: 'Batch Summary',
          screenName: 'epBatchSummary'
        }
      }
    ]
  }
];

