import { Link } from 'src/app/shared/models/link.model';

export class BatchModel {
  batchId: string;
  depositBank: string;
  controlBank: string;
  postedDate: string;
  userId: string;
  depositDate: string;
  method: string;
  systemDate: any;
  batchAmount: number;
  businessGroup: any;
  currency = 'USD';
  numberOfTransactions: number;
  links: Array<Link>;

  constructor(values: any = {}) {
    Object.assign(this, values);
  }
}

export class PaymentModel {
  batchId: string;
  paymentSequenceNumber: string;
  paymentAmount = 0;
  paymentType: string;
  paymentDate: string;
  postmarkDate: string;
  user: string;
  identifier: string;
  identifierType: string;
  policySymbol: string;
  lineOfBusiness: string;
  policyNumber: string;
  dueDate: string;
  remitterId: string;
  payable: string;
  systemDate: any;
  suspenseReason: any;
  additionalId: string;
  comment: string;
  reviewCode: string;
  links: Array<Link>;
  paymentId: string;
  paymentOnHold = false;
  payableItem: any;
  paymentMethod: string;
  creditCardNumber: any;
  creditCardType: string;
  paymentComments: string;
  accountNumber: any;
  routingNumber: any;
  fullName: any;
  accountType: any;
  creditCardExpiryDate: any;
  cvvNumber: any;
  storeCard: any;
  storeAccount: any;
  amount: any;
  address: any;
  paymentProfileId: any;
  customerProfileId: any;
  attributeValue: any;
  walletType: any;
  walletNumber: any;
  walletIdentifier: any;
  constructor(values: any = {}) {
    Object.assign(this, values);
  }
}
