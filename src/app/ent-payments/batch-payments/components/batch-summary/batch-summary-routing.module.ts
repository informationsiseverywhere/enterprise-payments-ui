import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { paymentsSummaryRoutes } from './batch-summary-routes';

@NgModule({
  imports: [
    RouterModule.forChild(paymentsSummaryRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class BatchSummaryRoutingModule {
}
