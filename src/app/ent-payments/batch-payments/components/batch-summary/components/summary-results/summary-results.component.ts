import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaginationComponent } from 'src/app/libs/pagination/pagination.component';
import { PolicyPaymentComponent } from '../batch-header/policy-payment/policy-payment.component';
import { GroupPaymentComponent } from '../batch-header/group-payment/group-payment.component';
import { AgencyPaymentComponent } from '../batch-header/agency-payment/agency-payment.component';
import { AccountPaymentComponent } from '../batch-header/account-payment/account-payment.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { EditUnidentifiedPaymentComponent } from '../batch-header/edit-unidentified-payment/edit-unidentified-payment.component';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-summary-results',
  templateUrl: './summary-results.component.html',
})
export class SummaryResultsComponent implements OnInit {
  @Input() currency: any;
  @Input() multiCurrencyEnabled: any;
  @Input() batchId: string;
  @Input() systemDate: string;
  @Input() postedDate: any;
  @Input() userId: any;
  @Input() isAcceptBatch: boolean;
  @Input() isDepositBatch: boolean;
  @Input() page: any;
  @Input() size: any;
  batchPaymentList: any[] = [];
  @Output() isStatementsEmpty: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('batchSummaryEditModal', { static: false }) batchSummaryEditModal: ModalComponent;
  @ViewChild('batchSummaryDeleteModal', { static: false }) batchSummaryDeleteModal: ModalComponent;
  @ViewChild('batchGroupPaymentEditModal', { static: false }) batchGroupPaymentEditModal: ModalComponent;
  @ViewChild('batchUnidentifiedPaymentEditModal', { static: false }) batchUnidentifiedPaymentEditModal: ModalComponent;
  @Output() reloadSummary: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild(PaginationComponent) paginator: PaginationComponent;
  public isBatchPaymentListLoaded = false;

  constructor(
    private batchPaymentsService: BatchPaymentsService,
    private route: ActivatedRoute,
    public securityEngineService: SecurityEngineService,
    private commonService: CommonService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.page = queryParams.page;
        this.size = queryParams.size;
        this.getBatchPayments();
      }
    );
  }

  getBatchPayments() {
    this.isBatchPaymentListLoaded = false;
    this.batchPaymentList && (this.batchPaymentList.length = this.size < 5 ? this.size : 5);
    this.batchPaymentsService.getPaymentList(this.batchId, this.postedDate, this.userId, null, null, this.size, this.page).subscribe(
      data => {
        this.isBatchPaymentListLoaded = true;
        this.refreshBatchPayments(data);
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, 'isBatchPaymentListLoaded', 'batchPaymentList', err);
        this.isStatementsEmpty.emit(this.batchPaymentList);
      });
  }

  private refreshBatchPayments(data?: any): void {
    this.batchPaymentList = this.commonService.tablePaginationHandler(this.paginator, data);
    this.isStatementsEmpty.emit(this.batchPaymentList);
  }

  refreshBatchPaymentsModal(event?: any) {
    if (event) { this.reloadSummary.emit('true'); }
  }

  batchPaymentActions(action: string, batchPayment: any) {
    if (batchPayment.identifierType === 'Consumer Direct') {
      if (batchPayment.isUnidentified) {
        switch (action) {
          case 'Edit':
            this.openBatchUnidentifiedAccountPaymentEditModal(batchPayment, 'Edit Unidentified Account Payment', 'AccountPayment');
            break;
          case 'Delete':
            this.openBatchSummaryDeleteModal(batchPayment);
            break;
        }
      } else {
        switch (action) {
          case 'Edit':
            this.openBatchSummaryEditModal(batchPayment);
            break;
          case 'Delete':
            this.openBatchSummaryDeleteModal(batchPayment);
            break;
        }
      }

    } else if (batchPayment.identifierType === 'Group') {
      if (batchPayment.isUnidentified) {
        switch (action) {
          case 'Edit':
            this.openBatchUnidentifiedAccountPaymentEditModal(batchPayment, 'Edit Unidentified Group Payment', 'Group');
            break;
          case 'Delete':
            this.openBatchSummaryDeleteModal(batchPayment);
            break;
        }
      } else {
        switch (action) {
          case 'Edit':
            this.openBatchGroupPaymentEditModal(batchPayment);
            break;
          case 'Delete':
            this.openBatchSummaryDeleteModal(batchPayment);
            break;
        }
      }
    } else if (batchPayment.identifierType === 'Agency') {
      if (batchPayment.isUnidentified) {
        switch (action) {
          case 'Edit':
            this.openBatchUnidentifiedAccountPaymentEditModal(batchPayment, 'Edit Unidentified Agency Payment', 'AgencyPayment');
            break;
          case 'Delete':
            this.openBatchSummaryDeleteModal(batchPayment);
            break;
        }
      } else {
        switch (action) {
          case 'Edit':
            this.openBatchAgencyPaymentEditModal(batchPayment);
            break;
          case 'Delete':
            this.openBatchSummaryDeleteModal(batchPayment);
            break;
        }
      }
    } else if (batchPayment.policyNumber) {
      if (batchPayment.isUnidentified) {
        switch (action) {
          case 'Edit':
            this.openBatchUnidentifiedAccountPaymentEditModal(batchPayment, 'Edit Unidentified Policy Payment', 'PolicyPayment');
            break;
          case 'Delete':
            this.openBatchSummaryDeleteModal(batchPayment);
            break;
        }
      } else {
        switch (action) {
          case 'Edit':
            this.openBatchPolicyPaymentEditModal(batchPayment);
            break;
          case 'Delete':
            this.openBatchSummaryDeleteModal(batchPayment);
            break;
        }
      }
    }
  }

  openBatchSummaryEditModal(batchPayment?: any) {
    this.batchSummaryEditModal.modalTitle = 'Edit Account Payment';
    this.batchSummaryEditModal.modalFooter = false;
    this.batchSummaryEditModal.okButtonText = 'SAVE';
    batchPayment.batchId = this.batchId;
    batchPayment.postedDate = this.postedDate;
    batchPayment.userId = this.userId;
    this.batchPaymentsService.systemDate = this.systemDate;
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchPaymentsService.modalAction = 'Edit';
    this.batchSummaryEditModal.icon = 'assets/images/model-icons/account-payment.svg';
    this.batchSummaryEditModal.open(AccountPaymentComponent);
  }

  openBatchPolicyPaymentEditModal(batchPayment?: any) {
    this.batchSummaryEditModal.modalTitle = 'Edit Policy Payment';
    this.batchSummaryEditModal.modalFooter = false;
    this.batchSummaryEditModal.okButtonText = 'SAVE';
    batchPayment.batchId = this.batchId;
    batchPayment.postedDate = this.postedDate;
    batchPayment.userId = this.userId;
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchPaymentsService.modalAction = 'Edit';
    this.batchSummaryEditModal.icon = 'assets/images/model-icons/policy-payment.svg';
    this.batchSummaryEditModal.open(PolicyPaymentComponent);
  }

  openBatchSummaryDeleteModal(batchPayment?: any) {
    this.batchSummaryDeleteModal.modalTitle = 'Delete Batch Payment';
    this.batchSummaryDeleteModal.modalFooter = true;
    this.batchSummaryDeleteModal.okButtonText = 'Yes';
    this.batchSummaryDeleteModal.cancelButtonText = 'No';
    batchPayment.batchId = this.batchId;
    batchPayment.postedDate = this.postedDate;
    batchPayment.userId = this.userId;
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchSummaryDeleteModal.icon = 'assets/images/model-icons/delete.svg';
    this.batchSummaryDeleteModal.message = 'Are you sure you want to delete the Batch Payment immediately?';
    this.batchSummaryDeleteModal.open();
  }

  openBatchGroupPaymentEditModal(batchPayment?: any) {
    this.batchGroupPaymentEditModal.modalTitle = 'Edit Group Payment';
    this.batchGroupPaymentEditModal.modalFooter = false;
    this.batchGroupPaymentEditModal.okButtonText = 'SAVE';
    batchPayment.batchId = this.batchId;
    batchPayment.postedDate = this.postedDate;
    batchPayment.userId = this.userId;
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = 'Edit';
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchGroupPaymentEditModal.icon = 'assets/images/model-icons/group-payment.svg';
    this.batchGroupPaymentEditModal.open(GroupPaymentComponent);
  }

  openBatchAgencyPaymentEditModal(batchPayment?: any) {
    this.batchGroupPaymentEditModal.modalTitle = 'Edit Agency Payment';
    this.batchGroupPaymentEditModal.modalFooter = false;
    this.batchGroupPaymentEditModal.okButtonText = 'SAVE';
    batchPayment.batchId = this.batchId;
    batchPayment.postedDate = this.postedDate;
    batchPayment.userId = this.userId;
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = 'Edit';
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchGroupPaymentEditModal.icon = 'assets/images/model-icons/agency-payment.svg';
    this.batchGroupPaymentEditModal.open(AgencyPaymentComponent);
  }

  openBatchUnidentifiedAccountPaymentEditModal(batchPayment?: any, modalTitle?: any, modalAction?: any) {
    this.batchUnidentifiedPaymentEditModal.modalTitle = modalTitle;
    this.batchUnidentifiedPaymentEditModal.modalFooter = false;
    this.batchUnidentifiedPaymentEditModal.okButtonText = 'SAVE';
    batchPayment.batchId = this.batchId;
    batchPayment.postedDate = this.postedDate;
    batchPayment.userId = this.userId;
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = modalAction;
    this.batchPaymentsService.systemDate = this.systemDate;
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    if (modalAction === 'AccountPayment') {
      this.batchUnidentifiedPaymentEditModal.icon = 'assets/images/model-icons/account-payment.svg';
    } else if (modalAction === 'Group') {
      this.batchUnidentifiedPaymentEditModal.icon = 'assets/images/model-icons/group-payment.svg';
    } else if (modalAction === 'AgencyPayment') {
      this.batchUnidentifiedPaymentEditModal.icon = 'assets/images/model-icons/agency-payment.svg';
    } else {
      this.batchUnidentifiedPaymentEditModal.icon = 'assets/images/model-icons/policy-payment.svg';
    }
    this.batchUnidentifiedPaymentEditModal.open(EditUnidentifiedPaymentComponent);
  }

  deleteBatchPayment(event?: any) {
    if (event) {
      if (this.batchPaymentsService.paramsToModal) {
        this.batchPaymentsService.deleteBatchPaymentDetails(this.batchId, this.postedDate, this.userId, this.batchPaymentsService.paramsToModal.paymentSequenceNumber).subscribe(
          () => {
            this.batchPaymentsService.paramsToModal = null;
            this.refreshBatchPaymentsModal(true);
          },
          err => {
            this.commonService.handleErrorResponse(err);
          });
      }
    }
  }

  removeSpaces(status?: string) {
    return this.commonService.removeSpacesAndFWDash(status);
  }
}
