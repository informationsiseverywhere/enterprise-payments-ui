import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { QueryParam } from 'src/app/libs/select/models/select.model';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { DateService } from 'src/app/shared/services/date.service';
import { DropdownComponent } from 'src/app/libs/dropdown/dropdown.component';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { PaymentModel } from 'src/app/ent-payments/batch-payments/models/batch.model';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';


@Component({
  selector: 'app-account-payment',
  templateUrl: './account-payment.component.html',
  providers: [DatePipe, CurrencyPipe]
})
export class AccountPaymentComponent implements OnInit, AfterViewInit {
  public systemDate: any;
  private supportData: any;
  public batchAccountPaymentForm: FormGroup;
  public apiUrl: any;
  public paymentAmount: any;
  public amountList: any = [];
  public queryParam: any;
  public accountSelected: any;
  public accountBalanceDetail: any;
  public currentDueLabel: any;
  public PaymentTypeDropdown: any;
  public SuspenseReasonDropdown: any;
  public paymentType: any;
  private batchParam: any;
  private postedDate: any;
  private batchId: any;
  private userId: any;
  public paymentComments: any;
  public accountNumber: any;
  public dateReceived: any;
  private paymentModel: PaymentModel = new PaymentModel();
  private accountPaymentParam: any;
  public modalType: any;
  public paymentOnHold = false;
  public suspenseReason: any;
  public postmarkDate: any;
  public dueDate: any;
  public dateReceivedValid = false;
  public postmarkDateValid = false;
  @ViewChild('selectPaymentAmount', { static: false }) selectPaymentAmount: DropdownComponent;
  public isUnValidPaymentAmount = false;
  public batchPaymentDetail: any;
  isMultiCurrencyEnable: any;
  businessGroup: any;
  currency: any;
  paymentId: any;
  public suspenseDefaultValue: any;
  public currencyDropdown: any;
  public enableExchangeRate = false;
  constructor(
    private loadingService: LoadingService,
    private fb: FormBuilder,
    private modal: ModalComponent,
    private batchPaymentsService: BatchPaymentsService,
    private currencyPipe: CurrencyPipe,
    private hostUrlService: DynamicHostUrlService,
    public commonService: CommonService,
    private appConfig: AppConfiguration,
    private dateService: DateService,
    public securityEngineService: SecurityEngineService
  ) { }

  ngOnInit() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.batchAccountPaymentForm = this.fb.group({
      accountNumber: ['', Validators.required],
      paymentAmount: ['', Validators.required],
      paymentType: ['', Validators.required],
      dateReceived: ['', ValidationService.dateValidator],
      suspenseReason: [''],
      paymentOnHold: [false],
      dueDate: ['', ValidationService.dateValidator],
      postmarkDate: ['', ValidationService.dateValidator],
      paymentComments: ['', Validators.maxLength(80)],
      receivedCurrency: [''],
      exchangeRate: [''],
      paymentCalculatedAmount: [''],
      paymentId: ['']

    });
    if (this.batchPaymentsService.paramsToModal) {
      this.modalType = this.batchPaymentsService.modalAction;
      this.batchParam = this.batchPaymentsService.paramsToModal;
      this.postedDate = this.batchParam.postedDate;
      this.batchId = this.batchParam.batchId;
      this.userId = this.batchParam.userId;
      this.businessGroup = this.batchParam.businessGroup;
    }
    this.dateReceived = this.systemDate;
    this.getBatchPaymentSupportData();
    this.currency = this.batchPaymentsService.currency;
    this.isMultiCurrencyEnable = this.batchPaymentsService.multiCurrencyEnabled;
    this.setSelectAccountAPIUrl();
    this.resetReceivedCurrencyData();
  }

  public ngAfterViewInit(): void {
    if (this.batchPaymentsService.modalAction && this.batchPaymentsService.paramsToModal) {
      this.modalType = this.batchPaymentsService.modalAction;
      if (this.modalType === 'Edit') {
        const linkName = 'payment';
        this.accountPaymentParam = this.batchPaymentsService.paramsToModal;
        this.getBatchPaymentDetailFromLinks(linkName);
      }
    }
  }

  public setSelectAccountAPIUrl(): void{
    this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts';
    if (this.isMultiCurrencyEnable === 'true' && this.businessGroup) {
      const QUERYPARAMS = `type:DirectBill,type:GroupBill,currency:${this.currency},businessGroup:${this.businessGroup}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else if (this.isMultiCurrencyEnable !== 'true' && this.businessGroup) {
      const QUERYPARAMS = `type:DirectBill,type:GroupBill,businessGroup:${this.businessGroup}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else if (this.isMultiCurrencyEnable === 'true') {
      const QUERYPARAMS = `type:DirectBill,type:GroupBill,currency:${this.currency}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else {
      const QUERYPARAMS = 'type:DirectBill,type:GroupBill';
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    }
  }

  public resetReceivedCurrencyData(): void{
    this.isUnValidPaymentAmount = false;
    if (this.isMultiCurrencyEnable) {
      this.currencyDropdown = this.batchPaymentsService.currencyDropdown;
      const multiCurrencyEnabled = (this.isMultiCurrencyEnable === 'true' ? true : false);
      if (multiCurrencyEnabled) {
        this.receivedCurrency.setValidators(Validators.required);
        this.receivedCurrency.setValue(this.currency);
        this.exchangeRate.setValidators(ValidationService.numberValidation(0));
        this.exchangeRate.setValue(null);
        this.enableExchangeRate = false;
        this.paymentCalculatedAmount.setValue(null);
        this.setSelectPaymentAmount();
      } else {
        this.receivedCurrency.setValidators(null);
        this.exchangeRate.setValidators(null);
      }
      this.receivedCurrency.updateValueAndValidity();
      this.exchangeRate.updateValueAndValidity();
    }
  }

  private getBatchPaymentDetailFromLinks(linkName?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.commonService.getDatafromDetailLink(this.batchParam, linkName, 'GET').subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.batchPaymentDetail = { ...data };

        if (this.modalType === 'Edit') {
          if (!this.accountSelected) { this.accountSelected = new Object(); }
          this.accountSelected.payorName = this.batchPaymentDetail.payorName;
          this.accountSelected.accountNumber = this.batchPaymentDetail.identifier;
          this.accountSelected.accountId = this.batchPaymentDetail.accountId;
          if (this.batchPaymentDetail.payorAddress) { this.accountSelected.payorAddress = this.batchPaymentDetail.payorAddress; }
          this.accountNumber = this.batchPaymentDetail.identifier;
          this.dateReceived = this.batchPaymentDetail.paymentDate;
          this.paymentType = this.batchPaymentDetail.paymentType;
          this.paymentOnHold = this.batchPaymentDetail.paymentOnHold;
          this.suspenseReason = this.batchPaymentDetail.suspenseReason;
          this.dueDate = this.batchPaymentDetail.dueDate;
          this.postmarkDate = this.batchPaymentDetail.postmarkDate;
          this.paymentComments = this.batchPaymentDetail.paymentComments;
          const receivedCurrency = (this.batchPaymentDetail.receivedCurrency ? this.batchPaymentDetail.receivedCurrency : this.currency);
          this.receivedCurrency.setValue(receivedCurrency);
          this.exchangeRate.setValue(this.batchPaymentDetail.rate);
          const receivedAmount = this.isRateEntered(this.batchPaymentDetail.receivedCurrency, this.batchPaymentDetail.rate) ? this.batchPaymentDetail.paymentAmount : this.batchPaymentDetail.receivedAmount;
          this.paymentCalculatedAmount.setValue(receivedAmount);
          this.enableExchangeRate = false;
          if (this.batchPaymentDetail.rate && this.batchPaymentDetail.receivedCurrency) {
            this.enableExchangeRate = true;
          }
          this.paymentId = this.batchPaymentDetail.paymentId;
        }
        if (this.accountPaymentParam.accountId) {
          this.getBalanceDetails(this.accountPaymentParam.accountId);
        }
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  public setPaymentAmount(amount: any): void {
    this.paymentAmount = amount;
    this.isUnValidPaymentAmount = false;
  }

  private getDropdownItems(propertyName?: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (!this.paymentType && this.modalType !== 'Edit' && propertyName === PropertyName.PaymentType && sd.defaultValue) {
          this.batchAccountPaymentForm.controls['paymentType'].setValue(sd.defaultValue);
        }
        if (!this.suspenseReason && this.modalType !== 'Edit' && propertyName === PropertyName.SuspenseReason && sd.defaultValue) {
           this.suspenseDefaultValue = sd.defaultValue;
          this.batchAccountPaymentForm.controls['suspenseReason'].setValue(this.suspenseDefaultValue);
        }
      }
    }
  }

  public selectedAccount(data?: any): void {
    if (data) {
      this.resetReceivedCurrencyData();
      this.accountSelected = data;
      this.batchAccountPaymentForm.get('accountNumber').clearValidators();
      this.batchAccountPaymentForm.get('accountNumber').updateValueAndValidity();
      this.amountList = [];
      if (this.accountSelected.accountId) {
        this.getBalanceDetails(this.accountSelected.accountId);
      }
    } else {
      this.accountSelected = data;
    }
  }

  private getBalanceDetails(accountID?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBalances(accountID).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.accountBalanceDetail = data;
        this.amountList.push(
          {
            name: PropertyName.currentBalanceLabel + this.currencyPipe.transform
              (this.accountBalanceDetail.currentDue + this.accountBalanceDetail.pastDue, this.currency),
            value: this.accountBalanceDetail.currentDue + this.accountBalanceDetail.pastDue
          }
        );

        this.amountList.push(
          {
            name: PropertyName.totalBalanceLabel + this.currencyPipe.transform(this.accountBalanceDetail.accountBalance, this.currency),
            value: this.accountBalanceDetail.accountBalance
          }
        );
        this.setSelectPaymentAmount();
        this.getDropdownItems(PropertyName.PaymentType);
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  public setSelectPaymentAmount(): void{
    if (this.batchPaymentsService.modalAction === 'Add') {
      this.currentDueLabel = this.amountList[0]?.name;
      this.paymentAmount = this.accountBalanceDetail?.currentDue + this.accountBalanceDetail?.pastDue;
    } else if (this.batchPaymentsService.modalAction === 'Edit') {
      const paymentAmount = this.isRateEntered(this.accountPaymentParam.receivedCurrency, this.accountPaymentParam.rate) ? this.accountPaymentParam.receivedAmount : this.accountPaymentParam.paymentAmount;
      this.currentDueLabel = this.getCurrentDueLabel(paymentAmount);
      this.paymentAmount = paymentAmount;
    }
  }

  public getCurrentDueLabel(paymentAmount: any): string{
    let amountLabel = PropertyName.otherLabel + this.currencyPipe.transform(paymentAmount, this.currency);
    const amountItem = this.amountList.find(item => Number(item.value) === Number(paymentAmount));
    (amountItem) && (amountLabel = amountItem.name);
    return amountLabel;
  }

  public paymentOnHoldChanged(event?: any): void {
    this.paymentOnHold = event;
    this.batchAccountPaymentForm.get('suspenseReason') && this.batchAccountPaymentForm.get('suspenseReason').setValue(undefined);
    this.batchAccountPaymentForm.removeControl('suspenseReason');
    if (event) {
      this.batchAccountPaymentForm.addControl('suspenseReason', new FormControl('', Validators.required));
      this.suspenseReason = this.suspenseDefaultValue
    }
  }

  private getBatchPaymentSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchPaymentsSupportData(this.batchId, this.postedDate).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.PaymentType);
          this.getDropdownItems(PropertyName.SuspenseReason);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  public accountPaymentSubmit(): void {
    this.isUnValidPaymentAmount = false;
    if (this.isValidPaymentAmountEntered()) {
      this.isUnValidPaymentAmount = true;
      return;
    }
    this.paymentModel.paymentAmount = ((this.receivedCurrency.value !== this.currency && this.isMultiCurrencyEnable === 'true') ? this.paymentCalculatedAmount.value : this.paymentAmount);
    this.paymentModel.paymentType = this.paymentType;
    this.paymentModel.identifierType = PropertyName.ConsumerDirect;
    this.paymentModel.identifier = this.accountSelected.accountNumber;
    this.paymentModel.paymentOnHold = this.paymentOnHold;
    this.paymentModel.suspenseReason = this.paymentOnHold ? this.suspenseReason : null;
    this.paymentModel.dueDate = this.dueDate;
    this.paymentModel.postmarkDate = this.postmarkDate;
    this.paymentModel.paymentComments = this.paymentComments;
    this.paymentModel.paymentId = this.paymentId;
    if (!this.paymentModel.dueDate) {
      this.paymentModel.dueDate = null;
    }
    if (!this.paymentModel.postmarkDate) {
      this.paymentModel.postmarkDate = null;
    }
    if (this.dateReceived !== '') {
      this.paymentModel.paymentDate = this.dateReceived;
    }
    if (this.modalType === 'Add') {
      this.addAccountPaymentAction(this.paymentModel);
    } else if (this.modalType === 'Edit') {
      this.editAccountPaymentAction(this.paymentModel);
    }
  }

  private addAccountPaymentAction(paymentModel?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.addBatchPayments(this.batchId, this.postedDate, paymentModel).subscribe(
      (data: any) => {
        if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
          this.getTransactionRateHistory(data);
        } else {
          this.handleAccountPaymentResponse();
        }
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  private editAccountPaymentAction(paymentModel?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.editBatchPayments(this.batchId, this.postedDate, this.userId, this.accountPaymentParam.paymentSequenceNumber, paymentModel).subscribe(
      () => {
        if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
          this.getTransactionRateHistory(this.batchPaymentDetail);
        } else if (this.batchPaymentDetail.receivedAmount) {
          this.deleteTransactionRateHistory(this.batchPaymentDetail);
        } else {
          this.handleAccountPaymentResponse();
        }
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  private deleteTransactionRateHistory(data?: any): void {
    this.batchPaymentsService.deleteTransactionRateHistory(data).subscribe(
      () => {
        if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
          this.getTransactionRateHistory(data);
        } else {
          this.handleAccountPaymentResponse();
        }
      },
      err => {
        this.handleAccountPaymentResponse();
        this.commonService.handleErrorResponse(err);
      });
  }

  private getTransactionRateHistory(data?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    const body = {
      paymentAmount: this.paymentCalculatedAmount.value,
      paymentCurrency: this.currency,
      receivedCurrency: this.receivedCurrency.value,
      receivedAmount: this.paymentAmount,
      rate: this.exchangeRate.value
    };
    this.batchPaymentsService.getTransactionRateHistory(data, body).subscribe(
      () => {
        this.handleAccountPaymentResponse();
      },
      err => {
        this.handleAccountPaymentResponse();
        this.commonService.handleErrorResponse(err);
      });
  }

  private handleAccountPaymentResponse(): void {
    this.loadingService.toggleLoadingIndicator(false);
    this.batchPaymentsService.paramsToModal = null;
    this.modal.close(true);
  }

  private isValidPaymentAmountEntered(): boolean {
    if (this.paymentAmount <= 0) {
      return true;
    }
    return false;
  }

  private isRateEntered(receivedCurrency?: string, exchangeRate?: any): boolean {
    return (receivedCurrency !== this.currency && (Number(exchangeRate) > 0));
  }

  public resetDateValidate(event?: any, type?: any): void {
    if (event && type) {
      if (event.value > this.systemDate && event.selectionDayTxt) {
        this[type + 'Valid'] = true;
      } else {
        this[type + 'Valid'] = false;
      }
    }
  }

  updateAmount(value: any) {
    if (value) {
      this.isUnValidPaymentAmount = false;
      const paymentCalculatedAmount = Number(this.paymentAmount) * Number(this.exchangeRate.value);
      const roundOffValue = this.roundOff(paymentCalculatedAmount, 2);
      this.paymentCalculatedAmount.setValue(roundOffValue);
    }
  }

  roundOff(num: any, places: any) {
    const x = Math.pow(10, places);
    return Math.round(num * x) / x;
  }

  public getReceivedCurrency(fromCurrency: any): any {
    this.isUnValidPaymentAmount = false;
    if (fromCurrency && fromCurrency !== this.currency) {
      this.loadingService.toggleLoadingIndicator(true);
      this.batchPaymentsService.getExchangeRates(fromCurrency, this.currency, this.postedDate).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          if (data) {
            this.enableExchangeRate = true;
            const rate = data.rate ? data.rate : 1;
            this.exchangeRate.setValue(rate);
            const paymentCalculatedAmount = Number(this.paymentAmount) * Number(this.exchangeRate.value);
            this.paymentCalculatedAmount.setValue(paymentCalculatedAmount);
          }
        },
        (err) => {
          this.loadingService.toggleLoadingIndicator(false);
          this.commonService.handleErrorResponse(err);
        });
    } else {
      this.exchangeRate.setValue(null);
      this.enableExchangeRate = false;
      this.paymentCalculatedAmount.setValue(null);
      (this.paymentAmount) && (this.currentDueLabel = this.getCurrentDueLabel(this.paymentAmount));
    }
  }

  public clearPaymentAmount() : void {
    this.isUnValidPaymentAmount = false;
  }

  get receivedCurrency() { return this.batchAccountPaymentForm.get('receivedCurrency'); }
  get exchangeRate() { return this.batchAccountPaymentForm.get('exchangeRate'); }
  get paymentCalculatedAmount() { return this.batchAccountPaymentForm.get('paymentCalculatedAmount'); }
}
