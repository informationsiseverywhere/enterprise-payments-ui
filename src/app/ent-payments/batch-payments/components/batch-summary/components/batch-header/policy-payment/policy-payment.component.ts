import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { QueryParam } from 'src/app/libs/select/models/select.model';
import { SelectComponent } from 'src/app/libs/select/select.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { DateService } from 'src/app/shared/services/date.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { PaymentModel } from 'src/app/ent-payments/batch-payments/models/batch.model';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-policy-payment',
  templateUrl: './policy-payment.component.html',
  providers: [DatePipe, CurrencyPipe]
})

export class PolicyPaymentComponent implements OnInit, AfterViewInit {
  public systemDate: any;
  private supportData: any;
  public batchPolicyPaymentForm: FormGroup;
  public apiUrl: string;
  public paymentAmount: any;
  public amountList: any = [];
  public queryParam: any;
  public accountSelected: any;
  public currentDueLabel: string;
  public PaymentTypeDropdown: any;
  public PayableItemDropdown: any;
  public paymentType: any;
  private batchParam: any;
  private postedDate: any;
  private batchId: any;
  accountNumber: any;
  public dateReceived: any;
  private paymentModel: PaymentModel = new PaymentModel();
  private accountPaymentParam: any;
  public modalType: any;
  public policyNumber: any;
  public policyAmount: any;
  public policyAmountDetail: any;
  public postmarkDate: any;
  public dueDate: any;
  public paymentComments: any;
  public isUnValidPaymentAmount = false;
  public dateReceivedValid = false;
  public postmarkDateValid = false;
  public paymentOnHold = false;
  public suspenseReason: any;
  public payableItem: any;
  public SuspenseReasonDropdown: any;
  public batchPaymentDetail: any;
  public policyNumberValue: any;
  isMultiCurrencyEnable: any;
  businessGroup: any;
  currency: any;
  suspenseDefaultValue: any;
  public currencyDropdown: any;
  public enableExchangeRate = false;
  public paymentId: any;
  public userId: any;
  @ViewChild('policyPaymentSelect') policyPaymentSelect: SelectComponent;
  policySymbolDropdown: any;
  constructor(
    private loadingService: LoadingService,
    private fb: FormBuilder,
    private modal: ModalComponent,
    private batchPaymentsService: BatchPaymentsService,
    private currencyPipe: CurrencyPipe,
    public commonService: CommonService,
    private appConfig: AppConfiguration,
    private hostUrlService: DynamicHostUrlService,
    private alertService: AlertService,
    private dateService: DateService,
    public securityEngineService: SecurityEngineService
  ) { }

  ngOnInit() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.batchPolicyPaymentForm = this.fb.group({
      policyNumber: ['', Validators.required],
      paymentAmount: ['', Validators.required],
      paymentType: ['', Validators.required],
      dateReceived: ['', ValidationService.dateValidator],
      suspenseReason: [''],
      paymentOnHold: [false],
      payableItem: [''],
      dueDate: ['', ValidationService.dateValidator],
      postmarkDate: ['', ValidationService.dateValidator],
      paymentComments: ['', Validators.maxLength(80)],
      receivedCurrency: [''],
      exchangeRate: [''],
      paymentCalculatedAmount: [''],
      paymentId: [''],
      policySymbol: ['']
    });
    if (this.batchPaymentsService.paramsToModal) {
      this.modalType = this.batchPaymentsService.modalAction;
      this.batchParam = this.batchPaymentsService.paramsToModal;
      this.postedDate = this.batchParam.postedDate;
      this.batchId = this.batchParam.batchId;
      this.userId = this.batchParam.userId;
      this.businessGroup = this.batchParam.businessGroup;
    }
    this.dateReceived = this.systemDate;
    this.getBatchPaymentSupportData();
    this.currency = this.batchPaymentsService.currency;
    this.isMultiCurrencyEnable = this.batchPaymentsService.multiCurrencyEnabled;
    this.setSelectAccountAPIUrl();
    this.resetReceivedCurrencyData();
  }

  ngAfterViewInit() {
    if (this.batchPaymentsService.modalAction && this.batchPaymentsService.paramsToModal) {
      this.modalType = this.batchPaymentsService.modalAction;
      if (this.modalType === 'Edit') {
        const linkName = 'payment';
        this.accountPaymentParam = this.batchPaymentsService.paramsToModal;
        this.getBatchPaymentDetailFromLinks(linkName);
      }
    }
  }

  public setSelectAccountAPIUrl(): void{
    this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts';
    if (this.isMultiCurrencyEnable === 'true' && this.businessGroup) {
      const QUERYPARAMS = `type:DirectBill,type:GroupBill,currency:${this.currency},businessGroup:${this.businessGroup}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else if (this.isMultiCurrencyEnable !== 'true' && this.businessGroup) {
      const QUERYPARAMS = `type:DirectBill,type:GroupBill,businessGroup:${this.businessGroup}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else if (this.isMultiCurrencyEnable === 'true') {
      const QUERYPARAMS = `type:DirectBill,type:GroupBill,currency:${this.currency}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else {
      const QUERYPARAMS = 'type:DirectBill,type:GroupBill';
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    }
  }

  public resetReceivedCurrencyData(): void{
    this.isUnValidPaymentAmount = false;
     if (this.isMultiCurrencyEnable) {
      this.currencyDropdown = this.batchPaymentsService.currencyDropdown;
      const multiCurrencyEnabled = (this.isMultiCurrencyEnable === 'true' ? true : false);
      if (multiCurrencyEnabled) {
        this.receivedCurrency.setValidators(Validators.required);
        this.receivedCurrency.setValue(this.currency);
        this.exchangeRate.setValidators(ValidationService.numberValidation(0));
        this.exchangeRate.setValue(null);
        this.enableExchangeRate = false;
        this.paymentCalculatedAmount.setValue(null);
        this.setSelectPaymentAmount();
      } else {
        this.receivedCurrency.setValidators(null);
        this.exchangeRate.setValidators(null);
      }
      this.receivedCurrency.updateValueAndValidity();
      this.exchangeRate.updateValueAndValidity();
    }
  }

  private getBatchPaymentDetailFromLinks(linkName?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.commonService.getDatafromDetailLink(this.batchParam, linkName, 'GET').subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.batchPaymentDetail = { ...data };

        if (this.modalType === 'Edit') {
          if (!this.accountSelected) { this.accountSelected = new Object(); }
          this.accountSelected.payorName = this.batchPaymentDetail.payorName;
          this.accountSelected.accountNumber = this.batchPaymentDetail.identifier;
          this.accountSelected.accountId = this.batchPaymentDetail.accountId;
          if (this.batchPaymentDetail.payorAddress) { this.accountSelected.payorAddress = this.batchPaymentDetail.payorAddress; }
          this.accountNumber = this.batchPaymentDetail.identifier;
          this.dateReceived = this.batchPaymentDetail.paymentDate;
          this.paymentType = this.batchPaymentDetail.paymentType;
          this.paymentOnHold = this.batchPaymentDetail.paymentOnHold;
          this.suspenseReason = this.batchPaymentDetail.suspenseReason;
          this.dueDate = this.batchPaymentDetail.dueDate;
          this.postmarkDate = this.batchPaymentDetail.postmarkDate;
          this.paymentComments = this.batchPaymentDetail.paymentComments;
          this.policyNumberValue = this.batchPaymentDetail.policyNumber;
          this.policyNumber = this.policyNumberValue;
          this.payableItem = this.batchPaymentDetail.payableItem;
          this.getPolicyAmount();
          const receivedCurrency = (this.batchPaymentDetail.receivedCurrency ? this.batchPaymentDetail.receivedCurrency : this.currency);
          this.receivedCurrency.setValue(receivedCurrency);
          this.exchangeRate.setValue(this.batchPaymentDetail.rate);
          const receivedAmount = this.isRateEntered(this.batchPaymentDetail.receivedCurrency, this.batchPaymentDetail.rate) ? this.batchPaymentDetail.paymentAmount : this.batchPaymentDetail.receivedAmount;
          this.paymentCalculatedAmount.setValue(receivedAmount);
          this.enableExchangeRate = false;
          if (this.batchPaymentDetail.rate && this.batchPaymentDetail.receivedCurrency) {
            this.enableExchangeRate = true;
          }
          this.paymentId = this.batchPaymentDetail.paymentId;
        }
        if (this.accountPaymentParam.accountId) {
          this.getPolicyAmountDetail();
        }
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  public checkFirstLoad(): void {
    if (!this.policyNumber) {
      this.policyPaymentSelect.dropdownItems = [];
      this.policyPaymentSelect.hasNext = false;
    }
  }

  public checkPolicyNumber(event: any): void {
    this.accountSelected = undefined;
    this.policyNumber = event && event.trim();
    if (this.policyNumber !== '') {
      this.queryParam.query = '';
      if (this.isMultiCurrencyEnable === 'true' && this.businessGroup) {
        const QUERYPARAMS = `policyNumber:${this.policyNumber},type:DirectBill,type:GroupBill,currency:${this.currency},businessGroup:${this.businessGroup}`;
        this.queryParam.filters = QUERYPARAMS;
      } else if (this.isMultiCurrencyEnable !== 'true' && this.businessGroup) {
        const QUERYPARAMS = `policyNumber:${this.policyNumber},type:DirectBill,type:GroupBill,businessGroup:${this.businessGroup}`;
        this.queryParam.filters = QUERYPARAMS;
      } else if (this.isMultiCurrencyEnable === 'true') {
        const QUERYPARAMS = `policyNumber:${this.policyNumber},type:DirectBill,type:GroupBill,currency:${this.currency}`;
        this.queryParam.filters = QUERYPARAMS;
      } else {
        const QUERYPARAMS = `policyNumber:${this.policyNumber},type:DirectBill,type:GroupBill`;
        this.queryParam.filters = QUERYPARAMS;
      }
    }
  }

  private getPolicyAmount(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getPolicyAmount(this.accountSelected.accountId, this.policyNumber).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data?.content?.length > 0) {
          this.policySymbolDropdown = data.content;
          if (data?.content?.length === 1) {
            this.policyAmount = data.content[0];
            this.policySymbol.patchValue(this.policyAmount.policySymbol);
            this.getPolicyAmountDetail();
          } else {
            if (this.modalType === 'Edit') {
              const policyAmount = data.content.filter((i: any) => {
                return (i.policySymbol === this.batchPaymentDetail.policySymbol);
              });
              if (policyAmount?.length > 0) {
                this.policyAmount = policyAmount[0];
              }
              this.policySymbol.patchValue(this.policyAmount.policySymbol);
              this.getPolicyAmountDetail();
            }
            if (data?.content?.length > 1) {
              this.policySymbolDropdown = this.getUnique(data?.content, 'policySymbol');
              if (this.policySymbolDropdown.length === 1) {
                this.policyAmount = this.policySymbolDropdown[0];
                this.policySymbol.patchValue(this.policyAmount.policySymbol);
                this.getPolicyAmountDetail();
              }
            }
          }
          if (!this.policyAmount?.policySymbol) {
            this.policySymbol.setValidators(null);
          } else {
            this.policySymbol.setValidators([Validators.required]);
          }
          this.policySymbol.updateValueAndValidity();
        }
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  getUnique(arr, comp) {

    // store the comparison  values in array
    const unique = arr.map(e => e[comp])

      // store the indexes of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the false indexes & return unique objects
      .filter((e) => arr[e]).map(e => arr[e]);

    return unique;
  }
  selectedPolicySymbol(event: any) {
    this.policyAmount = event;
    if (event) {
      this.getPolicyAmountDetail();
    }
    console.log(event);
  }

  public paymentOnHoldChanged(event?: any): void {
    this.paymentOnHold = event;
    this.batchPolicyPaymentForm.get('suspenseReason') && this.batchPolicyPaymentForm.get('suspenseReason').setValue(undefined);
    this.batchPolicyPaymentForm.removeControl('suspenseReason');
    if (event) {
      this.batchPolicyPaymentForm.addControl('suspenseReason', new FormControl('', Validators.required));
      this.suspenseReason = this.suspenseDefaultValue;
    }
  }
  private getPolicyAmountDetail(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getPolicyAmountDetail(this.accountSelected.accountId,
      this.policyAmount.policyId, this.policyAmount.policyEffectiveDate).subscribe(
        data => {
          this.amountList = [];
          this.loadingService.toggleLoadingIndicator(false);
          this.policyAmountDetail = data;
          this.amountList.push(
            {
              name: 'Current Balance: ' + this.currencyPipe.transform
                (this.policyAmountDetail.currentDue + this.policyAmountDetail.pastDue, this.currency),
              value: this.policyAmountDetail.currentDue + this.policyAmountDetail.pastDue
            }
          );

          this.amountList.push(
            {
              name: 'Total balance: ' + this.currencyPipe.transform
                (this.policyAmountDetail.futureDue + this.policyAmountDetail.currentDue + this.policyAmountDetail.pastDue, this.currency),
              value: this.policyAmountDetail.total
            }
          );
          this.setSelectPaymentAmount();
          this.getDropdownItems(PropertyName.PaymentType);
          this.getDropdownItems(PropertyName.PayableItem);
        },
        err => {
          this.commonService.handleRedirectErrorPageResponse(err);
        });
  }

  public setSelectPaymentAmount(): void{
    if (this.batchPaymentsService.modalAction === 'Add') {
      this.currentDueLabel = this.amountList[1]?.name;
      this.paymentAmount = this.policyAmountDetail?.futureDue + this.policyAmountDetail?.currentDue + this.policyAmountDetail?.pastDue;
    } else if (this.batchPaymentsService.modalAction === 'Edit') {
      const paymentAmount = this.isRateEntered(this.accountPaymentParam.receivedCurrency, this.accountPaymentParam.rate) ? this.accountPaymentParam.receivedAmount : this.accountPaymentParam.paymentAmount;
      this.currentDueLabel = this.getCurrentDueLabel(paymentAmount);
      this.paymentAmount = paymentAmount;
    }
  }

  public getCurrentDueLabel(paymentAmount: any): string{
    let amountLabel = PropertyName.otherLabel + this.currencyPipe.transform(paymentAmount, this.currency);
    const amountItem = this.amountList.find(item => Number(item.value) === Number(paymentAmount));
    (amountItem) && (amountLabel = amountItem.name);
    return amountLabel;
  }

  public setPaymentAmount(amount?: any): void {
    this.paymentAmount = amount;
    this.isUnValidPaymentAmount = false;
  }

  private getDropdownItems(propertyName?: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (!this.paymentType && this.batchPaymentsService.modalAction === 'Add' && propertyName === PropertyName.PaymentType && sd.defaultValue) {
          this.batchPolicyPaymentForm.controls['paymentType'].setValue(sd.defaultValue);
        }
        if (!this.suspenseReason && this.batchPaymentsService.modalAction === 'Add' && propertyName === PropertyName.SuspenseReason && sd.defaultValue) {
          this.suspenseDefaultValue = sd.defaultValue;
          this.batchPolicyPaymentForm.controls['suspenseReason'].setValue(this.suspenseDefaultValue);
        }
        if (!this.payableItem && this.batchPaymentsService.modalAction === 'Add' && propertyName === PropertyName.PayableItem && sd.defaultValue) {
          this.batchPolicyPaymentForm.controls['payableItem'].setValue(sd.defaultValue);
        }
      }
    }
  }

  public selectedAccount(data?: any): void {
    if (data) {
      this.resetReceivedCurrencyData();
      this.accountSelected = data;
      this.batchPolicyPaymentForm.get('policyNumber').setValue(this.policyNumber);
      this.amountList = [];
      if (this.accountSelected.policies) {
        this.policyNumber = this.policyNumber;
      } else {
        this.policyNumber = undefined;
        setTimeout(() => {
          this.alertService.error('The account number ' + data.accountNumber + ' doesn\'t have the policy. Please select another account!');
        }, 100);
      }
      if (this.accountSelected.accountId && this.policyNumber) {
        this.getPolicyAmount();
      }
    } else {
      this.accountSelected = data;
    }
  }

  private getBatchPaymentSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchPaymentsSupportData(this.batchId, this.postedDate).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.PaymentType);
          this.getDropdownItems(PropertyName.SuspenseReason);
          this.getDropdownItems(PropertyName.PayableItem);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  public policyPaymentSubmit(): void {
    this.isUnValidPaymentAmount = false;
    if (this.isValidPaymentAmountEntered()) {
      this.isUnValidPaymentAmount = true;
      return;
    }
    this.paymentModel.paymentAmount = ((this.receivedCurrency.value !== this.currency && this.isMultiCurrencyEnable === 'true') ? this.paymentCalculatedAmount.value : this.paymentAmount);
    this.paymentModel.paymentType = this.paymentType;
    this.paymentModel.policyNumber = this.policyNumber;
    this.paymentModel.paymentOnHold = this.paymentOnHold;
    this.paymentModel.suspenseReason = this.paymentOnHold ? this.suspenseReason : null;
    this.paymentModel.payableItem = this.payableItem;
    this.paymentModel.dueDate = this.dueDate;
    this.paymentModel.postmarkDate = this.postmarkDate;
    this.paymentModel.paymentComments = this.paymentComments;
    this.paymentModel.identifier = this.accountSelected.accountNumber;
    this.paymentModel.identifierType = this.paymentModel.identifier ? PropertyName.ConsumerDirect : null;
    this.paymentModel.paymentId = this.paymentId;
    this.paymentModel.policySymbol = this.policyAmount.policySymbol;
    if (!this.paymentModel.dueDate) {
      this.paymentModel.dueDate = null;
    }
    if (!this.paymentModel.postmarkDate) {
      this.paymentModel.postmarkDate = null;
    }
    if (this.dateReceived !== '') {
      this.paymentModel.paymentDate = this.dateReceived;
    }
    if (this.modalType === 'Add') {
      this.addAccountPaymentAction(this.paymentModel);
    } else if (this.modalType === 'Edit') {
      this.editAccountPaymentAction(this.paymentModel);
    }
  }

  private addAccountPaymentAction(paymentModel?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.addBatchPayments(this.batchId, this.postedDate, paymentModel).subscribe(
      (data: any) => {
        if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
          this.getTransactionRateHistory(data);
        } else {
          this.handleAccountPaymentResponse();
        }
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  private editAccountPaymentAction(paymentModel?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.editBatchPayments
      (this.batchId, this.postedDate, this.userId, this.accountPaymentParam.paymentSequenceNumber, paymentModel).subscribe(
        (data: any) => {
          if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
            this.getTransactionRateHistory(this.batchPaymentDetail);
          } else if (this.batchPaymentDetail.receivedAmount) {
            this.deleteTransactionRateHistory(this.batchPaymentDetail);
          } else {
            this.handleAccountPaymentResponse();
          }
        },
        err => {
          this.commonService.handleErrorResponse(err);
        });
  }

  private deleteTransactionRateHistory(data?: any): void {
    this.batchPaymentsService.deleteTransactionRateHistory(data).subscribe(
      () => {
        if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
          this.getTransactionRateHistory(data);
        } else {
          this.handleAccountPaymentResponse();
        }
      },
      err => {
        this.handleAccountPaymentResponse();
        this.commonService.handleErrorResponse(err);
      });
  }

  private getTransactionRateHistory(data?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    const body = {
      paymentAmount: this.paymentCalculatedAmount.value,
      paymentCurrency: this.currency,
      receivedCurrency: this.receivedCurrency.value,
      receivedAmount: this.paymentAmount,
      rate: this.exchangeRate.value
    };
    this.batchPaymentsService.getTransactionRateHistory(data, body).subscribe(
      () => {
        this.handleAccountPaymentResponse();
      },
      err => {
        this.handleAccountPaymentResponse();
        this.commonService.handleErrorResponse(err);
      });
  }

  private handleAccountPaymentResponse(): void {
    this.loadingService.toggleLoadingIndicator(false);
    this.batchPaymentsService.paramsToModal = null;
    this.modal.close(true);
  }
  private isValidPaymentAmountEntered(): boolean {
    if (this.paymentAmount <= 0) {
      return true;
    }
    return false;
  }

  private isRateEntered(receivedCurrency?: string, exchangeRate?: any): boolean {
    return (receivedCurrency !== this.currency && (Number(exchangeRate) > 0));
  }

  public resetDateValidate(event?: any, type?: any): void {
    if (event && type) {
      if (event.value > this.systemDate && event.selectionDayTxt) {
        this[type + 'Valid'] = true;
      } else {
        this[type + 'Valid'] = false;
      }
    }
  }

  updateAmount(value: any) {
    if (value) {
      this.isUnValidPaymentAmount = false;
      const paymentCalculatedAmount = Number(this.paymentAmount) * Number(this.exchangeRate.value);
      const roundOffValue = this.roundOff(paymentCalculatedAmount, 2);
      this.paymentCalculatedAmount.setValue(roundOffValue);
    }
  }

  roundOff(num: any, places: any) {
    const x = Math.pow(10, places);
    return Math.round(num * x) / x;
  }

  public getReceivedCurrency(fromCurrency: any): any {
    this.isUnValidPaymentAmount = false;
    if (fromCurrency && fromCurrency !== this.currency) {
      this.loadingService.toggleLoadingIndicator(true);
      this.batchPaymentsService.getExchangeRates(fromCurrency, this.currency, this.postedDate).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          if (data) {
            this.enableExchangeRate = true;
            const rate = data.rate ? data.rate : 1;
            this.exchangeRate.setValue(rate);
            const paymentCalculatedAmount = Number(this.paymentAmount) * Number(this.exchangeRate.value);
            this.paymentCalculatedAmount.setValue(paymentCalculatedAmount);
          }
        },
        (err) => {
          this.loadingService.toggleLoadingIndicator(false);
          this.commonService.handleErrorResponse(err);
        });
    } else {
      this.exchangeRate.setValue(null);
      this.enableExchangeRate = false;
      this.paymentCalculatedAmount.setValue(null);
      (this.paymentAmount) && (this.currentDueLabel = this.getCurrentDueLabel(this.paymentAmount));
    }
  }

  public clearPaymentAmount() : void {
    this.isUnValidPaymentAmount = false;
  }

  get receivedCurrency() { return this.batchPolicyPaymentForm.get('receivedCurrency'); }
  get exchangeRate() { return this.batchPolicyPaymentForm.get('exchangeRate'); }
  get paymentCalculatedAmount() { return this.batchPolicyPaymentForm.get('paymentCalculatedAmount'); }
  get policySymbol() { return this.batchPolicyPaymentForm.get('policySymbol'); }
}
