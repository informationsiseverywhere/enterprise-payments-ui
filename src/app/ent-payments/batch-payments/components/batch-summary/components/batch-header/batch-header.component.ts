import { Component, OnInit, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { AccountPaymentComponent } from './account-payment/account-payment.component';
import { PolicyPaymentComponent } from './policy-payment/policy-payment.component';
import { GroupPaymentComponent } from './group-payment/group-payment.component';
import { UnidentifiedPaymentComponent } from './unidentified-payment/unidentified-payment.component';
import { AgencyPaymentComponent } from './agency-payment/agency-payment.component';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { BatchModel } from 'src/app/ent-payments/batch-payments/models/batch.model';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';

@Component({
  selector: 'app-batch-header',
  templateUrl: './batch-header.component.html',
  styleUrls: ['./batch-header.component.scss']
})
export class BatchHeaderComponent implements OnInit {
  @Input() currency: any;
  @Input() multiCurrencyEnabled: any;
  @Input() batchId: any;
  @Input() userId: any;
  @Input() postedDate: any;
  @Input() controlBank: any;
  @Input() depositBank: any;
  @Input() businessGroup: any;
  @Input() systemDate: string;
  @Input() isBatchDetailLoaded = false;
  @Input() isShowHeader = false;
  @Output() reloadSummary: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('batchPaymentsEditModal') batchPaymentsEditModal: ModalComponent;
  @ViewChild('batchAccountPaymentModal') batchAccountPaymentModal: ModalComponent;
  @ViewChild('batchPolicyPaymentModal') batchPolicyPaymentModal: ModalComponent;
  @ViewChild('batchGroupPaymentModal') batchGroupPaymentModal: ModalComponent;
  @ViewChild('batchAgencyPaymentModal') batchAgencyPaymentModal: ModalComponent;
  @ViewChild('batchUnidentifiedPaymentModal') batchUnidentifiedPaymentModal: ModalComponent;

  constructor(
    public appConfig: AppConfiguration,
    public securityEngineService: SecurityEngineService,
    private batchPaymentsService: BatchPaymentsService,
    private paymentSecurityConfigService: PaymentSecurityConfigService) { }

  ngOnInit() {
    this.batchPaymentsService.systemDate = this.systemDate;
  }

  public performAction(action: string): void {
    const batchPayment: BatchModel = new BatchModel();
    batchPayment.batchId = this.batchId;
    batchPayment.postedDate = this.postedDate;
    batchPayment.businessGroup = this.businessGroup;
    switch (action) {
      case 'accountPayment':
        this.openBatchAccountPaymentModal(batchPayment);
        break;
      case 'groupPayment':
        this.openBatchGroupPaymentModal(batchPayment);
        break;
      case 'agencyPayment':
        this.openBatchAgencyPaymentModal(batchPayment);
        break;
      case 'policyPayment':
        this.openBatchPolicyPaymentModal(batchPayment);
        break;
      case 'unidentifiedPayment':
        this.openBatchUnidentifiedPaymentModal(batchPayment);
        break;
    }
  }

  openBatchAccountPaymentModal(batchPayment: BatchModel) {
    this.batchAccountPaymentModal.modalTitle = 'Account Payment';
    this.batchAccountPaymentModal.modalFooter = false;
    this.batchAccountPaymentModal.okButtonText = 'SUBMIT';
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchPaymentsService.modalAction = 'Add';
    this.batchAccountPaymentModal.icon = 'assets/images/model-icons/account-payment.svg';
    this.batchAccountPaymentModal.open(AccountPaymentComponent);
  }

  openBatchPolicyPaymentModal(batchPayment: BatchModel) {
    this.batchPolicyPaymentModal.modalTitle = 'Policy Payment';
    this.batchPolicyPaymentModal.modalFooter = false;
    this.batchPolicyPaymentModal.okButtonText = 'SUBMIT';
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = 'Add';
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchPolicyPaymentModal.icon = 'assets/images/model-icons/policy-payment.svg';
    this.batchPolicyPaymentModal.open(PolicyPaymentComponent);
  }

  openBatchUnidentifiedPaymentModal(batchPayment: BatchModel) {
    this.batchUnidentifiedPaymentModal.modalTitle = 'Express Entry';
    this.batchUnidentifiedPaymentModal.modalFooter = false;
    this.batchUnidentifiedPaymentModal.okButtonText = 'SUBMIT';
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = 'Add';
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchUnidentifiedPaymentModal.icon = 'assets/images/options/unidentified-payment-header.svg';
    this.batchUnidentifiedPaymentModal.open(UnidentifiedPaymentComponent);
  }

  public getAccountPayment(event: any): void {
    if (event) {
      this.reloadSummary.emit(true);
      if (this.batchPaymentsService.addAnotherPayment) {
        this.batchPaymentsService.addAnotherPayment = false;
        const batchPayment: BatchModel = new BatchModel();
        batchPayment.batchId = this.batchId;
        batchPayment.postedDate = this.postedDate;
        setTimeout(() => {
          this.openBatchUnidentifiedPaymentModal(batchPayment);
        }, 500);
      } else {
        this.batchPaymentsService.searchType = null;
      }
    } else {
      this.batchPaymentsService.searchType = null;
    }
  }

  openBatchGroupPaymentModal(batchPayment: BatchModel) {
    this.batchGroupPaymentModal.modalTitle = 'Group Payment';
    this.batchGroupPaymentModal.modalFooter = false;
    this.batchGroupPaymentModal.okButtonText = 'SUBMIT';
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = 'Add';
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchGroupPaymentModal.icon = 'assets/images/model-icons/group-payment.svg';
    this.batchGroupPaymentModal.open(GroupPaymentComponent);
  }

  openBatchAgencyPaymentModal(batchPayment: BatchModel) {
    this.batchAgencyPaymentModal.modalTitle = 'Agency Payment';
    this.batchAgencyPaymentModal.modalFooter = false;
    this.batchAgencyPaymentModal.okButtonText = 'SUBMIT';
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = 'Add';
    this.batchPaymentsService.currency = this.currency;
    this.batchPaymentsService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.batchAgencyPaymentModal.icon = 'assets/images/model-icons/agency-payment.svg';
    this.batchAgencyPaymentModal.open(AgencyPaymentComponent);
  }

  public openSecurity(event?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event);
  }
}
