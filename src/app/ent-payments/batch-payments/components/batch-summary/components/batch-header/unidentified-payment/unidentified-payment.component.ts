import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { DateService } from 'src/app/shared/services/date.service';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { PaymentModel } from 'src/app/ent-payments/batch-payments/models/batch.model';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

const ACCOUNTPAYMENT = 'AccountPayment';
const GROUPPAYMENT = 'GroupPayment';
const POLICYPAYMENT = 'PolicyPayment';
const AGENCYPAYMENT = 'AgencyPayment';

@Component({
  selector: 'app-unidentified-payment',
  templateUrl: './unidentified-payment.component.html'
})

export class UnidentifiedPaymentComponent implements OnInit, AfterViewInit {
  public searchType: string;
  public batchUnidentifiedPaymentForm: FormGroup;
  private supportData: any;
  private postedDate: any;
  private batchId: any;
  private batchParam: any;
  public PaymentTypeDropdown: any;
  public PayableItemDropdown: any;
  public systemDate: any;
  public dateReceived: any;
  public paymentType: any;
  public paymentAmount: any;
  public accountNumber: any;
  public groupAccountNumber: any;
  public agencyAccountNumber: any;
  public PolicySymbolDropdown: any;
  public policySymbol: any;
  public paymentComments: any;
  public policyNumber: any;
  private paymentModel: PaymentModel = new PaymentModel();
  public isUnValidPaymentAmount = false;
  public paymentId: string;
  public paymentOnHold = false;
  public suspenseReason: any;
  public SuspenseReasonDropdown: any;
  public payableItem: any;
  public postmarkDate: any;
  public dueDate: any;
  public dateReceivedValid = false;
  public postmarkDateValid = false;
  public policyAccountNumber: any;
  public suspenseDefaultValue: any;
  public paymentTypeDefaultValue: any;
  public policySymbolDefaultValue: any;
  public payableItemDefaultValue: any;
  @ViewChild('expressEntries', { static: false }) expressEntries: ElementRef;

  constructor(
    private fb: FormBuilder,
    private modal: ModalComponent,
    private batchPaymentsService: BatchPaymentsService,
    public commonService: CommonService,
    private loadingService: LoadingService,
    public securityEngineService: SecurityEngineService,
    private dateService: DateService) { }

  ngOnInit() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    if (this.batchPaymentsService.searchType) {
      this.searchType = this.batchPaymentsService.searchType;
    } else {
      this.searchType = ACCOUNTPAYMENT;
    }
    this.batchUnidentifiedPaymentForm = this.fb.group({
      paymentType: ['', Validators.required],
      dateReceived: ['', ValidationService.dateValidator],
      paymentAmount: ['', Validators.required],
      paymentId: ['', Validators.maxLength(10)],
      accountNumber: ['', Validators.compose([Validators.required, Validators.maxLength(30)])],
      groupAccountNumber: ['', Validators.compose([Validators.required, Validators.maxLength(20)])],
      agencyAccountNumber: ['', Validators.compose([Validators.required, Validators.maxLength(20)])],
      policySymbol: [''],
      policyNumber: ['', Validators.compose([Validators.required, Validators.maxLength(25)])],
      suspenseReason: [''],
      paymentOnHold: [false],
      payableItem: [''],
      dueDate: ['', ValidationService.dateValidator],
      postmarkDate: ['', ValidationService.dateValidator],
      policyAccountNumber: [''],
      paymentComments: ['', Validators.maxLength(80)]
    });
    if (this.batchPaymentsService.paramsToModal) {
      this.batchParam = this.batchPaymentsService.paramsToModal;
      this.postedDate = this.batchParam.postedDate;
      this.batchId = this.batchParam.batchId;
    }
    this.getBatchPaymentSupportData();
    this.showHideControl();
    this.dateReceived = this.systemDate;
  }

  ngAfterViewInit() {
    this.checkSecurityOptions();
  }

  public showActiveControls(selectedValue?: string): void {
    this.searchType = selectedValue;
    this.batchPaymentsService.searchType = selectedValue;
    this.showHideControl();
  }
  public submitAddAnotherPayment(): void {
    this.batchPaymentsService.addAnotherPayment = true;
    this.batchPaymentsService.searchType = undefined;
    this.unidentifiedPaymentSubmit();
  }
  public unidentifiedPaymentSubmit(): void {
    this.isUnValidPaymentAmount = false;
    if (this.isValidPaymentAmountEntered()) {
      this.isUnValidPaymentAmount = true;
      return;
    }
    this.paymentModel = new PaymentModel();
    this.paymentModel.paymentAmount = this.paymentAmount;
    this.paymentModel.paymentType = this.paymentType;
    this.paymentModel.paymentId = this.paymentId;
    this.paymentModel.paymentOnHold = this.paymentOnHold;
    this.paymentModel.suspenseReason = this.paymentOnHold ? this.suspenseReason : null;
    this.paymentModel.payableItem = this.payableItem;
    this.paymentModel.dueDate = this.dueDate;
    this.paymentModel.postmarkDate = this.postmarkDate;
    this.paymentModel.paymentComments = this.paymentComments;
    if (!this.paymentModel.dueDate) {
      this.paymentModel.dueDate = null;
    }
    if (!this.paymentModel.postmarkDate) {
      this.paymentModel.postmarkDate = null;
    }
    if (this.dateReceived !== '') {
      this.paymentModel.paymentDate = this.dateReceived;
    }
    if (this.searchType === ACCOUNTPAYMENT) {
      this.paymentModel.identifierType = 'Consumer Direct';
      this.paymentModel.identifier = this.accountNumber;
    } else if (this.searchType === GROUPPAYMENT) {
      this.paymentModel.identifierType = 'Group';
      this.paymentModel.identifier = this.groupAccountNumber;
    } else if (this.searchType === AGENCYPAYMENT) {
      this.paymentModel.identifierType = 'Agency';
      this.paymentModel.identifier = this.agencyAccountNumber;
    } else if (this.searchType === POLICYPAYMENT) {
      this.paymentModel.policyNumber = this.policyNumber.toUpperCase();
      this.paymentModel.identifier = (this.policyAccountNumber === '' ? null : this.policyAccountNumber);
      this.paymentModel.identifierType = this.policyAccountNumber ? 'Consumer Direct' : null;
      if (this.policySymbol !== '') {
        this.paymentModel.policySymbol = this.policySymbol;
      }
    }
    this.addUnidentifiedPaymentAction(this.paymentModel);
  }

  public addUnidentifiedPaymentAction(paymentModel?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.addBatchPayments(this.batchId, this.postedDate, paymentModel).subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.batchPaymentsService.paramsToModal = null;
        this.modal.close(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  private showHideControl(): void {
    this.isUnValidPaymentAmount = false;
    this.batchUnidentifiedPaymentForm.reset();
    if (this.searchType === ACCOUNTPAYMENT) {
      this.batchUnidentifiedPaymentForm.controls['groupAccountNumber'] && this.batchUnidentifiedPaymentForm.controls['groupAccountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.controls['agencyAccountNumber'] && this.batchUnidentifiedPaymentForm.controls['agencyAccountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.controls['policyNumber'] && this.batchUnidentifiedPaymentForm.controls['policyNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.addControl('accountNumber', new FormControl('', Validators.compose([Validators.required, Validators.maxLength(30)])));
      this.batchUnidentifiedPaymentForm.removeControl('groupAccountNumber');
      this.batchUnidentifiedPaymentForm.removeControl('policyNumber');
      this.batchUnidentifiedPaymentForm.removeControl('agencyAccountNumber');
      this.batchUnidentifiedPaymentForm.controls['paymentType'].setValue(this.paymentTypeDefaultValue);
      this.batchUnidentifiedPaymentForm.controls['policySymbol'].setValue(this.policySymbolDefaultValue);
      this.batchUnidentifiedPaymentForm.controls['payableItem'].setValue(this.payableItemDefaultValue);
      this.batchUnidentifiedPaymentForm.updateValueAndValidity();

    } else if (this.searchType === GROUPPAYMENT) {
      this.batchUnidentifiedPaymentForm.controls['agencyAccountNumber'] && this.batchUnidentifiedPaymentForm.controls['agencyAccountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.controls['accountNumber'] && this.batchUnidentifiedPaymentForm.controls['accountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.controls['policyNumber'] && this.batchUnidentifiedPaymentForm.controls['policyNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.addControl('groupAccountNumber', new FormControl('', Validators.compose([Validators.required, Validators.maxLength(20)])));
      this.batchUnidentifiedPaymentForm.removeControl('accountNumber');
      this.batchUnidentifiedPaymentForm.removeControl('policyNumber');
      this.batchUnidentifiedPaymentForm.removeControl('agencyAccountNumber');
      this.batchUnidentifiedPaymentForm.controls['paymentType'].setValue(this.paymentTypeDefaultValue);
      this.batchUnidentifiedPaymentForm.controls['policySymbol'].setValue(this.policySymbolDefaultValue);
      this.batchUnidentifiedPaymentForm.controls['payableItem'].setValue(this.payableItemDefaultValue);
      this.batchUnidentifiedPaymentForm.updateValueAndValidity();
    } else if (this.searchType === AGENCYPAYMENT) {
      this.batchUnidentifiedPaymentForm.controls['groupAccountNumber'] && this.batchUnidentifiedPaymentForm.controls['groupAccountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.controls['accountNumber'] && this.batchUnidentifiedPaymentForm.controls['accountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.controls['policyNumber'] && this.batchUnidentifiedPaymentForm.controls['policyNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.addControl('agencyAccountNumber', new FormControl('', Validators.compose([Validators.required, Validators.maxLength(20)])));
      this.batchUnidentifiedPaymentForm.removeControl('accountNumber');
      this.batchUnidentifiedPaymentForm.removeControl('policyNumber');
      this.batchUnidentifiedPaymentForm.removeControl('groupAccountNumber');
      this.batchUnidentifiedPaymentForm.controls['paymentType'].setValue(this.paymentTypeDefaultValue);
      this.batchUnidentifiedPaymentForm.controls['policySymbol'].setValue(this.policySymbolDefaultValue);
      this.batchUnidentifiedPaymentForm.controls['payableItem'].setValue(this.payableItemDefaultValue);
      this.batchUnidentifiedPaymentForm.updateValueAndValidity();

    } else if (this.searchType === POLICYPAYMENT) {
      this.batchUnidentifiedPaymentForm.controls['agencyAccountNumber'] && this.batchUnidentifiedPaymentForm.controls['agencyAccountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.controls['accountNumber'] && this.batchUnidentifiedPaymentForm.controls['accountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.controls['groupAccountNumber'] && this.batchUnidentifiedPaymentForm.controls['groupAccountNumber'].setValue(undefined);
      this.batchUnidentifiedPaymentForm.addControl('policyNumber', new FormControl('', Validators.compose([Validators.required, Validators.maxLength(25)])));
      this.batchUnidentifiedPaymentForm.removeControl('accountNumber');
      this.batchUnidentifiedPaymentForm.removeControl('groupAccountNumber');
      this.batchUnidentifiedPaymentForm.removeControl('agencyAccountNumber');
      this.batchUnidentifiedPaymentForm.controls['paymentType'].setValue(this.paymentTypeDefaultValue);
      this.policySymbol = this.policySymbolDefaultValue
      this.payableItem = this.payableItemDefaultValue;
      this.batchUnidentifiedPaymentForm.updateValueAndValidity();
    }
    this.paymentAmount = '';
    this.batchUnidentifiedPaymentForm.controls['dateReceived'] && this.batchUnidentifiedPaymentForm.controls['dateReceived'].setValue(this.systemDate);
  }

  private getBatchPaymentSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchPaymentsSupportData(this.batchId, this.postedDate).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.PaymentType);
          this.getDropdownItems(PropertyName.PolicySymbol);
          this.getDropdownItems(PropertyName.SuspenseReason);
          this.getDropdownItems(PropertyName.PayableItem);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  public paymentOnHoldChanged(event?: any): void {
    this.paymentOnHold = event;
    this.batchUnidentifiedPaymentForm.get('suspenseReason') && this.batchUnidentifiedPaymentForm.get('suspenseReason').setValue(undefined);
    this.batchUnidentifiedPaymentForm.removeControl('suspenseReason');
    if (event) {
      this.batchUnidentifiedPaymentForm.addControl('suspenseReason', new FormControl('', Validators.required));
      this.suspenseReason = this.suspenseDefaultValue
    }
  }
  private getDropdownItems(propertyName?: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (!this.paymentType && propertyName === PropertyName.PaymentType && sd.defaultValue) {
          this.paymentTypeDefaultValue = sd.defaultValue;
          this.batchUnidentifiedPaymentForm.controls['paymentType'].setValue(this.paymentTypeDefaultValue);
        }
        if (!this.suspenseReason && propertyName === PropertyName.SuspenseReason && sd.defaultValue) {
          this.suspenseDefaultValue = sd.defaultValue;
          this.batchUnidentifiedPaymentForm.controls['suspenseReason'].setValue(this.suspenseDefaultValue);
        }
        if (!this.policySymbol && propertyName === PropertyName.PolicySymbol && sd.defaultValue) {
          this.policySymbolDefaultValue = sd.defaultValue;
          this.batchUnidentifiedPaymentForm.controls['policySymbol'].setValue(this.policySymbolDefaultValue);
        }
        if (!this.payableItem && propertyName === PropertyName.PayableItem && sd.defaultValue) {
          this.payableItemDefaultValue = sd.defaultValue
          this.batchUnidentifiedPaymentForm.controls['payableItem'].setValue(this.payableItemDefaultValue);
        }
      }
    }
  }

  private isValidPaymentAmountEntered(): boolean {
    if (this.paymentAmount <= 0) {
      return true;
    }
    return false;
  }

  public resetDateValidate(event?: any, type?: any): void {
    if (event && type) {
      if (event.value > this.systemDate && event.selectionDayTxt) {
        this[type + 'Valid'] = true;
      } else {
        this[type + 'Valid'] = false;
      }
    }
  }

  public resetMountValidate(event?: any): void {
    this.isUnValidPaymentAmount = false;
  }

  public convertToUpperCase(event?: any, property?: any): void {
    if (event) { this[property] = event; }
  }

  public getSecurityNameByPaymentType(fieldNameSuffix?: any): any {
    let typeName: string = "";
    switch (this.searchType) {
      case ACCOUNTPAYMENT:
        typeName = "expEntDba";
        break;
      case GROUPPAYMENT:
        typeName = "expEntGA";
        break;
      case AGENCYPAYMENT:
        typeName = "expEntAgency";
        break;
      case POLICYPAYMENT:
        typeName = "expEntPol";
        break;
    }
    return typeName + fieldNameSuffix;
  }

  public checkSecurityOptions(): any {
    if (this.expressEntries) {
      const entries = this.expressEntries.nativeElement.children;
      for (let i = 0; i < entries.length; i++) {
        if (!entries[i].classList.contains('disable-security-options')) {
          (entries[i]?.getAttribute('id')) && (this.searchType = entries[i]?.getAttribute('id'));
          break;
        }
      }
    }
  }
}
