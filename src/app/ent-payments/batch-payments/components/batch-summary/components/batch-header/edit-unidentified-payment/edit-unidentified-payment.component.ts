import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { DateService } from 'src/app/shared/services/date.service';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { PaymentModel } from 'src/app/ent-payments/batch-payments/models/batch.model';
import { PropertyName } from 'src/app/shared/enum/common.enum';

const ACCOUNTPAYMENT = 'AccountPayment';
const GROUPPAYMENT = 'Group';
const POLICYPAYMENT = 'PolicyPayment';
const AGENCYPAYMENT = 'AgencyPayment';

@Component({
  selector: 'app-edit-unidentified-payment',
  templateUrl: './edit-unidentified-payment.component.html'
})

export class EditUnidentifiedPaymentComponent implements OnInit {
  public searchType: string;
  public editBatchUnidentifiedPaymentForm: FormGroup;
  private supportData: any;
  private postedDate: any;
  private batchId: any;
  private userId: any;
  private batchParam: any;
  public PaymentTypeDropdown: any;
  public systemDate: any;
  public dateReceived: any;
  public paymentType: any;
  public paymentAmount: any;
  public accountNumber: any;
  public groupAccountNumber: any;
  public PolicySymbolDropdown: any;
  public policySymbol: any;
  public policyNumber: any;
  public agencyAccountNumber: any;
  private modalAction: any;
  public paymentId: any;
  private paymentModel: PaymentModel = new PaymentModel();
  public isUnValidPaymentAmount = false;
  public paymentOnHold = false;
  public suspenseReason: any;
  public SuspenseReasonDropdown: any;
  public payableItem: any;
  public dueDate: any;
  public postmarkDate: any;
  public dateReceivedValid = false;
  public postmarkDateValid = false;
  public PayableItemDropdown: any;
  public batchPaymentDetail: any;
  public policyAccountNumber: any;
  public paymentComments: any;
  constructor(
    private fb: FormBuilder,
    private modal: ModalComponent,
    private batchPaymentsService: BatchPaymentsService,
    public commonService: CommonService,
    private dateService: DateService,
    private loadingService: LoadingService) { }

  ngOnInit() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.editBatchUnidentifiedPaymentForm = this.fb.group({
      paymentType: ['', Validators.required],
      dateReceived: ['', ValidationService.dateValidator],
      paymentAmount: ['', Validators.required],
      paymentId: [''],
      accountNumber: ['', Validators.required],
      groupAccountNumber: ['', Validators.required],
      agencyAccountNumber: ['', Validators.required],
      policySymbol: [''],
      policyNumber: ['', Validators.required],
      suspenseReason: [''],
      paymentOnHold: [false],
      payableItem: [''],
      dueDate: ['', ValidationService.dateValidator],
      postmarkDate: ['', ValidationService.dateValidator],
      policyAccountNumber: [''],
      paymentComments: ['']
    });
    if (this.batchPaymentsService.paramsToModal) {
      this.batchParam = this.batchPaymentsService.paramsToModal;
      this.postedDate = this.batchParam.postedDate;
      this.batchId = this.batchParam.batchId;
      this.userId = this.batchParam.userId;
      this.modalAction = this.batchPaymentsService.modalAction;
      this.searchType = this.modalAction;
      const linkName = 'payment';
      this.getBatchPaymentDetailFromLinks(linkName);
    }
    this.showHideControl();
    this.dateReceived = this.systemDate;
    this.getBatchPaymentSupportData();
  }


  private getBatchPaymentDetailFromLinks(linkName?: any) : void {
    this.loadingService.toggleLoadingIndicator(true);
    this.commonService.getDatafromDetailLink(this.batchParam, linkName, 'GET').subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.batchPaymentDetail = data;
        if(this.batchPaymentDetail) {
          if (this.modalAction === 'AccountPayment') {
            this.accountNumber = this.batchPaymentDetail.identifier;
          } else if (this.modalAction === 'Group') {
            this.groupAccountNumber = this.batchPaymentDetail.identifier;
          } else if (this.modalAction === 'AgencyPayment') {
            this.agencyAccountNumber = this.batchPaymentDetail.identifier;
          } else if (this.modalAction === 'PolicyPayment') {
            this.policyAccountNumber = this.batchPaymentDetail.identifier;
            this.policySymbol = this.batchPaymentDetail.policySymbol;
            this.policyNumber = this.batchPaymentDetail.policyNumber;
          }
          this.dateReceived = this.batchPaymentDetail.paymentDate;
          this.paymentId = this.batchPaymentDetail.paymentId;
          this.paymentAmount = this.batchPaymentDetail.paymentAmount;
          this.paymentType = this.batchPaymentDetail.paymentType;
          this.paymentOnHold = this.batchPaymentDetail.paymentOnHold;
          this.suspenseReason = this.batchPaymentDetail.suspenseReason;
          this.payableItem = this.batchPaymentDetail.payableItem;
          this.dueDate = this.batchPaymentDetail.dueDate;
          this.postmarkDate = this.batchPaymentDetail.postmarkDate;
          this.paymentComments = this.batchPaymentDetail.paymentComments;
        }
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  public editUnidentifiedPaymentSubmit(): void {
    this.isUnValidPaymentAmount = false;
    if (this.isValidPaymentAmountEntered()) {
      this.isUnValidPaymentAmount = true;
      return;
    }
    this.paymentModel = new PaymentModel();

    this.paymentModel.paymentId = this.paymentId;
    this.paymentModel.paymentAmount = this.paymentAmount;
    this.paymentModel.paymentType = this.paymentType;
    this.paymentModel.paymentOnHold = this.paymentOnHold;
    this.paymentModel.suspenseReason = this.paymentOnHold ? this.suspenseReason : null;
    this.paymentModel.payableItem = this.payableItem;
    this.paymentModel.dueDate = this.dueDate;
    this.paymentModel.postmarkDate = this.postmarkDate;
    this.paymentModel.paymentComments = this.paymentComments;
    if (this.dateReceived !== '') {
      this.paymentModel.paymentDate = this.dateReceived;
    }
    if (!this.paymentModel.dueDate) {
      this.paymentModel.dueDate = null;
    }
    if (!this.paymentModel.postmarkDate) {
      this.paymentModel.postmarkDate = null;
    }
    if (this.searchType === ACCOUNTPAYMENT) {
      this.paymentModel.identifierType = 'Consumer Direct';
      this.paymentModel.identifier = this.accountNumber;
    } else if (this.searchType === GROUPPAYMENT) {
      this.paymentModel.identifierType = 'Group';
      this.paymentModel.identifier = this.groupAccountNumber;
    } else if (this.searchType === AGENCYPAYMENT) {
      this.paymentModel.identifierType = 'Agency';
      this.paymentModel.identifier = this.agencyAccountNumber;
    } else if (this.searchType === POLICYPAYMENT) {
      this.paymentModel.policyNumber = this.policyNumber;
      this.paymentModel.identifier = this.policyAccountNumber;
      this.paymentModel.identifierType = this.policyAccountNumber ? 'Consumer Direct' : null;
      if (this.policySymbol !== '') {
        this.paymentModel.policySymbol = this.policySymbol;
      }
    }

    this.addUnidentifiedPaymentAction(this.paymentModel);
  }

  private addUnidentifiedPaymentAction(paymentModel?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.editBatchPayments(this.batchId, this.postedDate, this.userId, this.batchParam.paymentSequenceNumber, paymentModel).subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.batchPaymentsService.paramsToModal = null;
        this.modal.close(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  private showHideControl(): void {
    if (this.searchType === ACCOUNTPAYMENT) {
      this.editBatchUnidentifiedPaymentForm.controls['groupAccountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['groupAccountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['agencyAccountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['agencyAccountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['policySymbol'] && this.editBatchUnidentifiedPaymentForm.controls['policySymbol'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['policyNumber'] && this.editBatchUnidentifiedPaymentForm.controls['policyNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.addControl('accountNumber', new FormControl('', Validators.required));
      this.editBatchUnidentifiedPaymentForm.removeControl('groupAccountNumber');
      this.editBatchUnidentifiedPaymentForm.removeControl('policySymbol');
      this.editBatchUnidentifiedPaymentForm.removeControl('policyNumber');
      this.editBatchUnidentifiedPaymentForm.removeControl('agencyAccountNumber');
    } else if (this.searchType === GROUPPAYMENT) {
      this.editBatchUnidentifiedPaymentForm.controls['accountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['accountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['agencyAccountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['agencyAccountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['policySymbol'] && this.editBatchUnidentifiedPaymentForm.controls['policySymbol'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['policyNumber'] && this.editBatchUnidentifiedPaymentForm.controls['policyNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.addControl('groupAccountNumber', new FormControl('', Validators.required));
      this.editBatchUnidentifiedPaymentForm.removeControl('accountNumber');
      this.editBatchUnidentifiedPaymentForm.removeControl('policySymbol');
      this.editBatchUnidentifiedPaymentForm.removeControl('policyNumber');
      this.editBatchUnidentifiedPaymentForm.removeControl('agencyAccountNumber');
    } else if (this.searchType === AGENCYPAYMENT) {
      this.editBatchUnidentifiedPaymentForm.controls['accountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['accountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['policySymbol'] && this.editBatchUnidentifiedPaymentForm.controls['policySymbol'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['policyNumber'] && this.editBatchUnidentifiedPaymentForm.controls['policyNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['groupAccountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['groupAccountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.addControl('agencyAccountNumber', new FormControl('', Validators.required));
      this.editBatchUnidentifiedPaymentForm.removeControl('accountNumber');
      this.editBatchUnidentifiedPaymentForm.removeControl('policySymbol');
      this.editBatchUnidentifiedPaymentForm.removeControl('policyNumber');
      this.editBatchUnidentifiedPaymentForm.removeControl('groupAccountNumber');
    } else if (this.searchType === POLICYPAYMENT) {
      this.editBatchUnidentifiedPaymentForm.controls['accountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['accountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['agencyAccountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['agencyAccountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.controls['groupAccountNumber'] && this.editBatchUnidentifiedPaymentForm.controls['groupAccountNumber'].setValue(undefined);
      this.editBatchUnidentifiedPaymentForm.addControl('policySymbol', new FormControl(''));
      this.editBatchUnidentifiedPaymentForm.addControl('policyNumber', new FormControl('', Validators.required));
      this.editBatchUnidentifiedPaymentForm.removeControl('accountNumber');
      this.editBatchUnidentifiedPaymentForm.removeControl('groupAccountNumber');
      this.editBatchUnidentifiedPaymentForm.removeControl('agencyAccountNumber');
    }
  }

  private getBatchPaymentSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchPaymentsSupportData(this.batchId, this.postedDate).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.PaymentType);
          this.getDropdownItems(PropertyName.PolicySymbol);
          this.getDropdownItems(PropertyName.SuspenseReason);
          this.getDropdownItems(PropertyName.PayableItem);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }
  public paymentOnHoldChanged(event?: any): void {
    this.paymentOnHold = event;
    this.editBatchUnidentifiedPaymentForm.controls['suspenseReason'] && this.editBatchUnidentifiedPaymentForm.controls['suspenseReason'].setValue(undefined);
    this.editBatchUnidentifiedPaymentForm.removeControl('suspenseReason');
    if (event) {
      this.editBatchUnidentifiedPaymentForm.addControl('suspenseReason', new FormControl('', Validators.required));
    }
  }
  private getDropdownItems(propertyName?: string): void {
    debugger
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
  }

  private isValidPaymentAmountEntered(): boolean {
    if (this.paymentAmount <= 0) {
      return true;
    }
    return false;
  }
  public resetDateValidate(event?: any, type?: any): void {
    if (event && type) {
      if (event.value > this.systemDate && event.selectionDayTxt) {
        this[type + 'Valid'] = true;
      } else {
        this[type + 'Valid'] = false;
      }
    }
  }

  public resetMountValidate(event?: any): void {
    this.isUnValidPaymentAmount = false;
  }
  public convertToUpperCase(event?: any, property?: any): void {
    if (event) { this[property] = event; }
  }
}
