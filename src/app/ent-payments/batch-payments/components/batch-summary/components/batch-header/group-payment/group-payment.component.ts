import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { QueryParam } from 'src/app/libs/select/models/select.model';
import { SelectComponent } from 'src/app/libs/select/select.component';
import { DropdownComponent } from 'src/app/libs/dropdown/dropdown.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { DateService } from 'src/app/shared/services/date.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { PaymentModel } from 'src/app/ent-payments/batch-payments/models/batch.model';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-group-payment',
  templateUrl: './group-payment.component.html',
  providers: [DatePipe, CurrencyPipe]
})
export class GroupPaymentComponent implements OnInit, AfterViewInit {
  systemDate: any;
  supportData: any;
  batchGroupPaymentForm: FormGroup;
  apiUrl: string;
  paymentAmount: any;
  amountList: any = [];
  queryParam: any;
  queryParamDueDate: QueryParam = new QueryParam('', null, null, 1, 10);
  accountId: any;
  accountSelected: any;
  accountBalanceDetail: any;
  currentDueLabel: any;
  PaymentTypeDropdown: any;
  paymentType: any;
  batchParam: any;
  postedDate: any;
  paymentComments: any;
  batchId: any;
  accountNumber: any;
  dateReceived: any;
  paymentModel: PaymentModel = new PaymentModel();
  accountPaymentParam: any;
  modalType: any;
  statementDueDateDropdown: any;
  statementDueDate: any;
  statementDueDateList: any;
  isOtherAmount = false;
  statementDueDateFormat: any;
  paymentDetail: any;
  isUnValidPaymentAmount = false;
  isUnValidDateReceived = false;
  postmarkDate: any;
  businessGroup: any;
  public SuspenseReasonDropdown: any;
  @ViewChild('selectDueDate') selectDueDate: SelectComponent;
  @ViewChild('selectPaymentAmount') selectPaymentAmount: DropdownComponent;
  paymentOnHold = false;
  suspenseReason: any;
  public suspenseDefaultValue: any;
  public batchPaymentDetail: any;
  public isMultiCurrencyEnable: any;
  public currency: any;
  public currencyDropdown: any;
  public enableExchangeRate = false;
  public paymentId: any;
  public userId: any;
  constructor(
    private loadingService: LoadingService,
    private fb: FormBuilder,
    private modal: ModalComponent,
    private batchPaymentsService: BatchPaymentsService,
    private currencyPipe: CurrencyPipe,
    private datePipe: DatePipe,
    private appConfig: AppConfiguration,
    private hostUrlService: DynamicHostUrlService,
    private dateService: DateService,
    public commonService: CommonService,
    public securityEngineService: SecurityEngineService
  ) { }

  ngOnInit() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.batchGroupPaymentForm = this.fb.group({
      accountNumber: ['', Validators.required],
      paymentAmount: ['', Validators.required],
      paymentType: ['', Validators.required],
      dateReceived: ['', ValidationService.dateValidator],
      statementDueDate: [''],
      statementDueDateFormat: [''],
      suspenseReason: [''],
      paymentOnHold: [false],
      dueDate: ['', ValidationService.dateValidator],
      postmarkDate: ['', ValidationService.dateValidator],
      paymentComments: ['', Validators.maxLength(80)],
      receivedCurrency: [''],
      exchangeRate: [''],
      paymentCalculatedAmount: [''],
      paymentId: ['']

    });
    if (this.batchPaymentsService.paramsToModal) {
      this.batchParam = this.batchPaymentsService.paramsToModal;
      this.postedDate = this.batchParam.postedDate;
      this.batchId = this.batchParam.batchId;
      this.userId = this.batchParam.userId;
      this.businessGroup = this.batchParam.businessGroup;
    }
    this.dateReceived = this.systemDate;
    this.postmarkDate = this.systemDate;
    this.getBatchPaymentSupportData();
    this.currency = this.batchPaymentsService.currency;
    this.isMultiCurrencyEnable = this.batchPaymentsService.multiCurrencyEnabled;
    this.setSelectAccountAPIUrl();
    this.resetReceivedCurrencyData();
  }

  public ngAfterViewInit(): void {
    if (this.batchPaymentsService.modalAction && this.batchPaymentsService.paramsToModal) {
      this.modalType = this.batchPaymentsService.modalAction;
      if (this.modalType === 'Edit') {
        const linkName = 'payment';
        this.accountPaymentParam = this.batchPaymentsService.paramsToModal;
        this.getBatchPaymentDetailFromLinks(linkName);
      }
    }
  }

  public setSelectAccountAPIUrl(): void{
    this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.groupBilling) + 'v1/billing/accounts';
    if (this.isMultiCurrencyEnable === 'true' && this.businessGroup) {
      const QUERYPARAMS = `type:Group,currency:${this.currency},businessGroup:${this.businessGroup}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else if (this.isMultiCurrencyEnable !== 'true' && this.businessGroup) {
      const QUERYPARAMS = `type:Group,businessGroup:${this.businessGroup}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else if (this.isMultiCurrencyEnable === 'true') {
      const QUERYPARAMS = `type:Group,currency:${this.currency}`;
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    } else {
      const QUERYPARAMS = 'type:Group';
      this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    }
  }

  public resetReceivedCurrencyData(): void{
    this.isUnValidPaymentAmount = false;
    if (this.isMultiCurrencyEnable) {
      this.currencyDropdown = this.batchPaymentsService.currencyDropdown;
      const multiCurrencyEnabled = (this.isMultiCurrencyEnable === 'true' ? true : false);
      if (multiCurrencyEnabled) {
        this.receivedCurrency.setValidators(Validators.required);
        this.receivedCurrency.setValue(this.currency);
        this.exchangeRate.setValidators(ValidationService.numberValidation(0));
        this.exchangeRate.setValue(null);
        this.enableExchangeRate = false;
        this.paymentCalculatedAmount.setValue(null);
        this.paymentAmount && this.setSelectPaymentAmount('dueDate', this.paymentAmount);
      } else {
        this.receivedCurrency.setValidators(null);
        this.exchangeRate.setValidators(null);
      }
      this.receivedCurrency.updateValueAndValidity();
      this.exchangeRate.updateValueAndValidity();
    }
  }

  private getBatchPaymentDetailFromLinks(linkName?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.commonService.getDatafromDetailLink(this.batchParam, linkName, 'GET').subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.batchPaymentDetail = { ...data };

        if (this.modalType === 'Edit') {
          if (!this.accountSelected) { this.accountSelected = new Object(); }
          this.accountSelected.payorName = this.batchPaymentDetail.payorName;
          this.accountSelected.accountNumber = this.batchPaymentDetail.identifier;
          this.accountSelected.accountId = this.batchPaymentDetail.accountId;
          if (this.batchPaymentDetail.payorAddress) { this.accountSelected.payorAddress = this.batchPaymentDetail.payorAddress; }
          this.accountNumber = this.batchPaymentDetail.identifier;
          this.dateReceived = this.batchPaymentDetail.paymentDate;
          this.paymentType = this.batchPaymentDetail.paymentType;
          this.paymentOnHold = this.batchPaymentDetail.paymentOnHold;
          this.suspenseReason = this.batchPaymentDetail.suspenseReason;
          this.statementDueDate = this.batchPaymentDetail.dueDate;
          this.statementDueDateFormat = this.datePipe.transform(this.batchPaymentDetail.dueDate, 'MMM dd, yyyy');
          this.postmarkDate = this.batchPaymentDetail.postmarkDate;
          this.paymentComments = this.batchPaymentDetail.paymentComments;
          const receivedCurrency = (this.batchPaymentDetail.receivedCurrency ? this.batchPaymentDetail.receivedCurrency : this.currency);
          this.receivedCurrency.setValue(receivedCurrency);
          this.exchangeRate.setValue(this.batchPaymentDetail.rate);
          const receivedAmount = this.isRateEntered(this.batchPaymentDetail.receivedCurrency, this.batchPaymentDetail.rate) ? this.batchPaymentDetail.paymentAmount : this.batchPaymentDetail.receivedAmount;
          this.paymentCalculatedAmount.setValue(receivedAmount);
          this.enableExchangeRate = false;
          if (this.batchPaymentDetail.rate && this.batchPaymentDetail.receivedCurrency) {
            this.enableExchangeRate = true;
          }
          this.paymentId = this.batchPaymentDetail.paymentId;
        }
        if (this.accountPaymentParam.accountId) {
          this.getBalanceDetails(this.accountPaymentParam.accountId);
          this.getStatementList(this.accountPaymentParam.accountId);
        }
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  public async selectedstatementDueDate(event?: any): Promise<void> {
    this.statementDueDate = event.statementDueDate;
    if (event) {
      await this.getBalanceDetails(this.accountSelected.accountId, 'dueDate', event);
    } else {
      await this.getBalanceDetails(this.accountSelected.accountId, 'dueDate');
    }

  }

  setPaymentAmount(amount?: any) {
    this.paymentAmount = amount;
    this.isUnValidPaymentAmount = false;
    const amountLabel = this.selectPaymentAmount.value.substring(0, 5);
    if (amountLabel === 'Other' && this.isOtherAmount === false) {
      this.isOtherAmount = true;
    } else if (amountLabel !== 'Other' && this.isOtherAmount === true) {
      this.isOtherAmount = false;
    }
  }

  getDropdownItems(propertyName?: any) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (!this.paymentType && this.modalType !== 'Edit' && propertyName === PropertyName.PaymentType && sd.defaultValue) {
          this.batchGroupPaymentForm.controls['paymentType'].setValue(sd.defaultValue);
        }
        if (!this.suspenseReason && this.modalType !== 'Edit' && propertyName === PropertyName.SuspenseReason && sd.defaultValue) {
           this.suspenseDefaultValue = sd.defaultValue;
          this.batchGroupPaymentForm.controls['suspenseReason'].setValue(this.suspenseDefaultValue);
        }
      }
    }
  }

  paymentOnHoldChanged(event?: any) {
    this.paymentOnHold = event;
    this.batchGroupPaymentForm.get('suspenseReason') && this.batchGroupPaymentForm.get('suspenseReason').setValue(undefined);
    this.batchGroupPaymentForm.removeControl('suspenseReason');
    if (event) {
      this.batchGroupPaymentForm.addControl('suspenseReason', new FormControl('', Validators.required));
      this.suspenseReason = this.suspenseDefaultValue
    }
  }
  public selectedAccount(data?: any): void {
    if (data) {
      this.resetReceivedCurrencyData();
      this.accountSelected = data;
      this.batchGroupPaymentForm.get('accountNumber').clearValidators();
      this.batchGroupPaymentForm.get('accountNumber').updateValueAndValidity();
      this.amountList = [];
      this.statementDueDate = '';
      this.statementDueDateFormat = '';
      this.isOtherAmount = false;

      this.getBalanceDetails(this.accountSelected.accountId);
      this.getStatementList(this.accountSelected.accountId)
    } else {
      this.accountSelected = data;
    }
  }

  getStatementList(accountID?: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getStatementList(accountID).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          const statementDueDateItems: Array<any> = [];
          this.statementDueDateList = data.content;
          for (const sd of this.statementDueDateList) {
            const statementDueDateFormat = this.datePipe.transform(sd.statementDueDate, 'MMM dd, yyyy');
            sd.statementDueDateFormat = statementDueDateFormat;
            statementDueDateItems.push(sd);
          }
          this.statementDueDateDropdown = statementDueDateItems;
        } else {
          this.statementDueDateDropdown = [];
        }
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  public async getBalanceDetails(accountID?: any, dueDate?: any, statementAmount?: any): Promise<void> {
    this.loadingService.toggleLoadingIndicator(true);
    await this.batchPaymentsService.getGroupBalances(accountID).toPromise().then(
      data => {
        this.amountList = [];
        this.loadingService.toggleLoadingIndicator(false);
        this.accountBalanceDetail = data;
        if (statementAmount && statementAmount.dueAmount) {
          this.amountList.push(
            {
              name: PropertyName.currentDueLabel + this.currencyPipe.transform(statementAmount.dueAmount, this.currency),
              value: statementAmount.dueAmount
            }
          );
        }
        this.amountList.push(
          {
            name: PropertyName.outstandingAmountLabel + this.currencyPipe.transform(this.accountBalanceDetail.totalOutstandingAmount, this.currency),
            value: this.accountBalanceDetail.totalOutstandingAmount
          }
        );
        if (this.batchPaymentsService.modalAction === 'Add') {
          this.setSelectPaymentAmount(dueDate, statementAmount);
        }else if(this.batchPaymentsService.modalAction === 'Edit') {
          if (this.accountPaymentParam.dueDate) {
            this.amountList = [];
            const currentDueAmount = statementAmount ? statementAmount.dueAmount : this.accountPaymentParam.currentDue;
            if (currentDueAmount != 0) {
              this.amountList.push(
                {
                  name: PropertyName.currentDueLabel + this.currencyPipe.transform(currentDueAmount, this.currency),
                  value: currentDueAmount
                }
              );
            }
            this.amountList.push(
              {
                name: PropertyName.outstandingAmountLabel + this.currencyPipe.transform(this.accountBalanceDetail.totalOutstandingAmount, this.currency),
                value: this.accountBalanceDetail.totalOutstandingAmount
              }
            );
          }
          this.setSelectPaymentAmount(dueDate, statementAmount ? statementAmount.dueAmount : this.accountPaymentParam?.currentDue);
        }
        this.getDropdownItems(PropertyName.PaymentType);
      },
      err => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }

  public setSelectPaymentAmount(dueDate?: any, statementAmount?: any): void{
    if (this.batchPaymentsService.modalAction === 'Add') {
      this.setDefaultPaymentAmount(dueDate, statementAmount);
    } else if (this.batchPaymentsService.modalAction === 'Edit') {
      if(dueDate){
        if(statementAmount){
          this.currentDueLabel = this.amountList[0]?.name;
          this.paymentAmount = statementAmount;
          this.selectPaymentAmount && (this.selectPaymentAmount.value = this.currentDueLabel);
          this.updateAmount(this.paymentAmount);
        }
      }else{
        const paymentAmount = this.isRateEntered(this.accountPaymentParam.receivedCurrency, this.accountPaymentParam.rate) ? this.accountPaymentParam.receivedAmount : this.accountPaymentParam.paymentAmount;
        this.currentDueLabel = this.getCurrentDueLabel(paymentAmount);
        this.paymentAmount = paymentAmount;
      }
    }
  }

  public setDefaultPaymentAmount(dueDate?: any, statementAmount?: any): void{
    this.currentDueLabel = this.amountList[0]?.name;
    if (this.accountBalanceDetail.totalOutstandingAmount) { this.paymentAmount = this.accountBalanceDetail.totalOutstandingAmount; }
    if (statementAmount && statementAmount.dueAmount) { this.paymentAmount = statementAmount.dueAmount; }
    if (!dueDate || !this.statementDueDateFormat) { this.paymentAmount = this.accountBalanceDetail.totalOutstandingAmount; }
    this.selectPaymentAmount && (this.selectPaymentAmount.value = this.currentDueLabel);
    this.updateAmount(this.paymentAmount);
  }

  public getCurrentDueLabel(statementAmount: any): string{
    let amountLabel = PropertyName.otherLabel + this.currencyPipe.transform(statementAmount, this.currency);
    const amountItem = this.amountList.find(item => Number(item.value) === Number(statementAmount));
    (amountItem) && (amountLabel = amountItem.name);
    return amountLabel;
  }

  getBatchPaymentSupportData() {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchPaymentsSupportData(this.batchId, this.postedDate).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.SuspenseReason);
          this.getDropdownItems(PropertyName.PaymentType);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  groupPaymentSubmit() {
    if (this.isValidPaymentAmountEntered()) {
      this.isUnValidPaymentAmount = true;
    } else {
      this.isUnValidPaymentAmount = false;
    }
    if (this.isValidDateReceivedEntered()) {
      this.isUnValidDateReceived = true;
    } else {
      this.isUnValidDateReceived = false;
    }
    if (this.isUnValidDateReceived || this.isUnValidPaymentAmount) {
      return;
    }
    this.paymentModel = new PaymentModel();
    this.paymentModel.paymentAmount = ((this.receivedCurrency.value !== this.currency && this.isMultiCurrencyEnable === 'true') ? this.paymentCalculatedAmount.value : this.paymentAmount);
    this.paymentModel.paymentType = this.paymentType;
    this.paymentModel.identifierType = PropertyName.Group;
    if (this.statementDueDate !== '') { this.paymentModel.dueDate = this.statementDueDate; }
    this.paymentModel.identifier = this.accountSelected.accountNumber;
    this.paymentModel.paymentOnHold = this.paymentOnHold;
    this.paymentModel.suspenseReason = this.paymentOnHold ? this.suspenseReason : null;
    this.paymentModel.postmarkDate = this.postmarkDate;
    this.paymentModel.paymentComments = this.paymentComments;
    this.paymentModel.paymentId = this.paymentId;
    if (!this.paymentModel.postmarkDate) {
      this.paymentModel.postmarkDate = null;
    }
    if (this.dateReceived !== '') {
      this.paymentModel.paymentDate = this.dateReceived;
    }
    if (this.modalType === 'Add') {
      this.addAccountPaymentAction(this.paymentModel);
    } else if (this.modalType === 'Edit') {
      this.editAccountPaymentAction(this.paymentModel);
    }
  }

  private addAccountPaymentAction(paymentModel?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.addBatchPayments(this.batchId, this.postedDate, paymentModel).subscribe(
      (data: any) => {
        if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
          this.getTransactionRateHistory(data);
        } else {
          this.handleAccountPaymentResponse();
        }
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  private editAccountPaymentAction(paymentModel?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.editBatchPayments
      (this.batchId, this.postedDate, this.userId, this.accountPaymentParam.paymentSequenceNumber, paymentModel).subscribe(
        (data: any) => {
          if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
            this.getTransactionRateHistory(this.batchPaymentDetail);
          } else if (this.batchPaymentDetail.receivedAmount) {
            this.deleteTransactionRateHistory(this.batchPaymentDetail);
          } else {
            this.handleAccountPaymentResponse();
          }
        },
        err => {
          this.commonService.handleErrorResponse(err);
        });
  }

  private deleteTransactionRateHistory(data?: any): void {
    this.batchPaymentsService.deleteTransactionRateHistory(data).subscribe(
      () => {
        if (this.isRateEntered(this.receivedCurrency.value, this.exchangeRate.value)) {
          this.getTransactionRateHistory(data);
        } else {
          this.handleAccountPaymentResponse();
        }
      },
      err => {
        this.handleAccountPaymentResponse();
        this.commonService.handleErrorResponse(err);
      });
  }

  private getTransactionRateHistory(data?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    const body = {
      paymentAmount: this.paymentCalculatedAmount.value,
      paymentCurrency: this.currency,
      receivedCurrency: this.receivedCurrency.value,
      receivedAmount: this.paymentAmount,
      rate: this.exchangeRate.value
    };
    this.batchPaymentsService.getTransactionRateHistory(data, body).subscribe(
      () => {
        this.handleAccountPaymentResponse();
      },
      err => {
        this.handleAccountPaymentResponse();
        this.commonService.handleErrorResponse(err);
      });
  }

  handleAccountPaymentResponse() {
    this.loadingService.toggleLoadingIndicator(false);
    this.batchPaymentsService.paramsToModal = null;
    this.modal.close(true);
  }

  isValidPaymentAmountEntered() {
    if (this.paymentAmount <= 0) {
      return true;
    }
    return false;
  }

  isValidDateReceivedEntered() {
    if (this.dateReceived > this.systemDate) {
      return true;
    }
    return false;
  }

  private isRateEntered(receivedCurrency?: string, exchangeRate?: any): boolean {
    return (receivedCurrency !== this.currency && (Number(exchangeRate) > 0));
  }

  resetDateValidate() {
    this.isUnValidDateReceived = false;
  }

  clearStatementDate() {
    this.statementDueDate = null;
  }

  updateAmount(value: any) {
    if (value) {
      this.isUnValidPaymentAmount = false;
      const paymentCalculatedAmount = Number(this.paymentAmount) * Number(this.exchangeRate.value);
      const roundOffValue = this.roundOff(paymentCalculatedAmount, 2);
      this.paymentCalculatedAmount.setValue(roundOffValue);
    }
  }

  roundOff(num: any, places: any) {
    const x = Math.pow(10, places);
    return Math.round(num * x) / x;
  }

  public getReceivedCurrency(fromCurrency: any): any {
    this.isUnValidPaymentAmount = false;
    if (fromCurrency && fromCurrency !== this.currency) {
      this.loadingService.toggleLoadingIndicator(true);
      this.batchPaymentsService.getExchangeRates(fromCurrency, this.currency, this.postedDate).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          if (data) {
            this.enableExchangeRate = true;
            const rate = data.rate ? data.rate : 1;
            this.exchangeRate.setValue(rate);
            const paymentCalculatedAmount = Number(this.paymentAmount) * Number(this.exchangeRate.value);
            this.paymentCalculatedAmount.setValue(paymentCalculatedAmount);
          }
        },
        (err) => {
          this.loadingService.toggleLoadingIndicator(false);
          this.commonService.handleErrorResponse(err);
        });
    } else {
      this.exchangeRate.setValue(null);
      this.enableExchangeRate = false;
      this.paymentCalculatedAmount.setValue(null);
      (this.paymentAmount) && (this.currentDueLabel = this.getCurrentDueLabel(this.paymentAmount));
    }
  }
  public clearPaymentAmount() : void {
    this.isUnValidPaymentAmount = false;
  }

  get receivedCurrency() { return this.batchGroupPaymentForm.get('receivedCurrency'); }
  get exchangeRate() { return this.batchGroupPaymentForm.get('exchangeRate'); }
  get paymentCalculatedAmount() { return this.batchGroupPaymentForm.get('paymentCalculatedAmount'); }
}
