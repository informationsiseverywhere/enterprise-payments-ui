import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-edit-batch-total',
  templateUrl: './edit-batch-total.component.html'
})

export class EditBatchTotalComponent implements OnInit {
  public editBatchTotalForm: FormGroup;
  public batchAmount: any = 0;
  public modalType: any;
  supportData: any;
  public currencyDropdown: any;
  public multiCurrencyDropdown: any;
  public multiCurrencyEnabled: boolean;
  currency: any;
  constructor(
    public loadingService: LoadingService,
    private modal: ModalComponent,
    public commonService: CommonService,
    private fb: FormBuilder,
    public batchPaymentsService: BatchPaymentsService) { }

  ngOnInit() {
    this.getBatchPaymentsSupportData();
    this.editBatchTotalForm = this.fb.group({
      batchAmount: ['', Validators.required],
      currency: ['']
    });
    this.batchAmount = this.batchPaymentsService.batchAmount;
    this.currency = this.batchPaymentsService.currency;
  }

  public saveBatchTotal(): void {
    let requestBody = {};
    requestBody = {
      batchAmount: this.batchAmount
    };
    this.editBatchTotal(requestBody);
  }

  getBatchPaymentsSupportData() {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.Currency);
          this.getDropdownItems(PropertyName.MultiCurrency);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  getDropdownItems(propertyName: string) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    if (this.multiCurrencyDropdown) {
      this.multiCurrencyEnabled = this.multiCurrencyDropdown[0] === 'true' ? true : false;
    }
  }

  private editBatchTotal(requestAddUpdateBatch?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.editBatchTotal(requestAddUpdateBatch).subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.modal.close(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

}
