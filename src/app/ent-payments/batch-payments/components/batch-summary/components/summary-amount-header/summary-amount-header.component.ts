import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { BatchActionsComponent } from '../../../batch-payments/batch-actions/batch-actions.component';
import { EditBatchTotalComponent } from './edit-batch-total/edit-batch-total.component';
import { BatchPaymentsService } from 'src/app/ent-payments/batch-payments/services/batch-payments.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';
import { AddBatchComponent } from '../../../batch-payments/add-batch/add-batch.component';

@Component({
  selector: 'app-summary-amount-header',
  templateUrl: './summary-amount-header.component.html',
  styleUrls: ['./summary-amount-header.component.scss']
})
export class SummaryAmountHeaderComponent implements OnInit {
  @Input() batchDetail: any;
  @Input() controlBank: any;
  @Input() depositBank: any;
  @Input() depositDate: any;
  @Input() userId: any;
  @Input() systemDate: string;
  @Input() isBatchDetailLoaded: boolean;
  @ViewChild('batchPaymentsEditModal') batchPaymentsEditModal: ModalComponent;
  @ViewChild('batchPaymentsAcceptModal') batchPaymentsAcceptModal: ModalComponent;
  @Output() reloadSummary: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() downloadReportExcel: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('editBatchTotalModal') editBatchTotalModal: ModalComponent;

  constructor(
    private batchPaymentsService: BatchPaymentsService,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    private router: Router) { }

  ngOnInit() { }

  public openEditBatchModal(modalType: any, batchPayment: any): void {
    this.batchPaymentsEditModal.modalTitle = `${modalType} Batch`;
    this.batchPaymentsEditModal.modalFooter = false;
    this.batchPaymentsService.modalType = modalType;
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsEditModal.icon = 'assets/images/model-icons/add-batch.svg';
    this.batchPaymentsEditModal.open(AddBatchComponent);
  }
  getAccountPayment(event?: any) {
    if (event) {
      if (this.batchPaymentsService.modalAction === 'Accept' || this.batchPaymentsService.modalType === 'Edit') {
        this.reloadSummary.emit(true);
      } else {
        this.router.navigate(['/ent-payments', 'batch-payments']);
      }
    }
    this.batchPaymentsService.modalAction = null;
    this.batchPaymentsService.modalType = null;
    this.batchPaymentsService.paramsToModal = null;
  }
  handleAction(batchPayment?: any, action?: string) {
    switch (action) {
      case 'edit':
        this.openEditBatchModal('Edit', batchPayment);
        break;
      case 'accept':
        this.openBatchPaymentAcceptModal(batchPayment);
        break;
      case 'deposit':
        this.openBatchPaymentDepositModal(batchPayment);
        break;
      case 'acceptAndDeposit':
        this.openBatchPaymentAcceptDepositModal(batchPayment);
        break;
    }
  }

  openBatchPaymentAcceptModal(batchPayment?: any) {
    this.openAcceptDepositModal(batchPayment, 'Batch Accept', 'Accept');
  }

  openBatchPaymentDepositModal(batchPayment?: any) {
    this.openAcceptDepositModal(batchPayment, 'Batch Deposit', 'Deposit');
  }

  openBatchPaymentAcceptDepositModal(batchPayment?: any) {
    this.openAcceptDepositModal(batchPayment, 'Batch Accept and Deposit', 'AcceptAndDeposit');
  }

  openAcceptDepositModal(batchPayment?: any, modalTitle?: string, modalAction?: string) {
    this.batchPaymentsAcceptModal.modalTitle = modalTitle;
    this.batchPaymentsAcceptModal.modalFooter = false;
    this.batchPaymentsAcceptModal.okButtonText = 'SUBMIT';
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = modalAction;
    this.batchPaymentsAcceptModal.icon = 'assets/images/model-icons/batch-accept.svg';
    this.batchPaymentsAcceptModal.open(BatchActionsComponent);
  }

  public getContentToExport(): void {
    this.downloadReportExcel.emit(true);
  }

  public saveBatchTotalModal(event?: any): void {
    if (event) { this.reloadSummary.emit(true); }
  }

  public editBatchPaymentsDetails(): void {
    this.batchPaymentsService.systemDate = this.systemDate;
    this.batchPaymentsService.batchId = this.batchDetail.batchId;
    this.batchPaymentsService.postedDate = this.batchDetail.postedDate;
    this.batchPaymentsService.batchAmount = this.batchDetail.batchAmount;
    this.batchPaymentsService.currency = this.batchDetail.currency;
    this.batchPaymentsService.userId = this.batchDetail.userId;
    this.editBatchTotalModal.modalTitle = 'Edit Batch Total';
    this.editBatchTotalModal.modalFooter = false;
    this.editBatchTotalModal.icon = 'assets/images/model-icons/edit-batch.svg';
    this.editBatchTotalModal.open(EditBatchTotalComponent);
  }

  public openSecurity(event?: any) : void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, 'batchDetailSection');
  }
}
