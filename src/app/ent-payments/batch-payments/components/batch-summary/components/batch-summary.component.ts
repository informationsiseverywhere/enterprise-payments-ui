import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DateService } from 'src/app/shared/services/date.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { SummaryResultsComponent } from './summary-results/summary-results.component';
import { BatchPaymentsService } from '../../../services/batch-payments.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-batch-summary',
  templateUrl: './batch-summary.component.html',
  styleUrls: ['./batch-summary.component.scss']
})
export class BatchSummaryComponent implements OnInit {
  batchId: any;
  postedDate: any;
  userId: any;
  accountNumber: any;
  systemDate: any;
  batchDetail: any;
  depositDate: any;
  controlBank: any;
  depositBank: any;
  businessGroup: any;
  size: any;
  page: any;
  @ViewChild('resultList', { static: false }) resultList: SummaryResultsComponent;
  batchPaymentList: any;
  public isBatchDetailLoaded = false;
  supportData: any;
  multiCurrencyDropdown: any;
  multiCurrencyEnabled: any;
  currencyDropdown: any;
  public isShowHeader = true;
  constructor(
    private route: ActivatedRoute,
    private dateService: DateService,
    private batchPaymentsService: BatchPaymentsService,
    private loadingService: LoadingService,
    public securityEngineService: SecurityEngineService,
    private commonService: CommonService) {
    this.batchPaymentList = new Array(5);
    this.batchDetail = new Object();
  }
  ngOnInit() {
    this.route.parent.params.subscribe(
      params => {
        this.batchId = params.batchId;
        this.postedDate = params.postedDate;
        this.userId = params.userId;
      });

    this.route.queryParams.subscribe(
      queryParams => {
        this.page = queryParams.page;
        this.size = queryParams.size;
      });

    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });

    if (this.batchId && this.postedDate &&  this.userId) {
      this.getBatchSummaryDetail(this.batchId, this.postedDate, this.userId);
    }
    this.getBatchPaymentsSupportData();
  }

  getBatchPaymentsSupportData() {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.MultiCurrency);
          this.getDropdownItems(PropertyName.Currency);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  getDropdownItems(propertyName: string) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    if (this.multiCurrencyDropdown) {
      this.multiCurrencyEnabled = this.multiCurrencyDropdown[0];
    }
    if (this.currencyDropdown) { this.batchPaymentsService.currencyDropdown = this.currencyDropdown; }
  }

  getBatchSummaryDetail(batchId: string, postedDate: any, userId: any) {
    this.isBatchDetailLoaded = false;
    this.batchPaymentsService.getBatchDetails(batchId, postedDate, userId).subscribe(
      data => {
        this.isBatchDetailLoaded = true;
        this.batchDetail = data;
        this.depositDate = this.batchDetail.depositDate;
        this.controlBank = this.batchDetail.controlBank;
        this.depositBank = this.batchDetail.depositBank;
        this.businessGroup = this.batchDetail.businessGroup;
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, 'isBatchDetailLoaded', 'batchDetail', err);
      }
    );
  }

  isStatementsEmpty(event: any) {
    this.batchPaymentList = event;
  }
  reloadSummary(event: any) {
    if (event) {
      this.resultList.getBatchPayments();
      if (this.batchId && this.postedDate && this.userId) {
        this.getBatchSummaryDetail(this.batchId, this.postedDate, this.userId);
      }
    }
  }
  public downloadReportExcel(event: any): void {
    if (event) {
      this.batchPaymentsService.getBatchDetailReportExcel(this.batchId, this.postedDate, this.userId).subscribe(
        (data: any) => {
          const myBlob = new Blob([data], {
            type: 'application/vnd.ms-excel'
          });
          if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(myBlob, 'batchDetailReport ' + this.batchId + '.xlsx');
          } else {
            const url = window.URL.createObjectURL(data);
            const link = document.createElement('a');
            document.body.appendChild(link);
            link.download = 'batchDetailReport ' + this.batchId + '.xlsx';
            link.href = url;
            link.click();
          }
        });
    }
  }

  public checkShowBatchHeader(): any {
    this.isShowHeader = true;
    if (this.securityEngineService.securityData.accountPayment) {
      const list = ['accountPayment', 'groupPayment', 'agencyPayment', 'policyPayment', 'expressEntry'];
      let isHide = true;
      list.forEach(item => {
        if (!this.securityEngineService.checkVisibility(item, 'Hide')) { isHide = false; }
      });
      this.isShowHeader = !isHide;
    }
  }
}
