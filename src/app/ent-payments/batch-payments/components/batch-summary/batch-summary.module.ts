import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { BatchSummaryRoutingModule } from './batch-summary-routing.module';

import { BatchSummaryComponent } from './components/batch-summary.component';
import { BatchHeaderComponent } from './components/batch-header/batch-header.component';
import { SummaryResultsComponent } from './components/summary-results/summary-results.component';
import { SummaryHeaderComponent } from './components/summary-header/summary-header.component';
import { SummaryAmountHeaderComponent } from './components/summary-amount-header/summary-amount-header.component';
import { EditBatchTotalComponent } from './components/summary-amount-header/edit-batch-total/edit-batch-total.component';
import { AccountPaymentComponent } from './components/batch-header/account-payment/account-payment.component';
import { AgencyPaymentComponent } from './components/batch-header/agency-payment/agency-payment.component';
import { EditUnidentifiedPaymentComponent } from './components/batch-header/edit-unidentified-payment/edit-unidentified-payment.component';
import { GroupPaymentComponent } from './components/batch-header/group-payment/group-payment.component';
import { PolicyPaymentComponent } from './components/batch-header/policy-payment/policy-payment.component';
import { UnidentifiedPaymentComponent } from './components/batch-header/unidentified-payment/unidentified-payment.component';
import { BatchPaymentsModule } from '../../batch-payments.module';

@NgModule({
  declarations: [
    BatchSummaryComponent,
    SummaryResultsComponent,
    SummaryHeaderComponent,
    BatchHeaderComponent,
    SummaryAmountHeaderComponent,
    EditBatchTotalComponent,
    AccountPaymentComponent,
    AgencyPaymentComponent,
    EditUnidentifiedPaymentComponent,
    GroupPaymentComponent,
    PolicyPaymentComponent,
    UnidentifiedPaymentComponent
  ],
  entryComponents: [
    EditBatchTotalComponent,
    AccountPaymentComponent,
    AgencyPaymentComponent,
    EditUnidentifiedPaymentComponent,
    GroupPaymentComponent,
    PolicyPaymentComponent,
    UnidentifiedPaymentComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    BatchSummaryRoutingModule,
    BatchPaymentsModule
  ]
})
export class BatchSummaryModule { }
