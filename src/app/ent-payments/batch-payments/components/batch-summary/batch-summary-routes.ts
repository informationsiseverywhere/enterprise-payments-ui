import { Routes } from '@angular/router';

import { CommonComponent } from 'src/app/shared/components/common/common.component';
import { BatchSummaryComponent } from './components/batch-summary.component';

export const paymentsSummaryRoutes: Routes = [
  {
    path: '',
    component: CommonComponent,
    children: [
      {
        path: '',
        component: BatchSummaryComponent
      }
    ]
  }
];

