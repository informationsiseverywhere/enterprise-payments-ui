import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { DateService } from 'src/app/shared/services/date.service';
import { BatchPaymentsService } from '../../../services/batch-payments.service';
import { BatchModel } from '../../../models/batch.model';

@Component({
  selector: 'app-batch-actions',
  templateUrl: './batch-actions.component.html'
})
export class BatchActionsComponent implements OnInit {
  batchModel: BatchModel = new BatchModel();
  systemDate: any;
  supportData: any;
  batchActionForm: FormGroup;
  batchParam: any;
  batchDetail: any;
  batchId: any;
  postedDate: any;
  modalAction: any;
  depositDate: any;
  userId: any;


  constructor(
    private fb: FormBuilder,
    private modal: ModalComponent,
    private loadingService: LoadingService,
    private commonService: CommonService,
    private dateService: DateService,
    private batchPaymentsService: BatchPaymentsService) { }

  ngOnInit() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.batchActionForm = this.fb.group({
      depositDate: ['', Validators.compose([Validators.required, ValidationService.dateValidator])]
    });
    this.batchModel = new BatchModel();
    if (this.batchPaymentsService.paramsToModal) {
      this.batchParam = this.batchPaymentsService.paramsToModal;
      this.postedDate = this.batchParam.postedDate;
      this.batchId = this.batchParam.batchId;
      this.userId = this.batchParam.userId;
      this.modalAction = this.batchPaymentsService.modalAction;
      this.getBatchDetails();
    }
    if (this.modalAction === 'Deposit' || this.modalAction === 'AcceptAndDeposit') {
      this.depositDate = this.systemDate;
    }
  }

  getBatchDetails() {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchDetails(this.batchId, this.postedDate, this.userId).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.batchDetail = data;
          this.batchModel.depositBank = this.batchDetail.depositBank;
          this.batchModel.postedDate = this.batchDetail.postedDate;
          this.batchModel.depositDate = this.batchDetail.depositDate;
          this.batchModel.batchAmount = this.batchDetail.batchAmount;
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }


  getDropdownItems(propertyName: string) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
  }
  batchPaymentActionSubmit() {
    switch (this.modalAction) {
      case 'Accept':
        this.batchAcceptAction();
        break;
      case 'Deposit':
        this.batchDepositAction();
        break;
      case 'AcceptAndDeposit':
        this.batchAcceptAndDepositAction();
        break;
    }
  }

  batchAcceptAction() {
    let requestAddUpdateBatch = {};
    requestAddUpdateBatch = {
      isAccept: true
    };

    this.savePaymentActionModal(requestAddUpdateBatch);
  }
  batchDepositAction() {
    let requestAddUpdateBatch = {};
    requestAddUpdateBatch = {
      isDeposit: true,
      depositDate: this.depositDate
    };

    this.savePaymentActionModal(requestAddUpdateBatch);
  }
  batchAcceptAndDepositAction() {
    let requestAddUpdateBatch = {};
    requestAddUpdateBatch = {
      isAccept: true,
      isDeposit: true,
      depositDate: this.depositDate
    };

    this.savePaymentActionModal(requestAddUpdateBatch);
  }

  savePaymentActionModal(requestAddUpdateBatch) {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.updateBatch(this.batchId, this.postedDate, this.userId, requestAddUpdateBatch).subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.batchPaymentsService.paramsToModal = null;
        this.modal.close(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

}
