import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DateService } from 'src/app/shared/services/date.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-batch-payments',
  templateUrl: './batch-payments.component.html'
})
export class BatchPaymentsComponent implements OnInit {
  systemDate: any;
  batchPaymentList: any = [];
  filters: string;

  constructor(
    private route: ActivatedRoute,
    private dateService: DateService,
    public securityEngineService: SecurityEngineService
  ) { 
    this.batchPaymentList = new Array(5);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.filters = queryParams.filters;
      });
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
  }

  isBatchPaymentsEmpty(event: any) {
    this.batchPaymentList = event;
  }
}
