import { Component, Input, OnInit } from '@angular/core';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-batch-payments-header',
  templateUrl: './batch-payments-header.component.html'
})

export class BatchPaymentsHeaderComponent implements OnInit {

  @Input() isBatchPaymentListLoaded = false;

  constructor(public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService) { }

  ngOnInit() { }

  public openSecurity(event?: any) : void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, 'tableSection');
  }
}
