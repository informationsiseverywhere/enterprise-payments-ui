import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaginationComponent } from 'src/app/libs/pagination/pagination.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { AddBatchComponent } from '../add-batch/add-batch.component';
import { BatchActionsComponent } from '../batch-actions/batch-actions.component';
import { BatchPaymentsService } from '../../../services/batch-payments.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-batch-payments-results',
  templateUrl: './batch-payments-results.component.html'
})
export class BatchPaymentsResultsComponent implements OnInit {
  @Output() isBatchPaymentsEmpty: EventEmitter<any> = new EventEmitter<any>();
  @Input() systemDate: string;
  @Input() filters: any;
  batchPaymentList: any = [];
  @ViewChild('batchPaymentsEditModal', { static: false }) batchPaymentsEditModal: ModalComponent;
  @ViewChild('batchPaymentsAcceptModal', { static: false }) batchPaymentsAcceptModal: ModalComponent;
  @ViewChild('batchDeleteModal', { static: false }) batchDeleteModal: ModalComponent;
  @ViewChild(PaginationComponent) paginator: PaginationComponent;
  startDate: string;
  endDate: string;
  fromAmount: number;
  toAmount: number;
  page: any;
  size: any;
  returnAppId: any;
  navParam: any;
  public isBatchPaymentListLoaded = false;
  currency: any;
  allBatches: any;
  constructor(
    private batchPaymentsService: BatchPaymentsService,
    private router: Router,
    private route: ActivatedRoute,
    public securityEngineService: SecurityEngineService,
    private commonService: CommonService) {
    this.batchPaymentList = new Array(5);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.startDate = queryParams.since;
        this.endDate = queryParams.until;
        this.fromAmount = queryParams.fromAmount;
        this.toAmount = queryParams.toAmount;
        this.currency = queryParams.currency;
        this.page = queryParams.page;
        this.size = queryParams.size;
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
        this.allBatches = queryParams.allBatches
        this.getBatchPayments();
      }
    );
  }

  getBatchPayments() {
    this.isBatchPaymentListLoaded = false;
    this.batchPaymentList && (this.batchPaymentList.length = this.size < 5 ? this.size : 5);
    this.batchPaymentsService.getBatchList(this.startDate, this.endDate, this.fromAmount, this.toAmount, this.currency, this.size, this.page, this.allBatches).subscribe(
      data => {
        this.isBatchPaymentListLoaded = true;
        this.refreshBatchPayments(data);
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, 'isBatchPaymentListLoaded', 'batchPaymentList', err);
        this.isBatchPaymentsEmpty.emit(this.batchPaymentList);
      });
  }

  private refreshBatchPayments(data?: any): void {
    this.batchPaymentList = this.commonService.tablePaginationHandler(this.paginator, data);
    this.isBatchPaymentsEmpty.emit(this.batchPaymentList);
  }

  saveBatchPaymentsInfoModal(event?: any) {
    if (event) {
      this.batchPaymentsService.modalType = null;
      this.getBatchPayments();
    }
  }

  public addBatchModel(modalType: any, batchPayment: any): void {
    this.batchPaymentsEditModal.modalTitle = `${modalType} Batch`;
    this.batchPaymentsEditModal.modalFooter = false;
    this.batchPaymentsService.modalType = modalType;
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsEditModal.icon = 'assets/images/model-icons/add-batch.svg';
    this.batchPaymentsEditModal.open(AddBatchComponent);
  }

  dateFilterExecution(filterParameters?: any) {
    const paramObject: any = {};
    if (filterParameters.startDate && filterParameters.startDate !== '') {
      paramObject.since = filterParameters.startDate;
    }

    if (filterParameters.endDate && filterParameters.endDate !== '') {
      paramObject.until = filterParameters.endDate;
    }

    if (filterParameters.fromAmount && filterParameters.fromAmount !== '') {
      paramObject.fromAmount = filterParameters.fromAmount;
    }

    if (filterParameters.toAmount && filterParameters.toAmount !== '') {
      paramObject.toAmount = filterParameters.toAmount;
    }
    if (filterParameters.currency && filterParameters.currency !== '') {
      paramObject.currency = filterParameters.currency;
    }
    if (filterParameters.page && filterParameters.page !== '') {
      paramObject.page = filterParameters.page;
    }
    if (filterParameters.size && filterParameters.size !== '') {
      paramObject.size = filterParameters.size;
    }
    if (filterParameters.returnAppId) {
      paramObject.returnAppId = filterParameters.returnAppId;
    }
    if (filterParameters.allBatches) {
      paramObject.allBatches = filterParameters.allBatches;
    }
    if (filterParameters.navParam) {
      paramObject.nav = filterParameters.navParam;
    }
    this.router.navigate(['/ent-payments/batch-payments'], {
      queryParams: paramObject
    });
  }

  handleAction(batchPayment?: any, action?: string) {
    switch (action) {
      case 'edit':
        this.addBatchModel('Edit', batchPayment);
        break;
      case 'delete':
        this.openBatchPaymentDeleteModal(batchPayment);
        break;
      case 'accept':
        this.openBatchPaymentAcceptModal(batchPayment);
        break;
      case 'deposit':
        this.openBatchPaymentDepositModal(batchPayment);
        break;
      case 'acceptAndDeposit':
        this.openBatchPaymentAcceptDepositModal(batchPayment);
        break;
    }
  }

  openBatchPaymentDeleteModal(batchPayment?: any) {
    this.batchDeleteModal.modalTitle = 'Delete Batch';
    this.batchDeleteModal.modalFooter = true;
    this.batchDeleteModal.okButtonText = 'Yes';
    this.batchDeleteModal.cancelButtonText = 'No';
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchDeleteModal.icon = 'assets/images/model-icons/delete.svg';
    this.batchDeleteModal.message = 'Are you sure you want to delete the Batch immediately?';
    this.batchDeleteModal.open();
  }

  batchPaymentDelete(event?: any) {
    if (event) {
      if (this.batchPaymentsService.paramsToModal) {
        this.batchPaymentsService.deleteBatch(
          this.batchPaymentsService.paramsToModal.batchId, this.batchPaymentsService.paramsToModal.postedDate, this.batchPaymentsService.paramsToModal.userId).subscribe(
            () => {
              this.batchPaymentsService.paramsToModal = null;
              this.saveBatchPaymentsInfoModal(true);
            },
            err => {
              this.commonService.handleErrorResponse(err);
            });
      }
    }
  }

  openBatchPaymentAcceptModal(batchPayment?: any) {
    this.openAcceptDepositModal(batchPayment, 'Batch Accept', 'Accept');
  }

  openBatchPaymentDepositModal(batchPayment?: any) {
    this.openAcceptDepositModal(batchPayment, 'Batch Deposit', 'Deposit');
  }

  openBatchPaymentAcceptDepositModal(batchPayment?: any) {
    this.openAcceptDepositModal(batchPayment, 'Batch Accept and Deposit', 'AcceptAndDeposit');
  }

  openAcceptDepositModal(batchPayment?: any, modalTitle?: string, modalAction?: string) {
    this.batchPaymentsAcceptModal.modalTitle = modalTitle;
    this.batchPaymentsAcceptModal.modalFooter = false;
    this.batchPaymentsAcceptModal.okButtonText = 'SUBMIT';
    this.batchPaymentsService.paramsToModal = batchPayment;
    this.batchPaymentsService.modalAction = modalAction;
    this.batchPaymentsAcceptModal.icon = 'assets/images/model-icons/batch-accept.svg';
    this.batchPaymentsAcceptModal.open(BatchActionsComponent);
  }
}
