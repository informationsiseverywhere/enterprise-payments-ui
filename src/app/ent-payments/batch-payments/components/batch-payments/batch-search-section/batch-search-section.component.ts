import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

import { CommonService } from 'src/app/shared/services/common.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { BatchPaymentsResultsComponent } from '../batch-payments-results/batch-payments-results.component';
import { ImportPaymentsComponent } from '../import-payments/import-payments.component';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { BatchPaymentsService } from '../../../services/batch-payments.service';
import { AdvanceSearchTags } from 'src/app/ent-payments/payments-search/models/advance-search-enums.model';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-batch-search-section',
  templateUrl: './batch-search-section.component.html',
  styleUrls: ['./batch-search-section.component.scss']

})
export class BatchSearchSectionComponent implements OnInit, OnChanges {
  @ViewChild('importPaymentsComponent', { static: false }) importPaymentsComponent: ModalComponent;
  @Input() filters: any;
  @Input() resultList: BatchPaymentsResultsComponent;
  @Input() systemDate: string;
  @Input() isBatchPaymentListLoaded = false;
  public filterTagsList: Array<{ tag: string, name: string }> = [];
  private advanceSearchTags: AdvanceSearchTags = new AdvanceSearchTags();
  public page: any;
  public size: any;
  private returnAppId: any;
  private navParam: any;
  public batchPaymentSearchForm: FormGroup;
  public isGreater = false;
  public isGreaterAmount = false;
  supportData: any;
  currencyDropdown: any;
  public multiCurrencyDropdown: any;
  public multiCurrencyEnabled: any;
  public allBatchesDropdown: any;
  public allBatchesEnable: any;
  public isAllBatchesChecked: boolean;
  public filterParameters: any;
  public currencyDefaultValue: string = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private batchPaymentsService: BatchPaymentsService,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    public commonService: CommonService,
    private loadingService: LoadingService,) { }

  ngOnInit() {
    this.getBatchPaymentsSupportData();
    this.batchPaymentSearchForm = this.fb.group({
      startDate: ['', ValidationService.dateValidator],
      endDate: ['', ValidationService.dateValidator],
      toAmount: [''],
      fromAmount: [''],
      currency: ['']
    });
    let data: any = {};
    let filterParameters: any = {};
    this.route.queryParams.subscribe(
      queryParams => {
        this.page = queryParams.page;
        this.size = queryParams.size;
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
        this.isAllBatchesChecked = queryParams.allBatches;
        data = {
          startDate: queryParams.since,
          endDate: queryParams.until,
          toAmount: queryParams.toAmount,
          fromAmount: queryParams.fromAmount,
          currency: queryParams.currency
        };
        this.filterParameters = {
          ...data,
          page: this.page,
          size: this.size,
          returnAppId: this.returnAppId,
          navParam: this.navParam,
          allBatches: this.isAllBatchesChecked
        };
      }
    );
    this.patchData(data);
    this.dateFilterExecution(filterParameters);
  }

  patchData(data: any) {
    this.batchPaymentSearchForm.patchValue({
      startDate: (data.since ? data.since : data.startDate),
      endDate: (data.until ? data.until : data.endDate),
      toAmount: data.toAmount,
      fromAmount: data.fromAmount,
      currency: data.currency
    });
  }

  getBatchPaymentsSupportData() {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchSupportData().subscribe(
      data => {
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.MultiCurrency);
          this.getDropdownItems(PropertyName.Currency);
          this.getDropdownItems(PropertyName.allBatches);
        }
        this.loadingService.toggleLoadingIndicator(false);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  togglePayment() {
    this.filterParameters.allBatches = this.isAllBatchesChecked;
    this.searchBatchPayments();
  }
  getDropdownItems(propertyName: string) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (propertyName === PropertyName.Currency && sd.defaultValue) {
          this.currencyDefaultValue = sd.defaultValue;
          this.batchPaymentSearchForm.controls['currency'].setValue(this.currencyDefaultValue);
        }
      }
    }
    if (this.multiCurrencyDropdown) {
      this.multiCurrencyEnabled = false;
      if (this.multiCurrencyDropdown[0] === 'true') {
        this.multiCurrencyEnabled = true;
      }
    }
    if (this.allBatchesDropdown) {
      this.allBatchesEnable = false;
      if (this.allBatchesDropdown[0] === 'true') {
        this.allBatchesEnable = true;
      }
      else {
        this.isAllBatchesChecked = false;
        var snapshot = this.route.snapshot;
        const params = { ...snapshot.queryParams };
        delete params.allBatches
        this.router.navigate([], { queryParams: params });
      }
    }
  }
  get startDate() { return this.batchPaymentSearchForm.get('startDate'); }
  get endDate() { return this.batchPaymentSearchForm.get('endDate'); }
  get fromAmount() { return this.batchPaymentSearchForm.get('fromAmount'); }
  get toAmount() { return this.batchPaymentSearchForm.get('toAmount'); }
  get currency() { return this.batchPaymentSearchForm.get('currency'); }

  ngOnChanges() {
    if (this.filters) {
      this.setCheckboxesValue();
    }
  }

  private setCheckboxesValue(): void {
    if (this.filters instanceof Array) {
      for (const filter of this.filters) {
        this[filter + 'Param'] = true;
      }
    } else {
      this[this.filters + 'Param'] = true;
    }
  }

  public performAddBatch(): void {
    this.resultList.addBatchModel('Add', null);
  }

  public dateFilterExecution(filterParameters?: any): void {
    if (filterParameters) {
      this.removeTagsItem('startDate');
      this.removeTagsItem('endDate');
      this.removeTagsItem('fromAmount');
      this.removeTagsItem('toAmount');
      this.removeTagsItem('currency');
      if (filterParameters.startDate) {
        this.filterTagsList.push({
          tag: this.advanceSearchTags.startDate + this.commonService.convertDateFormat(filterParameters.startDate), name: 'startDate'
        });
      }
      if (filterParameters.endDate) {
        this.filterTagsList.push({
          tag: this.advanceSearchTags.endDate + this.commonService.convertDateFormat(filterParameters.endDate), name: 'endDate'
        });
      }
      if (filterParameters.fromAmount) {
        this.filterTagsList.push({ tag: this.advanceSearchTags.fromAmount + filterParameters.fromAmount, name: 'fromAmount' });
      }
      if (filterParameters.toAmount) {
        this.filterTagsList.push({ tag: this.advanceSearchTags.toAmount + filterParameters.toAmount, name: 'toAmount' });
      }
      if (filterParameters.currency) {
        this.filterTagsList.push({ tag: this.advanceSearchTags.currency + filterParameters.currency, name: 'currency' });
      }
      const data: any = {
        startDate: filterParameters.startDate,
        endDate: filterParameters.endDate,
        toAmount: filterParameters.toAmount,
        fromAmount: filterParameters.fromAmount,
        currency: filterParameters.currency || this.currencyDefaultValue
      };
      this.patchData(data);
      this.page = filterParameters.page;
      this.size = filterParameters.size;
      if (this.returnAppId) {
        filterParameters.returnAppId = this.returnAppId;
      }
      if (this.navParam) {
        filterParameters.nav = this.navParam;
      }
      this.batchPaymentsService.filterParameters = filterParameters;
      this.resultList.dateFilterExecution(filterParameters);
    }
  }

  public removeTagsFilter(name: string): void {
    if (name === 'startDate') {
      this.startDate.patchValue(null);
      this.batchPaymentsService.filterParameters.startDate = undefined;
    }
    if (name === 'endDate') {
      this.endDate.patchValue(null);
      this.batchPaymentsService.filterParameters.endDate = undefined;
    }
    if (name === 'fromAmount') {
      this.fromAmount.patchValue(null);
      this.batchPaymentsService.filterParameters.fromAmount = undefined;
    }
    if (name === 'toAmount') {
      this.toAmount.patchValue(null);
      this.batchPaymentsService.filterParameters.toAmount = undefined;
    }
    if (name === 'currency') {
      this.currency.patchValue(null);
      this.batchPaymentsService.filterParameters.currency = undefined;
    }
    const filterParameters: any = {
      startDate: this.startDate.value,
      endDate: this.endDate.value,
      fromAmount: this.fromAmount.value,
      toAmount: this.toAmount.value,
      currency: this.currency.value
    };
    this.removeTagsItem(name);
    if (this.returnAppId) {
      filterParameters.returnAppId = this.returnAppId;
    }
    if (this.navParam) {
      filterParameters.nav = this.navParam;
    }
    if (this.isAllBatchesChecked) {
      filterParameters.allBatches = this.isAllBatchesChecked;
    }
    if (!this.isAllBatchesChecked) {
      filterParameters.allBatches = this.isAllBatchesChecked;
    }
    this.resultList.dateFilterExecution(filterParameters);
  }

  private removeTagsItem(name: string): void {
    for (let i = 0; i < this.filterTagsList.length; i++) {
      if (this.filterTagsList[i].name === name) {
        this.filterTagsList.splice(i, 1);
      }
    }
  }

  public performImportPayments(): void {
    this.batchPaymentsService.systemDate = this.systemDate;
    this.importPaymentsComponent.modalTitle = 'Import Payments';
    this.importPaymentsComponent.modalFooter = false;
    this.importPaymentsComponent.icon = 'assets/images/model-icons/import-batch.svg';
    this.importPaymentsComponent.open(ImportPaymentsComponent);
  }

  public refreshBatchPayments(event: any): void {
    this.resultList.saveBatchPaymentsInfoModal(event);
  }

  public checkValid(event?: any, filterKey?: any): void {
    this[filterKey].patchValue(event.formatted);
    if (this.checkDateValid() && this.startDate.value && this.endDate.value && this.startDate.value > this.endDate.value) {
      this.isGreater = true;
    } else { this.isGreater = false; }
  }

  private checkDateValid(): any {
    return this.startDate.valid && this.endDate.valid ? true : false;
  }

  public checkAmountValid(): any {
    if (this.fromAmount.value !== '' && this.toAmount.value !== '' && Number(this.toAmount.value) < Number(this.fromAmount.value)) {
      this.isGreaterAmount = true;
    } else {
      this.isGreaterAmount = false;
    }
  }

  public isNotSelecting(): any {
    const currentParams = this.route.snapshot.queryParams;
    for (const key in currentParams) {
      if (currentParams.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  public searchBatchPayments(applyFilter = false): void {
    if (applyFilter) {
      this.filterParameters = {
        startDate: this.startDate.value,
        endDate: this.endDate.value,
        fromAmount: this.fromAmount.value,
        toAmount: this.toAmount.value,
        currency: this.currency.value,
        page: null,
        size: null,
        returnAppId: this.returnAppId,
        navParam: this.navParam,
        allBatches: this.isAllBatchesChecked
      };
    }
    this.dateFilterExecution(this.filterParameters);
  }

  public resetBatchPayments(): void {
    this.filterTagsList = [];
    const data: any = {
      startDate: undefined,
      endDate: undefined,
      toAmount: undefined,
      fromAmount: undefined,
      currency: undefined
    };
    this.patchData(data);
    this.getDropdownItems(PropertyName.Currency);
    this.router.navigate(['/ent-payments', 'batch-payments'], {
      queryParams: {
        returnAppId: this.returnAppId,
        nav: this.navParam,
        allBatches: this.securityEngineService.checkVisibility('allBatches', 'Show') ? this.isAllBatchesChecked : null,
        roleSequenceId: this.securityEngineService.roleSequenceId
      }
    });
  }

  public openSecurity(event?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, 'searchSection', this.multiCurrencyEnabled ? undefined : ['currency']);
  }
}
