import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-batch-payments-actions',
  templateUrl: './batch-payments-actions.component.html'
})
export class BatchPaymentsActionsComponent implements OnInit {
  @Input() batchPayment: any;
  @Output() actionClicked: EventEmitter<any> = new EventEmitter<any>();
  @Input() showDelete = true;
  @Input() isBatchPaymentListLoaded: boolean;
  public isAccept = false;
  public isDeposit = false;
  public isAcceptAndDeposit = false;
  public isEdit = false;
  public isDelete = false;

  constructor(public securityEngineService: SecurityEngineService) { }

  ngOnInit() { }

  public eventClick(): void {
    this.checkBatchPaymentOption();
  }

  private checkBatchPaymentOption(): void {
    if (this.batchPayment) {
      this.isEdit = this.batchPayment?.isEdit;
      this.isDelete =this.batchPayment?.isDelete;
      this.isAccept = this.batchPayment?.isAccept;
      this.isDeposit = this.batchPayment?.isDeposit;
      this.isAcceptAndDeposit = this.batchPayment?.isAcceptAndDeposit;
    }
  }

  public performAction(action: string): void {
    this.actionClicked.emit(action);
  }
}
