import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { DateService } from 'src/app/shared/services/date.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { BatchPaymentsService } from '../../../services/batch-payments.service';
import { BatchModel } from '../../../models/batch.model';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-add-batch',
  templateUrl: './add-batch.component.html'
})
export class AddBatchComponent implements OnInit {
  public batchModel: BatchModel = new BatchModel();
  public systemDate: any;
  private supportData: any;
  public addBatchForm: FormGroup;
  public DepositBankDropdown: any;
  public ControlBankDropdown: any;
  public currencyDropdown: any;
  public multiCurrencyDropdown: any;
  public multiCurrencyEnabled: any;
  public isBatchId = true;
  public modalType: any;
  public postDatePickerOptions = {
    componentDisabled: false
  };
  public multiCurrencyHide = false;
  public batchDetails: any;
  public currentDepositBank: string;
  public currentControlBank: string;
  constructor(
    private fb: FormBuilder,
    private modal: ModalComponent,
    private loadingService: LoadingService,
    private dateService: DateService,
    private batchPaymentsService: BatchPaymentsService,
    public commonService: CommonService,
    public securityEngineService: SecurityEngineService,
    private cdr: ChangeDetectorRef,
    private router: Router) { }

  ngOnInit() {
    this.addBatchForm = this.fb.group({
      postedDate: ['', Validators.compose([Validators.required, ValidationService.dateValidator])],
      depositDate: ['', Validators.compose([Validators.required, ValidationService.dateValidator])],
      batchAmount: ['', Validators.required],
      depositBank: ['', Validators.required],
      controlBank: ['', Validators.required],
      currency: [''],
      batchId: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(4), ValidationService.batchIdValidator])]
    });
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.getBatchPaymentsSupportData();
  }

  initialize() {
    if (this.batchPaymentsService.modalType) {
      this.modalType = this.batchPaymentsService.modalType;
      if (this.modalType === 'Add') {
        this.patchValues(null);
      } else {
        this.postDatePickerOptions = {
          componentDisabled: true
        };
        this.batchModel = { ...this.batchPaymentsService.paramsToModal };
        this.getBatchDetails();
      }
    }
  }

  getBatchPaymentsSupportData() {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchSupportData().subscribe(
      data => {
        if (data) {
          this.supportData = data?.supportData || null;
          this.multiCurrencyDropdown = this.commonService.getDropdownData(PropertyName.MultiCurrency, this.supportData);
          this.DepositBankDropdown = this.commonService.getDropdownData(PropertyName.DepositBank, this.supportData);
          this.currencyDropdown = this.commonService.getDropdownData(PropertyName.Currency, this.supportData);
          this.checkMultiCurrencyActived();
          this.initialize();
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private checkMultiCurrencyActived(): void{
    if (this.multiCurrencyDropdown?.propertyValues) {
      this.multiCurrencyEnabled = false;
      this.multiCurrencyHide = false;
      if (this.multiCurrencyDropdown.propertyValues[0] === 'true') {
        this.multiCurrencyHide = true;
        if (this.batchPaymentsService.modalType === 'Edit') {
          this.multiCurrencyEnabled = this.batchPaymentsService.paramsToModal.numberOfTransactions !== 0 ? true : false;
        }
        if (this.multiCurrencyHide) {
          this.form.currency.setValidators(Validators.required);
        } else {
          this.form.currency.setValidators(null);
        }
        this.form.currency.updateValueAndValidity();
      }
    }
  }

  public convertToUpperCase(event?: any, property?: any): void {
    if (event) { this.form[property].patchValue(event); }
  }

  patchValues(data: any) {
    this.addBatchForm.patchValue({
      postedDate: data?.postedDate || this.systemDate,
      depositDate: data?.depositDate || this.systemDate,
      currency: data?.currency || this.currencyDropdown?.defaultValue,
      depositBank: data?.depositBank || this.DepositBankDropdown?.defaultValue,
      controlBank: data?.controlBank || '',
      batchAmount: data?.batchAmount || 0,
      batchId: (data?.batchId || null)
    });
    if(this.form.depositBank.value){
      this.getControlBankSupportData();
    }
    this.loadingService.toggleLoadingIndicator(false);
  }

  getBatchDetails() {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchDetails(this.batchModel.batchId, this.batchModel.postedDate, this.batchModel.userId).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.batchDetails = data;
          this.currentControlBank = this.batchDetails.controlBank;
          this.currentDepositBank = this.batchDetails.depositBank;
          this.patchValues(data);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  public submit(): void {
    const body = this.addBatchForm.getRawValue();
    if (body.depositDate === '') { delete body.depositDate; }
    if (body.postedDate === '') { delete body.postedDate; }
    if (!this.multiCurrencyHide) { delete body.currency; }
    if (this.modalType === 'Add') {
      this.addBatch(body);
    } else {
      this.saveBatch(body);
    }
  }

  private addBatch(body?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.addBatch(body).subscribe(
      data => {
        if (data.batchId && data.postedDate && data.userId) {
          this.redirectToBatchSummary(data.batchId, data.postedDate, data.userId);
        }
        this.loadingService.toggleLoadingIndicator(false);
        this.reset();
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, null, null, err);
      });
  }

  public saveBatch(body: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.updateBatch(this.batchModel.batchId, this.batchModel.postedDate, this.batchModel.userId, body).subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset();
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, null, null, err);
      });
  }

  get form() { return this.addBatchForm.controls; }

  redirectToBatchSummary(batchId: any, postedDate: any, userId: any) {
    this.router.navigate(['/ent-payments', 'batch-payments', batchId, 'payments', postedDate, userId]);
  }

  private reset() {
    this.batchPaymentsService.paramsToModal = null;
    this.modal.close(true);
  }

  public selectedDepositBank(event?: any): void {
    this.form.controlBank.patchValue('');
    this.getControlBankSupportData();
  }

  private getControlBankSupportData(): void {
    const controlBankItems: Array<any> = [];
    let controlBankDefaultValue: string = '';
    if (this.form.depositBank.value) {
      this.loadingService.toggleLoadingIndicator(true);
      this.batchPaymentsService.getControlBankSupportData(this.form.depositBank.value).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          const controlBank: any = data;
          if(controlBank){
            (controlBank.defaultValue) && (controlBankDefaultValue = controlBank.defaultValue);
            if (controlBank.propertyValues) {
              for (const sd of controlBank.propertyValues) {
                controlBankItems.push(sd);
              }
            }
          }
          if (controlBankItems) {
            this.ControlBankDropdown = controlBankItems;
            if(this.modalType === 'Add'){
              this.form.controlBank.setValue(controlBankDefaultValue);
            }else if(this.currentDepositBank === this.form.depositBank.value){
              this.form.controlBank.setValue(this.currentControlBank);
            }
            this.cdr.detectChanges();
          }
        },
        err => {
          this.commonService.handleRedirectErrorPageResponse(err);
        });
    }
  }
}