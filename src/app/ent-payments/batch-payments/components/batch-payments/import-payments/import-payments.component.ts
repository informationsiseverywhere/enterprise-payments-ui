import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { FileUploadComponent } from 'src/app/libs/file-upload/file-upload.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { BatchPaymentsService } from '../../../services/batch-payments.service';
import { ImportPaymentsService } from '../../../services/import-payments.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

const IMPORT_PAYMENT_JOB_ID = 'batchPayments';

@Component({
  selector: 'app-import-payments',
  templateUrl: './import-payments.component.html'
})
export class ImportPaymentsComponent implements OnInit {
  @ViewChild('paymentUpload', { static: false }) paymentUpload: FileUploadComponent;
  importPaymentForm: FormGroup;
  postedDate: any;
  depositDate: any;
  systemDate: any;
  userId: any;
  fileNameUploaded: any;
  isFileUploaded = false;
  public isBatchId = true;
  supportData: any;
  currencyDropdown: any;
  multiCurrencyDropdown: any;
  multiCurrencyEnabled: boolean;
  public DepositBankDropdown: any;
  public ControlBankDropdown: any;
  public controlBank: any;
  public batchId: any;
  public depositBank: any;
  constructor(
    private modal: ModalComponent,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private fb: FormBuilder,
    public appConfig: AppConfiguration,
    private batchPaymentsService: BatchPaymentsService,
    private importPaymentsService: ImportPaymentsService,
    public securityEngineService: SecurityEngineService,
    private commonService: CommonService,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    this.importPaymentForm = this.fb.group({
      postedDate: ['', Validators.compose([Validators.required, ValidationService.dateValidator])],
      depositDate: ['', Validators.compose([Validators.required, ValidationService.dateValidator])],
      depositBank: ['', Validators.required],
      controlBank: ['', Validators.required],
      currency: [''],
      batchId: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(4), ValidationService.batchIdValidator])]
    });
    this.systemDate = this.batchPaymentsService.systemDate;
    this.postedDate = this.batchPaymentsService.systemDate;
    this.depositDate = this.batchPaymentsService.systemDate;
    this.userId = resultQueryParam.userId;
    this.getBatchPaymentsSupportData();
  }

  selectFileOutput(event: any) {
    this.isFileUploaded = false;
  }

  getBatchPaymentsSupportData() {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchPaymentsService.getBatchSupportData().subscribe(
      data => {
        if (data) {
          this.supportData = data?.supportData || null;
          this.multiCurrencyDropdown = this.commonService.getDropdownData(PropertyName.MultiCurrency, this.supportData);
          this.DepositBankDropdown = this.commonService.getDropdownData(PropertyName.DepositBank, this.supportData);
          this.currencyDropdown = this.commonService.getDropdownData(PropertyName.Currency, this.supportData);
          this.checkMultiCurrencyActived();
          this.initialize();
          this.loadingService.toggleLoadingIndicator(false);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  get form() { return this.importPaymentForm.controls; }

  private checkMultiCurrencyActived(): void {
    if (this.multiCurrencyDropdown?.propertyValues) {
      this.multiCurrencyEnabled = false;
      if (this.multiCurrencyDropdown?.propertyValues[0] === 'true') {
        this.multiCurrencyEnabled = true;
        if (this.multiCurrencyEnabled) {
          this.importPaymentForm.get('currency').setValidators(Validators.required);
        } else {
          this.importPaymentForm.get('currency').setValidators(null);
        }
        this.importPaymentForm.get('currency').updateValueAndValidity();
      }
    }
  }

  private initialize(): void {
    this.depositBank = this.DepositBankDropdown?.defaultValue || '';
    this.importPaymentForm.controls['currency'].setValue(this.currencyDropdown?.defaultValue);
    if (this.depositBank) {
      this.getControlBankSupportData();
    }
  }

  uploadFile(formData: FormData) {
    this.loadingService.toggleLoadingIndicator(true);
    this.importPaymentsService.uploadFile(IMPORT_PAYMENT_JOB_ID, formData).subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.isFileUploaded = true;
        this.alertService.success('Uploaded file successfully.');
        this.fileNameUploaded = this.paymentUpload.filename;
        this.paymentUpload.isSupportedFile = false;
      },
      (err) => {
        this.commonService.handleErrorResponse(err);
      });
  }

  public convertToUpperCase(event?: any, property?: any): void {
    if (event) { this.form[property].patchValue(event); }
  }

  importPaymentsSubmit() {
    this.loadingService.toggleLoadingIndicator(true);
    this.importPaymentsService.generateImportPayment(
      this.postedDate, this.fileNameUploaded, this.depositDate, this.batchId, this.depositBank, this.controlBank, this.userId, this.importPaymentForm.get('currency').value).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.verifyImportPaymentStatus(data);
        },
        err => {
          this.commonService.handleErrorResponse(err);
        });
  }

  verifyImportPaymentStatus(data: any) {
    const status = data ? data.jobStatusCode : null;
    if (status === 'STARTED' || 'STARTING') {
      this.alertService.success('Import payment initiated.');
      this.modal.close(true);
    } else if (status === 'FAILED') {
      const exitDescription = data.exitDescription;
      this.alertService.error(exitDescription);
    }
  }

  public downloadTemplate(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.importPaymentsService.downloadTemplate(IMPORT_PAYMENT_JOB_ID).subscribe(
      (data: any) => {
        this.loadingService.toggleLoadingIndicator(false);
        const myBlob = new Blob([data], {
          type: 'application/vnd.ms-excel;text/csv'
        });
        if (navigator.msSaveBlob) { // IE 10+
          navigator.msSaveBlob(myBlob, IMPORT_PAYMENT_JOB_ID + '.csv');
        } else {
          const url = window.URL.createObjectURL(data);
          const link = document.createElement('a');
          document.body.appendChild(link);
          link.download = IMPORT_PAYMENT_JOB_ID + '.csv';
          link.href = url;
          link.click();
        }
      },
      (err: any) => {
        this.commonService.handleErrorResponse(err);
      });
  }

  public selectedDepositBank(): void {
    this.controlBank = '';
    this.getControlBankSupportData();
  }

  private getControlBankSupportData(): void {
    const controlBankItems: Array<any> = [];
    let controlBankDefaultValue: string = '';
    if (this.depositBank) {
      this.loadingService.toggleLoadingIndicator(true);
      this.batchPaymentsService.getControlBankSupportData(this.depositBank).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          const controlBank: any = data;
          if (controlBank) {
            (controlBank.defaultValue) && (controlBankDefaultValue = controlBank.defaultValue);
            if (controlBank.propertyValues) {
              for (const sd of controlBank.propertyValues) {
                controlBankItems.push(sd);
              }
            }
          }
          if (controlBankItems) {
            this.ControlBankDropdown = controlBankItems;
            this.form.controlBank.setValue(controlBankDefaultValue);
            this.cdr.detectChanges();
          }
        },
        err => {
          this.commonService.handleRedirectErrorPageResponse(err);
        });
    }
    if (controlBankItems) {
      this.ControlBankDropdown = controlBankItems;
    }
  }
}