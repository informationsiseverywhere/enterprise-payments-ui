import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { paymentsRoutes } from './batch-payments-routes';

@NgModule({
  imports: [
    RouterModule.forChild(paymentsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class BatchPaymentsRoutingModule {
}
