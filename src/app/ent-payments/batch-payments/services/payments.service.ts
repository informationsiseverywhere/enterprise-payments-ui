import { Injectable } from '@angular/core';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { UnidentifiedPaymentModel } from 'src/app/ent-payments/unidentified-payment/model/unidentified-payment.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  public objectToModal: any;
  newPaymentDetails: any;
  amount: any;
  accountBalanceDetail: any;
  accountDetails: any;
  storedAccountDetails: any;
  accountId: any;
  policyDetails: any;
  planUpdateDetails: any;
  agentType: any;
  apiUrls: any;
  multiHostUrls: any;
  currency: any;
  agencies: any;
  isAgencySweepPayorChange: any;
  isUserAgent: any;
  public isUnidentifiedAgencySweepPayment: boolean = false;
  public unidentifiedPaymentDetails: UnidentifiedPaymentModel = new UnidentifiedPaymentModel();

  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    public commonService: CommonService) {
    this.multiHostUrls = this.appConfig.multiHostUrls;
  }

  getBalances(accountId?: string, type?: string) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId + '/balance';
    if (type === 'Group') {
      url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.groupBilling) + 'v1/billing/accounts/' + accountId + '/balance';
    }
    return this.restService.getByUrl(url);
  }
  getPolicyBalanceDetails(accountId?: any, policyId?: any, policyEffectiveDate?: any, type?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId +
      '/contracts/' + policyId + '/amounts?polEffDt=' + policyEffectiveDate;

    return this.restService.getByUrl(url);
  }

  getPolicyAccountDetail(accountId?: any, policyId?: any, policyEffectiveDate?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId +
      '/contracts/' + policyId + '?polEffDt=' + policyEffectiveDate;

    return this.restService.getByUrl(url);
  }
  getStatementDetail(accountId?: string, statementDate?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.groupBilling) + 'v1/billing/accounts/' + accountId + '/statements/' + statementDate;

    return this.restService.getByUrl(url);
  }
  makePayment(payment?: any, type?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments) + 'v1/payments?mode=charge_profile';
    if (type === 'recurringPayments') {
      url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments) + 'v1/subscriptions?mode=charge_profile';
    }

    return this.restService.postByUrl(url, payment);
  }
  makeNewPayment(payment?: any, type?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments) + 'v1/payments';
    if (type === 'recurringPayments') {
      url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments) + 'v1/subscriptions';
    }
    return this.restService.postByUrl(url, payment);
  }
  callToEnterprisePaymentsAPI(payment?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/payments';

    return this.restService.postByUrl(url, payment);
  }

  setupAutoPaymentOff(payment?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/subscriptions';

    return this.restService.postByUrl(url, payment);
  }

  getPaymentPdf(paymentSequenceNumber?: any, postedDate?: string, userId?: string, batchId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/payments/' +
      paymentSequenceNumber + '/pdf?postedDate=' + postedDate + '&userId=' + userId + '&batchId=' + batchId;

    return this.restService.getPDFByUrlWithoutParsingToJson(url);
  }

  getInvoiceScheduleList(accountId?: any, since?: any, until?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId + '/schedule';

    if (since) {
      url = url + '?since=' + since;
      if (until) {
        url = url + '&until=' + until;
      }
    } else if (until) {
      url = url + '?until=' + until;
    }

    return this.restService.getByUrl(url);
  }
  getPaymentsSupportData() {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments)}v1/billing/payments`;
    return this.restService.optionsByUrl(url);
  }
  emailsNotifications(details: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/notifications/emails';
    return this.restService.postByUrl(url, details);
  }
  smsNotification(details: any) {
    const url = this.multiHostUrls.notifications.smsNotification;
    return this.restService.postByUrl(url, details);
  }
  getPaymentDetailByUrl(url: string) {
    return this.restService.getByUrl(url);
  }

  createQuoteSchedule(accountId?: any, body?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId + '/quotes';
    return this.restService.postByUrl(url, body);
  }

  getAllQuoteSchedule(accountId?: any, size?: any, page?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId + '/quotes';
    url = this.commonService.buildSearchQueryUrl(url, page, size);
    return this.restService.getByUrl(url);
  }
  getQuoteSchedule(accountId?: any, plan?: any, size?: any, page?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId + '/quotes/' + plan;
    url = this.commonService.buildSearchQueryUrl(url, page, size);
    return this.restService.getByUrl(url);
  }

  savePlan(accountId?: any, body?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId;
    return this.restService.patchByUrl(url, body);
  }

  public getAgentDetails(agent?: string, isUserAgent?: any): any {
    let url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client)}v1/`;
    if (isUserAgent === 'true') {
      url += `userAgencies?filters=type:Agent,agentNumber:${agent}`;
    } else {
      url += `parties?filters=type:Agent,agentNumber:${agent}`;
    }
    return this.restService.getByUrl(url);
  }

  public getUserAgencies(agencies: any, isUserAgent: any): any {
    let url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client)}v1/`;
    if (isUserAgent === 'true') {
      url += `userAgencies?filters=${agencies}`;
    } else {
      url += `parties?filters=${agencies}`;
    }
    return this.restService.getByUrl(url);
  }

  public getAgencySweepPaymentSupportData(accountId?: string): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId;
    return this.restService.optionsByUrl(url);
  }

  public getStoredCard(clientId?: any): any {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments) + 'v1/profiles/' + clientId;
    return this.restService.getByUrl(url);
  }

  getPartiesSupportdata() {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties';
    return this.restService.optionsByUrl(url);
  }

  public getClientStoredCard(clientId?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties/' + clientId + '/clientBankProfiles';
    return this.restService.getByUrl(url);
  }

  public createClientCreditBankAccount(clientId?: any, body?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties/' + clientId + '/clientBankProfiles';
    return this.restService.postByUrl(url, body);
  }

  public getAgentStoredCard(clientId?: any, billingType?: any, bankProfileAttribute?: any, agencySweepPaymentIntegration?: boolean): any {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties/' + clientId + '/clientBankProfiles';
    if (agencySweepPaymentIntegration) {
      url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties/' + clientId + '/bankProfiles';
    }
    if (bankProfileAttribute === 'true') {
      url += '?filters=type:Account,value:' + billingType;
    }
    return this.restService.getByUrl(url);
  }

  public getAccountDetails(accountId?: any, type?: any): any {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts?filters=accountId:' + accountId;
    if (type === 'Group') {
      url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.groupBilling) + 'v1/billing/accounts?filters=accountId:' + accountId + ',type:' + type;
    }
    return this.restService.getByUrl(url);
  }

  public getAccountDetailsWithoutFilter(accountId?: any, type?: any): any {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId;
    if (type === 'Group') {
      url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.groupBilling) + 'v1/billing/accounts/' + accountId + '?filters=type:' + type;
    }
    return this.restService.getByUrl(url);
  }

  public validatePaymentMethod(body?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/validatepaymentmethod';
    return this.restService.postByUrl(url, body);
  }

  public getPaymentProfiles(url?: any): any {
    return this.restService.getByUrl(url);;
  }

  getEftPlanSupportData(accountId: any) {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling)}v1/billing/accounts/${accountId}`;
    return this.restService.optionsByUrl(url);
  }

  public generateToken(body?: any): any {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments)}v1/generateToken`;
    return this.restService.postByUrl(url, body);
  }

  getSummary(partyId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties/' + partyId;
    return this.restService.getByUrl(url);
  }

  getPolicySymbol(accountId?: any, policyNumber?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId +
      '/contracts?filters=policyNumber:' + policyNumber;
    return this.restService.getByUrl(url);
  }
}
