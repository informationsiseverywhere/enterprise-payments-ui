import { Injectable } from '@angular/core';

import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Injectable({
  providedIn: 'root'
})
export class BatchPaymentsService {
  public batchId: any;
  public postedDate: any;
  public userId: any;
  public paymentDetailsObjectToModal: any;
  public filterType: any;
  public paramsToModal: any;
  public modalAction: any;
  public filterParameters: any;
  public addAnotherPayment = false;
  public searchType: string;
  public modalType: any;
  public batchAmount: any;
  systemDate: any;
  public currency: any;
  public multiCurrencyEnabled: any;
  public currencyDropdown: any;
  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    public commonService: CommonService) { }

  getPaymentList(batchId?: any, postedDate?: any, userId?: any, identifier?: any, paymentType?: any, size?: any, page?: any) {
    const url = this.buildPaymentListExecutionUrl(batchId, postedDate, userId, identifier, paymentType, size, page);
    return this.restService.getByUrl(url);

  }

  buildPaymentListExecutionUrl(batchId: any, postedDate: any, userId: any, identifier: any, paymentType: any, size: any, page: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '/payments?postedDate=' + postedDate + '&userId=' + userId;
    const params: any = [];
    identifier && params.push({ name: 'identifier', value: identifier });
    paymentType && params.push({ name: 'paymentType', value: paymentType });
    let queryString = '';
    for (const param of params) {
      queryString += param.name + ':' + param.value;
      if (param !== params[params.length - 1]) {
        queryString += ',';
      }
    }
    if (queryString !== '') {
      url += '&filters=' + queryString;
    }

    url = this.commonService.buildSearchQueryUrl(url, page, size);
    return url;

  }

  getBatchList(since?: string, until?: string, fromAmount?: any, toAmount?: any, currency?: any, size?: any, page?: any, allBatches?:any) {
    const url = this.buildExecutionUrl(since, until, fromAmount, toAmount, currency, size, page, allBatches);
    return this.restService.getByUrl(url);
  }

  deleteBatch(batchId?: any, postedDate?: any, userId?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '?postedDate=' + postedDate + '&userId=' + userId;
    return this.restService.deleteByUrl(url);
  }

  updateBatch(batchId?: any, postedDate?: any, userId?: any, body?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '?postedDate=' + postedDate + '&userId=' + userId;
    return this.restService.patchByUrl(url, body);
  }

  updateBatchPayment(batchId?: any, postedDate?: any, paymentSequenceNumber?: any, body?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '/payments/'
      + paymentSequenceNumber + '?postedDate=' + postedDate;
    return this.restService.patchByUrl(url, body);
  }

  getBalances(accountId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId + '/balance';
    return this.restService.getByUrl(url);
  }

  getGroupBalances(accountId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.groupBilling) + 'v1/billing/accounts/' + accountId + '/balance';
    return this.restService.getByUrl(url);
  }

  getAgencyBalances(accountId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.agencyBilling) + 'v1/billing/accounts/' + accountId + '/balance';
    return this.restService.getByUrl(url);
  }

  getStatementList(accountId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.groupBilling) + 'v1/billing/accounts/' + accountId + '/statements';
    return this.restService.getByUrl(url);
  }

  getAgencyStatementList(accountId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.agencyBilling) + 'v1/billing/accounts/' + accountId + '/statements';
    return this.restService.getByUrl(url);
  }

  getAccountDetails(accountId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId;
    return this.restService.getByUrl(url);
  }

  getGroupAccountDetails(accountId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.groupBilling) + 'v1/billing/accounts/' + accountId;
    return this.restService.getByUrl(url);
  }

  getAgencyAccountDetails(accountId?: string) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.agencyBilling) + 'v1/billing/accounts/' + accountId;
    return this.restService.getByUrl(url);
  }

  buildExecutionUrl(since?: string, until?: string, fromAmount?: any, toAmount?: any, currency?: any, size?: any, page?: any, allBatches?:any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches';
    const params: any = [];
    if (since) {
      params.push({ name: 'since', value: since });
    }
    if (allBatches) {
      params.push({ name: 'allBatches', value: allBatches });
    }
    if (until) {
      params.push({ name: 'until', value: until });
    }
    if (fromAmount) {
      params.push({ name: 'fromAmount', value: fromAmount });
    }
    if (toAmount) {
      params.push({ name: 'toAmount', value: toAmount });
    }
    if (currency) {
      params.push({ name: 'currency', value: currency });
    }
    let queryString = '';
    for (const param of params) {
      queryString += param.name + ':' + param.value;
      if (param !== params[params.length - 1]) {
        queryString += ',';
      }
    }
    if (queryString !== '') {
      url += '?filters=' + queryString;
    }

    url = this.commonService.buildSearchQueryUrl(url, page, size);
    return url;
  }

  getBatchDetails(batchId?: any, postedDate?: any, userId?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '?postedDate=' + postedDate + '&userId=' + userId;
    return this.restService.getByUrl(url);
  }

  getBatchSupportData() {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches';
    return this.restService.optionsByUrl(url);
  }

  addBatch(processType?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches';
    return this.restService.postByUrl(url, processType);
  }

  public editBatchTotal(processType?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + this.batchId + '?postedDate=' + this.postedDate + '&userId=' + this.userId;
    return this.restService.patchByUrl(url, processType);
  }

  getBatchPaymentsSupportData(batchId?: any, postedDate?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '/payments?postedDate=' + postedDate;
    return this.restService.optionsByUrl(url);
  }

  getPaymentDetails(batchId?: any, postedDate?: any, seqNumber?: any, size?) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '/payments/' + seqNumber + '?postedDate=' + postedDate;
    url = this.commonService.buildSearchQueryUrl(url, undefined, size);
    return this.restService.getByUrl(url);

  }

  addBatchPayments(batchId?: any, postedDate?: any, processType?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '/payments?postedDate=' + postedDate;
    return this.restService.postByUrl(url, processType);
  }

  editBatchPayments(batchId?: any, postedDate?: any, userId?: any, paymentSequenceNumber?: any, processType?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '/payments/' + paymentSequenceNumber + '?postedDate=' + postedDate + '&userId=' + userId;
    return this.restService.patchByUrl(url, processType);
  }

  deleteBatchPaymentDetails(batchId?: any, postedDate?: any, userId?: any, paymentSequenceNumber?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '/payments/' + paymentSequenceNumber + '?postedDate=' + postedDate + '&userId=' + userId;
    return this.restService.deleteByUrl(url);
  }

  getPolicyDetail(policyNumber?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts?filters=policyNumber:' + policyNumber;
    return this.restService.getByUrl(url);
  }

  getPolicyAmount(accountId?: any, policyNumber?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId +
      '/contracts?filters=policyNumber:' + policyNumber;
    return this.restService.getByUrl(url);
  }

  getPolicyAmountDetail(accountId?: any, policyId?: any, polEffDt?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts/' + accountId +
      '/contracts/' + policyId + '/amounts?polEffDt=' + polEffDt;
    return this.restService.getByUrl(url);
  }

  public getBatchDetailReportExcel(batchId?: any, postedDate?: any, userId?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/batches/' + batchId + '/_report?postedDate=' + postedDate + '&userId=' + userId;
    return this.restService.getByUrlWithoutParsingToJsonObserved(url);
  }

  getExchangeRates(fromCurrency?: any, toCurrency?: any, postedDate?: any) {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments)}v1/utils/exchangeRates/${postedDate}?fromCurrency=${fromCurrency}&toCurrency=${toCurrency}`;
    return this.restService.getByUrl(url);
  }

  getTransactionRateHistory(queryParams?: any, body?: any) {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments)}v1/billing/batches/` +
      `${queryParams.batchId}/payments/${queryParams.paymentSequenceNumber}/transactionRateHistory?postedDate=${queryParams.postedDate}`;
    return this.restService.postByUrl(url, body);
  }

  deleteTransactionRateHistory(queryParams?: any) {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments)}v1/billing/batches/` +
      `${queryParams.batchId}/payments/${queryParams.paymentSequenceNumber}/transactionRateHistory?postedDate=${queryParams.postedDate}`;
    return this.restService.deleteByUrl(url);
  }

  public getControlBankSupportData(depositBank?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/banks/' + depositBank + '/supportData';
    return this.restService.getByUrl(url);
  }
}
