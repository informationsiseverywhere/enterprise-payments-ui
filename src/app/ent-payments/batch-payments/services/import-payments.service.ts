import { Injectable } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';

@Injectable({
  providedIn: 'root'
})
export class ImportPaymentsService {
  apiUrls: any;

  constructor(
    private restService: RestService,
    private appConfig: AppConfiguration) {
      this.apiUrls = this.appConfig.apiUrls;
     }

  uploadFile(jobId?: string, formData?: FormData) {
    const url = this.apiUrls.batch + 'v1/jobs/' + jobId + '/fileUpload';
    return this.restService.postFormDataByUrl(url, formData);
  }

  public downloadTemplate(jobId?: string): any {
    const url = this.apiUrls.batch + 'v1/jobs/' + jobId + '/templates ';
    return this.restService.getByUrlWithoutParsingToJsonObserved(url);
  }

  generateImportPayment(entryDate?: any, batchPayments?: any, depositDate?: any, batchId?: any, depositBank?: any, controlBank?: any, userId?: any, currency?: any) {
    let url = this.apiUrls.batch + 'jobs/batchPayments';
    const params: any = [];
    entryDate && params.push({ name: 'entryDate', value: entryDate });
    batchPayments && params.push({ name: 'batch-payments', value: batchPayments });
    depositDate && params.push({ name: 'depositDate', value: depositDate });
    depositBank && params.push({ name: 'depositBank', value: depositBank });
    controlBank && params.push({ name: 'controlBank', value: controlBank });
    userId && params.push({ name: 'userId', value: userId });
    currency && params.push({ name: 'currency', value: currency });
    batchId && params.push({ name: 'batchId', value: batchId });
    let queryString = '';
    for (const param of params) {
      queryString += param.name + '=' + param.value;
      if (param !== params[params.length - 1]) {
        queryString += ',';
      }
    }
    if (queryString !== '') {
      url += '?jobParameters=' + queryString;
    }
    return this.restService.postByUrl(url, undefined);
  }
}
