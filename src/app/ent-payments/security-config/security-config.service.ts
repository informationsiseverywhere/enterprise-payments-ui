import { Injectable } from '@angular/core';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentSecurityConfigService {
  public objectToModal: any;
  public objectToModalData: any;
  public objectToModalAction: any;

  constructor(
    private restService: RestService,
    private securityEngineService: SecurityEngineService,
    private loadingService: LoadingService,
    public commonService: CommonService) { }

  public getBillingSecuritySettingMetadata(fieldName?: any, configType?: string): any {
    let url = 'assets/metadata/payment-security-config/';

    switch (fieldName) {
      case 'epDashBoard':
        url += 'dashboard.json';
        break;
      case 'epBatchList':
        if(configType === 'tableSection') { url += 'batch-payments-table.json'; }
        else if(configType === 'searchSection') { url += 'batch-payments.json'; }
        break;
      case 'epBatchSummary':
        if(configType === 'tableSection') { url += 'batch-summary-table.json'; }
        else if(configType === 'batchDetailSection') { url += 'batch-summary-detail.json'; }
        else { url += 'batch-summary.json'; }
        break;
      case 'epPaymentsSearchUnidentifiedPayments':
        if(configType === 'tableSection') { url += 'unidentified-payment-search-table.json'; }
        else if(configType === 'searchSection') { url += 'unidentified-payment-search.json'; }
        break;
      case 'epPaymentsSearchIdentifiedPayments':
        if(configType === 'tableSection') { url += 'identified-payment-search-table.json'; }
        else if(configType === 'searchSection') { url += 'identified-payment-search.json'; }
        break;
      case 'epProvideBillingInformation':
        url += 'express-payments.json';
        break;
      case 'epAddNewPaymentMethod':
        if(configType === 'RecurringPayment') { url += 'add-recurring-payment-method.json'; }
        else if(configType === 'UnidentifiedPayment') { url += 'add-unidentified-payment-method.json'; }
        else { url += 'add-payment-method.json'; }
        break;
      case 'epBatchRejections':
        url += 'batch-rejections.json';
        break;
      case 'epBatchRejectionSummary':
        if(configType === 'tableSection') { url += 'batch-rejection-detail-table.json'; }
        else { url += 'batch-rejection-detail.json'; }
        break;
      case 'epPayments':
        if(configType === 'paymentType') { url += 'payment-type.json'; }
        else if(configType === 'selectPayment') { url += 'select-payment.json'; }
        else if(configType === 'selectRecurringPayment') { url += 'select-recurring-payment.json'; }
        else if(configType === 'selectRecurringPaymentAcceptJS') { url += 'select-recurring-payment-acceptJS.json'; }
        else if(configType === 'choosePaymentMethod') { url += 'choose-payment-method.json'; }
        else if(configType === 'chooseAgencySweepPaymentMethod') { url += 'choose-agency-sweep-payment-method.json'; }
        else if(configType === 'confirmAndRecurringPay') { url += 'confirm-and-recurring-pay.json'; }
        else { url += 'confirm-and-pay.json'; }
        break;
      case 'epUnidentifiedPayments':
        if(configType === 'PolicyPayment') { url += 'unidentified-policy-confirm-and-pay.json'; }
        else if(configType === 'AccountBillPayment') { url += 'unidentified-bill-confirm-and-pay.json'; }
        break;
    }
    return this.restService.getByUrlWithoutOptions(url);
  }

  public openSecurityParentSection(
    roleSequenceId?: any,
    parentName?: any,
    configType?: string,
    excludeField?: string[]
  ): void {
    this.securityEngineService.roleSequenceId = roleSequenceId;
    this.securityEngineService.resetSecurityList();
    this.securityEngineService.currentFieldName = parentName;
    this.securityEngineService.openConfigSection = this.securityEngineService.securityData[
      parentName
    ];
    this.securityEngineService.securityFieldData = this.securityEngineService.securityData[
      parentName
    ];
    this.loadingService.toggleLoadingIndicator(true);
    this.getBillingSecuritySettingMetadata(
      this.securityEngineService.openConfigSection.fieldName,
      configType
    ).subscribe(
      (data) => {
        this.securityEngineService.securityConfigList = data ? data : null;
        this.removeExcludeField(excludeField, this.securityEngineService.securityConfigList);
        this.securityEngineService.getSecurityControlSetting(
          roleSequenceId,
          parentName
        );
        this.loadingService.toggleLoadingIndicator(false);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

  public openSecuritySection(
    roleSequenceId?: any,
    fieldName?: any,
    configType?: string,
    excludeField?: string[]
  ): void {
    this.securityEngineService.roleSequenceId = roleSequenceId;
    this.securityEngineService.currentFieldName = fieldName;
    this.securityEngineService.resetSecurityList();
    this.loadingService.toggleLoadingIndicator(true);
    this.getBillingSecuritySettingMetadata(fieldName, configType).subscribe(
      (data) => {
        this.securityEngineService.securityConfigList = data ? data : null;
        this.removeExcludeField(excludeField, this.securityEngineService.securityConfigList);
        this.securityEngineService.securityOpen = true;
        let securityDataTemp;
        if (fieldName === this.securityEngineService.additionalSecurityName) {
          securityDataTemp = JSON.parse(
            JSON.stringify(this.securityEngineService.additionalSecuritySettings)
          );
        } else {
          securityDataTemp = JSON.parse(
            JSON.stringify(this.securityEngineService.securityData)
          );
        }
        this.securityEngineService.securitySectionData = securityDataTemp;
        this.securityEngineService.securitySectionList = Object.keys(
          securityDataTemp
        ).map((key) => securityDataTemp[key]);
        this.loadingService.toggleLoadingIndicator(false);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

  private removeExcludeField(excludeField?: any, securityConfigList?: any): void{
    if(excludeField && excludeField.length > 0){
      for (const fieldName of excludeField) {
        securityConfigList = securityConfigList.filter(item => item.fieldName != fieldName);
        for (const item of securityConfigList) {
          if(item.values){ item.values = item.values.filter(item => item.fieldName != fieldName); }
        }
      }
    }
  }

  public getSupplementalFieldsSecuritySection(
    parentFieldName?: any,
    technicalKey?: any
  ): any {
    this.securityEngineService.supplementalFieldsSettings = undefined;
    technicalKey = technicalKey ? technicalKey : 'All';
    this.securityEngineService.getSupplementalFields(technicalKey).subscribe(
      (data) => {
        let supplementalFields = data ? data.fieldList : null;
        this.securityEngineService.technicalKey = technicalKey;
        supplementalFields = supplementalFields.filter(
          (item) => item.parentFieldName === parentFieldName
        );
        this.securityEngineService.supplementalFieldsSettings = JSON.parse(
          JSON.stringify(supplementalFields)
        );
      },
      (err) => {
        this.commonService.handleErrorResponse(err);
      }
    );
  }
}

