import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { LibraryModule } from '../libs/libs.module';
import { EntPaymentsRoutingModule } from './ent-payments-routing.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ExpressPaymentsComponent } from './express-payments/express-payments.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    EntPaymentsRoutingModule
  ],
  declarations: [
    DashboardComponent,
    ExpressPaymentsComponent
  ],
  entryComponents: []
})
export class EntPaymentsModule { }
