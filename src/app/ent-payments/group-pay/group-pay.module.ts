import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { GroupPayRoutingModule } from './group-pay-routing.module';

import { GroupPayComponent } from './components/group-pay.component';

@NgModule({
  declarations: [
    GroupPayComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    GroupPayRoutingModule
  ]
})
export class GroupPayModule { }
