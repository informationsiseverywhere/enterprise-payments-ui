import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrencyPipe } from '@angular/common';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentReceivedComponent } from 'src/app/shared/components/payment-received/payment-received.component';
import { PaymentModel } from '../../batch-payments/models/batch.model';
import { PaymentService } from '../../batch-payments/services/payments.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from '../../security-config/security-config.service';
import { IconName, PropertyName } from 'src/app/shared/enum/common.enum';
import { PaymentusService } from 'src/app/shared/services/paymentus.service';
import { PaymentusTokenModel } from 'src/app/shared/models/paymentus.model';
import { AlertService } from 'src/app/libs/alert/services/alert.service';

@Component({
  selector: 'app-group-pay',
  templateUrl: './group-pay.component.html',
  styleUrls: ['./group-pay.component.scss'],
  providers: [CurrencyPipe]
})
export class GroupPayComponent implements OnInit {
  accountId: any;
  public propertyName = PropertyName;
  public iconName = IconName;
  accountBalanceDetail: any;
  isCurrentDue = false;
  isAccountBalance = false;
  isOtherAmount = false;
  public isPaymentMethod = PropertyName.inprogress;
  public isSelectAmount = PropertyName.active;
  public isConfirm = PropertyName.inprogress;
  amount: any;
  payment: PaymentModel = new PaymentModel();
  isBankIndex: any;
  accountDetails: any;
  storedAccountDetails: any;
  addNewPayment: any;
  clientId: any;
  type = PropertyName.Group;
  accountNumber: any;
  payorAddress: any;
  imageId: any;
  payorName: any;
  isEdit = false;
  isOtherAmountEntered = false;
  public isAccountDetailLoaded = false;
  public isAccountBalanceDetailLoaded = false;
  public isAccountBalanceDetailError = false;
  public isShowPaymentMethodButton = true;

  @ViewChild('paymentReceivedModel', { static: false }) paymentReceivedModel: ModalComponent;
  headerName: string;
  statementDate: any;
  isCustomerProfileId: any;
  returnAppId: any;
  imageUrl: any;
  currency: any;
  supportData: any;
  public oneTimePaymentIntegrationDropdown: any;
  public oneTimePaymentIntegration = false;
  paymentProviderDropdown: any;
  paymentProvider: any;
  summaryData: any;
  constructor(
    private router: Router,
    private loadingService: LoadingService,
    private route: ActivatedRoute,
    public appConfig: AppConfiguration,
    private paymentService: PaymentService,
    private paymentusService: PaymentusService,
    private currencyPipe: CurrencyPipe,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    private alertService: AlertService,
    public commonService: CommonService) {
    this.imageUrl = IconName.userProfilePath;
  }

  ngOnInit() {
    this.getPaymentsSupportData();
    this.route.queryParams.subscribe(
      queryParams => {
        if (queryParams.userId) {
          this.onSuccessNavigate(PropertyName.cancel);
        } else {
          this.returnAppId = queryParams.returnAppId;
          this.isCustomerProfileId = queryParams.isCustomerProfileId;
          if (this.paymentService.accountId === queryParams.accountId) {
            this.accountId = queryParams.accountId;
            this.statementDate = queryParams.statementDate;
            this.paymentService.accountId = this.accountId;
            this.paymentService.accountBalanceDetail = undefined;
          } else {
            this.accountId = queryParams.accountId;
            this.statementDate = queryParams.statementDate;
            this.paymentService.accountId = this.accountId;
            this.paymentService.accountBalanceDetail = undefined;
            this.paymentService.accountDetails = undefined;
            this.paymentService.storedAccountDetails = undefined;
          }
        }
      });
  }

  private getPaymentsSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentsSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.oneTimePaymentIntegration);
          this.getDropdownItems(PropertyName.paymentProvider);
          this.checkData();
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    let oneTimePaymentIntegration = 'false';
    this.oneTimePaymentIntegrationDropdown && (oneTimePaymentIntegration = this.oneTimePaymentIntegrationDropdown[0]);
    this.paymentProviderDropdown && (this.paymentProvider = this.paymentProviderDropdown[0]);
    if (oneTimePaymentIntegration === 'true') {
      this.oneTimePaymentIntegration = true;
    }
  }


  checkData() {
    this.getBalanceDetails(this.accountId, this.type);
    this.getAccountDetails(this.accountId, this.type);
    if (this.paymentService.newPaymentDetails) {
      this.addNewPayment = this.paymentService.newPaymentDetails;
      this.payment.amount = this.paymentService.amount;
      if (this.isCustomerProfileId) {
        this.makePaymentLoop(PropertyName.isPaymentMethod, true);
      } else {
        this.makePaymentLoop(PropertyName.isSelectAmount, true);
      }
    } else {
      if (this.payment && this.payment.paymentMethod && this.isSelectAmount !== PropertyName.active) {
        this.makePaymentLoop(PropertyName.isPaymentMethod, this.isPaymentMethod === PropertyName.active ? false : true);
      } else {
        this.makePaymentLoop(PropertyName.isSelectAmount, false);
      }
    }
  }
  getBalanceDetails(accountId: any, type: any) {
    this.isAccountBalanceDetailLoaded = false;
    this.isAccountBalanceDetailError = false;
    this.accountBalanceDetail = {};
    if (this.paymentService.accountBalanceDetail) {
      this.accountBalanceDetail = this.paymentService.accountBalanceDetail;
    } else {
      this.paymentService.getBalances(accountId, type).subscribe(
        data => {
          this.isAccountBalanceDetailLoaded = true;
          this.accountBalanceDetail.accountBalance = data.totalOutstandingAmount;
          this.statementDate ? this.getStatementDetail(this.accountId, this.statementDate) :
            this.checkSelectAmount(this.accountBalanceDetail);
        },
        (err) => {
          this.isAccountBalanceDetailError = true;
          this.commonService.handleErrorAPIResponse(this, 'isAccountBalanceDetailLoaded', 'accountBalanceDetail', err);
        });
    }
  }
  checkSelectAmount(data?: any) {
    if (data.accountBalance === 0) {
      this.isAccountBalance = false;
      if (data.currentDue === 0) {
        this.isCurrentDue = false;
        this.isOtherAmount = true;
        this.selectAmount(null, PropertyName.isOtherAmount);
      } else {
        if (this.statementDate) {
          this.isCurrentDue = true;
          this.selectAmount(data.currentDue, PropertyName.isCurrentDue);
        } else {
          this.accountBalanceDetail.currentDue = 0;
          this.isOtherAmount = true;
          this.selectAmount(1, PropertyName.isOtherAmount);
        }
      }
    } else {
      this.selectAmount(data.currentDue, PropertyName.isCurrentDue);
      this.isCurrentDue = true;
    }
  }
  selectAmount(value?: any, radioCheck?: any) {
    if (radioCheck !== PropertyName.isOtherAmount) { this.payment.amount = null; }
    if (radioCheck === PropertyName.isCurrentDue) {
      this[radioCheck] = true;
      this.isAccountBalance = false;
      this.isOtherAmount = false;
      this.payment.amount = value;
      this.isOtherAmountEntered = false;
    } else if (radioCheck === PropertyName.isAccountBalance) {
      this[radioCheck] = true;
      this.isCurrentDue = false;
      this.isOtherAmount = false;
      this.payment.amount = value;
      this.isOtherAmountEntered = false;
    } else if (radioCheck === PropertyName.isOtherAmount) {
      this[radioCheck] = true;
      this.isAccountBalance = false;
      this.isCurrentDue = false;
      if (!this.isOtherAmountEntered) {
        this.payment.amount = 1;
        this.isOtherAmountEntered = true;
      }
    }
  }
  getAccountDetails(accountId?: any, type?: any) {
    this.isAccountDetailLoaded = false;
    if (this.paymentService.accountDetails) {
      this.accountDetails = this.paymentService.accountDetails;
      this.clientId = this.accountDetails.payorId;
      this.payorName = this.accountDetails ? this.accountDetails.payorName : '';
      this.accountNumber = this.accountDetails ? this.accountDetails.accountNumber : '';
      this.payorAddress = this.accountDetails ? this.accountDetails.payorAddress : '';
      this.imageId = this.accountDetails ? this.accountDetails.imageId : undefined;
      this.currency = this.accountDetails ? this.accountDetails.currency : undefined;
      this.isAccountDetailLoaded = true;
      this.getStoredCard(this.clientId);
      if ((this.paymentProvider === PropertyName.paymentus && this.oneTimePaymentIntegration)) {
        this.getSummary(this.clientId);
      }
    } else {
      this.paymentService.getAccountDetails(accountId, type).subscribe(
        data => {
          this.accountDetails = data ? data.content[0] : null;
          this.paymentService.accountDetails = this.accountDetails;
          this.clientId = this.accountDetails.payorId;
          this.payorName = this.accountDetails ? this.accountDetails.payorName : '';
          this.accountNumber = this.accountDetails ? this.accountDetails.accountNumber : '';
          this.payorAddress = this.accountDetails ? this.accountDetails.payorAddress : '';
          this.imageId = this.accountDetails ? this.accountDetails.imageId : undefined;
          this.currency = this.accountDetails ? this.accountDetails.currency : undefined;
          this.isAccountDetailLoaded = true;
          this.getStoredCard(this.clientId);
          if ((this.paymentProvider === PropertyName.paymentus && this.oneTimePaymentIntegration)) {
            this.getSummary(this.clientId);
          }
        },
        (err) => {
          this.commonService.handleErrorAPIResponse(this, 'isAccountDetailLoaded', 'accountDetails', err);
        });
    }
  }
  public getStatementDetail(accountId?: any, statementDate?: any): void {
    this.paymentService.getStatementDetail(accountId, statementDate).subscribe(
      data => {
        const statement = data.statementInformation;
        const statementDueDate = data.statementInformation.statementDueDate;
        let paidAmount = statement.dueAmount ? statement.dueAmount : 0;
        if (statement.status.trim() === PropertyName.Paid) { paidAmount = 0; }
        this.accountBalanceDetail.currentDue = paidAmount;
        this.accountBalanceDetail.statementDueDate = statementDueDate;
        this.paymentService.accountBalanceDetail = this.accountBalanceDetail;
        if (!this.paymentService.newPaymentDetails) {
          this.checkSelectAmount(this.accountBalanceDetail);
        }
      },
      (err) => {
        this.commonService.handleErrorResponse(err);
      }
    );
  }
  getStoredCard(clientId?: any) {
    if (this.paymentService.storedAccountDetails) {
      this.storedAccountDetails = this.paymentService.storedAccountDetails;
      if (this.paymentService.newPaymentDetails) {
        if (!this.storedAccountDetails) {
          this.storedAccountDetails = {
            paymentProfiles: []
          };
        }
        this.selectAddNewBank(this.paymentService.newPaymentDetails);
      }
    } else {
      if (this.oneTimePaymentIntegration) {
        this.paymentService.getStoredCard(clientId).subscribe(
          data => {
            this.storedAccountDetails = data;
            this.paymentService.storedAccountDetails = data;
            if (this.addNewPayment) {
              if (!data) {
                this.storedAccountDetails = {
                  paymentProfiles: []
                };
              }
              this.selectAddNewBank(this.addNewPayment);
            } else {
              if (data) {
                this.selectBank(data.paymentProfiles[0], 0);
              }
            }
          },
          () => {
            this.storedAccountDetails = null;
            this.paymentService.storedAccountDetails = this.storedAccountDetails;
            if (this.addNewPayment) {
              if (!this.storedAccountDetails) {
                this.storedAccountDetails = {
                  paymentProfiles: []
                };
              }
              this.selectAddNewBank(this.addNewPayment);
            }
          });
      } else {
        this.storedAccountDetails = null;
        this.paymentService.storedAccountDetails = this.storedAccountDetails;
        if (this.paymentService.newPaymentDetails) { this.addNewPayment = this.paymentService.newPaymentDetails; }
        if (this.addNewPayment) {
          if (!this.storedAccountDetails) {
            this.storedAccountDetails = {
              paymentProfiles: []
            };
          }
          this.selectAddNewBank(this.addNewPayment);
        }

      }
    }
  }
  selectAddNewBank(data?: any) {
    if (data.paymentMethod === PropertyName.BankAccount) {
      this.storedAccountDetails.paymentProfiles.push({
        name: data.fullName,
        bankAccount: {
          accountType: PropertyName.saving,
          routingNumber: data.routingNumber,
          accountNumber: data.accountNumber,
          nameOnAccount: data.fullName
        }
      });
    } else if (data.paymentMethod === PropertyName.CreditCard) {
      this.storedAccountDetails.paymentProfiles.push({
        name: data.fullName,
        creditCard: {
          cardType: data.creditCardType,
          cardNumber: data.creditCardNumber
        }
      });
    }
    const index = this.storedAccountDetails.paymentProfiles.length - 1;
    this.selectBank(this.storedAccountDetails.paymentProfiles[index], index);
  }

  selectBank(data?: any, index?: any) {
    this.isBankIndex = index;
    const paymentMethod = this.isObject(data);
    this.payment.address = data.address;
    this.payment.paymentProfileId = data.paymentProfileId;
    this.payment.customerProfileId = this.storedAccountDetails.customerProfileId;
    if (paymentMethod === PropertyName.bankAccount) {
      this.payment.paymentMethod = PropertyName.BankAccount;
      this.payment.fullName = data.name;
      this.payment.accountType = data.bankAccount.accountType;
      this.payment.accountNumber = data.bankAccount.accountNumber;
      this.payment.routingNumber = data.bankAccount.routingNumber;
    } else if (paymentMethod === PropertyName.creditCard) {
      this.payment.paymentMethod = PropertyName.CreditCard;
      this.payment.fullName = data.name;
      this.payment.creditCardType = data.creditCard.cardType;
      this.payment.creditCardNumber = data.creditCard.cardNumber;
      this.payment.creditCardExpiryDate = data.creditCard.expirationDate;
    } else if (paymentMethod === PropertyName.digitalWallet) {
      this.payment.paymentMethod = data?.digitalWallet?.walletType;
      this.payment.fullName = data.name;
      this.payment.walletType = data?.digitalWallet?.walletType;
      this.payment.walletNumber = data.digitalWallet.walletNumber;
      this.payment.walletIdentifier = data.digitalWallet?.walletIdentifier;
      this.payment.attributeValue = data.attributeValue;
    }
  }

  public isObject(value?: any): string {
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.creditCard)) {
      return PropertyName.creditCard;
    }
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.bankAccount)) {
      return PropertyName.bankAccount;
    }
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.digitalWallet)) {
      return PropertyName.digitalWallet;
    }
  }

  public navigateToAddPayment(): void {
    if (!this.storedAccountDetails) {
      this.paymentService.amount = this.payment.amount;
      if ((this.paymentProvider === PropertyName.authorizeNet && this.oneTimePaymentIntegration) ||
        ((this.paymentProvider === '' || this.paymentProvider === PropertyName.authorizeNet) && !this.oneTimePaymentIntegration) ||
        ((this.paymentProvider === PropertyName.authorizeNet || this.paymentProvider === PropertyName.paymentus)
          && !this.oneTimePaymentIntegration)) {
        this.router.navigate(['/group-payment', 'add-new-payment'], {
          queryParams: {
            accountId: this.accountId,
            isCustomerProfileId: false,
            returnAppId: this.returnAppId,
            statementDate: this.statementDate,
            type: this.type
          }
        });
      } else if ((this.paymentProvider === PropertyName.paymentus && this.oneTimePaymentIntegration)) {
        setTimeout(() => {
          this.alertService.error('No Profiles available for making this payment');
        }, 100);
        //this.paymentusFlow();
      }
    }
  }

  private getSummary(partyId?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getSummary(partyId).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.summaryData = data;
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private generateToken(): void {
    const payload: PaymentusTokenModel = new PaymentusTokenModel({
      accountNumber: this.accountNumber,
      amount: this.payment.amount,
      firstName: this.summaryData?.details?.name?.given,
      lastName: this.summaryData?.details?.name?.family,
      iframe: false,
      emailId: this.accountDetails?.emailId,
      phoneNumber: this.accountDetails?.phoneNumber,
      zipCode: this.accountDetails?.payorAddress?.slice(this.accountDetails?.payorAddress?.length - 5)
    })
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentusService.generateSecureToken(payload).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        setTimeout(() => {
          this.printStatement(PropertyName.cancel);
        }, 100);
        const url = this.paymentusService.getSecureTokenizationWebUrl(data?.token);
        window.open(
          url,
          PropertyName.blank
        );
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  paymentusFlow() {
    this.generateToken();
  }

  makePaymentLoop(value?: any, activeClass?: any) {
    this.paymentService.amount = this.payment.amount;
    if (value === PropertyName.isSelectAmount && activeClass) {
      this[value] = PropertyName.complete;
      this.isPaymentMethod = PropertyName.active;
      this.isConfirm = PropertyName.inprogress;
      this.headerName = PropertyName.PaymentMethod;
    } else if (value === PropertyName.isSelectAmount && !activeClass) {
      this[value] = PropertyName.active;
      this.isPaymentMethod = PropertyName.inprogress;
      this.isConfirm = PropertyName.inprogress;
      this.headerName = PropertyName.ChooseAmount;
    } else if (value === PropertyName.isPaymentMethod && activeClass) {
      this[value] = PropertyName.complete;
      this.isConfirm = PropertyName.active;
      this.isSelectAmount = PropertyName.complete;
      this.headerName = 'Confirm & Pay';
    }
  }
  makePay() {
    if (this.addNewPayment) {
      this.makePayWithNewPayments();
    } else {
      this.makePayWithStoredCards();
    }
  }
  makePayWithStoredCards() {
    const payload: any = {
      customerProfileId: this.payment.customerProfileId,
      paymentProfileId: this.payment.paymentProfileId,
      amount: this.payment.amount
    };
    if (this.paymentProvider === PropertyName.paymentus) {
      payload.accountNumber = this.accountNumber
    }
    if (this.oneTimePaymentIntegration) {
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.makePayment(payload).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.callToEnterprisePaymentsAPI(data, this.payment);
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.reset(true, null);
        });
    } else {
      this.callToEnterprisePaymentsAPI(null, this.payment);
    }
  }
  makePayWithNewPayments() {
    const payment = {
      paymentNonce: this.addNewPayment.paymentNonce,
      amount: this.payment.amount,
      customer: {
        firstName: '',
        lastName: this.addNewPayment.fullName,
        merchantcustomerid: this.clientId
      },
      storeCard: this.addNewPayment.paymentMethod === PropertyName.BankAccount ? this.addNewPayment.storeAccount : this.addNewPayment.storeCard
    };
    if (this.storedAccountDetails) {
      payment['customerProfileId'] = this.storedAccountDetails.customerProfileId;
    }
    if (this.oneTimePaymentIntegration) {
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.makeNewPayment(payment).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.callToEnterprisePaymentsAPI(data, this.addNewPayment);
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.reset(true, null);
        });
    } else {
      this.callToEnterprisePaymentsAPI(null, this.addNewPayment);
    }
  }
  callToEnterprisePaymentsAPI(responseData?: any, payment?: any) {
    let requestBody: any = {};

    if (responseData) {
      if (payment.paymentMethod === PropertyName.BankAccount) {
        requestBody.bankAccountNumber = responseData.accountNumber ? responseData?.accountNumber : payment?.accountNumber;
        requestBody.paymentId = responseData.transactionId;
        requestBody.transactionId = responseData.paymentId;
        requestBody.authorizationResponseType = responseData.avsCode;
        requestBody.paymentComment = responseData.transactionMessage;
      } else {
        requestBody.paymentId = responseData.transactionId;
        requestBody.transactionId = responseData.paymentId;
        requestBody.authorizationResponse = responseData.responseCode;
        requestBody.authorization = responseData.authCode;
        requestBody.creditCardNumber = responseData.accountNumber ? responseData.accountNumber : payment.creditCardNumber;
        requestBody.creditCardType = responseData.accountType ? responseData.accountType : payment.creditCardType;
        requestBody.authorizationResponseType = responseData.avsCode + responseData.cvvCode + responseData.cavvCode;
        requestBody.paymentComment = responseData.transactionMessage;
        requestBody.creditCardExpiryDate = PropertyName.XXXX;
      }
    } else {
      requestBody.bankAccountNumber = payment.accountNumber;
    }

    if (payment.paymentMethod === PropertyName.BankAccount) {
      requestBody.paymentMethod = PropertyName.BankAccount;
      requestBody.accountType = payment.accountType;
      requestBody.paymentType = this.oneTimePaymentIntegration ? PropertyName.Echeck : PropertyName.WebsiteCheck1xACH;
      requestBody.routingNumber = payment.routingNumber;
      requestBody.paymentAmount = this.payment.amount;
      requestBody.identifier = this.accountDetails.accountNumber;
      requestBody.identifierType = PropertyName.Group;
      requestBody.dueDate = this.accountBalanceDetail.statementDueDate;
    } else {
      requestBody.paymentMethod = PropertyName.CreditCard;
      requestBody.paymentType = PropertyName.CreditCardPayment;
      requestBody.paymentAmount = this.payment.amount;
      requestBody.identifier = this.accountDetails.accountNumber;
      requestBody.identifierType = PropertyName.Group;
      requestBody.dueDate = this.accountBalanceDetail.statementDueDate;
      if (!responseData) {
        requestBody.creditCardNumber = payment.creditCardNumber;
        requestBody.creditCardType = payment.creditCardType;
        requestBody.creditCardExpiryDate = this.commonService.removeSpacesAndFWDash(payment.creditCardExpiryDate);
      }
    }
    if (payment.paymentMethod === PropertyName.Paypal) {
      requestBody.paymentMethod = PropertyName.Paypal;
      requestBody.walletNumber = payment.walletNumber;
      requestBody.paymentType = PropertyName.Paypal;
      requestBody.walletType = PropertyName.Paypal;
      requestBody.walletIdentifier = payment.walletIdentifier;
    }
    if (payment.paymentMethod === PropertyName.PaypalCredit) {
      requestBody.paymentMethod = PropertyName.PaypalCredit;
      requestBody.walletNumber = payment.walletNumber;
      requestBody.paymentType = PropertyName.PaypalCredit;
      requestBody.walletType = PropertyName.PaypalCredit;
      requestBody.walletIdentifier = payment.walletIdentifier;
    }
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.callToEnterprisePaymentsAPI(requestBody).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentCreatedResponse: any = data;
        let paymentDetailUrl = '';
        if (paymentCreatedResponse.links && paymentCreatedResponse.links.length > 0) {
          paymentDetailUrl = paymentCreatedResponse.links[0].href;
          this.getPaymentDetail(paymentDetailUrl, requestBody);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset(true, null);
      });
  }
  getPaymentDetail(url?: any, requestBody?: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentDetailByUrl(url).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentDetailInfo: any = data;
        requestBody.paymentSequenceNumber = paymentDetailInfo.paymentSequenceNumber;
        requestBody.batchId = paymentDetailInfo.batchId;
        requestBody.postedDate = paymentDetailInfo.postedDate;
        requestBody.userId = paymentDetailInfo.userId;
        this.reset(true, requestBody);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset(true, null);
      });
  }
  reset(status?: any, requestBody?: any) {
    if (status) {
      this.isPaymentMethod = PropertyName.inprogress;
      this.isSelectAmount = PropertyName.active;
      this.isConfirm = PropertyName.inprogress;
      this.isBankIndex = -1;
      this.payment = new PaymentModel();
      this.isCurrentDue = false;
      this.isAccountBalance = false;
      this.isOtherAmount = false;
      this.storedAccountDetails = undefined;
      this.addNewPayment = undefined;
      this.paymentService.amount = undefined;
      this.paymentService.newPaymentDetails = undefined;
      this.paymentService.accountBalanceDetail = undefined;
      this.paymentService.storedAccountDetails = undefined;
      this.openPaymentReceivedModel(requestBody);
    }
  }
  private openPaymentReceivedModel(data?: any): void {
    if (data) {
      if (this.oneTimePaymentIntegration) {
        this.paymentReceivedModel.modalTitle = PropertyName.PaymentReceived;
        this.paymentReceivedModel.subModalTitle = 'Your payment of ' +
          this.currencyPipe.transform(data.paymentAmount, this.currency) + ' is received . It may take up you 24 hours of your online balance to display this payment.';
        this.paymentReceivedModel.icon = IconName.success;
      } else {
        this.paymentReceivedModel.modalTitle = PropertyName.PaymentSubmitted;
        this.paymentReceivedModel.subModalTitle = 'Your payment of ' + this.currencyPipe.transform(data.paymentAmount, this.currency) + ' is successfully submitted.';
        this.paymentReceivedModel.icon = IconName.success;
      }
    } else {
      this.paymentReceivedModel.modalTitle = PropertyName.PaymentFailed;
      this.paymentReceivedModel.subModalTitle = 'We can`t process your Payment . Please contact Customer Service for further assistance.';
      this.paymentReceivedModel.icon = IconName.fail;
    }
    this.paymentService.objectToModal = data;
    this.paymentReceivedModel.modalFooter = false;
    this.paymentReceivedModel.open(PaymentReceivedComponent);
  }

  public printStatement(status?: any): void {
    this.paymentService.accountDetails = undefined;
    this.paymentService.objectToModal = undefined;
    if (this.returnAppId) {
      this.onSuccessNavigate(status);
    } else {
      setTimeout(() => {
        this.router.navigate(['/ent-payments', PropertyName.expressPayments]);
      }, 100);
    }
  }
  public onSuccessNavigate(status?: any): void {
    const resultQueryParam = JSON.parse(sessionStorage.getItem(PropertyName.resultQueryParam));
    const cancelUrl = resultQueryParam.cancelUrl;
    const returnUrl = resultQueryParam.returnUrl;
    const url = (status === PropertyName.cancel ? cancelUrl : returnUrl);
    this.commonService.navigateToUrl(url, PropertyName.self, true);
  }

  public goBack(): void {
    this.isPaymentMethod = PropertyName.inprogress;
    this.isSelectAmount = PropertyName.active;
    this.isConfirm = PropertyName.inprogress;
    this.isBankIndex = -1;
    this.payment = new PaymentModel();
    this.isCurrentDue = false;
    this.isAccountBalance = false;
    this.isOtherAmount = false;
    this.storedAccountDetails = undefined;
    this.addNewPayment = undefined;
    this.paymentService.amount = undefined;
    this.paymentService.accountDetails = null;
    this.paymentService.newPaymentDetails = undefined;
    this.paymentService.accountBalanceDetail = undefined;
    this.paymentService.storedAccountDetails = undefined;
    this.paymentService.agentType = null;
    this.checkData();
  }

  public openSecurity(event?: any, configType?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, configType);
  }

  public checkSecurityOptions(): any {
    this.isShowPaymentMethodButton = true;
    if (this.isSelectAmount === PropertyName.active) {
      if (this.securityEngineService.checkVisibility(PropertyName.currentBalance, PropertyName.Show) && this.accountBalanceDetail.currentDue !== 0) {
        this.selectAmount(this.accountBalanceDetail.currentDue, PropertyName.isCurrentDue);
      } else if (this.securityEngineService.checkVisibility(PropertyName.totalBalance, PropertyName.Show) && this.accountBalanceDetail.accountBalance !== 0) {
        this.selectAmount(this.accountBalanceDetail.accountBalance, PropertyName.isAccountBalance);
      } else if (this.securityEngineService.checkVisibility(PropertyName.otherAmount, PropertyName.Show)) {
        this.selectAmount(null, PropertyName.isOtherAmount);
      } else {
        this.isAccountBalance = false;
        this.isCurrentDue = false;
        this.isOtherAmount = false;
        this.isShowPaymentMethodButton = false;
      }
    }
  }
}
