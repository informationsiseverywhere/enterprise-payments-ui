import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { groupPayRoutes } from './group-pay-routes';

@NgModule({
  imports: [
    RouterModule.forChild(groupPayRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class GroupPayRoutingModule {
}
