import { Routes } from '@angular/router';

import { AuthGuard } from 'src/app/ent-auth/auth.guard';
import { NavigationComponent } from '../navigation/navigation.component';
import { GroupPayComponent } from './components/group-pay.component';
import { AlternatePaymentMethodComponent } from 'src/app/shared/components/alternate-payment-method/alternate-payment-method.component';


export const groupPayRoutes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
      {
        path: '',
        component: GroupPayComponent
      },
      {
        path: 'add-new-payment',
        component: AlternatePaymentMethodComponent,
        data: {
          breadcrumb: 'Add New Payments',
          headerTitle: 'Add New Payments',
          screenName: 'epAddNewPaymentMethod'
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

