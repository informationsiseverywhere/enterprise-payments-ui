export const DashboardMetadata = [
  {
    id: 1,
    title: 'Batch Payments',
    description: 'The Batch Payments page allows you to enter a new batch of cash or edit an existing batch of cash. It also allows you to add, change, or delete the payment details of a batch.',
    iconPath: 'assets/images/dashboard/batch-payment.svg',
    redirectUrl: 'batch-payments',
    securityName: 'batchPayments'
  },
  {
    id: 2,
    title: 'Payments Search',
    description: 'Quickly search or locate Payments which may reside in accounts or unidentified suspense. Payments residing in accounts or unidentified suspense are available for further processing. (Backoffice Use Only)',
    iconPath: 'assets/images/dashboard/search-payment.svg',
    redirectUrl: '',
    securityName: 'paymentSearch'
  },
  {
    id: 3,
    title: 'Express Payments',
    description: 'Quickly view & pay current bill(s) using MasterCard, Visa, Discover, American Express, Personal Checking or Savings Account.',
    iconPath: 'assets/images/dashboard/express-payment.svg',
    redirectUrl: 'express-payments',
    securityName: 'expressPayment'
  },
  {
    id: 3,
    title: 'Batch Rejection',
    description: 'The Batch Rejection page allows you to enter a new batch of cash or edit an existing batch of cash. It also allows you to add, change, or delete the payment details of a batch.',
    iconPath: 'assets/images/dashboard/batch-rejection.svg',
    redirectUrl: 'batch-rejection',
    securityName: 'batchRejection'
  }
];
