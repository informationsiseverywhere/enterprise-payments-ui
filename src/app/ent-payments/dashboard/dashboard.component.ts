
import { Component, ElementRef, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardMetadata } from './models/dashboard-metadata.model';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from '../security-config/security-config.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { PaymentService } from '../batch-payments/services/payments.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  sub: any;
  dashboardMetadata: any;
  numbers: any;
  endLineItems: Array<any> = [];
  returnAppId: any;
  navParam: any;
  public isNotAvailable = false;
  supportData: any;
  paymentProviderDropdown: any;
  paymentProvider: any;
  @ViewChild('dashboard', { static: false }) dashboard: ElementRef;

  constructor(private route: ActivatedRoute,
    public securityEngineService: SecurityEngineService,
    private router: Router,
    public appConfig: AppConfiguration,
    private paymentService: PaymentService,
    private loadingService: LoadingService,
    private paymentSecurityConfigService: PaymentSecurityConfigService) { }

  ngOnInit() {
    this.getPaymentsSupportData();
    this.route.parent.queryParams.subscribe(
      queryParams => {
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
      });
    this.dashboardMetadata = DashboardMetadata;
  }

  private getPaymentsSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentsSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.paymentProvider);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    this.paymentProviderDropdown && (this.paymentProvider = this.paymentProviderDropdown[0]);
    if (this.paymentProvider !== '' && this.paymentProvider !== PropertyName.paymentus) {
      if (this.dashboardMetadata.length === 4) {
        this.dashboardMetadata.pop();
      }
    }
  }

  public checkSecurityOptions(): any {
    let count = 0;
    this.dashboardMetadata.forEach((metadata: { securityName: any; }) => {
      if(this.securityEngineService.checkVisibility(metadata.securityName, 'Hide')) { count++; }
    });
    return count === this.dashboardMetadata.length ? false : true;
  }
  
  public openSecurity(event?: any) : void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event);
  }

  public redirectToPage(redirectUrl?: string, securityName?: string) : void{
    let redirectPageUrl: string = redirectUrl;
    if(securityName){
      switch (securityName) {
        case 'paymentSearch':
          if(!this.securityEngineService.checkVisibility('unIdentifiedPaymentSearch', 'Show')){
            redirectPageUrl = 'payments-search/identified-payments';
          }else{
            redirectPageUrl = 'payments-search/unidentified-payments';
          }
          break;
        default: break;
      }
    }
    const paramObject: any = {};
    paramObject.returnAppId = this.returnAppId ? this.returnAppId : null;
    paramObject.nav = this.navParam ? this.navParam : null;
    this.router.navigate([this.appConfig.landingPageLink[0] + '/' + redirectPageUrl], {
      queryParams: paramObject
    });
  }
}
