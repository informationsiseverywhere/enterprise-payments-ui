import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { UnidentifiedPaymentRoutingModule } from './unidentified-payment-routing.module';

import { UnidentifiedPaymentComponent } from './components/unidentified-payment.component';

@NgModule({
  declarations: [
    UnidentifiedPaymentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    UnidentifiedPaymentRoutingModule
  ]
})
export class UnidentifiedPaymentModule { }
