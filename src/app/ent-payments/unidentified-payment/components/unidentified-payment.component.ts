import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentReceivedComponent } from 'src/app/shared/components/payment-received/payment-received.component';
import { PaymentModel } from '../../batch-payments/models/batch.model';
import { PaymentService } from '../../batch-payments/services/payments.service';
import { CurrencyPipe } from '@angular/common';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { HalProcessor } from 'src/app/shared/services/utilities.service';
import { UnidentifiedPaymentModel } from 'src/app/ent-payments/unidentified-payment/model/unidentified-payment.model';
import { SelectAgentComponent } from 'src/app/shared/components/select-agent/select-agent.component';
import { PaymentSecurityConfigService } from '../../security-config/security-config.service';
import { IconName, PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-unidentified-payment',
  templateUrl: './unidentified-payment.component.html',
  styleUrls: ['./unidentified-payment.component.scss'],
  providers: [CurrencyPipe]
})

export class UnidentifiedPaymentComponent implements OnInit {
  public payment: PaymentModel = new PaymentModel();
  public unidentifiedPaymentDetails: UnidentifiedPaymentModel = new UnidentifiedPaymentModel();
  public isBankIndex: any;
  public storedAccountDetails: any;
  public isEdit = false;
  public returnAppId: any;
  public isAgencySweep: any;
  public currency: any;
  public type: any;
  public identifier: any;
  public policyNumber: any;
  public policySymbol: any;
  public isUnidentifiedAgencySweepPayment: boolean = false;
  public accountDetails: any;
  public isPaymentMethod = PropertyName.inprogress;
  public isConfirm = PropertyName.inprogress;
  public headerName = PropertyName.PaymentMethod;
  public imageId: any;
  public payorName: any;
  public payorAddress: any;
  public agent: any;
  public paymentProfileList: any;
  public billingType: any;
  public isShowAccountDetailSection = true;
  public isAccountDetailLoaded = false;
  public imageUrl: any;
  public isAgencySweepPayorChange: any;
  public isPaymentProfilesLoaded = false;
  public mockPaymentProfileSkeleton = [];
  public paymentAmount: any;
  public identifierType: any;
  public isPolicyUnidentifiedPayment: boolean = false;
  private isUserAgent: any;
  private agencies: any;
  private supportData: any;
  private bankProfileAttributeCheck: any;
  private addNewPayment: any;
  private clientId: any;
  oneTimePaymentIntegrationDropdown: any;
  oneTimePaymentIntegration: boolean;
  paymentProviderDropdown: any;
  paymentProvider: any;
  agencySweepPaymentIntegrationDropdown: any;
  agencySweepPaymentIntegration = false;
  supportDataPayments: any;
  @ViewChild('selectAgentModal', { static: false }) selectAgentModal: ModalComponent;
  @ViewChild('paymentReceivedModel', { static: false }) paymentReceivedModel: ModalComponent;
  constructor(public appConfig: AppConfiguration,
    public securityEngineService: SecurityEngineService,
    public commonService: CommonService,
    public halProcessor: HalProcessor,
    private router: Router,
    private loadingService: LoadingService,
    private route: ActivatedRoute,
    private paymentService: PaymentService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    private currencyPipe: CurrencyPipe) {
    this.imageUrl = IconName.userProfilePath;
    this.mockPaymentProfileSkeleton = new Array(3);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        if (queryParams.userId) {
          this.onSuccessNavigate(PropertyName.cancel);
        } else {
          this.returnAppId = queryParams.returnAppId;
          this.type = queryParams.type;
          this.billingType = queryParams.billingType;
          this.getPaymentsSupportData();
          this.isUnidentifiedAgencySweepPayment = this.paymentService.isUnidentifiedAgencySweepPayment;
          if (this.paymentService.newPaymentDetails) { this.addNewPayment = this.paymentService.newPaymentDetails; }
          if (this.paymentService.unidentifiedPaymentDetails) {
            this.unidentifiedPaymentDetails = this.paymentService.unidentifiedPaymentDetails;
            this.identifierType = this.unidentifiedPaymentDetails.identifierType;
            this.identifier = this.unidentifiedPaymentDetails.identifier;
            this.paymentAmount = this.unidentifiedPaymentDetails.paymentAmount;
            this.policyNumber = this.unidentifiedPaymentDetails.policyNumber;
            this.policySymbol = this.unidentifiedPaymentDetails.policySymbol;
            this.policyNumber && (this.isPolicyUnidentifiedPayment = true);
            this.payment.amount = this.unidentifiedPaymentDetails.paymentAmount;
          }
          if (this.isUnidentifiedAgencySweepPayment) {
            this.getPartiesSupportdata();
            this.accountDetails = this.paymentService.accountDetails;
            this.isAgencySweepPayorChange = this.paymentService.isAgencySweepPayorChange;
            this.isUserAgent = this.paymentService.isUserAgent;
            this.agencies = this.paymentService.agencies;
            if (this.payment) {
              this.makePaymentLoop(PropertyName.isPaymentMethod, this.isPaymentMethod != PropertyName.complete ? false : true);
            } else {
              this.makePaymentLoop(PropertyName.isPaymentMethod, true);
            }
          } else {
            this.makePaymentLoop(PropertyName.isPaymentMethod, true);
            if (this.addNewPayment) {
              if (!this.storedAccountDetails) {
                this.storedAccountDetails = {
                  paymentProfiles: []
                };
              }
              this.selectAddNewBank(this.addNewPayment);
            } else {
              this.router.navigate(['/ent-payments', PropertyName.expressPayments]);
            }
          }
        }
      });

  }

  private getPaymentsSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentsSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportDataPayments = data;
          this.getDropdownItems(PropertyName.oneTimePaymentIntegration);
          this.getDropdownItems(PropertyName.agencySweepPaymentIntegration);
          this.getDropdownItems(PropertyName.paymentProvider);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getDropdownItems(propertyName?: string): void {
    for (const sd of this.supportDataPayments.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    let oneTimePaymentIntegration = 'false';
    this.oneTimePaymentIntegrationDropdown && (oneTimePaymentIntegration = this.oneTimePaymentIntegrationDropdown[0]);
    let agencySweepPaymentIntegration = 'false';
    this.agencySweepPaymentIntegrationDropdown && (agencySweepPaymentIntegration = this.agencySweepPaymentIntegrationDropdown[0]);
    this.paymentProviderDropdown && (this.paymentProvider = this.paymentProviderDropdown[0]);
    this.oneTimePaymentIntegration = false;
    this.agencySweepPaymentIntegration = false;
    if (oneTimePaymentIntegration === 'true') {
      this.oneTimePaymentIntegration = true;
    }
    if (agencySweepPaymentIntegration === 'true') {
      this.agencySweepPaymentIntegration = true;
    }
  }

  private selectAddNewBank(data?: any): void {
    if (data.paymentMethod === PropertyName.BankAccount) {
      this.storedAccountDetails.paymentProfiles.push({
        name: data.fullName,
        bankAccount: {
          accountType: PropertyName.saving,
          routingNumber: data.routingNumber,
          accountNumber: data.accountNumber,
          nameOnAccount: data.fullName
        }
      });
    } else if (data.paymentMethod === PropertyName.CreditCard) {
      this.storedAccountDetails.paymentProfiles.push({
        name: data.fullName,
        creditCard: {
          cardType: data.creditCardType,
          cardNumber: data.creditCardNumber
        }
      });
    }
    const index = this.storedAccountDetails.paymentProfiles.length - 1;
    this.selectBank(this.storedAccountDetails.paymentProfiles[index], index);
  }

  public selectBank(data?: any, index?: any): void {
    this.isBankIndex = index;
    const paymentMethod = this.isObject(data);
    this.payment.address = data && data.address ? data.address : null;
    this.payment.paymentProfileId = data && data.paymentProfileId ? data.paymentProfileId : null;
    this.payment.customerProfileId = this.storedAccountDetails.customerProfileId;
    if (paymentMethod === 'bankAccount') {
      this.payment.paymentMethod = PropertyName.BankAccount;
      this.payment.fullName = data.name;
      this.payment.accountType = data.bankAccount.accountType;
      this.payment.accountNumber = data.bankAccount.accountNumber;
      this.payment.routingNumber = data.bankAccount.routingNumber;
      this.payment.attributeValue = data.attributeValue;
    } else if (paymentMethod === 'creditCard') {
      this.payment.paymentMethod = PropertyName.CreditCard;
      this.payment.fullName = data.name;
      this.payment.creditCardType = data.creditCard.cardType;
      this.payment.creditCardNumber = data.creditCard.cardNumber;
      this.payment.creditCardExpiryDate = data.creditCard.expirationDate;
      this.payment.attributeValue = data.attributeValue;
    }
  }

  public isObject(value?: any): string {
    if (typeof value === 'object' && value.hasOwnProperty('creditCard')) {
      return 'creditCard';
    }
    if (typeof value === 'object' && value.hasOwnProperty('bankAccount')) {
      return 'bankAccount';
    }
  }

  public makePay(): void {
    if (this.addNewPayment) {
      this.makePayWithNewPayments();
    }
  }

  private makePayWithNewPayments(): void {
    const payment: any = {
      paymentNonce: this.addNewPayment.paymentNonce,
      amount: this.payment.amount,
      customer: {
        firstName: '',
        lastName: this.addNewPayment.fullName
      },
      storeCard: this.addNewPayment.paymentMethod === PropertyName.BankAccount ? this.addNewPayment.storeAccount : this.addNewPayment.storeCard
    };
    if (this.storedAccountDetails) {
      payment.customerProfileId = this.storedAccountDetails.customerProfileId;
    }
    if (this.oneTimePaymentIntegration) {
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.makeNewPayment(payment).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.callToEnterprisePaymentsAPI(data, this.addNewPayment);
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.reset(true, null);
        });
    } else {
      this.callToEnterprisePaymentsAPI(null, this.addNewPayment);
    }
  }

  private callToEnterprisePaymentsAPI(responseData?: any, payment?: any): any {
    let requestBody: any = {
      identifier: this.identifier,
      identifierType: this.identifier ? this.identifierType : null,
      paymentAmount: this.payment.amount,
      policySymbol: this.policySymbol,
      policyNumber: this.policyNumber
    };
    if (responseData) {
      requestBody.paymentComments = responseData.transactionMessage;
      requestBody.paymentId = responseData.transactionId;
      requestBody.transactionId = responseData.paymentId;
      if (payment.paymentMethod === PropertyName.BankAccount) {
        requestBody.bankAccountNumber = responseData.accountNumber ? responseData?.accountNumber : this.payment?.accountNumber;
        requestBody.authorizationResponseType = responseData.avsCode;
      } else {
        requestBody.authorizationResponse = responseData.responseCode;
        requestBody.authorization = responseData.authCode;
        requestBody.authorizationResponseType = responseData.avsCode + responseData.cvvCode + responseData.cavvCode;
        requestBody.creditCardNumber = responseData.accountNumber ? responseData.accountNumber : payment.creditCardNumber;
        requestBody.creditCardType = responseData.accountType ? responseData.accountType : payment.creditCardType;
        requestBody.creditCardExpiryDate = PropertyName.XXXX;
      }
    } else {
      requestBody.bankAccountNumber = payment.accountNumber;
    }
    if (payment.paymentMethod === PropertyName.BankAccount) {
      requestBody.paymentMethod = PropertyName.BankAccount;
      requestBody.accountType = payment.accountType;
      requestBody.paymentType = this.oneTimePaymentIntegration ? PropertyName.Echeck : PropertyName.WebsiteCheck1xACH;
      requestBody.routingNumber = payment.routingNumber;
    } else {
      requestBody.paymentMethod = PropertyName.CreditCard;
      requestBody.paymentType = PropertyName.CreditCardPayment;
      if (!responseData) {
        requestBody.creditCardNumber = payment.creditCardNumber;
        requestBody.creditCardType = payment.creditCardType;
        requestBody.creditCardExpiryDate = this.commonService.removeSpacesAndFWDash(payment.creditCardExpiryDate);
      }
    }
    requestBody = this.commonService.removeEmptyObjectEntries(requestBody);
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.callToEnterprisePaymentsAPI(requestBody).subscribe(
      data => {
        requestBody.paymentSequenceNumber = data.paymentSequenceNumber;
        requestBody.postedDate = data.postedDate;
        requestBody.batchId = data.batchId;
        requestBody.userId = data.userId;
        this.loadingService.toggleLoadingIndicator(false);
        const paymentCreatedResponse: any = data;
        let paymentDetailUrl = '';
        if (paymentCreatedResponse.links && paymentCreatedResponse.links.length > 0) {
          paymentDetailUrl = paymentCreatedResponse.links[0].href;
          this.getPaymentDetail(paymentDetailUrl, requestBody);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset(true, null);
      });
  }

  private getPaymentDetail(url?: any, requestBody?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentDetailByUrl(url).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentDetailInfo: any = data;
        requestBody.paymentSequenceNumber = paymentDetailInfo.paymentSequenceNumber;
        requestBody.batchId = paymentDetailInfo.batchId;
        requestBody.postedDate = paymentDetailInfo.postedDate;
        requestBody.userId = paymentDetailInfo.userId;
        this.reset(true, requestBody);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset(true, null);
      });
  }

  private reset(status?: any, requestBody?: any): void {
    if (status) {
      const accountDetails: any = {
        identifier: this.commonService.removeSpacesAndFWDash(this.identifier),
        type: this.type,
        policySymbol: this.policySymbol,
        policyNumber: this.policyNumber,
        isPolicyUnidentifiedPayment: this.isPolicyUnidentifiedPayment
      };
      this.paymentService.accountDetails = accountDetails;
      this.paymentService.unidentifiedPaymentDetails = undefined;
      this.openPaymentReceivedModel(requestBody);
    }
  }

  private openPaymentReceivedModel(data?: any): void {
    if (data) {
      if (data.paymentMethod !== PropertyName.AgencySweep && this.oneTimePaymentIntegration) {
        this.paymentReceivedModel.modalTitle = PropertyName.PaymentReceived;
        this.paymentReceivedModel.subModalTitle = 'Your payment of ' +
          this.currencyPipe.transform(data.paymentAmount, this.currency) + ' is received . It may take up to 24 hours of your online balance to display this payment.';
        this.paymentReceivedModel.icon = IconName.success;
      } else {
        this.paymentReceivedModel.modalTitle = PropertyName.PaymentSubmitted;
        this.paymentReceivedModel.subModalTitle = 'Your payment of ' + this.currencyPipe.transform(data.paymentAmount, this.currency) + ' is successfully submitted.';
        this.paymentReceivedModel.icon = IconName.success;
      }
    } else {
      this.paymentReceivedModel.modalTitle = PropertyName.PaymentFailed;
      this.paymentReceivedModel.subModalTitle = 'We can`t process your Payment . Please contact Customer Service for further assistance.';
      this.paymentReceivedModel.icon = IconName.fail;
    }
    this.paymentService.objectToModal = data;
    this.paymentReceivedModel.modalFooter = false;
    this.paymentReceivedModel.open(PaymentReceivedComponent);
  }

  public printStatement(): void {
    this.paymentService.accountDetails = undefined;
    this.paymentService.objectToModal = undefined;
    if (this.returnAppId) {
      this.onSuccessNavigate();
    } else {
      setTimeout(() => {
        this.router.navigate(['/ent-payments', PropertyName.expressPayments]);
      }, 100);
    }
  }

  public onSuccessNavigate(status?: any): void {
    const resultQueryParam = JSON.parse(sessionStorage.getItem(PropertyName.resultQueryParam));
    const cancelUrl = resultQueryParam.cancelUrl;
    const returnUrl = resultQueryParam.returnUrl;
    const url = (status === PropertyName.cancel ? cancelUrl : returnUrl);
    this.commonService.navigateToUrl(url, PropertyName.self, true);
  }

  public goBack(): void {
    this.router.navigate(['/ent-payments', PropertyName.expressPayments]);
  }

  public makePayASP(): void {
    const bankDetails = this.storedAccountDetails.paymentProfiles[this.isBankIndex].bankAccount;
    const requestBody: any = {
      identifier: this.identifier,
      identifierType: this.identifier ? PropertyName.ConsumerDirect : null,
      policySymbol: this.policySymbol,
      policyNumber: this.policyNumber,
      paymentAmount: this.payment.amount,
      accountType: bankDetails.accountType,
      routingNumber: bankDetails.routingNumber,
      bankAccountNumber: bankDetails.accountNumber,
      paymentMethod: PropertyName.AgencySweep,
      paymentType: PropertyName.AgencySweepPayment,
      agencyNumber: this.agent,
      paymentProfileId: this.storedAccountDetails.paymentProfiles[this.isBankIndex].paymentProfileId
    };
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.callToEnterprisePaymentsAPI(requestBody).subscribe(
      data => {
        requestBody.paymentSequenceNumber = data.paymentSequenceNumber;
        requestBody.postedDate = data.postedDate;
        requestBody.batchId = data.batchId;
        requestBody.userId = data.userId;
        this.loadingService.toggleLoadingIndicator(false);
        const paymentCreatedResponse: any = data;
        let paymentDetailUrl = '';
        if (paymentCreatedResponse.links && paymentCreatedResponse.links.length > 0) {
          paymentDetailUrl = paymentCreatedResponse.links[0].href;
          this.getPaymentDetail(paymentDetailUrl, requestBody);
        }
        this.loadingService.toggleLoadingIndicator(false);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.reset(true, null);
      });
  }

  public makePaymentLoop(value?: any, activeClass?: any): void {
    if (value === PropertyName.isPaymentMethod && !activeClass) {
      this[value] = PropertyName.active;
      this.isPaymentMethod = PropertyName.active;
      if (this.isConfirm !== PropertyName.noProfilesFound) { this.isConfirm = PropertyName.inprogress; }
      this.headerName = PropertyName.PaymentMethod;
    } else if (value === PropertyName.isPaymentMethod && activeClass) {
      this[value] = PropertyName.complete;
      this.isConfirm = PropertyName.active;
      this.headerName = PropertyName.ConfirmPay;
    }
  }

  public selectAgentEvent(event: any): void {
    if (event) {
      this.paymentService.accountDetails = null;
      this.paymentService.accountBalanceDetail = undefined;
      this.paymentService.storedAccountDetails = undefined;
      this.accountDetails = event;
      this.paymentService.accountDetails = event;
      this.clientId = event.partyId;
      this.payorName = this.accountDetails ? this.accountDetails.name : '';
      this.payorAddress = this.accountDetails.addresses ? this.accountDetails.addresses[0].address : '';
      this.imageId = this.accountDetails ? this.accountDetails.pictureId : undefined;
      this.agent = this.accountDetails ? this.accountDetails.agentNumber : undefined;
      this.isShowAccountDetailSection = true;
      this.isAccountDetailLoaded = true;
      this.getAgentStoredCard(this.clientId, this.billingType, this.bankProfileAttributeCheck);
    }
  }

  private getAgentStoredCard(clientId?: any, billingType?: any, bankProfileAttribute?: any): void {
    this.isPaymentProfilesLoaded = false;
    this.paymentService.getAgentStoredCard(clientId, billingType, bankProfileAttribute, this.agencySweepPaymentIntegration).subscribe(
      data => {
        this.paymentProfileList = data ? data.content : null;
        this.isPaymentProfilesLoaded = true;
        this.getPaymentProfiles(true);
        this.paymentService.storedAccountDetails = this.storedAccountDetails;
        if (this.isPaymentMethod != PropertyName.complete) {
          if (this.storedAccountDetails.paymentProfiles && this.storedAccountDetails.paymentProfiles.length <= 0) {
            this.isConfirm = PropertyName.noProfilesFound;
          } else {
            this.isConfirm = PropertyName.inprogress;
          }
        }
      },
      () => {
        this.isPaymentProfilesLoaded = true;
        this.storedAccountDetails = null;
        this.paymentService.storedAccountDetails = this.storedAccountDetails;
      });
  }

  public getPaymentProfiles(isCheckOverrideBgrp = false): void {
    let list = this.paymentProfileList;
    if (this.bankProfileAttributeCheck === 'true') {
      if (isCheckOverrideBgrp) {
        list = this.paymentProfileList.filter(element => element.isOverrideBgrp);
      }
    }
    this.storedAccountDetails = { paymentProfiles: (list && list.length > 0) ? list : this.paymentProfileList };
    this.selectBank(this.storedAccountDetails.paymentProfiles[0], 0);
  }

  private getPartiesSupportdata(): void {
    this.paymentService.getPartiesSupportdata().subscribe(
      data => {
        if (data) {
          this.supportData = data;
          this.getBankProfile(PropertyName.bankProfileAttribute);
          this.selectAgentEvent(this.accountDetails);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getBankProfile(propertyName: string): any {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Check'] = sd.propertyValues;
      }
    }
    if (this.bankProfileAttributeCheck) { this.bankProfileAttributeCheck = this.bankProfileAttributeCheck[0]; };
  }

  public checkAgencyModalChange(): boolean {
    if (this.isAgencySweepPayorChange === 'true' && this.isConfirm !== PropertyName.active) {
      return true;
    } else {
      return false;
    }
  }

  public openAgentModal(type?: any): void {
    this.selectAgentModal.modalTitle = type + ' Agent';
    this.paymentService.agentType = type;
    this.paymentService.isUserAgent = this.isUserAgent;
    this.paymentService.isAgencySweepPayorChange = this.isAgencySweepPayorChange;
    this.paymentService.agencies = this.agencies;
    this.selectAgentModal.modalFooter = false;
    this.selectAgentModal.icon = 'assets/images/model-icons/agent.svg';
    this.selectAgentModal.open(SelectAgentComponent);
  }

  public openSecurity(event?: any, configType?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, configType);
  }
}
