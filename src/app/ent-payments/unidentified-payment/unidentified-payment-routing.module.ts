import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { unidentifiedPaymentRoutes } from './unidentified-payment-routes';

@NgModule({
  imports: [
    RouterModule.forChild(unidentifiedPaymentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class UnidentifiedPaymentRoutingModule {
}
