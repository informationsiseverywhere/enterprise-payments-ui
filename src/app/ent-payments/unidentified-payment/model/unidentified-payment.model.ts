export class UnidentifiedPaymentModel {
    identifierType: string;
    identifier: string;
    paymentAmount: number;
    policySymbol: string;
    policyNumber: string;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}