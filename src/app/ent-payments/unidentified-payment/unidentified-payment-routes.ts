import { Routes } from '@angular/router';

import { AuthGuard } from 'src/app/ent-auth/auth.guard';
import { NavigationComponent } from '../navigation/navigation.component';
import { UnidentifiedPaymentComponent } from './components/unidentified-payment.component';
import { AlternatePaymentMethodComponent } from 'src/app/shared/components/alternate-payment-method/alternate-payment-method.component';

export const unidentifiedPaymentRoutes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
      {
        path: '',
        component: UnidentifiedPaymentComponent
      },
      {
        path: 'add-new-payment',
        component: AlternatePaymentMethodComponent,
        data: {
          breadcrumb: 'Add New Payments',
          headerTitle: 'Add New Payments',
          screenName: 'epAddNewPaymentMethod'
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

