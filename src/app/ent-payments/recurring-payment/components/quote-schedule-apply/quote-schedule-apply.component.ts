import { Component, OnInit, OnChanges, Input, ViewChild, Output, EventEmitter } from '@angular/core';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { QuoteSchedulePreviewComponent } from './quote-schedule-preview/quote-schedule-preview.component';
import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';

@Component({
  selector: 'app-quote-schedule-apply',
  templateUrl: './quote-schedule-apply.component.html',
  styleUrls: ['./quote-schedule-apply.component.scss']
})

export class QuoteScheduleApplyComponent implements OnInit, OnChanges {
  @Input() accountDetails: any;
  @Input() isDisabled = false;
  @Input() isShowSelectPayment: boolean;
  goldPlan: any;
  @ViewChild('quoteSchedulePreviewModel') quoteSchedulePreviewModel: ModalComponent;
  @Output() updateplan: EventEmitter<any> = new EventEmitter<any>();
  size: number;
  quotePlanList: any;
  public isQuotePlanListLoaded = false;
  public isQuotePlanListError = false;
  currency: any;
  constructor(
    private paymentService: PaymentService,
    private commonService: CommonService) {
    this.quotePlanList = new Array(5);
  }

  ngOnInit() { }

  ngOnChanges(changes: any) {
    if (!changes.isDisabled || changes.isDisabled.firstChange) {
      if (this.accountDetails) {
        this.currency = this.accountDetails.currency;
        this.goldPlan = [
          {
            accountPlan: 'Gold Biweekly Account',
            amount: 150,
            currentPlan: false
          },
          {
            accountPlan: 'Gold Monthly Account',
            amount: 250,
            currentPlan: false
          },
          {
            accountPlan: 'Gold Bimonthly Account',
            amount: 450,
            currentPlan: false
          },
          {
            accountPlan: 'Gold Annual Account',
            amount: 1000,
            currentPlan: false
          }
        ];
        this.goldPlan.unshift({
          accountPlan: this.accountDetails.accountPlan,
          amount: 100,
          currentPlan: true
        });
        this.quotePlan();
      }
    }
  }
  details(plan?: any) {
    this.paymentService.objectToModal = plan;
    this.paymentService.accountId = this.accountDetails.accountId;
    this.paymentService.currency = this.accountDetails.currency;
    this.quoteSchedulePreviewModel.modalTitle = plan + ' Preview';
    this.quoteSchedulePreviewModel.modalFooter = true;
    this.quoteSchedulePreviewModel.okButton = true;
    this.quoteSchedulePreviewModel.okButtonText = 'DONE';
    this.quoteSchedulePreviewModel.cancelButton = false;
    this.quoteSchedulePreviewModel.icon = 'assets/images/model-icons/edit-plan.svg';
    this.quoteSchedulePreviewModel.open(QuoteSchedulePreviewComponent);
  }

  quotePlan() {
    const plan = {
      newAccountPlan: 'ALL'
    };
    this.isQuotePlanListLoaded = false;
    this.paymentService.createQuoteSchedule(this.accountDetails.accountId, plan).subscribe(
      () => {
        this.size = 5;
        this.getAllQuoteSchedule(this.size, this.accountDetails.accountId);
      },
      err => {
        this.isQuotePlanListError = true;
        this.commonService.handleErrorAPIResponse(this, 'isQuotePlanListLoaded', 'quotePlanList', err);
      });
  }
  getAllQuoteSchedule(size?: any, accountId?: any) {
    this.isQuotePlanListLoaded = false;
    this.paymentService.getAllQuoteSchedule(accountId, size, null).subscribe(
      data => {
        this.isQuotePlanListLoaded = true;
        this.quotePlanList = data ? data.content : null;
      },
      err => {
        this.isQuotePlanListError = true;
        this.commonService.handleErrorAPIResponse(this, 'isQuotePlanListLoaded', 'quotePlanList', err);
      });
  }
  selectPlan(plan?: any, index?: any) {
    if (index !== 0) {
      let requestUpdateProfile = {};
      requestUpdateProfile = {
        accountPlan: plan,
        billingType: 'Account Bill'
      };
      this.updateplan.emit(requestUpdateProfile);
    } else {
      this.updateplan.emit(false);
    }
  }
}
