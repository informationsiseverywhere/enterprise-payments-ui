import { Component, OnInit, ViewChild } from '@angular/core';

import { PaginationComponent } from 'src/app/libs/pagination/pagination.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';

@Component({
  selector: 'app-quote-schedule-preview',
  templateUrl: './quote-schedule-preview.component.html',
  styleUrls: ['./quote-schedule-preview.component.scss']
})
export class QuoteSchedulePreviewComponent implements OnInit {
  private plan: any;
  public size: number;
  public quoteList: any;
  public isQuoteListLoaded = false;
  public isQuoteListError = false;
  @ViewChild(PaginationComponent) paginator: PaginationComponent;
  currency: any;
  constructor(
    private paymentService: PaymentService,
    private commonService: CommonService) {
    this.quoteList = new Array(5);
  }

  ngOnInit() {
    if (this.paymentService.objectToModal) {
      this.plan = this.paymentService.objectToModal;
      this.currency = this.paymentService.currency;
      if (this.paymentService.accountId) {
        this.createQuoteSchedule(this.paymentService.accountId);
      }
    }
  }
  createQuoteSchedule(accountId?: any) {
    const requestBody = {
      newAccountPlan: this.plan
    };
    this.isQuoteListLoaded = false;
    this.paymentService.createQuoteSchedule(accountId, requestBody).subscribe(
      () => {
        this.size = 5;
        this.getQuoteSchedule(this.size, accountId)
      },
      err => {
        this.isQuoteListError = true;
        this.commonService.handleErrorAPIResponse(this, 'isQuoteListLoaded', 'quoteList', err);
      });
  }
  getQuoteSchedule(size?: any, accountId?: any) {
    this.isQuoteListLoaded = false;
    this.quoteList && (this.quoteList.length = this.size < 5 ? this.size : 5);
    this.paymentService.getQuoteSchedule(accountId, this.plan, size, null).subscribe(
      data => {
        this.isQuoteListLoaded = true;
        this.refreshQuoteSchedule(data);
      },
      (err) => {
        this.isQuoteListError = true;
        this.commonService.handleErrorAPIResponse(this, 'isQuoteListLoaded', 'quoteList', err);
      });
  }

  public refreshQuoteSchedule(data?: any): void {
    this.quoteList = data ? data.accountScheduleList : 'No Data';
    this.isQuoteListLoaded = true;
    this.commonService.tablePaginationHandler(this.paginator, data);
  }

  public isLoading(event: any): void {
    this.isQuoteListLoaded = false;
    this.quoteList && (this.quoteList.length = this.size < 5 ? this.size : 5);
  }
}
