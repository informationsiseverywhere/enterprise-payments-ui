import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentusProfileCreateComponent } from './paymentus-profile-create.component';

describe('PaymentusProfileCreateComponent', () => {
  let component: PaymentusProfileCreateComponent;
  let fixture: ComponentFixture<PaymentusProfileCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentusProfileCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentusProfileCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
