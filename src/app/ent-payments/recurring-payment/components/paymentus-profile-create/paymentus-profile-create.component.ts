import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentModel } from 'src/app/ent-payments/batch-payments/models/batch.model';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { DomSanitizer, SafeResourceUrl, } from '@angular/platform-browser';
import { PropertyName } from 'src/app/shared/enum/common.enum';
import { PaymentusService } from 'src/app/shared/services/paymentus.service';
import { PaymentusSecureTokenModel } from 'src/app/shared/models/paymentus.model';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-paymentus-profile-create',
  templateUrl: './paymentus-profile-create.component.html',
  styleUrls: ['./paymentus-profile-create.component.scss']
})
export class PaymentusProfileCreateComponent implements OnInit {
  payment: PaymentModel = new PaymentModel();
  accountId: any;
  isCustomerProfileId: any;
  type: any;
  subType: any;
  returnAppId: any;
  clientId: any;
  multiHostUrls: any;
  url: SafeResourceUrl;
  public paymentusTokenLoaded: boolean = false;
  iframeHeight: string;
  public propertyName = PropertyName;
  constructor(
    private loadingService: LoadingService,
    private route: ActivatedRoute,
    private appConfig: AppConfiguration,
    private paymentusService: PaymentusService,
    private commonService: CommonService,
    public sanitizer: DomSanitizer,
    private router: Router
  ) {
    this.multiHostUrls = this.appConfig.multiHostUrls;
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      queryParams => {
        this.accountId = queryParams.accountId;
        this.type = queryParams.type;
        this.subType = queryParams.subType;
        this.isCustomerProfileId = queryParams.isCustomerProfileId;
        this.returnAppId = queryParams.returnAppId;
        this.clientId = queryParams.clientId
      });
    this.payment.paymentMethod = PropertyName.Echeck;
    this.iframeHeight = PropertyName.Echeck;
    this.generateToken();
  }

  private generateToken(): void {
    const payload: PaymentusSecureTokenModel = new PaymentusSecureTokenModel({
      postbackUrl: this.paymentusService.getPaymentusPostBackURL(),
      iframe: true,
      clientId: this.clientId,
      paymentMethod: this.payment.paymentMethod
    });
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentusTokenLoaded = false;
    this.paymentusService.generateSecureToken(payload).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.paymentusTokenLoaded = true;
        const url = this.paymentusService.getSecureTokenizationIframeUrl(data?.token);
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url);
      },
      (error) => {
        this.loadingService.toggleLoadingIndicator(false);
        this.paymentusTokenLoaded = true;
        this.commonService.handleErrorResponse(error);
      });
  }
  public showHideControls(selectedValue: string): void {
    this.payment = new PaymentModel();
    this.payment.paymentMethod = selectedValue;
    this.iframeHeight = this.commonService.removeSpaces(selectedValue);
    this.generateToken();
  }

  @HostListener('window:message', ['$event'])
  public onMessage(event: any): void {
    this.messageHandler(event);
  }

  public messageHandler(event: any): void {
    if (event && this.paymentusService.isPaymentusMethodCreated(event)) {
      this.router.navigate(['/recurring-payment'], {
        queryParams: {
          accountId: this.accountId,
          isPaymentus: true,
          returnAppId: this.returnAppId
        }
      });
    }
  }

  cancel() {
    this.router.navigate(['/recurring-payment'], {
      queryParams: {
        accountId: this.accountId,
        returnAppId: this.returnAppId
      }
    });
  }

}
