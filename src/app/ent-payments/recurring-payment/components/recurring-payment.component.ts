import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { QuoteScheduleApplyComponent } from './quote-schedule-apply/quote-schedule-apply.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { DateService } from 'src/app/shared/services/date.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentReceivedComponent } from 'src/app/shared/components/payment-received/payment-received.component';
import { PaymentModel } from '../../batch-payments/models/batch.model';
import { PaymentService } from '../../batch-payments/services/payments.service';
import { CurrencyPipe } from '@angular/common';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from '../../security-config/security-config.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IconName, PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-recurring-payment',
  templateUrl: './recurring-payment.component.html',
  styleUrls: ['./recurring-payment.component.scss'],
  providers: [CurrencyPipe]
})
export class RecurringPaymentComponent implements OnInit {
  public propertyName = PropertyName;
  public iconName = IconName;
  accountId: any;
  accountBalanceDetail: any;
  isPaymentMethod = PropertyName.inprogress;
  isSelectAmount = PropertyName.active;
  isConfirm = PropertyName.inprogress;
  amount: any;
  payment: PaymentModel = new PaymentModel();
  isBankIndex: any;
  accountDetails: any;
  storedAccountDetails: any;
  addNewPayment: any;
  clientId: any;
  type = PropertyName.DirectBill;
  subType = PropertyName.RecurringPayment;
  accountNumber: any;
  payorAddress: any;
  imageId: any;
  payorName: any;
  isEdit = false;
  headerName: string;
  isCustomerProfileId: boolean;
  returnAppId: any;
  accountScheduleList: any;
  systemDate: any;
  eftSupportdata: any;
  public isCurrentDue = false;
  public isAccountBalance = false;
  public isOtherAmount = false;
  multiHostUrls: any;
  imageUrl: any;
  public isAccountDetailLoaded = false;
  public isAccountBalanceDetailLoaded = false;
  currency: any;
  private isOtherAmountEntered = false;
  public isShowSelectPayment = true;
  public form: FormGroup;
  autoPaymentIntegrationDropdown: any;
  paymentProviderDropdown: any;
  paymentProvider: any;
  autoPaymentIntegration: boolean;
  cardEftPlanDropdown: any;
  bankEftPlanDropdown: any;
  PayPalEftPlanDropdown: any;
  PayPalCreditEftPlanDropdown: any;
  @ViewChild('paymentReceivedModel', { static: false }) paymentReceivedModel: ModalComponent;
  @ViewChild('quoteApply', { static: false }) quoteApply: QuoteScheduleApplyComponent;
  isPaymentus: any;
  constructor(
    private router: Router,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private route: ActivatedRoute,
    public appConfig: AppConfiguration,
    private dateService: DateService,
    public commonService: CommonService,
    private currencyPipe: CurrencyPipe,
    private fb: FormBuilder,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    private paymentService: PaymentService) {
    this.multiHostUrls = this.appConfig.multiHostUrls;
    this.imageUrl = IconName.userProfilePath;
  }

  ngOnInit() {

    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.form = this.fb.group({
      collectionType: ['', Validators.required],
    });
    this.route.queryParams.subscribe(
      queryParams => {
        this.returnAppId = queryParams.returnAppId;
        this.isCustomerProfileId = queryParams.isCustomerProfileId;
        this.isPaymentus = queryParams.isPaymentus;
        if (this.paymentService.accountId === queryParams.accountId) {
          this.accountId = queryParams.accountId;
          this.paymentService.accountId = this.accountId;
          this.paymentService.accountBalanceDetail = undefined;
        } else {
          this.accountId = queryParams.accountId;
          this.paymentService.accountId = this.accountId;
          this.paymentService.accountBalanceDetail = undefined;
          this.paymentService.accountDetails = undefined;
          this.paymentService.storedAccountDetails = undefined;
        }
        if (this.isPaymentus) {
          this.paymentService.storedAccountDetails = undefined;
        }
        this.getEftSupportData();
      });
  }

  get collectionType() { return this.form.get('collectionType'); }

  checkData() {
    this.getBalanceDetails(this.accountId, this.type);
    this.getAccountDetails(this.accountId, this.type);
    if (this.paymentService.newPaymentDetails) {
      this.addNewPayment = this.paymentService.newPaymentDetails;
      this.payment.amount = this.paymentService.amount;
      if (this.isCustomerProfileId) {
        this.makePaymentLoop(PropertyName.isPaymentMethod, true);
      } else {
        this.makePaymentLoop(PropertyName.isSelectAmount, true);
      }
    } else {
      if (this.payment && this.payment.paymentMethod && this.isSelectAmount !== PropertyName.active) {
        this.makePaymentLoop(PropertyName.isPaymentMethod, this.isPaymentMethod === PropertyName.active ? false : true);
      } else {
        if (this.isPaymentus) {
          this.payment.amount = this.paymentService.amount;
          this.makePaymentLoop(PropertyName.isSelectAmount, true);
        } else {
          this.makePaymentLoop(PropertyName.isSelectAmount, false);
        }
      }
    }
  }

  private getEftSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getEftPlanSupportData(this.accountId).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.eftSupportdata = data;
          this.getDropdownItems(PropertyName.bankEftPlan);
          this.getDropdownItems(PropertyName.cardEftPlan);
          this.getDropdownItems(PropertyName.PaypalEftPlan);
          this.getDropdownItems(PropertyName.PaypalCreditEftPlan);
          this.getDropdownItems(PropertyName.autoPaymentIntegration);
          this.getDropdownItems(PropertyName.paymentProvider);
          this.checkData();
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.eftSupportdata.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    let autoPaymentIntegration = 'false';
    this.autoPaymentIntegrationDropdown && (autoPaymentIntegration = this.autoPaymentIntegrationDropdown[0]);
    this.paymentProviderDropdown && (this.paymentProvider = this.paymentProviderDropdown[0]
    );
    this.autoPaymentIntegration = false;
    if (autoPaymentIntegration === 'true') {
      this.autoPaymentIntegration = true;
    }
  }


  getBalanceDetails(accountId?: any, type?: any) {
    this.isAccountBalanceDetailLoaded = false;
    if (this.paymentService.accountBalanceDetail) {
      this.accountBalanceDetail = this.paymentService.accountBalanceDetail;
      this.isAccountBalanceDetailLoaded = true;
      if (!this.paymentService.newPaymentDetails) {
        const amount = this.accountBalanceDetail.currentDue + this.accountBalanceDetail.pastDue;
        this.checkPaymentAmount(amount);
      }
    } else {
      this.accountBalanceDetail = {};
      this.paymentService.getBalances(accountId, type).subscribe(
        data => {
          this.isAccountBalanceDetailLoaded = true;
          this.accountBalanceDetail = data;
          this.paymentService.accountBalanceDetail = data;
          if (!this.paymentService.newPaymentDetails) {
            const amount = data.currentDue + data.pastDue;
            this.checkPaymentAmount(amount);
          }
        },
        (err) => {
          this.commonService.handleErrorAPIResponse(this, 'isAccountBalanceDetailLoaded', 'accountBalanceDetail', err);
        });
    }
  }

  checkPaymentAmount(value: any) {
    if (!this.autoPaymentIntegration) {
      if (value === 0) {
        this.payment.amount = null;
      }
    } else if (this.autoPaymentIntegration) {
      if (value === 0 && this.paymentProvider !== PropertyName.paymentus) {
        this.payment.amount = 1;
        this.selectAmount(this.payment.amount, PropertyName.isOtherAmount);
      }
    }
    this.checkSecurityOptions()
  }

  getAccountDetails(accountId?: any, type?: any) {
    this.isAccountDetailLoaded = false;
    if (this.paymentService.accountDetails) {
      this.accountDetails = this.paymentService.accountDetails;
      this.clientId = this.accountDetails ? this.accountDetails.payorId : '';
      this.payorName = this.accountDetails ? this.accountDetails.payorName : '';
      this.accountNumber = this.accountDetails ? this.accountDetails.accountNumber : '';
      this.payorAddress = this.accountDetails ? this.accountDetails.payorAddress : '';
      this.imageId = this.accountDetails ? this.accountDetails.imageId : undefined;
      this.currency = this.accountDetails ? this.accountDetails.currency : undefined;
      this.isAccountDetailLoaded = true;
      this.getStoredCard(this.clientId);
    } else {
      this.paymentService.getAccountDetails(accountId, type).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.accountDetails = data ? data.content[0] : null;
          this.paymentService.accountDetails = this.accountDetails;
          this.clientId = this.accountDetails ? this.accountDetails.payorId : '';
          this.payorName = this.accountDetails ? this.accountDetails.payorName : '';
          this.accountNumber = this.accountDetails ? this.accountDetails.accountNumber : '';
          this.payorAddress = this.accountDetails ? this.accountDetails.payorAddress : '';
          this.imageId = this.accountDetails ? this.accountDetails.imageId : undefined;
          this.currency = this.accountDetails ? this.accountDetails.currency : undefined;
          this.isAccountDetailLoaded = true;
          this.getStoredCard(this.clientId);
        },
        (err) => {
          this.commonService.handleErrorAPIResponse(this, 'isAccountDetailLoaded', 'accountDetails', err);
        });
    }
  }

  getStoredCard(clientId?: any) {
    if (this.paymentService.storedAccountDetails) {
      this.storedAccountDetails = this.paymentService.storedAccountDetails;
      if (this.paymentService.newPaymentDetails) {
        if (!this.storedAccountDetails) {
          this.storedAccountDetails = {
            paymentProfiles: []
          };
        }
        this.selectAddNewBank(this.paymentService.newPaymentDetails);
      }
    } else {
      if (this.autoPaymentIntegration) {
        this.getStoredCardList(clientId);
      } else {
        this.storedAccountDetails = null;
        this.paymentService.storedAccountDetails = this.storedAccountDetails;
        if (this.paymentService.newPaymentDetails) { this.addNewPayment = this.paymentService.newPaymentDetails; }
        if (this.addNewPayment) {
          if (!this.storedAccountDetails) {
            this.storedAccountDetails = {
              paymentProfiles: []
            };
          }
          this.selectAddNewBank(this.addNewPayment);
        }
      }
    }
  }

  getStoredCardList(clientId: any) {
    this.paymentService.getStoredCard(clientId).subscribe(
      data => {
        this.storedAccountDetails = data;
        this.paymentService.storedAccountDetails = data;
        if (this.addNewPayment) {
          if (!data) {
            this.storedAccountDetails = {
              paymentProfiles: []
            };
          }
          this.selectAddNewBank(this.addNewPayment);
        } else {
          if (data) {
            this.selectBank(data.paymentProfiles[0], 0);
          }
        }
      },
      () => {
        this.storedAccountDetails = null;
        this.paymentService.storedAccountDetails = this.storedAccountDetails;
        if (this.addNewPayment) {
          if (!this.storedAccountDetails) {
            this.storedAccountDetails = {
              paymentProfiles: []
            };
          }
          this.selectAddNewBank(this.addNewPayment);
        }
      });
  }
  getInvoiceSchedule(accountId?: string, size?: string, systemDate?: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getInvoiceScheduleList(accountId, size, systemDate).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.accountScheduleList = data ? data.accountScheduleList : null;
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  selectAmount(value?: any, radioCheck?: any) {
    if (radioCheck !== PropertyName.isOtherAmount) { this.payment.amount = null; }
    if (radioCheck === PropertyName.isCurrentDue) {
      this[radioCheck] = true;
      this.isAccountBalance = false;
      this.isOtherAmount = false;
      this.payment.amount = value;
      this.isOtherAmountEntered = false;
    } else if (radioCheck === PropertyName.isAccountBalance) {
      this[radioCheck] = true;
      this.isCurrentDue = false;
      this.isOtherAmount = false;
      this.payment.amount = null;
      this.isOtherAmountEntered = false;
    } else if (radioCheck === PropertyName.isOtherAmount) {
      this[radioCheck] = true;
      this.isAccountBalance = false;
      this.isCurrentDue = false;
      if (!this.isOtherAmountEntered) {
        this.payment.amount = 1;
        this.isOtherAmountEntered = true;
      }
    }
  }
  private selectAddNewBank(data?: any): void {
    if (data.paymentMethod === PropertyName.BankAccount) {
      this.storedAccountDetails.paymentProfiles.push({
        name: data.fullName,
        bankAccount: {
          accountType: PropertyName.saving,
          routingNumber: data.routingNumber,
          accountNumber: data.accountNumber,
          nameOnAccount: data.fullName
        }
      });
    } else if (data.paymentMethod === PropertyName.CreditCard) {
      this.storedAccountDetails.paymentProfiles.push({
        name: data.fullName,
        creditCard: {
          cardType: data.creditCardType,
          cardNumber: data.creditCardNumber
        }
      });
    }
    const index = this.storedAccountDetails.paymentProfiles.length - 1;
    this.selectBank(this.storedAccountDetails.paymentProfiles[index], index);
  }

  selectBank(data?: any, index?: any) {
    this.isBankIndex = index;
    const paymentMethod = this.isObject(data);
    this.payment.address = data.address;
    this.payment.paymentProfileId = data.paymentProfileId;
    this.payment.customerProfileId = this.storedAccountDetails.customerProfileId;
    if (paymentMethod === PropertyName.bankAccount) {
      this.payment.paymentMethod = PropertyName.BankAccount;
      this.payment.fullName = data?.name ? data?.name : data?.bankAccount?.nameOnAccount;
      this.payment.accountType = data?.bankAccount.accountType;
      this.payment.accountNumber = data?.bankAccount.accountNumber;
      this.payment.routingNumber = data?.bankAccount.routingNumber;
    } else if (paymentMethod === PropertyName.creditCard) {
      this.payment.paymentMethod = PropertyName.CreditCard;
      this.payment.fullName = data?.name;
      this.payment.creditCardType = data?.creditCard.cardType;
      this.payment.creditCardNumber = data?.creditCard.cardNumber;
      this.payment.creditCardExpiryDate = data?.creditCard.expirationDate;
    } else if (paymentMethod === PropertyName.digitalWallet) {
      this.payment.paymentMethod = data?.digitalWallet?.walletType;
      this.payment.fullName = data.name;
      this.payment.walletType = data?.digitalWallet?.walletType;
      this.payment.walletNumber = data.digitalWallet.walletNumber;
      this.payment.walletIdentifier = data.digitalWallet?.walletIdentifier;
      this.payment.attributeValue = data.attributeValue;
    }
  }

  public isObject(value?: any): string {
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.creditCard)) {
      return PropertyName.creditCard;
    }
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.bankAccount)) {
      return PropertyName.bankAccount;
    }
    if (typeof value === 'object' && value.hasOwnProperty(PropertyName.digitalWallet)) {
      return PropertyName.digitalWallet;
    }
  }

  navigateToAddPayment() {
    if (!this.storedAccountDetails) {
      this.paymentService.amount = this.payment.amount;
      if ((this.paymentProvider === PropertyName.authorizeNet && this.autoPaymentIntegration) ||
        ((this.paymentProvider === '' || this.paymentProvider === PropertyName.authorizeNet) && !this.autoPaymentIntegration) ||
        ((this.paymentProvider === PropertyName.authorizeNet || this.paymentProvider === PropertyName.paymentus)
          && !this.autoPaymentIntegration)) {
        this.router.navigate(['/recurring-payment', 'add-new-payment'], {
          queryParams: {
            accountId: this.accountId,
            isCustomerProfileId: false,
            returnAppId: this.returnAppId,
            subType: this.subType
          }
        });
      } else if ((this.paymentProvider === PropertyName.paymentus && this.autoPaymentIntegration)) {
        this.paymentusFlow();
      }
    } else if (this.storedAccountDetails && (this.paymentProvider === PropertyName.paymentus && this.autoPaymentIntegration)) {
      this.payment.amount = 0;
      this.makePaymentLoop(PropertyName.isSelectAmount, true);
    }
  }

  paymentusFlow() {
    this.paymentService.amount = this.payment.amount ? this.payment.amount : this.payment.paymentAmount;
    this.router.navigate(['/recurring-payment', 'paymentus-profile-create'], {
      queryParams: {
        accountId: this.accountId,
        isCustomerProfileId: false,
        returnAppId: this.returnAppId,
        subType: this.subType,
        clientId: this.clientId
      }
    });
  }

  makePaymentLoop(value?: any, activeClass?: any) {
    this.paymentService.amount = this.payment.amount ? this.payment.amount : this.payment.paymentAmount;
    if (value === PropertyName.isSelectAmount && activeClass) {
      this[value] = PropertyName.complete;
      this.isPaymentMethod = PropertyName.active;
      this.isConfirm = PropertyName.inprogress;
      this.headerName = PropertyName.PaymentMethod;
    } else if (value === PropertyName.isSelectAmount && !activeClass) {
      this[value] = PropertyName.active;
      this.isPaymentMethod = PropertyName.inprogress;
      this.isConfirm = PropertyName.inprogress;
      this.headerName = PropertyName.ChooseAmount;
    } else if (value === PropertyName.isPaymentMethod && activeClass) {
      this[value] = PropertyName.complete;
      this.isConfirm = PropertyName.active;
      this.isSelectAmount = PropertyName.complete;
      this.headerName = PropertyName.ReviewAndConfirm;
    }
  }

  showPaymentAmount() {
    if (!this.autoPaymentIntegration) {
      if ((this.accountBalanceDetail?.currentDue + this.accountBalanceDetail?.pastDue) > 0 && this.accountBalanceDetail?.accountBalance > 0 && this.payment.amount) {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  makePay() {
    if (this.addNewPayment) {
      if (!this.autoPaymentIntegration) {
        if ((this.accountBalanceDetail?.currentDue + this.accountBalanceDetail?.pastDue) === 0 ||
          this.accountBalanceDetail?.accountBalance === 0) {
          this.setupAutoPayment();
        } else if ((this.accountBalanceDetail?.currentDue + this.accountBalanceDetail?.pastDue) !== 0 &&
          this.accountBalanceDetail?.accountBalance !== 0 && !this.payment.amount) {
          this.setupAutoPayment();
        } else {
          this.makePayWithNewPayments();
        }
      } else {
        this.makePayWithNewPayments();
      }
    } else {
      if ((this.paymentProvider === PropertyName.paymentus && this.autoPaymentIntegration && (this.payment?.amount === 0 || this.payment?.paymentAmount === 0))) {
        if (this.payment?.amount > 0) {
          this.makePayWithStoredCards();
        } else {
          this.setupAutoPayment();
        }
      } else {
        this.makePayWithStoredCards();
      }
    }
  }

  makePayWithStoredCards() {
    let requestBody: any = {
      customerProfileId: this.payment.customerProfileId,
      paymentProfileId: this.payment.paymentProfileId,
      amount: this.payment.amount,
      appExternalId: this.accountNumber,
      customer: {
        merchantcustomerid: this.clientId
      },
      transactionType: PropertyName.authCaptureTransaction,
    };
    if ((this.paymentProvider === PropertyName.paymentus && this.autoPaymentIntegration)) {
      requestBody.accountNumber = this.accountNumber;
    }
    if (this.autoPaymentIntegration) {
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.makePayment(requestBody, PropertyName.recurringPayments).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.savePlan(data, this.payment);
        },
        err => {
          this.loadingService.toggleLoadingIndicator(false);
          this.reset(true, null);
          this.readErrMsgResponse(err);
        });
    } else {
      this.callToEnterprisePaymentsAPI(null, this.payment);
    }
  }

  makePayWithNewPayments() {
    const payment: any = {
      paymentNonce: this.addNewPayment.paymentNonce,
      amount: this.payment.amount,
      appExternalId: this.accountNumber,
      customer: {
        firstName: '',
        lastName: this.addNewPayment.fullName,
        merchantcustomerid: this.clientId
      },
      transactionType: PropertyName.authCaptureTransaction,
      storeCard: this.addNewPayment.paymentMethod === PropertyName.BankAccount ? this.addNewPayment.storeAccount : this.addNewPayment.storeCard
    };
    if (this.storedAccountDetails) {
      payment.customerProfileId = this.storedAccountDetails.customerProfileId;
    }
    if (this.autoPaymentIntegration) {
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.makeNewPayment(payment, PropertyName.recurringPayments).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          this.savePlan(data, this.addNewPayment);
        },
        err => {
          this.loadingService.toggleLoadingIndicator(false);
          this.reset(true, null);
          this.readErrMsgResponse(err);
        });
    } else {
      this.callToEnterprisePaymentsAPI(null, this.addNewPayment);
    }
  }

  setupAutoPayment() {
    const requestBody: any = {
      identifier: this.accountNumber,
      isRecurring: true,
      identifierType: PropertyName.ConsumerDirect,
      bankAccountNumber: this.addNewPayment?.accountNumber ? this.addNewPayment?.accountNumber : this.payment?.accountNumber,
      paymentMethod: this.addNewPayment?.paymentMethod ? this.addNewPayment?.paymentMethod : this.payment?.paymentMethod,
      collectionType: this.collectionType.value
    };
    if (requestBody.paymentMethod === PropertyName.BankAccount) {
      requestBody.accountType = this.addNewPayment?.accountType ? this.addNewPayment?.accountType : this.payment?.accountType;
      requestBody.paymentType = PropertyName.WebsiteCheck1xACH;
      requestBody.routingNumber = this.addNewPayment?.routingNumber ? this.addNewPayment?.routingNumber : this.payment?.routingNumber;
      requestBody.bankHolderName = this.addNewPayment?.fullName ? this.addNewPayment?.fullName : this.payment?.fullName;
    } else {
      requestBody.paymentType = PropertyName.CreditCardPayment;
      requestBody.creditCardNumber = this.addNewPayment?.creditCardNumber ? this.addNewPayment?.creditCardNumber : this.payment?.creditCardNumber;
      requestBody.creditCardType = this.addNewPayment?.creditCardType ? this.addNewPayment?.creditCardType : this.payment.creditCardType;
      requestBody.creditCardExpiryDate = this.commonService.removeSpacesAndFWDash(this.addNewPayment?.creditCardExpiryDate ? this.addNewPayment?.creditCardExpiryDate : this.payment?.creditCardExpiryDate);
    }
    if ((this.paymentProvider === PropertyName.paymentus && this.autoPaymentIntegration && (this.payment?.amount === 0 || this.payment?.paymentAmount === 0))) {
      requestBody.paymentProfileId = this.payment?.paymentProfileId;
      requestBody.email = this.accountDetails.emailId;
    }
    if (requestBody.paymentMethod === PropertyName.Paypal) {
      requestBody.paymentMethod = PropertyName.Paypal;
      requestBody.walletNumber = this.payment.walletNumber;
      requestBody.paymentType = PropertyName.Paypal;
      requestBody.walletType = PropertyName.Paypal;
      requestBody.walletIdentifier = requestBody.walletIdentifier;
    }
    if (requestBody.paymentMethod === PropertyName.PaypalCredit) {
      requestBody.paymentMethod = PropertyName.PaypalCredit;
      requestBody.walletNumber = this.payment.walletNumber;
      requestBody.paymentType = PropertyName.PaypalCredit;
      requestBody.walletType = PropertyName.PaypalCredit;
      requestBody.walletIdentifier = this.payment.walletIdentifier;
    }

    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.setupAutoPaymentOff(requestBody).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentCreatedResponse: any = data;
        let paymentDetailUrl = '';
        if (paymentCreatedResponse.links && paymentCreatedResponse.links.length > 0) {
          paymentDetailUrl = paymentCreatedResponse.links[0].href;
          requestBody.paymentModelType = PropertyName.subscriptions;
          this.getPaymentDetail(paymentDetailUrl, requestBody);
        }
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  savePlan(responseData?: any, payment?: any) {
    if (this.paymentService.planUpdateDetails) {
      this.loadingService.toggleLoadingIndicator(true);
      this.paymentService.savePlan(this.accountDetails.accountId, this.paymentService.planUpdateDetails).subscribe(
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.callToEnterprisePaymentsAPI(responseData, payment);
        },
        err => {
          this.commonService.handleErrorResponse(err);
        });
    } else {
      this.callToEnterprisePaymentsAPI(responseData, payment);
    }
  }

  callToEnterprisePaymentsAPI(responseData?: any, payment?: any) {
    const requestBody: any = {
      paymentAmount: this.payment.amount,
      identifier: this.accountDetails.accountNumber,
      isRecurring: true,
      identifierType: PropertyName.ConsumerDirect,
      collectionType: this.collectionType.value
    };

    if (responseData) {
      requestBody.paymentComments = responseData.transactionMessage;
      requestBody.paymentId = responseData.transactionId;
      requestBody.transactionId = responseData.paymentId;
      if (payment.paymentMethod === PropertyName.BankAccount) {
        requestBody.bankAccountNumber = responseData.accountNumber ? responseData.accountNumber : payment?.accountNumber;
        requestBody.authorizationResponseType = responseData.avsCode;
      } else {
        requestBody.authorizationResponse = responseData.responseCode;
        requestBody.authorization = responseData.authCode;
        requestBody.authorizationResponseType = responseData.avsCode + responseData.cvvCode + responseData.cavvCode;
        requestBody.creditCardNumber = responseData.accountNumber ? responseData.accountNumber : this.payment.creditCardNumber;
        requestBody.creditCardType = responseData.accountType ? responseData.accountType : this.payment.creditCardType;
        requestBody.creditCardExpiryDate = PropertyName.XXXX;
      }
    } else {
      requestBody.bankAccountNumber = payment.accountNumber;
    }

    if (payment.paymentMethod === PropertyName.BankAccount) {
      requestBody.paymentMethod = PropertyName.BankAccount;
      requestBody.accountType = payment.accountType;
      requestBody.paymentType = this.autoPaymentIntegration ? PropertyName.Echeck : PropertyName.WebsiteCheck1xACH;
      requestBody.routingNumber = payment.routingNumber;
    } else {
      requestBody.paymentMethod = PropertyName.CreditCard;
      requestBody.paymentType = PropertyName.CreditCardPayment;
      if (!responseData) {
        requestBody.creditCardNumber = payment.creditCardNumber;
        requestBody.creditCardType = payment.creditCardType;
        requestBody.creditCardExpiryDate = this.commonService.removeSpacesAndFWDash(payment.creditCardExpiryDate);
      }
    }
    if (payment.paymentMethod === PropertyName.Paypal) {
      requestBody.paymentMethod = PropertyName.Paypal;
      requestBody.walletNumber = payment.walletNumber;
      requestBody.paymentType = PropertyName.Paypal;
      requestBody.walletType = PropertyName.Paypal;
      requestBody.walletIdentifier = payment.walletIdentifier;
    }
    if (payment.paymentMethod === PropertyName.PaypalCredit) {
      requestBody.paymentMethod = PropertyName.PaypalCredit;
      requestBody.walletNumber = payment.walletNumber;
      requestBody.paymentType = PropertyName.PaypalCredit;
      requestBody.walletType = PropertyName.PaypalCredit;
      requestBody.walletIdentifier = payment.walletIdentifier;
    }
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.callToEnterprisePaymentsAPI(requestBody).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentCreatedResponse: any = data;
        let paymentDetailUrl = '';
        if (paymentCreatedResponse.links && paymentCreatedResponse.links.length > 0) {
          paymentDetailUrl = paymentCreatedResponse.links[0].href;
          this.getPaymentDetail(paymentDetailUrl, requestBody);
        }
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  getPaymentDetail(url: string, requestBody?: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentDetailByUrl(url).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentDetailInfo: any = data;
        requestBody.paymentSequenceNumber = paymentDetailInfo.paymentSequenceNumber;
        requestBody.batchId = paymentDetailInfo.batchId;
        requestBody.postedDate = paymentDetailInfo.postedDate;
        requestBody.userId = paymentDetailInfo.userId;
        this.reset(true, requestBody);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        const body = {
          paymentModelType: PropertyName.subscriptionsFail
        };
        this.reset(true, body);
      });
  }

  reset(status?: any, requestBody?: any) {
    if (status) {
      this.openPaymentReceivedModel(requestBody);
    }
  }

  private openPaymentReceivedModel(data?: any): void {
    if (data) {
      if (data?.paymentModelType === PropertyName.subscriptions) {
        this.paymentReceivedModel.modalTitle = PropertyName.AutoPaymentSetupCompleted;
        this.paymentReceivedModel.icon = IconName.success;
      } else if (data?.paymentModelType === PropertyName.subscriptionsFail) {
        this.paymentReceivedModel.modalTitle = PropertyName.AutoPaymentSetupFailed;
        this.paymentReceivedModel.subModalTitle = 'We can`t setup your auto payment . Please contact Customer Service for further assistance.';
        this.paymentReceivedModel.icon = IconName.fail;
      } else {
        if (this.autoPaymentIntegration) {
          this.paymentReceivedModel.modalTitle = PropertyName.PaymentReceived;
          this.paymentReceivedModel.subModalTitle = 'Your payment of ' +
            this.currencyPipe.transform(data.paymentAmount, this.currency) + ' is received . It may take up you 24 hours of your online balance to display this payment.';
          this.paymentReceivedModel.icon = IconName.success;
        } else {
          this.paymentReceivedModel.modalTitle = PropertyName.PaymentSubmitted;
          this.paymentReceivedModel.subModalTitle = 'Your payment of ' + this.currencyPipe.transform(data.paymentAmount, this.currency) + ' is successfully submitted.';
          this.paymentReceivedModel.icon = IconName.success;
        }
      }
    } else {
      this.paymentReceivedModel.modalTitle = 'transaction was unsuccessful.';
      this.paymentReceivedModel.subModalTitle = 'We can`t process your Payment . Please contact Customer Service for further assistance.';
      this.paymentReceivedModel.icon = IconName.fail;
    }
    this.paymentService.objectToModal = data;
    this.paymentReceivedModel.modalFooter = false;
    this.paymentReceivedModel.open(PaymentReceivedComponent);
  }

  public printStatement(status?: any): void {
    this.paymentService.accountDetails = undefined;
    this.paymentService.objectToModal = undefined;
    if (this.returnAppId) {
      this.onSuccessNavigate(status);
    } else {
      setTimeout(() => {
        this.router.navigate(['/ent-payments', 'express-payments']);
      }, 100);
    }
  }
  public onSuccessNavigate(status?: any): void {
    const resultQueryParam = JSON.parse(sessionStorage.getItem(PropertyName.resultQueryParam));
    const cancelUrl = resultQueryParam.cancelUrl;
    const returnUrl = resultQueryParam.returnUrl;
    const url = (status === PropertyName.cancel ? cancelUrl : returnUrl);
    this.resetData();
    this.commonService.navigateToUrl(url, PropertyName.self, true);
  }

  private resetData(): void {
    this.isPaymentMethod = PropertyName.inprogress;
    this.isSelectAmount = PropertyName.active;
    this.isConfirm = PropertyName.inprogress;
    this.isBankIndex = -1;
    this.payment = new PaymentModel();
    this.storedAccountDetails = undefined;
    this.addNewPayment = undefined;
    this.paymentService.amount = undefined;
    this.paymentService.newPaymentDetails = undefined;
    this.paymentService.accountBalanceDetail = undefined;
    this.paymentService.storedAccountDetails = undefined;
    this.paymentService.planUpdateDetails = undefined;
  }

  updateplan(event?: any) {
    if (this.storedAccountDetails) {
      if ((this.accountBalanceDetail?.currentDue + this.accountBalanceDetail?.pastDue) === 0 && this.paymentProvider === PropertyName.paymentus) {
        this.payment.amount = 0;
      }
      this.makePaymentLoop(PropertyName.isSelectAmount, true);
    }
    if (!this.storedAccountDetails) { this.navigateToAddPayment(); }
    this.paymentService.planUpdateDetails = undefined;
    if (event) {
      this.paymentService.planUpdateDetails = event;
    }
  }

  readErrMsgResponse(err?: any) {
    this.loadingService.toggleLoadingIndicator(false);
    if (err.error.errorDescription) {
      this.alertService.error(err.error.errorDescription);
    } else {
      if (err.error.message) {
        this.alertService.error(err.error.message[0].text);
      }
    }
  }


  public getEftDropdown(paymentMethod: any) {
    if (paymentMethod === PropertyName.CreditCard) {
      return this.cardEftPlanDropdown;
    } else if ((paymentMethod === PropertyName.BankAccount)) {
      return this.bankEftPlanDropdown;
    } else if ((paymentMethod === PropertyName.Paypal)) {
      return this.PayPalEftPlanDropdown;
    } else if ((paymentMethod === PropertyName.PaypalCredit)) {
      return this.PayPalCreditEftPlanDropdown;
    }
  }

  public goBack(): void {
    this.isPaymentMethod = PropertyName.inprogress;
    this.isSelectAmount = PropertyName.active;
    this.isConfirm = PropertyName.inprogress;
    this.isBankIndex = -1;
    this.payment = new PaymentModel();
    this.storedAccountDetails = undefined;
    this.addNewPayment = undefined;
    this.paymentService.amount = undefined;
    this.paymentService.accountDetails = null;
    this.paymentService.newPaymentDetails = undefined;
    this.paymentService.accountBalanceDetail = undefined;
    this.paymentService.storedAccountDetails = undefined;
    this.paymentService.agentType = null;
    this.isPaymentus = null;
    this.checkData();
  }

  public openSecurity(event?: any, configType?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, configType);
  }

  public checkSecurityOptions(): any {
    this.isShowSelectPayment = true;
    if (this.isSelectAmount === PropertyName.active && (this.accountBalanceDetail?.currentDue + this.accountBalanceDetail?.pastDue) != 0) {
      const currentBalanceProperty = this.securityEngineService.securityData?.currentBalance?.fieldProperty;
      const otherAmountProperty = this.securityEngineService.securityData?.otherAmount?.fieldProperty;
      const subWithoutPaymentProperty = this.securityEngineService.securityData?.subscribeWithoutPayment?.fieldProperty;

      if ((this.autoPaymentIntegration && currentBalanceProperty === PropertyName.Hide && otherAmountProperty === PropertyName.Hide) ||
        (!this.autoPaymentIntegration && currentBalanceProperty === PropertyName.Hide && otherAmountProperty === PropertyName.Hide && subWithoutPaymentProperty === PropertyName.Hide)) {
        this.isShowSelectPayment = false;
        this.selectAmount(this.accountBalanceDetail.currentDue + this.accountBalanceDetail.pastDue, PropertyName.isCurrentDue);
      } else {
        if (currentBalanceProperty === PropertyName.Show && this.accountBalanceDetail.currentDue + this.accountBalanceDetail.pastDue !== 0) {
          this.selectAmount(this.accountBalanceDetail.currentDue + this.accountBalanceDetail.pastDue, PropertyName.isCurrentDue);
        } else if (otherAmountProperty === PropertyName.Show) {
          this.selectAmount(null, PropertyName.isOtherAmount);
        } else if (!this.autoPaymentIntegration && (!subWithoutPaymentProperty || subWithoutPaymentProperty === PropertyName.Show)) {
          this.selectAmount(null, PropertyName.isAccountBalance);
        } else {
          this.isCurrentDue = false;
          this.isOtherAmount = false;
          this.isAccountBalance = false;
        }
      }
    }
  }
}

