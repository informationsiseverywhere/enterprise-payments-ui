import { Routes } from '@angular/router';

import { AuthGuard } from 'src/app/ent-auth/auth.guard';
import { NavigationComponent } from '../navigation/navigation.component';
import { RecurringPaymentComponent } from './components/recurring-payment.component';
import { AlternatePaymentMethodComponent } from 'src/app/shared/components/alternate-payment-method/alternate-payment-method.component';
import { PaymentusProfileCreateComponent } from './components/paymentus-profile-create/paymentus-profile-create.component';


export const recurringPaymentRoutes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
      {
        path: '',
        component: RecurringPaymentComponent
      },
      {
        path: 'add-new-payment',
        component: AlternatePaymentMethodComponent,
        data: {
          breadcrumb: 'Add New Payments',
          headerTitle: 'Add New Payments',
          screenName: 'epAddNewPaymentMethod'
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'paymentus-profile-create',
        component: PaymentusProfileCreateComponent,
        data: {
          breadcrumb: 'Paymentus Profile Create',
          headerTitle: 'Paymentus Profile Create',
          screenName: 'epAddNewPaymentMethod'
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

