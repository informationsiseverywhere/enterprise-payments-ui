import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { recurringPaymentRoutes } from './recurring-payment-routes';

@NgModule({
  imports: [
    RouterModule.forChild(recurringPaymentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class RecurringPaymentRoutingModule {
}
