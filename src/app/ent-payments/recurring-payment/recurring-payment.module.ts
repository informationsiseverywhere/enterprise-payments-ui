import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { RecurringPaymentRoutingModule } from './recurring-payment-routing.module';

import { RecurringPaymentComponent } from './components/recurring-payment.component';
import { QuoteScheduleApplyComponent } from './components/quote-schedule-apply/quote-schedule-apply.component';
import { QuoteSchedulePreviewComponent } from './components/quote-schedule-apply/quote-schedule-preview/quote-schedule-preview.component';
import { PaymentusProfileCreateComponent } from './components/paymentus-profile-create/paymentus-profile-create.component';

@NgModule({
  declarations: [
    RecurringPaymentComponent,
    QuoteScheduleApplyComponent,
    QuoteSchedulePreviewComponent,
    PaymentusProfileCreateComponent
  ],
  entryComponents: [QuoteSchedulePreviewComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    RecurringPaymentRoutingModule
  ]
})
export class RecurringPaymentModule { }
