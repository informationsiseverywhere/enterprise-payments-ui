import { Routes } from '@angular/router';
import { AuthGuard } from '../ent-auth/auth.guard';
import { SecurityConfigGuard } from '../ent-auth/security-config.guard';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ExpressPaymentsComponent } from './express-payments/express-payments.component';
import { AppLoggerComponent } from '../shared/components/app-logger/app-logger.component';

export const paymentsRoutes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
      {
        path: '',
        children: [
          {
            path: '',
            canActivate: [AuthGuard],
            component: DashboardComponent,
            data: {
              screenName: 'epDashBoard'
            }
          },
          {
            path: 'express-payments',
            component: ExpressPaymentsComponent,
            canActivate: [AuthGuard],
            data: {
              breadcrumb: 'Express Payments',
              headerTitle: 'Express Payments',
              screenName: 'epProvideBillingInformation'
            }
          },
          {
            path: 'batch-payments',
            canActivate: [AuthGuard],
            loadChildren: () => import('./batch-payments/batch-payments.module').then(mod => mod.BatchPaymentsModule),
            data: {
              breadcrumb: 'Batch Payments',
              screenName: 'epBatchPayments',
              headerTitle: 'Batch Payments'
            }
          },
          {
            path: 'payments-search/identified-payments',
            canActivate: [AuthGuard, SecurityConfigGuard],
            loadChildren: () => import('./payments-search/payments-search.module').then(mod => mod.PaymentsSearchModule),
            data: {
              breadcrumb: 'Payments Search - Identified Payments',
              headerTitle: 'Payments Search - Identified Payments',
              screenName: 'epPaymentsSearchIdentifiedPayments',
              permissions: {
                dependOnScreenName: 'epDashBoard',
                parentName: 'paymentSearch',
                only: ['identifiedPaymentSearch','unIdentifiedPaymentSearch'],
                redirectTo: {
                  identifiedPaymentSearch: {
                    navigationCommands: 'payments-search/identified-payments'
                  },
                  unIdentifiedPaymentSearch: {
                    navigationCommands: 'payments-search/unidentified-payments'
                  },
                  default: ''
                }
              }
            }
          },
          {
            path: 'payments-search/unidentified-payments',
            canActivate: [AuthGuard, SecurityConfigGuard],
            loadChildren: () => import('./payments-search/components/unidentified-payments/unidentified-payments.module').then(
              mod => mod.UnidentifiedPaymentsModule),
            data: {
              breadcrumb: 'Payments Search - Unidentified Payments',
              headerTitle: 'Payments Search - Unidentified Payments',
              screenName: 'epPaymentsSearchUnidentifiedPayments',
              permissions: {
                dependOnScreenName: 'epDashBoard',
                parentName: 'paymentSearch',
                only: ['unIdentifiedPaymentSearch','identifiedPaymentSearch'],
                redirectTo: {
                  unIdentifiedPaymentSearch: {
                    navigationCommands: 'payments-search/unidentified-payments'
                  },
                  identifiedPaymentSearch: {
                    navigationCommands: 'payments-search/identified-payments'
                  },
                  default: ''
                }
              }
            }
          },
          {
            path: 'batch-rejection',
            canActivate: [AuthGuard],
            loadChildren: () => import('./batch-rejection/batch-rejection.module').then(mod => mod.BatchRejectionModule),
            data: {
              breadcrumb: 'Batch Rejection',
              headerTitle: 'Batch Rejection',
              screenName: 'epBatchRejections'
            }
          },
          {
            path: ':roleSequenceId/configure-default-values',
            loadChildren: () => import('./configure-default-values/configure-default-values.module').then(mod => mod.ConfigureDefaultValuesModule),
            data: {
              breadcrumb: 'Configure Default Values',
              headerTitle: 'Configure Default Values',
            },
            canActivate: [AuthGuard],
          },
          {
            path: 'errors',
            component: AppLoggerComponent,
            canActivate: [AuthGuard],
            data: {
              headerTitle: 'errors'
            }
          }
        ]
      }
    ]
  }
];

