import { Routes } from '@angular/router';

import { AuthGuard } from 'src/app/ent-auth/auth.guard';
import { CommonComponent } from 'src/app/shared/components/common/common.component';
import { ConfigureDefaultValuesComponent } from './components/configure-default-values.component';


export const configureDefaultValuesRoutes: Routes = [
  {
    path: '',
    component: CommonComponent,
    children: [
      {
        path: '',
        children: [
          {
            path: '',
            canActivate: [AuthGuard],
            component: ConfigureDefaultValuesComponent
          }
        ]
      }
    ]
  }
];


