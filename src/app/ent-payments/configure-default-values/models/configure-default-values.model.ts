export class ScreenNameModel {
  screenName: any;
  description: any;
  screenId: any;
}

export class SectionNameModel {
  fieldName: any;
  fieldLabel: any;
  fieldId: any;
}
