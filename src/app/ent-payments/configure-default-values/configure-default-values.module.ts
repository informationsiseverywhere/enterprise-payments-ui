import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LayoutModule } from '@angular/cdk/layout';

import { ConfigureDefaultValuesComponent } from './components/configure-default-values.component';
import { LibraryModule } from 'src/app/libs/libs.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfigureDefaultValuesRoutingModule } from './configure-default-values-routing.module';


@NgModule({
  declarations: [
    ConfigureDefaultValuesComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    LayoutModule,
    SharedModule,
    LibraryModule,
    ConfigureDefaultValuesRoutingModule
  ]
})
export class ConfigureDefaultValuesModule { }
