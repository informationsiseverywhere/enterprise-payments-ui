import { TestBed } from '@angular/core/testing';

import { ConfigureDefaultValuesService } from './configure-default-values.service';

describe('ConfigureDefaultValuesService', () => {
  let service: ConfigureDefaultValuesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigureDefaultValuesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
