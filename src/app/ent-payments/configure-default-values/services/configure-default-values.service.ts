import { Injectable } from '@angular/core';

import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { RestService } from 'src/app/shared/services/rest.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigureDefaultValuesService {

  public objectToModal: any;
  applicationId: any;
  applicationList: any;
  applicationName: any;
  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    ) {
      this.applicationList = ComponentConfiguration.settings.applicationList;
      this.applicationName = this.applicationList.applicationName;
      this.applicationId = this.applicationList.applicationId;
     }

  getDefaultConfigurationDetail(applicationId?: any, screenId?: any, roleId?: any, fieldId?: any) {
    let url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/applications/${applicationId}/screens/${screenId}/defaultFields?filters=fieldType:Dropdown,role:${roleId}`;
    if (roleId == 0) {
      url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/applications/${applicationId}/screens/${screenId}/defaultFields?filters=fieldType:Dropdown`;
    }
    if (fieldId) {
      url += `,sectionId:${fieldId}`;
    }
    return this.restService.getByUrl(url);
  }

  updateDefaultConfigurationDetail(applicationId?: any, screenId?: any, roleId?: any, body?: any) {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/applications/${applicationId}/screens/${screenId}/defaultFields?filters=role:${roleId}`;
    return this.restService.putByUrl(url, body);
  }

  getScreenName(screenName?: any) {
    const url = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/applications/${this.applicationId}/screens?filters=screenName:${screenName}`;
    return this.restService.getByUrl(url);
  }
}
