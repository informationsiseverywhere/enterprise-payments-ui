import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { QueryParam } from 'src/app/libs/select/models/select.model';
import { SelectComponent } from 'src/app/libs/select/select.component';
import { SecurityConfigComponent } from 'src/app/shared/components/security-settings/security-config/security-config.component';
import { IconName, ModelObjects } from 'src/app/shared/enum/common.enum';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ComponentConfiguration } from 'src/app/shared/services/component-config.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { RoleService } from 'src/app/shared/services/role.service';

import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { ConfigureDefaultValuesService } from '../services/configure-default-values.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { ScreenNameModel, SectionNameModel } from '../models/configure-default-values.model';
import { IncludeExcludeValues } from '../enum/configure-default-values.enum';

@Component({
  selector: 'app-configure-default-values',
  templateUrl: './configure-default-values.component.html',
  styleUrls: ['./configure-default-values.component.scss']
})
export class ConfigureDefaultValuesComponent implements OnInit {
  apiUrl: any;
  sectionNameDropdown: any;
  public isValuesLoaded = false;
  selectedField: any;
  queryParam: QueryParam = new QueryParam('', '', null, 1, 10);
  queryParamSection: QueryParam = new QueryParam('', 'fieldType:section', null, 1, 10);
  applicationList: any;
  screenNameDetails: ScreenNameModel = new ScreenNameModel();
  sectionNameDetails: SectionNameModel = new SectionNameModel();
  sectionName: any;
  sectionApiUrl: string;
  valuesFieldsForm: FormGroup;
  @ViewChild('sectionSelect', { static: false }) sectionSelect: SelectComponent;
  roleSequenceId: any;
  querySectionName: any;
  queryScreenName: any;
  list: any = [];
  private previousTarget: any;
  previewed: any;
  dropdownSettings = {};
  @ViewChild('selectRoleModel', { static: false }) selectRoleModel: ModalComponent;
  toggleAvailableItems = false;
  toggleAssignedItems = false;
  @ViewChild('element') element: ElementRef;
  @ViewChild('assignedElements') assignedElements: ElementRef;
  enableSave = false;
  size: number;
  constructor(
    public securityEngineService: SecurityEngineService,
    private router: Router,
    private fb: FormBuilder,
    private appConfig: AppConfiguration,
    private route: ActivatedRoute,
    private roleService: RoleService,
    private commonService: CommonService,
    private hostUrlService: DynamicHostUrlService,
    private location: Location,
    private configureDefaultValuesService: ConfigureDefaultValuesService,
    private alertService: AlertService
  ) {
    this.applicationList = ComponentConfiguration.settings.applicationList;
    this.list = new Array(5);
    this.apiUrl = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/applications/${this.applicationList.applicationId}/screens`;
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      params => {
        this.roleSequenceId = params.roleSequenceId;
        this.route.queryParams.subscribe(
          queryParams => {
            this.queryScreenName = queryParams.screenName;
            this.getScreenName(queryParams.screenName);
            this.securityEngineService.getRoleDetail(params.roleSequenceId);
          });
      });
    this.valuesFieldsForm = this.fb.group({
      screenName: ['', Validators.required],
      sectionName: ['']
    });
    this.list = null;
    this.isValuesLoaded = true;
  }

  selectLevel() {
    this.roleService.objectToModal = ModelObjects.ConfigureDefaultValues;
    this.selectRoleModel.modalTitle = 'Select Configuration ';
    this.selectRoleModel.modalFooter = false;
    this.selectRoleModel.subModalTitle = 'Select the configuration which you would like to use for default configuration setup';
    this.selectRoleModel.icon = IconName.selectedRole;
    this.selectRoleModel.open(SecurityConfigComponent);
  }

  private getScreenName(screenName?: any): void {
    this.isValuesLoaded = false;
    this.configureDefaultValuesService.getScreenName(screenName).subscribe(
      data => {
        this.screenNameDetails = (data ? data.content[0] : null);
        this.selectedScreenName(this.screenNameDetails);
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, 'isValuesLoaded', 'screenNameDetails', err);
      });
  }

  public selectedScreenName(screenName: ScreenNameModel): void {
    this.sectionSelect.dropdownItems = [];
    this.sectionSelect.hasNext = false;
    this.screenNameDetails = new ScreenNameModel();
    this.valuesFieldsForm.controls.sectionName.reset();
    this.sectionNameDetails = new SectionNameModel();
    if (screenName) {
      this.screenNameDetails = screenName;
      this.sectionSelect.dropdownItems = [];
      this.sectionSelect.hasNext = false;
      this.sectionApiUrl = `${this.hostUrlService.getHostUrl(this.appConfig.apiUrls.utilities)}v1/utils/applications/${this.applicationList.applicationId}/screens/${screenName.screenId}/fields`;
      this.sectionSelect.getItemsByUrl(this.sectionApiUrl, this.queryParamSection);
      this.getValuesFields();
    }
  }

  public toggleActive(element: any, index: number, selectAllKey?: string): void {
    const parentEl = this[element].nativeElement.children;
    const el = parentEl[index];
    if (el.classList.contains('active')) {
      el.classList.remove('active');
    } else {
      el.classList.add('active');
    }
    if (parentEl && parentEl.length > 0) {
      const selectedItems = [];
      for (const item of parentEl) {
        if (item.classList.contains('active')) { selectedItems.push(item); }
      }
      if (selectedItems?.length > 0) {
        this[selectAllKey] = true;
      } else {
        this[selectAllKey] = false
      }
    }
  }

  addItem(element: any, parentIndex: number, check?: any) {
    this.enableSave = true;
    if (this.list[parentIndex].includeExcludeType !== IncludeExcludeValues.Exclude) {
      let item = [];
      for (const el of this[element].nativeElement.children) {
        if (el.classList.contains('active')) {
          if (!this.list[parentIndex].includeExcludeList) {
            this.list[parentIndex].includeExcludeList = [];
          }
          this.list[parentIndex].includeExcludeList?.push(el.innerText);
          el.classList.remove('active');
          item.push(el.innerText);
        }
      }
      this.list[parentIndex].supportDataItems = this.list[parentIndex].supportDataItems.filter((i: any) => {
        return !item.includes(i);
      });
      this[check] = false;
    } else {
      let item = [];
      for (const el of this[element].nativeElement.children) {
        if (el.classList.contains('active') && (this.list[parentIndex].defaultValue !== el.innerText)) {
          if (!this.list[parentIndex].includeExcludeList) {
            this.list[parentIndex].includeExcludeList = [];
          }
          this.list[parentIndex].includeExcludeList?.push(el.innerText);
        }
        if (el.classList.contains('active')) {
          item.push(el.innerText);
          el.classList.remove('active');
        }
      }
      this.list[parentIndex].supportDataItems = this.list[parentIndex].supportDataItems.filter((i: any) => {
        return !item.includes(i);
      });
      this[check] = false;
      const result = item.find((x: any) => x === this.list[parentIndex].defaultValue);
      if (result) {
        this.list[parentIndex].supportDataItems?.unshift(result);
        setTimeout(() => {
          this.alertService.error(`Default Value "${this.list[parentIndex].defaultValue}" can not be added to Exclude List. Please select a different value.`);
        }, 100);
      }
    }
  }

  removeItem(element: any, parentIndex: number, check?: any) {
    this.enableSave = true;
    if (this.list[parentIndex].includeExcludeType !== IncludeExcludeValues.Exclude) {
      let item = [];
      for (const el of this[element].nativeElement.children) {
        if (el.classList.contains('active') && (this.list[parentIndex].defaultValue !== el.innerText)) {
          item.push(el.innerText);
          this.list[parentIndex].supportDataItems?.unshift(el.innerText);
          el.classList.remove('active');
        }
        if (el.classList.contains('active')) {
          item.push(el.innerText);
          el.classList.remove('active');
        }
      }
      this.list[parentIndex].includeExcludeList = this.list[parentIndex].includeExcludeList.filter((i: any) => {
        return !item.includes(i);
      });
      this[check] = false;
      const result = item.find((x: any) => x === this.list[parentIndex].defaultValue);
      if (result) {
        this.list[parentIndex].includeExcludeList?.unshift(result);
        setTimeout(() => {
          this.alertService.error(`Default Value "${this.list[parentIndex].defaultValue}" can not be removed from Include List. Please select a different value.`);
        }, 100);
      }
    } else {
      let item = [];
      for (const el of this[element].nativeElement.children) {
        if (el.classList.contains('active')) {
          item.push(el.innerText);
          this.list[parentIndex].supportDataItems?.unshift(el.innerText);
          el.classList.remove('active');
        }
      }
      this.list[parentIndex].includeExcludeList = this.list[parentIndex].includeExcludeList.filter((i: any) => {
        return !item.includes(i);
      });
      this[check] = false;
    }
  }

  getValuesFields() {
    this.isValuesLoaded = false;
    this.list = [];
    this.configureDefaultValuesService.getDefaultConfigurationDetail(this.applicationList.applicationId, this.screenNameDetails?.screenId, this.roleSequenceId, this.sectionNameDetails?.fieldId).subscribe(
      data => {
        this.isValuesLoaded = true;
        for (const i of data?.fieldList) {
          if (!i?.supportData) {
            i.supportData = [];
          }
          if (i?.supportData?.length > 0) {
            i.supportDataItems = this.filterSupportDataItems(i);
          }
          if (!i?.includeExcludeList) {
            i.includeExcludeList = [];
          }
          i.includeExcludeType = (i?.includeExcludeType ? i?.includeExcludeType : IncludeExcludeValues.Exclude);
          if (i?.supportDataList?.length > 0) {
            if(i?.dependsOnFieldId){
              const result = data?.fieldList?.filter((x: any) => x?.fieldId === i?.dependsOnFieldId);
              if (result?.length > 0) {
                const value = i?.supportDataList?.filter((x: any) => x?.dependsOn === result[0]?.defaultValue);
                if (value?.length > 0) {
                  i.supportData = value[0].supportData;
                  i.supportDataItems = this.filterSupportDataItems(value[0]);
                  i.defaultValue = value[0]?.defaultValue;
                  i.includeExcludeType = (value[0]?.includeExcludeType ? value[0]?.includeExcludeType : IncludeExcludeValues.Exclude);
                  i.includeExcludeList = (value[0]?.includeExcludeList ? value[0]?.includeExcludeList : []);
                  i.filterValue = value[0]?.filterValue;
                }
              }
            }else{
              if(i?.supportDataList[0]?.supportData){
                i.supportData = i?.supportDataList[0].supportData;
                i.supportDataItems = this.filterSupportDataItems(i?.supportDataList[0]);
                i.defaultValue = i?.supportDataList[0]?.defaultValue;
                i.includeExcludeType = (i?.supportDataList[0]?.includeExcludeType ? i?.supportDataList[0]?.includeExcludeType : IncludeExcludeValues.Exclude);
                i.includeExcludeList = (i?.supportDataList[0]?.includeExcludeList ? i?.supportDataList[0]?.includeExcludeList : []);
              }
            }
          }
        }
        this.list = data?.fieldList;
                console.log(this.list);
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, 'isValuesLoaded', 'list', err);
      });
  }

  private filterSupportDataItems(value: any): any{
    if(value?.supportData?.length > 0 && value?.includeExcludeType){
      return value.supportData.filter((i: any) => {
        return !value.includeExcludeList.includes(i);
      });
    }    
    return value.supportData;
  }

  toggle(value: any, index: any) {
    this.enableSave = true;
    this.list[index].includeExcludeType = value;
    this.list[index].includeExcludeList = [];
    if (this.list[index].supportDataList?.length > 0) {
      if(this.list[index]?.dependsOnFieldId){
        const result = this.list?.filter((x: any) => x?.fieldId === this.list[index]?.dependsOnFieldId);
        if (result?.length > 0) {
          const value = this.list[index]?.supportDataList?.filter((x: any) => x?.dependsOn === result[0]?.defaultValue);
          if (value?.length > 0) {
            this.list[index].supportDataItems = value[0].supportData;
          }
        }
      }else{
        if(this.list[index]?.supportDataList[0]?.supportData){
          this.list[index].supportDataItems = this.list[index].supportDataList[0].supportData;
        }
      }
    }else{
      this.list[index].supportDataItems = this.list[index]?.supportData;
    }
    if(value && value !== IncludeExcludeValues.Exclude){
      this.includingDefaultValue(index);
    }
  }

  public includingDefaultValue(index: any): void {
    const defaultValue = this.list[index].supportDataItems.find((x: any) => x === this.list[index].defaultValue);
    if(defaultValue){
      this.enableSave = true;
      let item = [];
      for (const el of this.element?.nativeElement?.children) {
        if (el.innerText === defaultValue) {
          if (!this.list[index].includeExcludeList) {
            this.list[index].includeExcludeList = [];
          }
          this.list[index].includeExcludeList?.push(el.innerText);
          item.push(el.innerText);
        }
      }
      this.list[index].supportDataItems = this.list[index].supportDataItems.filter((i: any) => {
        return !item.includes(i);
      });
    }
  }

  selectedValue(event: any, index: any) {
    if (event) {
      if (this.list[index].includeExcludeType === IncludeExcludeValues.Exclude) {
        if (this.list[index].includeExcludeList?.length > 0) {
          const result = this.list[index].includeExcludeList.find((x: any) => x === event);
          if (result) {
            setTimeout(() => {
              this.alertService.error('Selected Default Value is part of Exclude List. Remove this from exclude list and retry.');
            }, 100);
            this.list[index].defaultValue = null;
          } else {
            this.defaultValueSelected(index, event);
          }
        } else {
          this.defaultValueSelected(index, event);
        }
      } else {
        this.defaultValueSelected(index, event);
      }
    } else {
      this.defaultValueSelected(index, event);
    }
  }

  defaultValueSelected(index: any, value: any) {
    this.enableSave = true;
    this.list[index].defaultValue = value;
    if (value && !this.list[index]?.supportDataList) {
      for (const i of this.list) {
        if (i?.supportDataList?.length > 0 && i?.dependsOnFieldId && i?.dependsOnFieldId === this.list[index].fieldId) {
          const result = i?.supportDataList?.filter((x: any) => x?.dependsOn === value);
          if (result?.length > 0) {
            i.supportDataItems = [...result[0]?.supportData];
            i.defaultValue = result[0]?.defaultValue;
            i.includeExcludeType = (result[0]?.includeExcludeType ? result[0]?.includeExcludeType : IncludeExcludeValues.Exclude);
            i.includeExcludeList = (result[0]?.includeExcludeList ? result[0]?.includeExcludeList : []);
            i.filterValue = result[0]?.dependsOn;
          } else {
            i.supportDataItems = [];
            i.includeExcludeList = [];
            i.defaultValue = null;
            i.filterValue = null;
          }
        }
      }
    }
  }

  view(field: any, event: any) {
    if (this.previousTarget === event.currentTarget) {
      this.previewed = null;
      this.previousTarget = null;
    } else {
      this.previewed = field;
      this.previousTarget = event.currentTarget;
    }
    this.disabledAddRemoveButtons();
  }

  public disabledAddRemoveButtons(): void {
    this.toggleAssignedItems = false;
    this.toggleAvailableItems = false;
  }

  public checkingButtonDisable(element: any, index: number, selectAllKey?: string): boolean {
    const parentEl = this[element]?.nativeElement?.children;
    let isItemSelected: boolean = false;
    if (parentEl && parentEl.length > 0) {
      const selectedItems = [];
      for (const item of parentEl) {
        if (item.classList.contains('active')) { selectedItems.push(item); }
      }
      if (selectedItems?.length > 0) {
        isItemSelected = true;
      }
    }
    if(!this[selectAllKey] || !isItemSelected){
      return true;
    }
    return false;
  }

  public getDefaultDropdownItems(field: any): any {
    if(field){
      if(field.includeExcludeType && field.includeExcludeType !== IncludeExcludeValues.Exclude && field.includeExcludeList?.length > 0){
        return field.includeExcludeList;
      }else{
        return field.supportDataItems;
      }
    }
  }

  public filterValueHandle(): void {
    for (const i of this.list) {
      if (i?.supportDataList?.length > 0 && i?.dependsOnFieldId) {
        const result = this.list?.filter((x: any) => x?.fieldId === i?.dependsOnFieldId);
        if (result?.length > 0) {
          i.filterValue = result[0]?.defaultValue;
        }
      }
    }
  }

  public selectedSectionName(sectionName: any): void {
    this.sectionNameDetails = sectionName;
    this.getValuesFields();
  }

  public back(): void {
    this.router.navigateByUrl(this.location.path(true));
  }

  editFieldsValues() {
    this.isValuesLoaded = false;
    for (const i of this.list) {
      if (i?.includeExcludeList?.length === 0) {
        i.includeExcludeType = null;
      }
    }
    this.filterValueHandle();
    const payload = {
      fieldList: this.list
    };
    this.configureDefaultValuesService.updateDefaultConfigurationDetail(this.applicationList.applicationId, this.screenNameDetails?.screenId, this.roleSequenceId, payload).subscribe(
      () => {
        this.isValuesLoaded = true;
        this.enableSave = false;
        this.getValuesFields();
      },
      err => {
        this.isValuesLoaded = true;
        this.enableSave = false;
        this.commonService.handleErrorResponse(err);
      });
  }
}

