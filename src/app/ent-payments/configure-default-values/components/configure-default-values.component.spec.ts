import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureDefaultValuesComponent } from './configure-default-values.component';

describe('ConfigureDefaultValuesComponent', () => {
  let component: ConfigureDefaultValuesComponent;
  let fixture: ComponentFixture<ConfigureDefaultValuesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigureDefaultValuesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureDefaultValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
