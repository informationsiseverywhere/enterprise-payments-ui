import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { configureDefaultValuesRoutes } from './configure-default-values-routes';

@NgModule({
  imports: [
    RouterModule.forChild(configureDefaultValuesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ConfigureDefaultValuesRoutingModule {
}
