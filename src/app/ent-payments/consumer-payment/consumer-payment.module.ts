import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { ConsumerPaymentRoutingModule } from './consumer-payment-routing.module';

import { ConsumerPaymentComponent } from './components/consumer-payment.component';

@NgModule({
  declarations: [
    ConsumerPaymentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    ConsumerPaymentRoutingModule
  ]
})
export class ConsumerPaymentModule { }
