import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { consumerPaymentRoutes } from './consumer-payment-routes';

@NgModule({
  imports: [
    RouterModule.forChild(consumerPaymentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ConsumerPaymentRoutingModule {
}
