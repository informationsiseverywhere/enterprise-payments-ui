import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { paymentsRoutes } from './ent-payments-routes';

@NgModule({
  imports: [
    RouterModule.forChild(paymentsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EntPaymentsRoutingModule {
}
