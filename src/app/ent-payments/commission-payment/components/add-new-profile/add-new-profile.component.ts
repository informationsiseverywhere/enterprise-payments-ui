import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';

import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';
import { CommonService } from 'src/app/shared/services/common.service';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-add-new-profile',
  templateUrl: './add-new-profile.component.html',
  styleUrls: ['./add-new-profile.component.scss']
})
export class AddNewProfileComponent implements OnInit {
  makeBankAccountForm: FormGroup;
  accountTypeDropdown: any = [];
  supportData: any;
  @Output() newProfileEvent: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    private fb: FormBuilder,
    public commonService: CommonService,
    private loadingService: LoadingService,
    private paymentService: PaymentService
  ) { }

  ngOnInit(): void {
    this.makeBankAccountForm = this.fb.group({
      fullName: ['', [Validators.maxLength(22)]],
      routingNumber: ['', [Validators.required, ValidationService.numericValidator, Validators.maxLength(9), Validators.minLength(9)]],
      accountNumber: ['', [Validators.required, ValidationService.numericValidator, Validators.maxLength(17)]],
      accountType: ['', Validators.required]
    });
    this.getPaymentsSupportData();
  }

  private getPaymentsSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentsSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.accountType);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (propertyName === PropertyName.accountType && sd.defaultValue) {
          this.makeBankAccountForm.controls['accountType'].setValue(sd.defaultValue);
        }
      }
    }
  }

  get form() { return this.makeBankAccountForm.controls; }

  addBankAccount() {
    const data = this.makeBankAccountForm.getRawValue();
    this.newProfileEvent.emit(data);
  }
  reset() {
    this.makeBankAccountForm.reset();
  }
}
