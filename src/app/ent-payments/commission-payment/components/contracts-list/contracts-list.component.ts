import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-contracts-list',
  templateUrl: './contracts-list.component.html',
  styleUrls: ['./contracts-list.component.scss']
})
export class ContractsListComponent implements OnInit {
  @Input() accountDetails: any;
  apiUrlContract: string;
  contractSelected: any;
  @Output() selectedProfileEvent: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    public commonService: CommonService,
    private appConfig: AppConfiguration,
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if (this.accountDetails.accountNumber) {
      this.apiUrlContract = `${this.appConfig.apiUrls.contractsCommissions}v1/commissions/${this.accountDetails.accountNumber}/contracts`;
    }
  }

  public selectedContract(data?: any): void {
    if (data) {
      this.contractSelected = data;
    }
  }

  public selectedProfile(data: any) {
    this.selectedProfileEvent.emit(data);
  }
}
