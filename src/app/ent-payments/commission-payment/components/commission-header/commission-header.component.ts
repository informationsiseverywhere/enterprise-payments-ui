import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IconName } from 'src/app/shared/enum/common.enum';

import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-commission-header',
  templateUrl: './commission-header.component.html',
  styleUrls: ['./commission-header.component.scss']
})
export class CommissionHeaderComponent implements OnInit {
  @Input() isAccountDetailLoaded: any;
  @Input() accountDetails: any;
  imageUrl: any;
  @Input() type: any = 'N';
  @Output() paymentType: EventEmitter<any> = new EventEmitter<any>();
  @Input() isConfirm: any;
  constructor(
    public appConfig: AppConfiguration,
    public commonService: CommonService
  ) {
    this.imageUrl = IconName.userProfilePath;
 }

  ngOnInit(): void {
  }

  selectType(event: any) {
    this.type = event;
    this.paymentType.emit(event);
  }

  public removeSpaces(status?: any): any {
    if (status) {
      return this.commonService.removeSpaces(status);
    }
    return 'empty';
  }
}
