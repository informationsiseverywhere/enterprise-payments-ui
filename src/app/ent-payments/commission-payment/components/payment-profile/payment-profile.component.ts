import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';
import { PaginationComponent } from 'src/app/libs/pagination/pagination.component';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-payment-profile',
  templateUrl: './payment-profile.component.html',
  styleUrls: ['./payment-profile.component.scss']
})
export class PaymentProfileComponent implements OnInit {
  @Input() contract: any;
  public isPaymentProfilesLoaded = false;
  paymentProfiles: any;
  public size: number;
  public isBankIndex: any;
  @ViewChild(PaginationComponent, { static: false }) paginator: PaginationComponent;
  @Output() selectedProfile: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    private paymentService: PaymentService,
    public commonService: CommonService
  ) {
    this.paymentProfiles = new Array(4);
   }

  ngOnInit(): void { }

  ngOnChanges(changes: any) {
    if (this.contract) {
      for (const sd of this.contract.links) {
        if (sd.rel === 'paymentProfile') {
          this.getPaymentProfiles(sd.href);
          continue;
        }
      }
    }
  }

  getPaymentProfiles(url: any) {
    this.isPaymentProfilesLoaded = false;
    this.paymentService.getPaymentProfiles(url).subscribe(
      data => {
        this.refreshData(data);
        this.isPaymentProfilesLoaded = true;
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, 'isPaymentProfilesLoaded', 'paymentProfiles', err);
      });
  }

  public refreshData(res: any): void {
    this.paymentProfiles = this.commonService.tablePaginationHandler(this.paginator, res);
  }

  public selectBank(data?: any, index?: any): void {
    this.isBankIndex = index;
    this.selectedProfile.emit(data.bankAccount);
  }
}
