import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaymentReceivedComponent } from 'src/app/shared/components/payment-received/payment-received.component';
import { PropertyName } from 'src/app/shared/enum/common.enum';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';

import { DateService } from 'src/app/shared/services/date.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentService } from '../../batch-payments/services/payments.service';
import { PaymentModel } from '../../policy-pay/models/payment.model';

@Component({
  selector: 'app-commission-payment',
  templateUrl: './commission-payment.component.html',
  styleUrls: ['./commission-payment.component.scss']
})
export class CommissionPaymentComponent implements OnInit {
  accountId: any;
  accountBalanceDetail: any;
  isSelectOptions = 'active';
  isConfirm = 'inprogress';
  public isAccountDetailLoaded = false;
  systemDate: any;
  returnAppId: any;
  accountDetails: any;
  selectedType = 'N';
  public eftPlanForm: FormGroup;
  supportData: any;
  cardEftPlans: any;
  bankEftPlans: any;
  payment: PaymentModel = new PaymentModel();
  headerName = 'Enrollment Options';
  @ViewChild('paymentReceivedModel', { static: false }) paymentReceivedModel: ModalComponent;
  enableNext = true;
  constructor(
    private dateService: DateService,
    private route: ActivatedRoute,
    private paymentService: PaymentService,
    public commonService: CommonService,
    private loadingService: LoadingService,
    public appConfig: AppConfiguration,
    private fb: FormBuilder,
    public securityEngineService: SecurityEngineService,
  ) { }

  ngOnInit(): void {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.route.queryParams.subscribe(
      queryParams => {
        this.returnAppId = queryParams.returnAppId;
        this.accountId = queryParams.accountId;
        this.checkData();
      });
    this.eftPlanForm = this.fb.group({
      collectionType: ['', Validators.required],
    });
  }

  checkData() {
    this.getAccountDetails(this.accountId);
  }

  getAccountDetails(accountId?: any) {
    this.isAccountDetailLoaded = false;
    this.paymentService.getAccountDetails(accountId, null).subscribe(
      (data: any) => {
        this.loadingService.toggleLoadingIndicator(false);
        this.accountDetails = data ? data.content[0] : null;
        this.paymentService.accountDetails = this.accountDetails;
        this.isAccountDetailLoaded = true;
      },
      (err: any) => {
        this.commonService.handleErrorAPIResponse(this, 'isAccountDetailLoaded', 'accountDetails', err);
      });
  }

  paymentType(event: any) {
    this.selectedType = event;
    this.enableNext = true;
  }
  get collectionType() { return this.eftPlanForm.get('collectionType'); }
  private getPaymentsSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getEftPlanSupportData(this.accountDetails?.accountId).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.bankEftPlan);
          this.getDropdownItems(PropertyName.cardEftPlan);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 's'] = sd.propertyValues;
      }
    }
  }
  checkSecurityOptions() { }
  goBack() {
    this.isSelectOptions = 'active';
    this.isConfirm = 'inprogress';
    this.selectedType = 'N';
    this.headerName = 'Enrollment Options';
    this.payment = null;
    this.enableNext = true;
  }

  setupAutoPayment() {
    const requestBody: any = {
      identifier: this.accountDetails.accountNumber,
      isRecurring: true,
      identifierType: 'Consumer Direct',
      bankAccountNumber: this.payment.accountNumber,
      paymentMethod: 'Bank Account',
      collectionType: this.collectionType.value,
      accountType: this.payment.accountType,
      paymentType: 'Website Check 1x ACH',
      routingNumber: this.payment.routingNumber,
      bankHolderName: this.payment?.fullName
    };
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.setupAutoPaymentOff(requestBody).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentCreatedResponse: any = data;
        let paymentDetailUrl = '';
        if (paymentCreatedResponse.links && paymentCreatedResponse.links.length > 0) {
          paymentDetailUrl = paymentCreatedResponse.links[0].href;
          requestBody.paymentModelType = 'subscriptions';
          this.getPaymentDetail(paymentDetailUrl, requestBody);
        }
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  getPaymentDetail(url: string, requestBody?: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentDetailByUrl(url).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        const paymentDetailInfo: any = data;
        requestBody.paymentSequenceNumber = paymentDetailInfo.paymentSequenceNumber;
        requestBody.batchId = paymentDetailInfo.batchId;
        requestBody.postedDate = paymentDetailInfo.postedDate;
        requestBody.userId = paymentDetailInfo.userId;
        this.reset(true, requestBody);
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        const body = {
          paymentModelType: 'subscriptionsFail'
        };
        this.reset(true, body);
      });
  }

  reset(status?: any, requestBody?: any) {
    if (status) {
      this.openPaymentReceivedModel(requestBody);
    }
  }

  private openPaymentReceivedModel(data?: any): void {
    if (data) {
      if (data?.paymentModelType === 'subscriptions') {
        this.paymentReceivedModel.modalTitle = 'Auto Payment Setup Completed';
        this.paymentReceivedModel.icon = 'assets/images/icons/success.svg';
      } else if (data?.paymentModelType === 'subscriptionsFail') {
        this.paymentReceivedModel.modalTitle = 'Auto Payment Setup failed';
        this.paymentReceivedModel.subModalTitle = 'We can`t setup your auto payment . Please contact Customer Service for further assistance.';
        this.paymentReceivedModel.icon = 'assets/images/icons/fail.svg';
      }
    }
    data.paymentDoneType = 'Commission';
    this.paymentService.objectToModal = data;
    this.paymentReceivedModel.modalFooter = false;
    this.paymentReceivedModel.open(PaymentReceivedComponent);
  }

  newProfileEvent(event: any) {
    this.payment = event;
    this.next();
  }

  selectedProfileEvent(event: any) {
    this.payment = event;
    this.enableNext = false;
  }

  next() {
    this.headerName = 'Review and Confirm';
    this.getPaymentsSupportData();
    this.isSelectOptions = 'complete';
    this.isConfirm = 'active';
  }
}
