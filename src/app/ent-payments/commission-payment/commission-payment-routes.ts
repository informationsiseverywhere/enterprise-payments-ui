import { Routes } from '@angular/router';

import { NavigationComponent } from '../navigation/navigation.component';
import { CommissionPaymentComponent } from './components/commission-payment.component';

export const commissionPaymentRoutes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
      {
        path: '',
        component: CommissionPaymentComponent
      }
    ]
  }
];

