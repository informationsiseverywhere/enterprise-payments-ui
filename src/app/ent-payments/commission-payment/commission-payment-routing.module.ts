import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { commissionPaymentRoutes } from './commission-payment-routes';

@NgModule({
  imports: [
    RouterModule.forChild(commissionPaymentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CommissionPaymentRoutingModule {
}
