import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibraryModule } from 'src/app/libs/libs.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommissionPaymentRoutingModule } from './commission-payment-routing.module';

import { CommissionPaymentComponent } from './components/commission-payment.component';
import { CommissionHeaderComponent } from './components/commission-header/commission-header.component';
import { ContractsListComponent } from './components/contracts-list/contracts-list.component';
import { PaymentProfileComponent } from './components/payment-profile/payment-profile.component';
import { AddNewProfileComponent } from './components/add-new-profile/add-new-profile.component';

@NgModule({
  declarations: [
    CommissionPaymentComponent,
    CommissionHeaderComponent,
    ContractsListComponent,
    PaymentProfileComponent,
    AddNewProfileComponent],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    CommissionPaymentRoutingModule
  ]
})
export class CommissionPaymentModule { }
