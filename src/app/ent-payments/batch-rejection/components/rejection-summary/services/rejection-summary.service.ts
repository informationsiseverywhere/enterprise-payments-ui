import { Injectable } from '@angular/core';

import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Injectable({
  providedIn: 'root'
})
export class RejectionSummaryService {
  public paramsToModal: any;
  public modalAction: any;
  apiUrls: any;

  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    public commonService: CommonService) { }

  getBatchDetails(batchId?: any, entryDate?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '?entryDate=' + entryDate;
    return this.restService.getByUrl(url);
  }

  public getBatchDetailReportExcel(batchId?: any, entryDate?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '/_report?entryDate=' + entryDate;
    return this.restService.getByUrlWithoutParsingToJson(url);
  }

  getRejectionsList(batchId?: any, entryDate?: any, size?: any, page?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '/payments?entryDate=' + entryDate;
    url = this.commonService.buildSearchQueryUrl(url, page, size);
    return this.restService.getByUrl(url);
  }

  getBatchEntrySupportData(batchId?: any, entryDate?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '/payments?entryDate=' + entryDate;
    return this.restService.optionsByUrl(url);
  }

  addBatchEntry(batchId?: any, entryDate?: any, data?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '/payments?entryDate=' + entryDate;
    return this.restService.postByUrl(url, data);
  }

  viewBatchEntry(batchId?: any, entryDate?: any, paymentSequenceNumber?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '/payments/'
      + paymentSequenceNumber + '?entryDate=' + entryDate;
    return this.restService.getByUrl(url);
  }

  updateBatchEntry(batchId?: any, entryDate?: any, paymentSequenceNumber?: any, body?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '/payments/'
      + paymentSequenceNumber + '?entryDate=' + entryDate;
    return this.restService.patchByUrl(url, body);
  }

  deleteBatchEntry(batchId?: any, entryDate?: any, paymentSequenceNumber?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '/payments/' + paymentSequenceNumber + '?entryDate=' + entryDate;
    return this.restService.deleteByUrl(url);
  }
}
