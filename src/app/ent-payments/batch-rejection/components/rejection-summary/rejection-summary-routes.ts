import { Routes } from '@angular/router';

import { CommonComponent } from 'src/app/shared/components/common/common.component';
import { RejectionSummaryComponent } from './components/rejection-summary.component';


export const rejectionSummaryRoutes: Routes = [
  {
    path: '',
    component: CommonComponent,
    children: [
      {
        path: '',
        component: RejectionSummaryComponent
      }
    ]
  }
];

