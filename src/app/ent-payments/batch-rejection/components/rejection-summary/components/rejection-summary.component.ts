import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DateService } from 'src/app/shared/services/date.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { RejectionSummaryService } from '../services/rejection-summary.service';
import { BatchEntryResultsComponent } from './batch-entry-results/batch-entry-results.component';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';

@Component({
  selector: 'app-rejection-summary',
  templateUrl: './rejection-summary.component.html'
})
export class RejectionSummaryComponent implements OnInit {
  batchId: any;
  entryDate: any;
  page: any;
  size: any;
  systemDate: any;
  batchPaymentList: any;
  public isBatchDetailLoaded = false;
  batchDetail: any;
  @ViewChild('resultList', { static: false }) resultList: BatchEntryResultsComponent;
  constructor(
    private route: ActivatedRoute,
    private dateService: DateService,
    private rejectionSummaryService: RejectionSummaryService,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    private commonService: CommonService) {
      this.batchPaymentList = new Array(5);
    }

  ngOnInit() {
    this.route.parent.params.subscribe(
      params => {
        this.batchId = params.batchId;
        this.entryDate = params.entryDate;
      });

    this.route.queryParams.subscribe(
      queryParams => {
        this.page = queryParams.page;
        this.size = queryParams.size;
      });

    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });

    if (this.batchId && this.entryDate) {
      this.getRejectionSummaryDetail(this.batchId, this.entryDate);
    }
  }

  getRejectionSummaryDetail(batchId: string, entryDate: any) {
    this.isBatchDetailLoaded = false;
    this.rejectionSummaryService.getBatchDetails(batchId, entryDate).subscribe(
      data => {
        this.isBatchDetailLoaded = true;
        this.batchDetail = data;
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, 'isBatchDetailLoaded', 'batchDetail', err);
      }
    );
  }

  isStatementsEmpty(event: any) {
    this.batchPaymentList = event;
  }
  addBatchEntry() {
    this.resultList.openBatchEntryAddModal();
  }
  reloadSummary(event: any) {
    if (event) {
      this.resultList.getBatchPayments();
      if (this.batchId && this.entryDate) {
        this.getRejectionSummaryDetail(this.batchId, this.entryDate);
      }
    }
  }

  public downloadReportExcel(): void {
    this.rejectionSummaryService.getBatchDetailReportExcel(this.batchId, this.entryDate).subscribe(
      (data: any) => {
        const myBlob = new Blob([data], {
          type: 'application/vnd.ms-excel'
        });
        if (navigator.msSaveBlob) { // IE 10+
          navigator.msSaveBlob(myBlob, 'batchDetailReport ' + this.batchId + '.xlsx');
        } else {
          const url = window.URL.createObjectURL(data);
          const link = document.createElement('a');
          document.body.appendChild(link);
          link.download = 'batchDetailReport ' + this.batchId + '.xlsx';
          link.href = url;
          link.click();
        }
      },
      () => { });
  }
  
  public openSecurity(event?: any, configType?: any) : void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, configType);
  }
}
