import { Component, OnInit } from '@angular/core';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-batch-entry-header',
  templateUrl: './batch-entry-header.component.html'
})
export class BatchEntryHeaderComponent implements OnInit {

  constructor(public securityEngineService: SecurityEngineService) { }

  ngOnInit(): void {
  }

  public openSecurity(event?: any) : void {
    // this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, 'tableSection');
  }
}
