import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PaginationComponent } from 'src/app/libs/pagination/pagination.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { RejectionSummaryService } from '../../services/rejection-summary.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { AddBatchEntryComponent } from '../add-batch-entry/add-batch-entry.component';
import { ViewBatchEntryComponent } from '../view-batch-entry/view-batch-entry.component';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-batch-entry-results',
  templateUrl: './batch-entry-results.component.html',
  styleUrls: ['./batch-entry-results.component.scss']
})
export class BatchEntryResultsComponent implements OnInit {
  @Input() batchDetail: any;
  @Input() page: any;
  @Input() size: any;
  @Input() entryDate: any;
  @Input() batchId: any;
  batchEntryList: any[] = [];
  @Output() isStatementsEmpty: EventEmitter<any> = new EventEmitter<any>();
  @Output() reloadSummary: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild(PaginationComponent) paginator: PaginationComponent;
  @ViewChild('addBatchEntryModal', { static: false }) addBatchEntryModal: ModalComponent;
  @ViewChild('deleteBatchEntryModal', { static: false }) deleteBatchEntryModal: ModalComponent;
  @ViewChild('viewBatchEntryModal', { static: false }) viewBatchEntryModal: ModalComponent;
  public isBatchEntryListLoaded = false;

  constructor(
    private rejectionSummaryService: RejectionSummaryService,
    private route: ActivatedRoute,
    public securityEngineService: SecurityEngineService,
    private commonService: CommonService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.page = queryParams.page;
        this.size = queryParams.size;
        this.getBatchPayments();
      }
    );
  }

  getBatchPayments() {
    this.isBatchEntryListLoaded = false;
    this.batchEntryList && (this.batchEntryList.length = this.size < 5 ? this.size : 5);
    this.rejectionSummaryService.getRejectionsList(this.batchId, this.entryDate, this.size, this.page).subscribe(
      data => {
        this.isBatchEntryListLoaded = true;
        this.refreshBatchPayments(data);
      },
      err => {
        this.isStatementsEmpty.emit(null);
        this.commonService.handleErrorAPIResponse(this, 'isBatchEntryListLoaded', 'batchEntryList', err);
      });
  }

  private refreshBatchPayments(data?: any): void {
    this.batchEntryList = this.commonService.tablePaginationHandler(this.paginator, data);
    this.isStatementsEmpty.emit(this.batchEntryList);
  }

  reload(event?: any) {
    if (event) { this.reloadSummary.emit(true); }
  }

  batchPaymentActions(action: string, batchPayment: any) {
    switch (action) {
      case 'Edit':
        this.openBatchEntryEditModal(batchPayment);
        break;
      case 'View':
        this.openBatchEntryViewModal(batchPayment);
        break;
      case 'Delete':
        this.openBatchEntryDeleteModal(batchPayment);
        break;
    }
  }


  openBatchEntryEditModal(batchPayment?: any) {
    this.addBatchEntryModal.modalTitle = 'Edit Batch Entry';
    this.addBatchEntryModal.modalFooter = false;
    this.rejectionSummaryService.paramsToModal = batchPayment;
    this.rejectionSummaryService.modalAction = 'Edit';
    this.addBatchEntryModal.icon = 'assets/images/model-icons/account-payment.svg';
    this.addBatchEntryModal.open(AddBatchEntryComponent);
  }

  openBatchEntryViewModal(batchPayment?: any) {
    this.viewBatchEntryModal.modalTitle = 'View Batch Entry';
    this.viewBatchEntryModal.modalFooter = false;
    this.rejectionSummaryService.paramsToModal = batchPayment;
    this.rejectionSummaryService.modalAction = 'View';
    this.viewBatchEntryModal.icon = 'assets/images/model-icons/detail-history.svg';
    this.viewBatchEntryModal.open(ViewBatchEntryComponent);
  }

  openBatchEntryAddModal() {
    this.addBatchEntryModal.modalTitle = 'Add Batch Entry';
    this.addBatchEntryModal.modalFooter = false;
    const details = {
      batchId: this.batchId,
      entryDate: this.entryDate,
    };
    this.rejectionSummaryService.paramsToModal = details;
    this.rejectionSummaryService.modalAction = 'Add';
    this.addBatchEntryModal.icon = 'assets/images/model-icons/account-payment.svg';
    this.addBatchEntryModal.open(AddBatchEntryComponent);
  }


  openBatchEntryDeleteModal(batchPayment?: any) {
    this.deleteBatchEntryModal.modalTitle = 'Delete Batch Entry';
    this.deleteBatchEntryModal.modalFooter = true;
    this.deleteBatchEntryModal.okButtonText = 'Yes';
    this.deleteBatchEntryModal.cancelButtonText = 'No';
    this.rejectionSummaryService.paramsToModal = batchPayment;
    this.deleteBatchEntryModal.icon = 'assets/images/model-icons/delete.svg';
    this.deleteBatchEntryModal.message = 'Are you sure you want to delete the Batch Entry immediately?';
    this.deleteBatchEntryModal.open();
  }


  deleteBatchEntry(event?: any) {
    if (event) {
      if (this.rejectionSummaryService.paramsToModal) {
        this.rejectionSummaryService.deleteBatchEntry(this.batchId, this.entryDate,
          this.rejectionSummaryService.paramsToModal.paymentSequenceNumber).subscribe(
            () => {
              this.rejectionSummaryService.paramsToModal = null;
              this.reload(true);
            },
            err => {
              this.commonService.handleErrorResponse(err);
            });
      }
    }
  }

  removeSpaces(status?: string) {
    return this.commonService.removeSpacesAndFWDash(status);
  }
}

