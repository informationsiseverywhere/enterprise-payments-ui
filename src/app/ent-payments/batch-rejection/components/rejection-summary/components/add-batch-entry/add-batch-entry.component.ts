import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DateService } from 'src/app/shared/services/date.service';

import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { BatchEntryModel } from '../../models/rejection-summary.model';
import { RejectionSummaryService } from '../../services/rejection-summary.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-add-batch-entry',
  templateUrl: './add-batch-entry.component.html'
})
export class AddBatchEntryComponent implements OnInit {
  systemDate: any;
  addBatchEntryForm: FormGroup;
  batchEntry: BatchEntryModel = new BatchEntryModel();
  RejectionReasonDropdown: any;
  supportData: any;
  traceInput = false;
  referenceInput = false;
  referenceNumberRequired = true;
  traceNumberRequired = true;
  constructor(
    private fb: FormBuilder,
    private modal: ModalComponent,
    private loadingService: LoadingService,
    private dateService: DateService,
    public commonService: CommonService,
    private rejectionSummaryService: RejectionSummaryService
  ) { }

  ngOnInit(): void {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.addBatchEntryForm = this.fb.group({
      transactionNumber: ['', Validators.required],
      referenceNumber: ['', Validators.required],
      settlementDate: ['', Validators.compose([Validators.required, ValidationService.dateValidator])],
      rejectionReason: ['', Validators.required],
    });
    if (this.rejectionSummaryService.paramsToModal) {
      this.batchEntry = Object.assign({}, this.rejectionSummaryService.paramsToModal);
      this.getBatchPaymentsSupportData();
    }
  }

  getBatchPaymentsSupportData() {
    this.loadingService.toggleLoadingIndicator(true);
    this.rejectionSummaryService.getBatchEntrySupportData(this.batchEntry.batchId, this.batchEntry.entryDate).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.RejectionReason);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  getDropdownItems(propertyName: string) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (!this.batchEntry.rejectionReason && this.rejectionSummaryService.modalAction === 'Add' && propertyName === PropertyName.RejectionReason && sd.defaultValue) {
          this.addBatchEntryForm.controls['rejectionReason'].setValue(sd.defaultValue);
        }
      }
    }
  }

  inputDisable($event, numberType) {
    if ($event === '') {
      this.addBatchEntryForm.get('transactionNumber').setValidators(Validators.required);
      this.addBatchEntryForm.get('referenceNumber').setValidators(Validators.required);
      this.addBatchEntryForm.get('transactionNumber').reset();
      this.addBatchEntryForm.get('referenceNumber').reset();
      this.referenceNumberRequired = true;
      this.traceNumberRequired= true;
      this.traceInput = false;
      this.referenceInput = false;
    } else if (numberType === 'transactionNumber') {
      this.addBatchEntryForm.get('referenceNumber').setValidators(null);
      this.addBatchEntryForm.get('referenceNumber').patchValue('');
      this.referenceNumberRequired = false;
      this.traceNumberRequired= true;
      this.traceInput = false;
      this.referenceInput = true;
    } else {
      this.addBatchEntryForm.get('transactionNumber').setValidators(null);
      this.addBatchEntryForm.get('transactionNumber').patchValue('');
      this.referenceNumberRequired = true;
      this.traceNumberRequired= false;
      this.traceInput = true;
      this.referenceInput = false;
    }
  }

  submit() {
    const body = {
      transactionNumber: this.batchEntry.transactionNumber,
      referenceNumber: this.batchEntry.referenceNumber,
      rejectionReason: this.batchEntry.rejectionReason,
      settlementDate: this.batchEntry.settlementDate
    };
    if (body.transactionNumber === '') {
      delete body.transactionNumber;
    }
    if (body.referenceNumber === '') {
      delete body.referenceNumber;
    }

    if (this.rejectionSummaryService.modalAction === 'Add') {
      this.addBatchEntry(body);
    }
    if (this.rejectionSummaryService.modalAction === 'Edit') {
      this.editBatchEntry(body);
    }
  }

  addBatchEntry(body: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.rejectionSummaryService.addBatchEntry(this.batchEntry.batchId, this.batchEntry.entryDate, body).subscribe(
      () => {
        this.rejectionSummaryService.paramsToModal = null;
        this.modal.close(true);
        this.loadingService.toggleLoadingIndicator(false);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  editBatchEntry(body: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.rejectionSummaryService.updateBatchEntry(this.batchEntry.batchId, this.batchEntry.entryDate, this.batchEntry.paymentSequenceNumber, body).subscribe(
      () => {
        this.rejectionSummaryService.paramsToModal = null;
        this.modal.close(true);
        this.loadingService.toggleLoadingIndicator(false);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }
}
