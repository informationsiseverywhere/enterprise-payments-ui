import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { BatchRejectionsService } from 'src/app/ent-payments/batch-rejection/services/batch-rejections.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-batch-details',
  templateUrl: './batch-details.component.html',
  styleUrls: ['./batch-details.component.scss']
})
export class BatchDetailsComponent implements OnInit {
  @Input() batchDetail: any;
  @Input() systemDate: string;
  @Input() isBatchDetailLoaded: boolean;
  @ViewChild('batchRejectionAcceptModal', { static: false }) batchRejectionAcceptModal: ModalComponent;
  @Output() reloadSummary: EventEmitter<any> = new EventEmitter<any>();
  @Output() downloadReportExcel: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private batchRejectionsService: BatchRejectionsService,
    private loadingService: LoadingService,
    private router: Router,
    public securityEngineService: SecurityEngineService,
    private commonService: CommonService) { }

  ngOnInit() { }

  public getContentToExport(): void {
    this.downloadReportExcel.emit('true');
  }

  public saveBatchTotalModal(event?: any): void {
    if (event) { this.reloadSummary.emit(true); }
  }

  openAcceptDepositModal(batch: any) {
    this.batchRejectionAcceptModal.modalTitle = 'Batch Accept';
    this.batchRejectionAcceptModal.modalFooter = true;
    this.batchRejectionAcceptModal.okButtonText = 'Yes';
    this.batchRejectionAcceptModal.cancelButtonText = 'No';
    this.batchRejectionsService.paramsToModal = batch;
    this.batchRejectionAcceptModal.icon = 'assets/images/model-icons/batch-accept.svg';
    this.batchRejectionAcceptModal.message = 'Are you sure you want to accept the Batch immediately?';
    this.batchRejectionAcceptModal.open();
  }

  batchAcceptAction(event: any) {
    if (event) {
      const request = {
        isAccept: true
      };
      this.loadingService.toggleLoadingIndicator(true);
      this.batchRejectionsService.updateBatch(this.batchDetail.batchId, this.batchDetail.entryDate, request).subscribe(
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.batchRejectionsService.paramsToModal = null;
          this.router.navigate(['/ent-payments', 'batch-rejection']);
        },
        err => {
          this.commonService.handleErrorResponse(err);
        });
    }
  }
}
