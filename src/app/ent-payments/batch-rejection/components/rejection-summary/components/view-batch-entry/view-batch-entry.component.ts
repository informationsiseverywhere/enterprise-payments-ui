import { Component, OnInit } from '@angular/core';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { RejectionSummaryService } from '../../services/rejection-summary.service';
import { BatchEntryModel } from '../../models/rejection-summary.model';

@Component({
  selector: 'app-view-batch-entry',
  templateUrl: './view-batch-entry.component.html',
  styleUrls: ['./view-batch-entry.component.scss']
})
export class ViewBatchEntryComponent implements OnInit {
  batchEntry: BatchEntryModel = new BatchEntryModel();
  paymentDetails: any;
  isLoading = false;

  constructor(
    private loadingService: LoadingService,
    public commonService: CommonService,
    private rejectionSummaryService: RejectionSummaryService
  ) { }

  ngOnInit(): void {
    if (this.rejectionSummaryService.paramsToModal) {
      this.batchEntry = Object.assign({}, this.rejectionSummaryService.paramsToModal);
      this.viewBatchEntry();
    }
  }

  public removeSpaces(status: string): any {
    return this.commonService.removeSpacesAndFWDash(status);
  }
  viewBatchEntry() {
    this.isLoading = false;
    this.loadingService.toggleLoadingIndicator(true);
    this.rejectionSummaryService.viewBatchEntry(this.batchEntry.batchId, this.batchEntry.entryDate, this.batchEntry.paymentSequenceNumber).subscribe(
      (data) => {
        this.isLoading = true;
        this.paymentDetails = data ? data : null;
        this.loadingService.toggleLoadingIndicator(false);
      },
      err => {
        this.isLoading = true;
        this.commonService.handleErrorResponse(err);
      });
  }
}
