import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { RejectionSummaryRoutingModule } from './rejection-summary-routing.module';

import { RejectionSummaryComponent } from './components/rejection-summary.component';
import { BatchDetailsComponent } from './components/batch-details/batch-details.component';
import { BatchEntryHeaderComponent } from './components/batch-entry-header/batch-entry-header.component';
import { BatchEntryResultsComponent } from './components/batch-entry-results/batch-entry-results.component';
import { AddBatchEntryComponent } from './components/add-batch-entry/add-batch-entry.component';
import { ViewBatchEntryComponent } from './components/view-batch-entry/view-batch-entry.component';

@NgModule({
  declarations: [
    RejectionSummaryComponent,
    BatchDetailsComponent,
    BatchEntryHeaderComponent,
    BatchEntryResultsComponent,
    AddBatchEntryComponent,
    ViewBatchEntryComponent
  ],
  entryComponents: [
    AddBatchEntryComponent,
    ViewBatchEntryComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    RejectionSummaryRoutingModule
  ]
})
export class RejectionSummaryModule { }
