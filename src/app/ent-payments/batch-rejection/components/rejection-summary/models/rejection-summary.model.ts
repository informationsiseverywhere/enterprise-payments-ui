import { Link } from 'src/app/shared/models/link.model';

export class BatchEntryModel {
  batchId: string;
  entryDate: string;
  rejectionReason: string;
  settlementDate: string;
  payorName: string;
  settlementAmount: string;
  accumulatedAmount: string;
  numberOfTransactions: any;
  rejectionStatus: any;
  transactionNumber: any;
  referenceNumber: any;
  paymentSequenceNumber: any;
  links: Array<Link>;

  constructor(values: any = {}) {
    Object.assign(this, values);
  }
}
