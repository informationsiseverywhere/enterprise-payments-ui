import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { rejectionSummaryRoutes } from './rejection-summary-routes';

@NgModule({
  imports: [
    RouterModule.forChild(rejectionSummaryRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class RejectionSummaryRoutingModule {
}
