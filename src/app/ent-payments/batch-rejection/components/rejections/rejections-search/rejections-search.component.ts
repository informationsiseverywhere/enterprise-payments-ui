import { Component, OnInit, OnChanges, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { RejectionsResultsComponent } from '../rejections-results/rejections-results.component';
import { AdvanceSearchTags } from 'src/app/ent-payments/payments-search/models/advance-search-enums.model';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { BatchRejectionsService } from '../../../services/batch-rejections.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';

@Component({
  selector: 'app-rejections-search',
  templateUrl: './rejections-search.component.html',
  styleUrls: ['./rejections-search.component.scss']
})
export class RejectionsSearchComponent implements OnInit, OnChanges {
  @Input() filters: any;
  @Input() resultList: RejectionsResultsComponent;
  @Input() systemDate: string;
  public filterTagsList: Array<{ tag: string, name: string }> = [];
  private advanceSearchTags: AdvanceSearchTags = new AdvanceSearchTags();
  public startDate: any;
  public endDate: any;
  public fromAmount: any;
  public toAmount: any;
  public page: any;
  public size: any;
  private returnAppId: any;
  private navParam: any;
  public batchPaymentSearchForm: FormGroup;
  public isGreater = false;
  public isGreaterAmount = false;

  @ViewChild('openPayorModal') openPayorModal: ModalComponent;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private batchRejectionsService: BatchRejectionsService,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    public commonService: CommonService) { }

  ngOnInit() {
    this.batchPaymentSearchForm = this.fb.group({
      startDate: ['', ValidationService.dateValidator],
      endDate: ['', ValidationService.dateValidator],
      toAmount: [''],
      fromAmount: ['']
    });
    this.route.queryParams.subscribe(
      queryParams => {
        this.startDate = queryParams.since;
        this.endDate = queryParams.until;
        this.fromAmount = queryParams.fromAmount;
        this.toAmount = queryParams.toAmount;
        this.page = queryParams.page;
        this.size = queryParams.size;
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
      }
    );
    const filterParameters = {
      startDate: this.startDate,
      endDate: this.endDate,
      fromAmount: this.fromAmount,
      toAmount: this.toAmount,
      page: this.page,
      size: this.size,
      returnAppId: this.returnAppId,
      navParam: this.navParam
    };
    this.dateFilterExecution(filterParameters);
  }

  ngOnChanges() {
    this.filters && this.setCheckboxesValue();
  }

  private setCheckboxesValue(): void {
    if (this.filters instanceof Array) {
      for (const filter of this.filters) {
        this[filter + 'Param'] = true;
      }
    } else {
      this[this.filters + 'Param'] = true;
    }
  }

  public performAddBatch(): void {
    this.resultList.addBatchPaymentsDetails();
  }

  public dateFilterExecution(filterParameters: any): void {
    if (filterParameters) {
      this.removeTagsItem('startDate');
      this.removeTagsItem('endDate');
      this.removeTagsItem('fromAmount');
      this.removeTagsItem('toAmount');
      if (filterParameters.startDate) {
        this.filterTagsList.push({
          tag: this.advanceSearchTags.startDate + this.commonService.convertDateFormat(filterParameters.startDate), name: 'startDate'
        });
      }
      if (filterParameters.endDate) {
        this.filterTagsList.push({
          tag: this.advanceSearchTags.endDate + this.commonService.convertDateFormat(filterParameters.endDate), name: 'endDate'
        });
      }
      if (filterParameters.fromAmount) {
        this.filterTagsList.push({ tag: this.advanceSearchTags.fromAmount + filterParameters.fromAmount, name: 'fromAmount' });
      }
      if (filterParameters.toAmount) {
        this.filterTagsList.push({ tag: this.advanceSearchTags.toAmount + filterParameters.toAmount, name: 'toAmount' });
      }
      this.startDate = filterParameters.startDate;
      this.endDate = filterParameters.endDate;
      this.fromAmount = filterParameters.fromAmount;
      this.toAmount = filterParameters.toAmount;
      this.page = filterParameters.page;
      this.size = filterParameters.size;
      if (this.returnAppId) {
        filterParameters.returnAppId = this.returnAppId;
      }
      if (this.navParam) {
        filterParameters.nav = this.navParam;
      }
      this.batchRejectionsService.filterParameters = filterParameters;
      this.resultList.dateFilterExecution(filterParameters);
    }
  }

  public removeTagsFilter(name: string): void {
    if (name === 'startDate') {
      this.startDate = undefined;
      this.batchRejectionsService.filterParameters.startDate = undefined;
    }
    if (name === 'endDate') {
      this.endDate = undefined;
      this.batchRejectionsService.filterParameters.endDate = undefined;
    }
    if (name === 'fromAmount') {
      this.fromAmount = undefined;
      this.batchRejectionsService.filterParameters.fromAmount = undefined;
    }
    if (name === 'toAmount') {
      this.toAmount = undefined;
      this.batchRejectionsService.filterParameters.toAmount = undefined;
    }
    const filterParameters = {
      startDate: this.startDate,
      endDate: this.endDate,
      fromAmount: this.fromAmount,
      toAmount: this.toAmount
    };
    this.removeTagsItem(name);
    if (this.returnAppId) {
      filterParameters['returnAppId'] = this.returnAppId;
    }
    if (this.navParam) {
      filterParameters['nav'] = this.navParam;
    }
    this.resultList.dateFilterExecution(filterParameters);
  }

  private removeTagsItem(name: string): void {
    for (let i = 0; i < this.filterTagsList.length; i++) {
      if (this.filterTagsList[i].name === name) {
        this.filterTagsList.splice(i, 1);
      }
    }
  }

  public checkValid(event?: any, filterKey?: any): void {
    this[filterKey] = event.formatted;
    if (this.checkDateValid() && this.startDate && this.endDate && this.startDate > this.endDate) {
      this.isGreater = true;
    } else { this.isGreater = false; }
  }

  private checkDateValid(): any {
    return this.batchPaymentSearchForm.get('startDate').valid && this.batchPaymentSearchForm.get('endDate').valid ? true : false;
  }

  public checkAmountValid(event?: any, filterKey?: any): any {
    if (this.fromAmount !== '' && this.toAmount !== '' && Number(this.toAmount) < Number(this.fromAmount)) {
      this.isGreaterAmount = true;
    } else { this.isGreaterAmount = false; }
  }

  public isNotSelecting(): any {
    const currentParams = this.route.snapshot.queryParams;
    for (const key in currentParams) {
      if (currentParams.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  public searchBatchPayments(): void {
    const filterParameters = {
      startDate: this.startDate,
      endDate: this.endDate,
      fromAmount: this.fromAmount,
      toAmount: this.toAmount,
      page: this.page,
      size: this.size,
      returnAppId: this.returnAppId,
      navParam: this.navParam
    };
    this.dateFilterExecution(filterParameters);
  }

  public resetBatchPayments(): void {
    this.filterTagsList = [];
    this.startDate = undefined;
    this.endDate = undefined;
    this.fromAmount = undefined;
    this.toAmount = undefined;
    this.router.navigate(['/ent-payments', 'batch-rejection'], {
      queryParams: {
        returnAppId: this.returnAppId,
        nav: this.navParam,
        roleSequenceId: this.securityEngineService.roleSequenceId
      }
    });
  }
  
  public openSecurity(event?: any) : void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event);
  }
}
