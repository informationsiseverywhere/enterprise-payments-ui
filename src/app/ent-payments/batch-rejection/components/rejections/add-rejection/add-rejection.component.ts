import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { DateService } from 'src/app/shared/services/date.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { BatchRejectionsService } from '../../../services/batch-rejections.service';
import { BatchRejectionModel } from '../../../models/batch-rejection.model';

@Component({
  selector: 'app-add-rejection',
  templateUrl: './add-rejection.component.html'
})
export class AddRejectionComponent implements OnInit {
  batchRejectionModel: BatchRejectionModel = new BatchRejectionModel();
  systemDate: any;
  addBatchForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private modal: ModalComponent,
    private loadingService: LoadingService,
    private dateService: DateService,
    private batchRejectionsService: BatchRejectionsService,
    public commonService: CommonService) { }

  ngOnInit() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
    this.addBatchForm = this.fb.group({
      entryDate: ['', Validators.compose([Validators.required, ValidationService.dateValidator])]
    });
    this.batchRejectionModel = new BatchRejectionModel();
    this.batchRejectionModel.entryDate = this.systemDate;
  }

  public saveBatchInfo(): void {
    const body = {
      entryDate: this.batchRejectionModel.entryDate
    };
    if (body.entryDate === '') {
      delete body.entryDate;
    }
    this.addBatch(body);
  }

  private addBatch(request?: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.batchRejectionsService.addBatchRejection(request).subscribe(
      data => {
        if (data.batchId && data.entryDate) {
          this.modal.close({
            batchId: data.batchId,
            entryDate: data.entryDate
          });
        }
        this.loadingService.toggleLoadingIndicator(false);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }
}
