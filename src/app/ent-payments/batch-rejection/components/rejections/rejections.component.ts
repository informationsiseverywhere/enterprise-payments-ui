import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';

import { DateService } from 'src/app/shared/services/date.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-rejections',
  templateUrl: './rejections.component.html'
})
export class RejectionsComponent implements OnInit {

  systemDate: any;
  batchRejectionList: any = [];
  filters: string;

  constructor(
    private route: ActivatedRoute,
    private dateService: DateService,
    public securityEngineService: SecurityEngineService
  ) {
    this.batchRejectionList = new Array(5);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.filters = queryParams.filters;
      });
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
  }

  isBatchRejectionsEmpty(event: any) {
    this.batchRejectionList = event;
  }
}
