import { Component, OnInit } from '@angular/core';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-rejections-header',
  templateUrl: './rejections-header.component.html'
})
export class RejectionsHeaderComponent implements OnInit {

  constructor(public securityEngineService: SecurityEngineService) { }

  ngOnInit(): void {
  }

}
