import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaginationComponent } from 'src/app/libs/pagination/pagination.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { BatchRejectionsService } from '../../../services/batch-rejections.service';
import { AddRejectionComponent } from '../add-rejection/add-rejection.component';

@Component({
  selector: 'app-rejections-results',
  templateUrl: './rejections-results.component.html'
})
export class RejectionsResultsComponent implements OnInit {
  @Output() isBatchRejectionsEmpty: EventEmitter<any> = new EventEmitter<any>();
  @Input() systemDate: string;
  @Input() filters: any;
  batchRejectionList: any = [];
  @ViewChild('addbatchRejectionsModal', { static: false }) addbatchRejectionsModal: ModalComponent;
  @ViewChild('batchRejectionAcceptModal', { static: false }) batchRejectionAcceptModal: ModalComponent;
  @ViewChild('batchRejectionDeleteModal', { static: false }) batchRejectionDeleteModal: ModalComponent;
  @ViewChild(PaginationComponent) paginator: PaginationComponent;
  startDate: string;
  endDate: string;
  fromAmount: number;
  toAmount: number;
  page: any;
  size: any;
  returnAppId: any;
  navParam: any;
  public isBatchRejectionListLoaded = false;

  constructor(
    private batchRejectionsService: BatchRejectionsService,
    private router: Router,
    private route: ActivatedRoute,
    private loadingService: LoadingService,
    public securityEngineService: SecurityEngineService,
    private commonService: CommonService) {
    this.batchRejectionList = new Array(5);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.startDate = queryParams.since;
        this.endDate = queryParams.until;
        this.fromAmount = queryParams.fromAmount;
        this.toAmount = queryParams.toAmount;
        this.page = queryParams.page;
        this.size = queryParams.size;
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
        this.getBatchRejections();
      }
    );
  }

  getBatchRejections() {
    this.isBatchRejectionListLoaded = false;
    this.isBatchRejectionsEmpty.emit(this.batchRejectionList);
    if (this.batchRejectionList) { (this.batchRejectionList.length = this.size < 5 ? this.size : 5); }
    this.batchRejectionsService.getBatchRejections(this.startDate, this.endDate, this.fromAmount, this.toAmount, this.size, this.page).subscribe(
      data => {
        this.isBatchRejectionListLoaded = true;
        this.refreshBatchPayments(data);
      },
      err => {
        this.commonService.handleErrorAPIResponse(this, 'isBatchRejectionListLoaded', 'batchRejectionList', err);
        this.isBatchRejectionsEmpty.emit(this.batchRejectionList);
      });
  }

  private refreshBatchPayments(data?: any): void {
    this.batchRejectionList = this.commonService.tablePaginationHandler(this.paginator, data);
    this.isBatchRejectionsEmpty.emit(this.batchRejectionList);
  }

  reload(event?: any) {
    if (event) {
      this.batchRejectionsService.paramsToModal = null;
      if (event.batchId && event.entryDate) {
        this.router.navigate(['/ent-payments', 'batch-rejection', event.batchId, 'summary', event.entryDate]);
      } else {
        this.getBatchRejections();
      }
    }
  }

  dateFilterExecution(filterParameters?: any) {
    const paramObject: any = {};
    if (filterParameters.startDate && filterParameters.startDate !== '') {
      paramObject.since = filterParameters.startDate;
    }

    if (filterParameters.endDate && filterParameters.endDate !== '') {
      paramObject.until = filterParameters.endDate;
    }

    if (filterParameters.fromAmount && filterParameters.fromAmount !== '') {
      paramObject.fromAmount = filterParameters.fromAmount;
    }

    if (filterParameters.toAmount && filterParameters.toAmount !== '') {
      paramObject.toAmount = filterParameters.toAmount;
    }
    if (filterParameters.page && filterParameters.page !== '') {
      paramObject.page = filterParameters.page;
    }
    if (filterParameters.size && filterParameters.size !== '') {
      paramObject.size = filterParameters.size;
    }
    if (filterParameters.returnAppId) {
      paramObject.returnAppId = filterParameters.returnAppId;
    }
    if (filterParameters.navParam) {
      paramObject.nav = filterParameters.navParam;
    }
    this.router.navigate(['/ent-payments', 'batch-rejection'], {
      queryParams: paramObject
    });
  }


  openBatchRejectionDeleteModal(batch?: any) {
    this.batchRejectionDeleteModal.modalTitle = 'Delete Batch Rejection';
    this.batchRejectionDeleteModal.modalFooter = true;
    this.batchRejectionDeleteModal.okButtonText = 'Yes';
    this.batchRejectionDeleteModal.cancelButtonText = 'No';
    this.batchRejectionsService.paramsToModal = batch;
    this.batchRejectionDeleteModal.icon = 'assets/images/model-icons/delete.svg';
    this.batchRejectionDeleteModal.message = 'Are you sure you want to delete the Batch immediately?';
    this.batchRejectionDeleteModal.open();
  }

  batchRejectionDelete(event?: any) {
    if (event) {
      const batchDetails = this.batchRejectionsService.paramsToModal;
      if (batchDetails) {
        this.batchRejectionsService.deleteBatch(
          batchDetails.batchId, batchDetails.entryDate).subscribe(
            () => {
              this.batchRejectionsService.paramsToModal = null;
              this.getBatchRejections();
            },
            err => {
              this.commonService.handleErrorResponse(err);
            });
      }
    }
  }

  public addBatchPaymentsDetails(): void {
    this.addbatchRejectionsModal.modalTitle = 'Add Batch Rejection';
    this.addbatchRejectionsModal.modalFooter = false;
    this.addbatchRejectionsModal.icon = 'assets/images/model-icons/add-batch.svg';
    this.addbatchRejectionsModal.open(AddRejectionComponent);
  }

  openAcceptDepositModal(batch: any) {
    this.batchRejectionAcceptModal.modalTitle = 'Batch Accept';
    this.batchRejectionAcceptModal.modalFooter = true;
    this.batchRejectionAcceptModal.okButtonText = 'Yes';
    this.batchRejectionAcceptModal.cancelButtonText = 'No';
    this.batchRejectionsService.paramsToModal = batch;
    this.batchRejectionAcceptModal.icon = 'assets/images/model-icons/batch-accept.svg';
    this.batchRejectionAcceptModal.message = 'Are you sure you want to accept the Batch immediately?';
    this.batchRejectionAcceptModal.open();
  }

  batchAcceptAction(event: any) {
    if (event) {
      const batchDetails = this.batchRejectionsService.paramsToModal;
      const request = {
        isAccept: true
      };
      this.loadingService.toggleLoadingIndicator(true);
      this.batchRejectionsService.updateBatch(batchDetails.batchId, batchDetails.entryDate, request).subscribe(
        () => {
          this.loadingService.toggleLoadingIndicator(false);
          this.batchRejectionsService.paramsToModal = null;
          this.getBatchRejections();
        },
        err => {
          this.commonService.handleErrorResponse(err);
        });
    }
  }
}
