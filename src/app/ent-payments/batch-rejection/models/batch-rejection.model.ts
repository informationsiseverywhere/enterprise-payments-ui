import { Link } from 'src/app/shared/models/link.model';

export class BatchRejectionModel {
  batchId: string;
  entryDate: string;
  isAccept: string;
  userName: string;
  accumulatedAmount: string;
  numberOfTransactions: any;
  links: Array<Link>;

  constructor(values: any = {}) {
    Object.assign(this, values);
  }
}
