import { Injectable } from '@angular/core';

import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Injectable({
  providedIn: 'root'
})
export class BatchRejectionsService {
  public paramsToModal: any;
  public filterParameters: any;
  apiUrls: any;

  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    public commonService: CommonService) { }

  getBatchRejections(since?: string, until?: string, fromAmount?: any, toAmount?: any, size?: any, page?: any) {
    const url = this.buildExecutionUrl(since, until, fromAmount, toAmount, size, page);
    return this.restService.getByUrl(url);
  }

  buildExecutionUrl(since?: string, until?: string, fromAmount?: any, toAmount?: any, size?: any, page?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches';
    const params: any = [];
    if (since) { params.push({ name: 'since', value: since }); }
    if (until) { params.push({ name: 'until', value: until }); }
    if (fromAmount) { params.push({ name: 'fromAmount', value: fromAmount }); }
    if (toAmount) { params.push({ name: 'toAmount', value: toAmount }); }
    let queryString = '';
    for (const param of params) {
      queryString += param.name + ':' + param.value;
      if (param !== params[params.length - 1]) {
        queryString += ',';
      }
    }
    if (queryString !== '') {
      url += '?filters=' + queryString;
    }

    url = this.commonService.buildSearchQueryUrl(url, page, size);
    return url;
  }

  addBatchRejection(body?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches';
    return this.restService.postByUrl(url, body);
  }

  updateBatch(batchId?: any, entryDate?: any, body?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '?entryDate=' + entryDate;
    return this.restService.patchByUrl(url, body);
  }

  deleteBatch(batchId?: any, entryDate?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/rejectionBatches/' + batchId + '?entryDate=' + entryDate;
    return this.restService.deleteByUrl(url);
  }
}
