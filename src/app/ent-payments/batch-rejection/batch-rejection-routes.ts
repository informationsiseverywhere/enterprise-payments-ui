import { Routes } from '@angular/router';

import { CommonComponent } from 'src/app/shared/components/common/common.component';
import { RejectionsComponent } from './components/rejections/rejections.component';
import { AuthGuard } from 'src/app/ent-auth/auth.guard';

export const batchRejectionRoutes: Routes = [
  {
    path: '',
    component: CommonComponent,
    children: [
      {
        path: '',
        component: RejectionsComponent
      },
      {
        path: ':batchId/summary/:entryDate',
        canActivate: [AuthGuard],
        loadChildren: () => import('./components/rejection-summary/rejection-summary.module').then(mod => mod.RejectionSummaryModule),
        data: {
          breadcrumb: 'Batch Rejection Summary',
          headerTitle: 'Batch Rejection Summary',
          screenName: 'epBatchRejectionSummary'
        }
      }
    ]
  }
];

