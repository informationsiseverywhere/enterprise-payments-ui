import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { BatchRejectionRoutingModule } from './batch-rejection-routing.module';
import { RejectionsComponent } from './components/rejections/rejections.component';
import { RejectionsSearchComponent } from './components/rejections/rejections-search/rejections-search.component';
import { RejectionsHeaderComponent } from './components/rejections/rejections-header/rejections-header.component';
import { RejectionsResultsComponent } from './components/rejections/rejections-results/rejections-results.component';
import { AddRejectionComponent } from './components/rejections/add-rejection/add-rejection.component';


@NgModule({
  declarations: [
    RejectionsComponent,
    RejectionsSearchComponent,
    RejectionsHeaderComponent,
    RejectionsResultsComponent,
    AddRejectionComponent
  ],
  entryComponents: [
    AddRejectionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    BatchRejectionRoutingModule
  ]
})
export class BatchRejectionModule { }
