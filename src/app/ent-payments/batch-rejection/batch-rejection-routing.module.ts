import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { batchRejectionRoutes } from './batch-rejection-routes';

@NgModule({
  imports: [
    RouterModule.forChild(batchRejectionRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class BatchRejectionRoutingModule {
}
