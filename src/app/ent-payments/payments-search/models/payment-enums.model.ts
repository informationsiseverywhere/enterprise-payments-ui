export class PaymentTags {
  startDate = 'From Date : ';
  endDate = 'To Date : ';
  minAmount = 'Minimum Amount : ';
  maxAmount = 'Maximum Amount : ';
  identifierType = 'Type : ';
  status = 'Status : ';
  paymentMethod = 'Payment Method : ';
  userId = 'User ID : ';
  paymentId = 'Payment ID : ';
  currency = 'Currency : ';
  cashWithApplication = 'Cash With Application : ';
}

export class PaymentIdentifierFilterLabels {
  AdditionalId = 'Additional Id';
  Agency = 'Agency Account Number';
  ConsumerDirect = 'Bill Account Number';
  Group = 'Group Account Number';
  PolicyAgreement = 'Policy/Agreement Number';
}
