export class AdvanceSearchTags {
  startDate = 'From Date : ';
  endDate = 'To Date : ';
  fromAmount = 'Minimum Amount : ';
  toAmount = 'Maximum Amount : ';
  currency = 'Currency : ';
}
