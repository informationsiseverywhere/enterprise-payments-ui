import { Component, OnInit, ViewChild, EventEmitter, Output, Input, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';

import { PaymentsSearchResultsComponent } from '../payments-search-results/payments-search-results.component';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { HalProcessor } from 'src/app/shared/services/utilities.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { PaymentTags } from '../../models/payment-enums.model';
import { PaymentCommonService } from '../../services/payment-common.service';
import { PaymentsSearchService } from '../../services/payments-search.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-payments-search-section',
  templateUrl: './payments-search-section.component.html',
  styleUrls: ['./payments-search-section.component.scss']
})
export class PaymentSearchSectionComponent implements OnInit {
  @Input() systemDate: any;
  @Input() since: any;
  @Input() until: any;
  @Input() paymentMethod: any;
  @Input() minAmount: any;
  @Input() maxAmount: any;
  @Input() identifierType: any;
  @Input() currency: any;
  @Input() page: any;
  @Input() size: any;
  private paymentTags: PaymentTags = new PaymentTags();
  supportData: any;
  startDate: any;
  endDate: any;
  isGreater = false;
  @Output() refreshSummary: EventEmitter<any> = new EventEmitter();
  @ViewChild('resultList', { static: false }) resultList: PaymentsSearchResultsComponent;
  @ViewChild('advancedFilters', { static: false }) advancedFilters: ModalComponent;
  @ViewChild('paymentFilterToggle', { static: false }) paymentFilterToggle: ElementRef;
  paymentMethodDropdown: any = [];
  identifierTypeDropdown: any = [];
  public currencyDropdown: any = [];
  public multiCurrencyDropdown: any = [];
  filterTagsList: Array<{ tag: string, name: string }> = [];
  keyUp: Subject<string> = new Subject<string>();
  returnAppId: any;
  navParam: any;
  userId: any;
  paymentId: any;
  public paymentSearchForm: FormGroup;
  public isGreaterAmount = false;
  @Input() isPaymentListLoaded = false;
  @ViewChild('filterControlList', { static: false }) filterControlList: ElementRef;
  public isShowAdvanceLink = true;
  public isFiltersExpand = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private paymentCommonService: PaymentCommonService,
    private halProcessor: HalProcessor,
    private fb: FormBuilder,
    private paymentsSearchService: PaymentsSearchService,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    public commonService: CommonService) {
  }

  ngOnInit() {
    this.paymentSearchForm = this.fb.group({
      startDate: ['', ValidationService.dateValidator],
      endDate: ['', ValidationService.dateValidator],
      type: [''],
      userId: [''],
      paymentId: [''],
      paymentMethod: [''],
      minAmount: [''],
      maxAmount: [''],
      currency: ['']
    });
    this.route.parent.queryParams.subscribe(
      queryParams => {
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
        this.since = queryParams.since;
        this.until = queryParams.until;
        this.paymentMethod = this.halProcessor.replacePlusSignsBySpaces(queryParams.paymentMethod);
        this.minAmount = queryParams.minAmount;
        this.maxAmount = queryParams.maxAmount;
        this.identifierType = this.halProcessor.replacePlusSignsBySpaces(queryParams.identifierType);
        this.userId = queryParams.filterUserId;
        this.currency = queryParams.currency;
        this.paymentId = queryParams.paymentId;
        if (!(this.since || this.until || this.paymentMethod || this.maxAmount ||
          this.minAmount || this.identifierType || this.userId || this.paymentId || this.currency)) {
          this.paymentsSearchService.filterParam = null;
        }
      this.getPaymentsListSupportData();
      });
    this.startDate = this.since;
    this.endDate = this.until;
    this.filterTagsList = [];
    this.paymentCommonService.initialPaymentFilterOption(this.startDate, this.endDate, this.minAmount, this.maxAmount,
      this.paymentMethod, this.identifierType, this.userId, this.paymentId, this.currency, this.paymentTags, undefined, undefined, undefined, this.filterTagsList);
  }

  public searchFilter(selectedItem?: any, filterKey?: string): void {
    this.paymentCommonService.searchFilter(selectedItem, filterKey, this.startDate, this.endDate, this.minAmount, this.maxAmount,
      this.paymentMethod, this.identifierType, this.userId, this.paymentId, this.currency, this.paymentTags, undefined, undefined, undefined, this.filterTagsList);
    this.refreshSummary.emit(true);
  }

  public checkAmountValid(filterKey?: any): any {
    if (this.maxAmount !== '' && this.minAmount !== '' && Number(this.maxAmount) < Number(this.minAmount)) {
      this.isGreaterAmount = true;
    } else {
      this.isGreaterAmount = false;
    }
  }

  public getPaymentsListSupportData(): void {
    this.paymentsSearchService.getPaymentsListSupportData().subscribe(
      data => {
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.paymentMethod);
          this.getDropdownItems(PropertyName.identifierType);
          this.getDropdownItems(PropertyName.MultiCurrency);
          this.getDropdownItems(PropertyName.Currency);
        }
      });
  }

  private getDropdownItems(propertyName?: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (!this.paymentMethod && propertyName === PropertyName.paymentMethod && sd.defaultValue) {
          this.paymentSearchForm.controls['paymentMethod'].setValue(sd.defaultValue);
        }
        if (!this.identifierType && propertyName === PropertyName.identifierType && sd.defaultValue) {
          this.paymentSearchForm.controls['type'].setValue(sd.defaultValue);
        }
        if (!this.currency && propertyName === PropertyName.Currency && sd.defaultValue) {
          this.currency = sd.defaultValue;
        }
      }
    }
  }

  public convertToUpperCase(event?: any, property?: any): void {
    if (event) { this.form[property].patchValue(event); }
  }

  get form() { return this.paymentSearchForm.controls; }

  public checkValid(event?: any, name?: any): void {
    this[name] = event.formatted;
    if (this.startDate && this.endDate && this.startDate > this.endDate) {
      this.isGreater = true;
    } else { this.isGreater = false; }
  }

  public removeTagsFilter(name?: string): void {
    if (name === 'since') {
      this.startDate = undefined;
    }
    if (name === 'until') {
      this.endDate = undefined;
    }
    if (name === 'fromAmount') {
      this.minAmount = '';
    }
    if (name === 'toAmount') {
      this.maxAmount = '';
    }
    if (name === 'paymentMethod') {
      this.paymentMethod = '';
    }
    if (name === 'identifierType') {
      this.identifierType = '';
    }
    if (name === 'userId') {
      this.userId = '';
    }
    if (name === 'paymentId') {
      this.paymentId = '';
    }
    if (name === 'currency') {
      this.currency = '';
    }
    let filterParam = {};
    filterParam = {
      since: this.startDate,
      until: this.endDate,
      toAmount: this.maxAmount,
      fromAmount: this.minAmount,
      paymentMethod: this.paymentMethod,
      identifierType: this.identifierType,
      userId: this.userId,
      paymentId: this.paymentId,
      currency: this.currency
    };
    this.paymentsSearchService.filterParam = filterParam;
    this.refreshSummary.emit(true);
    this.removeTagsItem(name);
  }

  removeTagsItem(name?: string) {
    for (let i = 0; i < this.filterTagsList.length; i++) {
      if (this.filterTagsList[i].name === name) {
        this.filterTagsList.splice(i, 1);
      }
    }
  }

  public resetIdentifiedPayments(): void {
    this.filterTagsList = [];
    this.startDate = undefined;
    this.endDate = undefined;
    this.minAmount = undefined;
    this.maxAmount = undefined;
    this.router.navigate(['/ent-payments/payments-search/identified-payments'], {
      queryParams: {
        returnAppId: this.returnAppId,
        nav: this.navParam,
        roleSequenceId: this.securityEngineService.roleSequenceId
      }
    });
  }

  public isNotSelecting(): any {
    const currentParams = this.route.snapshot.queryParams;
    for (const key in currentParams) {
      if (currentParams.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
  public togglePayment(): void {
    this.paymentsSearchService.filterParam = null;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        returnAppId: this.returnAppId,
        nav: this.navParam
      }
    };
    this.router.navigate(['/ent-payments/payments-search/unidentified-payments'], navigationExtras);
  }

  public searchIdentifiedPayments(): void {
    this.filterTagsList = [];
    this.paymentCommonService.initialPaymentFilterOption(this.startDate, this.endDate, this.minAmount, this.maxAmount,
      this.paymentMethod, this.identifierType, this.userId, this.paymentId, this.currency, this.paymentTags, undefined, undefined, undefined, this.filterTagsList);
    this.searchFilter(null, null);
    this.refreshSummary.emit(true);
    if (this.filterTagsList.length > 0) {
      this.moreFilters('isFiltersExpand');
      this.paymentFilterToggle.nativeElement.classList.remove('show');
    }
  }

  public moreFilters(value?: any): void {
    window.scroll(0, 0);
    this[value] = !this[value];
  }

  public openSecurity(event?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, 'searchSection', (this.multiCurrencyDropdown.length > 0 && this.multiCurrencyDropdown[0] === 'true') ? undefined : ['searchCurrency']);
  }

}

