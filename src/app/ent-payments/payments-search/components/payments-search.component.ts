import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PaymentsSearchResultsComponent } from './payments-search-results/payments-search-results.component';
import { DateService } from 'src/app/shared/services/date.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-payments-search',
  templateUrl: './payments-search.component.html'
})
export class PaymentsSearchComponent implements OnInit {
  accountId: any;
  accountNumber: any;
  systemDate: any;
  isShowActions: boolean;
  singleAction: boolean;
  paymentsList: any = [];
  query: string;
  filters: string;
  since: any;
  until: any;
  paymentMethod: any;
  minAmount: any;
  maxAmount: any;
  identifierType: any;
  page: any;
  size: any;
  userId: any;
  paymentId: any;
  currency: any;
  @ViewChild('paymentList', { static: true }) paymentList: PaymentsSearchResultsComponent;
  constructor(
    private route: ActivatedRoute,
    public securityEngineService: SecurityEngineService,
    private dateService: DateService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.since = queryParams.since;
        this.until = queryParams.until;
        this.paymentMethod = queryParams.paymentMethod;
        this.minAmount = queryParams.minAmount;
        this.maxAmount = queryParams.maxAmount;
        this.identifierType = queryParams.identifierType;
        this.userId = queryParams.filterUserId;
        this.paymentId = queryParams.paymentId;
        this.currency = queryParams.currency;
        this.page = queryParams.page;
        this.size = queryParams.size;
      }
    );
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
  }

  isPaymentEmpty(event?: any) {
    this.paymentsList = event;
  }

  reloadSummary(event?: any) {
    this.paymentList.reloadPaymentsSearch(event);
  }
}
