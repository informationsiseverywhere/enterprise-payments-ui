import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaginationComponent } from 'src/app/libs/pagination/pagination.component';
import { HalProcessor } from 'src/app/shared/services/utilities.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentsSearchResultsDetailComponent } from './payments-search-results-detail/payments-search-results-detail.component';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { PaymentsSearchService } from '../../services/payments-search.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-payments-search-results',
  templateUrl: './payments-search-results.component.html'
})
export class PaymentsSearchResultsComponent implements OnInit, OnChanges {
  itemSelectedCount = 0;
  isChecked: boolean;
  supportData: any;
  selectedPaymentsSearch: any;
  PolicyPlanDropdown: any;
  MasterCompanyNumberDropdown: any;
  LineOfBusinessDropdown: any;
  paymentList: any = [];
  paymentMethodDropdown: any = [];
  isFirstRun = true;
  previewedPaymentsSearch: any;
  returnAppId: any;
  navParam: any;
  uiUrls: any;
  public isPaymentListLoaded = false;
  @Input() systemDate: string;
  @Input() since: any;
  @Input() until: any;
  @Input() paymentMethod: any;
  @Input() minAmount: any;
  @Input() maxAmount: any;
  @Input() identifierType: any;
  @Input() currency: any;
  @Input() page: any;
  @Input() size: any;
  @Output() checkboxesSelected: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() isPaymentEmpty: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('PaymentDetailModal', { static: false }) PaymentDetailModal: ModalComponent;
  @Input() userId: any;
  @Input() paymentId: any;
  @ViewChild(PaginationComponent, { static: false }) paginator: PaginationComponent;

  constructor(
    private paymentsSearchService: PaymentsSearchService,
    private router: Router,
    private route: ActivatedRoute,
    private appConfig: AppConfiguration,
    private hostUrlService: DynamicHostUrlService,
    private halProcessor: HalProcessor,
    public securityEngineService: SecurityEngineService,
    private commonService: CommonService) {
    this.uiUrls = this.appConfig.uiUrls;
    this.paymentList = new Array(5);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.since = queryParams.since;
        this.until = queryParams.until;
        this.paymentMethod = this.halProcessor.replacePlusSignsBySpaces(queryParams.paymentMethod);
        this.minAmount = queryParams.minAmount;
        this.maxAmount = queryParams.maxAmount;
        this.identifierType = this.halProcessor.replacePlusSignsBySpaces(queryParams.identifierType);
        this.userId = queryParams.filterUserId;
        this.paymentId = queryParams.paymentId;
        this.currency = queryParams.currency;
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
        if (!(this.since || this.until || this.paymentMethod ||
          this.maxAmount || this.minAmount || this.identifierType || this.userId || this.paymentId || this.currency)) {
          this.paymentsSearchService.filterParam = null;
        }
      }
    );
    this.isFirstRun = true;
  }

  ngOnChanges(_changes: any) {
    if (this.isFirstRun) {
      this.getPaymentsSearch(this.since, this.until, this.paymentMethod,
        this.minAmount, this.maxAmount, this.identifierType, this.userId, this.paymentId, this.currency);
    }
  }

  reloadPaymentsSearch(event: any) {
    if (event) {
      const paramObject: any = {};
      if (this.paymentsSearchService.filterParam) {
        this.since = this.paymentsSearchService.filterParam.since;
        if (this.since) {
          paramObject.since = this.since;
        }
        this.until = this.paymentsSearchService.filterParam.until;
        if (this.until) {
          paramObject.until = this.until;
        }
        this.paymentMethod = this.paymentsSearchService.filterParam.paymentMethod;
        if (this.paymentMethod) {
          paramObject.paymentMethod = this.paymentMethod;
        }
        this.minAmount = this.paymentsSearchService.filterParam.fromAmount;
        if (this.minAmount) {
          paramObject.minAmount = this.minAmount;
        }
        this.maxAmount = this.paymentsSearchService.filterParam.toAmount;
        if (this.maxAmount) {
          paramObject.maxAmount = this.maxAmount;
        }
        this.identifierType = this.paymentsSearchService.filterParam.identifierType;
        if (this.identifierType) {
          paramObject.identifierType = this.identifierType;
        }
        this.userId = this.paymentsSearchService.filterParam.userId;
        if (this.userId) {
          paramObject.filterUserId = this.userId;
        }
        this.paymentId = this.paymentsSearchService.filterParam.paymentId;
        if (this.paymentId) {
          paramObject.paymentId = this.paymentId;
        }
        this.currency = this.paymentsSearchService.filterParam.currency;
        if (this.currency) {
          paramObject.currency = this.currency;
        }
        if (this.returnAppId) {
          paramObject.returnAppId = this.returnAppId;
        }
        if (this.navParam) {
          paramObject.nav = this.navParam;
        }
        this.router.navigate(['/ent-payments/payments-search/identified-payments'], {
          queryParams: paramObject
        });
      }
    }
  }


  getPaymentsSearch(since: string = null, until: string = null, paymentMethod: string = null,
    minAmount?: any, maxAmount?: any, identifierType?: any, userId?: any, paymentId?: any, currency?: any) {
    if (this.paymentsSearchService.filterParam) {
      since = this.paymentsSearchService.filterParam.since;
      until = this.paymentsSearchService.filterParam.until;
      paymentMethod = this.paymentsSearchService.filterParam.paymentMethod;
      minAmount = this.paymentsSearchService.filterParam.fromAmount;
      maxAmount = this.paymentsSearchService.filterParam.toAmount;
      identifierType = this.paymentsSearchService.filterParam.identifierType;
      userId = this.paymentsSearchService.filterParam.userId;
      paymentId = this.paymentsSearchService.filterParam.paymentId;
      currency = this.paymentsSearchService.filterParam.currency;
    }

    this.isPaymentListLoaded = false;
    this.paymentList && (this.paymentList.length = this.size < 5 ? this.size : 5);
    this.paymentsSearchService.getPaymentList(since, until, paymentMethod,
      minAmount, maxAmount, identifierType, userId, paymentId, currency, this.size, this.page).subscribe(
        data => {
          this.refreshPaymentsSearch(data);
          this.isPaymentListLoaded = true;
        },
        err => {
          this.commonService.handleErrorAPIResponse(this, 'isPaymentListLoaded', 'paymentList', err);
        });
  }

  getDropdownItems(propertyName: string) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
  }

  private refreshPaymentsSearch(data: any): void {
    this.itemSelectedCount = 0;
    this.checkboxesSelected.emit(false);
    this.paymentList = this.commonService.tablePaginationHandler(this.paginator, data);
    this.isPaymentEmpty.emit(this.paymentList);
  }

  filterExecution(filterParameters: any) {
    const paramObject: any = {};
    if (filterParameters.startDate && filterParameters.startDate !== '') {
      paramObject.since = filterParameters.startDate;
    }

    if (filterParameters.endDate && filterParameters.endDate !== '') {
      paramObject.until = filterParameters.endDate;
    }

    if (filterParameters.paymentMethod && filterParameters.paymentMethod !== '') {
      paramObject.paymentMethod = filterParameters.paymentMethod;
    }

    if (filterParameters.currency && filterParameters.currency !== '') {
      paramObject.currency = filterParameters.currency;
    }

    this.router.navigate(['/ent-payments/payments-search/identified-payments'], {
      queryParams: paramObject
    });
  }

  public goToAccountPage(payment?: any): void {
    let url = '';
    switch (payment.type) {
      case 'GroupBill':
        url = this.hostUrlService.getHostUrl(this.appConfig.uiUrls.groupBill);
        break;
      case 'AgencyBill':
        url = this.hostUrlService.getHostUrl(this.appConfig.uiUrls.agencyBill);
        break;
      case 'DirectBill':
        url = this.hostUrlService.getHostUrl(this.appConfig.uiUrls.directBilling);
        break;
      case 'Group':
        url = this.hostUrlService.getHostUrl(this.appConfig.uiUrls.groupBilling);
        break;
      case 'Agency':
        url = this.hostUrlService.getHostUrl(this.appConfig.uiUrls.agencyBilling);
        break;
    }
    url = url + payment.accountId + '/payments?since=' + payment.paymentDate;
    this.commonService.navigateToUrl(url, '_blank', true);
  }

  openPaymentDetail(payment: any) {
    this.paymentsSearchService.getPaymentsDetail(payment.paymentSequenceNumber,
      payment.postedDate, payment.userId, payment.batchId).subscribe(
        data => {
          this.paymentsSearchService.paymentDetail = data;
          this.PaymentDetailModal.modalTitle = 'Payment Details';
          this.PaymentDetailModal.icon = 'assets/images/model-icons/detail-history.svg';
          this.PaymentDetailModal.modalFooter = false;
          this.PaymentDetailModal.open(PaymentsSearchResultsDetailComponent);
        },
        () => {
          setTimeout(() => {
            this.router.navigate(['/ent-payments/errors']);
          }, 100);
        });
  }

  removeSpaces(status: string) {
    return this.commonService.removeSpacesAndFWDash(status);
  }
}
