import { Component, OnInit } from '@angular/core';

import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentsSearchService } from '../../../services/payments-search.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-payments-search-results-detail',
  templateUrl: './payments-search-results-detail.component.html',
  styleUrls: ['./payments-search-results-detail.component.scss']
})
export class PaymentsSearchResultsDetailComponent implements OnInit {
  public propertyName = PropertyName;
  public paymentDetails: any;
  public isAccountNumber = false;

  constructor(
    private paymentsSearchService: PaymentsSearchService,
    private commonService: CommonService) { }

  ngOnInit() {
    this.paymentDetails = Object.assign({}, this.paymentsSearchService.paymentDetail);
    this.isAccountNumber = true;
  }

  public removeSpaces(status: string): any {
    return this.commonService.removeSpacesAndFWDash(status);
  }
}
