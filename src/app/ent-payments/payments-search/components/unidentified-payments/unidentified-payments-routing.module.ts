import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { unidentifiedPaymentsRoutes } from './unidentified-payments-routes';

@NgModule({
  imports: [
    RouterModule.forChild(unidentifiedPaymentsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class UnidentifiedPaymentsRoutingModule {
}
