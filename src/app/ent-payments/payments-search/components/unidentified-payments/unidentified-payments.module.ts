import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { UnidentifiedPaymentsRoutingModule } from './unidentified-payments-routing.module';

import { UnidentifiedPaymentsComponent } from './components/unidentified-payments.component';
import { UnidentifiedPaymentSearchComponent } from './components/unidentified-payment-search/unidentified-payment-search.component';
import { UnidentifiedPaymentResultsComponent } from './components/unidentified-payment-results/unidentified-payment-results.component';
import {
  UnidentifiedDetailsComponent
} from './components/unidentified-payment-results/unidentified-details/unidentified-details.component';
import { UnidentifiedPaymentHeaderComponent } from './components/unidentified-payment-header/unidentified-payment-header.component';
import { PaymentsSearchActionsComponent } from './components/payments-search-actions/payments-search-actions.component';
import { WriteOffComponent } from './components/payments-search-actions/write-off/write-off.component';
import { UpdateComponent } from './components/payments-search-actions/update/update.component';
import { SuspendComponent } from './components/payments-search-actions/suspend/suspend.component';
import { ReverseComponent } from './components/payments-search-actions/reverse/reverse.component';
import { DisburseComponent } from './components/payments-search-actions/disburse/disburse.component';
import { DisburseSelectComponent } from './components/payments-search-actions/disburse-select/disburse-select.component';

@NgModule({
  declarations: [
    UnidentifiedPaymentsComponent,
    UnidentifiedPaymentSearchComponent,
    UnidentifiedPaymentResultsComponent,
    UnidentifiedDetailsComponent,
    UnidentifiedPaymentHeaderComponent,
    PaymentsSearchActionsComponent,
    WriteOffComponent,
    UpdateComponent,
    SuspendComponent,
    ReverseComponent,
    DisburseComponent,
    DisburseSelectComponent
  ],
  entryComponents: [
    UnidentifiedDetailsComponent,
    WriteOffComponent,
    UpdateComponent,
    SuspendComponent,
    ReverseComponent,
    DisburseComponent,
    DisburseSelectComponent],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    UnidentifiedPaymentsRoutingModule
  ]
})
export class UnidentifiedPaymentsModule { }
