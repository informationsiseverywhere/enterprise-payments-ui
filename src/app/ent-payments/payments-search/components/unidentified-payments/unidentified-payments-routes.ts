import { Routes } from '@angular/router';

import { CommonComponent } from 'src/app/shared/components/common/common.component';
import { UnidentifiedPaymentsComponent } from './components/unidentified-payments.component';

export const unidentifiedPaymentsRoutes: Routes = [
  {
    path: '',
    component: CommonComponent,
    children: [
      {
        path: '',
        component: UnidentifiedPaymentsComponent
      }
    ]
  }
];

