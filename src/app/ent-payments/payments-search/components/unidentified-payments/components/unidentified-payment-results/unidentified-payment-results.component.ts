import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaginationComponent } from 'src/app/libs/pagination/pagination.component';
import { HalProcessor } from 'src/app/shared/services/utilities.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { UnidentifiedDetailsComponent } from './unidentified-details/unidentified-details.component';
import { WriteOffComponent } from '../payments-search-actions/write-off/write-off.component';
import { ReverseComponent } from '../payments-search-actions/reverse/reverse.component';
import { SuspendComponent } from '../payments-search-actions/suspend/suspend.component';
import { DisburseSelectComponent } from '../payments-search-actions/disburse-select/disburse-select.component';
import { DisburseComponent } from '../payments-search-actions/disburse/disburse.component';
import { UpdateComponent } from '../payments-search-actions/update/update.component';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { PaymentOptions, PropertyName } from 'src/app/shared/enum/common.enum';

const BILLACCOUNT = 'BillAccount';

@Component({
  selector: 'app-unidentified-payment-results',
  templateUrl: './unidentified-payment-results.component.html'
})
export class UnidentifiedPaymentResultsComponent implements OnInit, OnChanges {
  itemSelectedCount = 0;
  isChecked: boolean;
  supportData: any;
  selectedPaymentsSearch: any;
  PolicyPlanDropdown: any;
  MasterCompanyNumberDropdown: any;
  LineOfBusinessDropdown: any;
  paymentList: any = [];
  paymentMethodDropdown: any = [];
  isFirstRun = true;
  previewedPaymentsSearch: any;
  returnAppId: any;
  navParam: any;
  public isPaymentListLoaded = false;
  public eftActivity: any;
  public creditCard: any;
  @Input() systemDate: string;
  @Input() since: any;
  @Input() until: any;
  @Input() paymentMethod: any;
  @Input() minAmount: any;
  @Input() maxAmount: any;
  @Input() page: any;
  @Input() size: any;
  @Input() userId: any;
  @Input() paymentId: any;
  @Input() identifierType: any;
  @Input() currency: any;
  @Input() status: Array<any> = [];
  @Input() identifier: any;
  @Input() cashWithApplication: any;
  @Output() isPaymentEmpty: EventEmitter<any> = new EventEmitter<any>();
  @Output() checkboxesSelected: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('paymentDetailsModal', { static: false }) paymentDetailsModal: ModalComponent;
  @ViewChild('unidentifiedPaymentWriteOffModal', { static: false }) unidentifiedPaymentWriteOffModal: ModalComponent;
  @ViewChild('unidentifiedPaymentReverseModal', { static: false }) unidentifiedPaymentReverseModal: ModalComponent;
  @ViewChild('unidentifiedPaymentSuspendModal', { static: false }) unidentifiedPaymentSuspendModal: ModalComponent;
  @ViewChild('unidentifiedPaymentDisburseModal') unidentifiedPaymentDisburseModal: ModalComponent;
  @ViewChild('unidentifiedPaymentRefundMethodSelectModal', { static: false }) unidentifiedPaymentRefundMethodSelectModal: ModalComponent;
  @ViewChild('unidentifiedPaymentUpdateModal', { static: false }) unidentifiedPaymentUpdateModal: ModalComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  selectedValue: any;
  @ViewChild(PaginationComponent, { static: false }) paginator: PaginationComponent;
  @Input() multiCurrencyEnabled: any;
  disbursementMethodTypes = [];
  constructor(
    private paymentsSearchService: PaymentsSearchService,
    private router: Router,
    private route: ActivatedRoute,
    private halProcessor: HalProcessor,
    public securityEngineService: SecurityEngineService,
    private loadingService: LoadingService,
    private commonService: CommonService) {
    this.paymentList = new Array(5);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.since = queryParams.since;
        this.until = queryParams.until;
        this.paymentMethod = this.halProcessor.replacePlusSignsBySpaces(queryParams.paymentMethod);
        this.minAmount = queryParams.minAmount;
        this.maxAmount = queryParams.maxAmount;
        this.identifierType = this.halProcessor.replacePlusSignsBySpaces(queryParams.identifierType);
        this.userId = queryParams.filterUserId;
        this.paymentId = queryParams.paymentId;
        this.currency = queryParams.currency;
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
        this.status = this.commonService.getFilterParams(queryParams.status);
        this.identifier = queryParams.identifier;
        this.cashWithApplication = queryParams.cashWithApplication;
        if (!(this.since || this.until || this.paymentMethod ||
          this.maxAmount || this.minAmount || this.identifierType || this.userId || this.paymentId || this.currency || this.identifier || this.status || this.cashWithApplication)) {
          this.paymentsSearchService.filterParam = null;
        }
      }
    );
    this.isFirstRun = true;
    this.selectedValue = BILLACCOUNT;
  }

  ngOnChanges(changes: any) {
    if (this.isFirstRun) {
      this.getPaymentsSearch(this.since, this.until, this.paymentMethod,
        this.minAmount, this.maxAmount, this.identifierType, this.userId, this.paymentId, this.currency, this.status, this.identifier, this.cashWithApplication);
    }
  }

  reloadPaymentsSearch(event: any) {
    if (event) {
      const paramObject: any = {};
      if (this.paymentsSearchService.filterParam) {
        this.since = this.paymentsSearchService.filterParam.since;
        if (this.since) {
          paramObject.since = this.since;
        }
        this.until = this.paymentsSearchService.filterParam.until;
        if (this.until) {
          paramObject.until = this.until;
        }
        this.paymentMethod = this.paymentsSearchService.filterParam.paymentMethod;
        if (this.paymentMethod) {
          paramObject.paymentMethod = this.paymentMethod;
        }
        this.minAmount = this.paymentsSearchService.filterParam.fromAmount;
        if (this.minAmount) {
          paramObject.minAmount = this.minAmount;
        }
        this.maxAmount = this.paymentsSearchService.filterParam.toAmount;
        if (this.maxAmount) {
          paramObject.maxAmount = this.maxAmount;
        }
        this.identifierType = this.paymentsSearchService.filterParam.identifierType;
        if (this.identifierType) {
          paramObject.identifierType = this.identifierType;
        }
        this.userId = this.paymentsSearchService.filterParam.userId;
        if (this.userId) {
          paramObject.filterUserId = this.userId;
        }
        this.paymentId = this.paymentsSearchService.filterParam.paymentId;
        if (this.paymentId) {
          paramObject.paymentId = this.paymentId;
        }
        this.currency = this.paymentsSearchService.filterParam.currency;
        if (this.currency) {
          paramObject.currency = this.currency;
        }
        this.status = this.paymentsSearchService.filterParam.status;
        if (this.status && this.status.length > 0) {
          paramObject.status = this.status;
        }
        this.identifier = this.paymentsSearchService.filterParam.identifier;
        if (this.identifier) {
          paramObject.identifier = this.identifier;
        }
        this.cashWithApplication = this.paymentsSearchService.filterParam.cashWithApplication;
        if (this.cashWithApplication) {
          paramObject.cashWithApplication = this.cashWithApplication;
        }
        if (this.returnAppId) {
          paramObject.returnAppId = this.returnAppId;
        }
        if (this.navParam) {
          paramObject.nav = this.navParam;
        }
        this.router.navigate(['/ent-payments/payments-search/unidentified-payments'], {
          queryParams: paramObject
        });
      }
    }
  }


  getPaymentsSearch(since: string = null, until: string = null,
    paymentMethod: string = null, minAmount?: any, maxAmount?: any, identifierType?: any, userId?: any, paymentId?: any, currency?: any, status?: any, identifier?: any, cashWithApplication?: any) {
    if (this.paymentsSearchService.filterParam) {
      since = this.paymentsSearchService.filterParam.since;
      until = this.paymentsSearchService.filterParam.until;
      paymentMethod = this.paymentsSearchService.filterParam.paymentMethod;
      minAmount = this.paymentsSearchService.filterParam.fromAmount;
      maxAmount = this.paymentsSearchService.filterParam.toAmount;
      identifierType = this.paymentsSearchService.filterParam.identifierType;
      userId = this.paymentsSearchService.filterParam.userId;
      paymentId = this.paymentsSearchService.filterParam.paymentId;
      currency = this.paymentsSearchService.filterParam.currency;
      status = this.paymentsSearchService.filterParam.status;
      identifier = this.paymentsSearchService.filterParam.identifier;
      cashWithApplication = this.paymentsSearchService.filterParam.cashWithApplication;
    }

    this.isPaymentListLoaded = false;
    this.paymentList && (this.paymentList.length = this.size < 5 ? this.size : 5);
    this.paymentsSearchService.getUnidentifieldPaymentList(since, until,
      paymentMethod, minAmount, maxAmount, identifierType, identifier, userId, paymentId, currency, status, cashWithApplication, this.size, this.page).subscribe(
        data => {
          this.isPaymentListLoaded = true;
          this.refreshPaymentsSearch(data);
        },
        err => {
          this.commonService.handleErrorAPIResponse(this, 'isPaymentListLoaded', 'paymentList', err);
        });
  }

  private refreshPaymentsSearch(data: any): void {
    this.itemSelectedCount = 0;
    this.checkboxesSelected.emit(false);
    this.paymentList = this.commonService.tablePaginationHandler(this.paginator, data);
    this.isPaymentEmpty.emit(this.paymentList);
  }

  filterExecution(filterParameters: any) {
    const paramObject: any = {};
    if (filterParameters.startDate && filterParameters.startDate !== '') {
      paramObject.since = filterParameters.startDate;
    }

    if (filterParameters.endDate && filterParameters.endDate !== '') {
      paramObject.until = filterParameters.endDate;
    }

    if (filterParameters.paymentMethod && filterParameters.paymentMethod !== '') {
      paramObject.paymentMethod = filterParameters.paymentMethod;
    }

    if (filterParameters.currency && filterParameters.currency !== '') {
      paramObject.currency = filterParameters.currency;
    }
    this.router.navigate(['/ent-payments/payments-search/unidentified-payments'], {
      queryParams: paramObject
    });
  }

  getUnidentifiedPaymentsDetail(payment: any) {
    this.paymentsSearchService.getUnidentifiedPaymentsDetail(payment).subscribe(
      data => {
        this.paymentsSearchService.paymentDetail = data;
        this.paymentDetailsModal.modalTitle = 'Payment Details';
        this.paymentDetailsModal.icon = 'assets/images/model-icons/detail-history.svg';
        this.paymentDetailsModal.modalFooter = false;
        this.paymentDetailsModal.open(UnidentifiedDetailsComponent);
      },
      () => {
        setTimeout(() => {
          this.router.navigate(['ent-payments/errors']);
        }, 100);
      });
  }

  removeSpaces(status: string) {
    return this.commonService.removeSpacesAndFWDash(status);
  }

  handleAction(payment: any, action: string) {
    switch (action) {
      case 'reverse':
        this.openReversePaymentModal(payment);
        break;
      case 'update':
        this.openUpdateModal(payment);
        break;
      case 'writeOff':
        this.openWriteOffModal(payment);
        break;
      case 'suspend':
        this.openSuspendModal(payment);
        break;
      case 'disburse':
        this.getPaymentsUnidentifiedSupportData(payment);
        break;
    }
  }

  openWriteOffModal(payment: any) {
    this.unidentifiedPaymentWriteOffModal.modalTitle = 'Write-off Suspense';
    this.unidentifiedPaymentWriteOffModal.icon = 'assets/images/model-icons/write-off.svg';
    this.unidentifiedPaymentWriteOffModal.modalFooter = false;
    this.unidentifiedPaymentWriteOffModal.okButtonText = 'SUBMIT';
    this.paymentsSearchService.paramsToModal = payment;
    this.unidentifiedPaymentWriteOffModal.open(WriteOffComponent);
  }

  openReversePaymentModal(payment: any) {
    this.unidentifiedPaymentReverseModal.modalTitle = 'Reverse Payment';
    this.unidentifiedPaymentReverseModal.icon = 'assets/images/model-icons/reverse-payment.svg';
    this.unidentifiedPaymentReverseModal.modalFooter = false;
    this.unidentifiedPaymentReverseModal.okButtonText = 'SUBMIT';
    this.paymentsSearchService.paramsToModal = payment;
    this.unidentifiedPaymentReverseModal.open(ReverseComponent);
  }
  openSuspendModal(payment: any) {
    this.unidentifiedPaymentSuspendModal.modalTitle = 'Suspend Payment';
    this.unidentifiedPaymentSuspendModal.icon = 'assets/images/model-icons/suspend-payments.svg';
    this.unidentifiedPaymentSuspendModal.modalFooter = false;
    this.unidentifiedPaymentSuspendModal.okButtonText = 'SUBMIT';
    this.paymentsSearchService.paramsToModal = payment;
    this.unidentifiedPaymentSuspendModal.open(SuspendComponent);
  }
  openRefundMethodSelectModal(payment: any) {
    this.unidentifiedPaymentRefundMethodSelectModal.modalTitle = 'Choose Refund Method';
    this.unidentifiedPaymentRefundMethodSelectModal.icon = 'assets/images/model-icons/payment-method.svg';
    this.unidentifiedPaymentRefundMethodSelectModal.modalFooter = false;
    this.unidentifiedPaymentRefundMethodSelectModal.okButtonText = 'NEXT';
    this.paymentsSearchService.paramsToModal = {};
    this.paymentsSearchService.paramsToModal = payment;
    this.paymentsSearchService.paramsToModal.disbursementMethodTypes = this.disbursementMethodTypes;
    this.unidentifiedPaymentRefundMethodSelectModal.open(DisburseSelectComponent);
  }

  getPaymentsUnidentifiedSupportData(payment: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.getPaymentsUnidentifiedSupportData(payment?.paymentSequenceNumber,
      payment?.postedDate, payment?.userId, payment?.batchId, payment?.distributionDate, PaymentOptions.disburse).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          if (data) {
            this.supportData = data;
            this.getDropdownItems(PropertyName.Disbursement_Method_Types, payment);
          }
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
        }
      );
  }

  getDropdownItems(propertyName: string, payment: any) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this.disbursementMethodTypes = sd.propertyValues;
      }
    }
    if (this.disbursementMethodTypes?.length > 0) {
      this.openRefundMethodSelectModal(payment);
    }
  }

  openDisburseModal(event: any) {
    if (event) {
      this.unidentifiedPaymentDisburseModal.modalTitle = 'Disburse Suspense';
      this.unidentifiedPaymentDisburseModal.icon = 'assets/images/model-icons/disburse-payment.svg';
      this.unidentifiedPaymentDisburseModal.modalFooter = false;
      this.unidentifiedPaymentDisburseModal.okButtonText = 'SUBMIT';
      this.unidentifiedPaymentDisburseModal.open(DisburseComponent);
    } else {
      this.paymentsSearchService.paramsToModal = null;
    }
  }
  openUpdateModal(payment: any) {
    this.unidentifiedPaymentUpdateModal.modalTitle = 'Update Suspense';
    this.unidentifiedPaymentUpdateModal.icon = 'assets/images/model-icons/update-update.svg';
    this.unidentifiedPaymentUpdateModal.modalFooter = false;
    this.unidentifiedPaymentUpdateModal.okButtonText = 'SUBMIT';
    this.paymentsSearchService.paramsToModal = payment;
    this.paymentsSearchService.paramsToModal.selectedValue = this.selectedValue;
    this.paymentsSearchService.multiCurrencyEnabled = this.multiCurrencyEnabled;
    this.unidentifiedPaymentUpdateModal.open(UpdateComponent);
  }
  openConfirmModal(value: any) {
    if (value.action) {
      if (value.status === 'success') {
        this.reloadPaymentSearch(true);
      }
    } else {
      if (value) {
        this.selectedValue = value;
        this.confirmModal.modalTitle = `Switch to ${(value === 'BillAccount' ? 'Consumer Direct' : (value === 'AgencyAccount' ? 'Agency' : 'Group'))}`;
        this.confirmModal.modalFooter = true;
        this.confirmModal.message = 'Unsaved data will be lost. Are you sure want to switch?';
        this.confirmModal.okButtonText = 'YES';
        this.confirmModal.cancelButtonText = 'NO';
        this.confirmModal.open();
      } else {
        this.paymentsSearchService.paramsToModal = null;
        this.selectedValue = BILLACCOUNT;
      }
    }
  }
  confirmModalSubmit(event: any) {
    if (event) {
      this.paymentsSearchService.paramsToModal.selectedValue = this.selectedValue;
      this.paymentsSearchService.paramsToModal.body = null;
      this.paymentsSearchService.paramsToModal.isChanged = false;
      this.openUpdateModal(this.paymentsSearchService.paramsToModal);
    } else {
      this.paymentsSearchService.paramsToModal.isChanged = true;
      this.selectedValue = this.paymentsSearchService.paramsToModal.selectedValue;
      this.openUpdateModal(this.paymentsSearchService.paramsToModal);
    }
  }
  reloadPaymentSearch(event: any) {
    if (event) {
      this.getPaymentsSearch(this.since, this.until, this.paymentMethod, this.minAmount, this.maxAmount);
    }
  }
}
