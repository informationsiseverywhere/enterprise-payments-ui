import { Component, OnInit } from '@angular/core';

import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-unidentified-details',
  templateUrl: './unidentified-details.component.html',
  styleUrls: ['./unidentified-details.component.scss']
})
export class UnidentifiedDetailsComponent implements OnInit {
  public paymentDetails: any;
  public isAccountNumber = false;
  public propertyName = PropertyName;
  constructor(
    private paymentsSearchService: PaymentsSearchService,
    private commonService: CommonService) { }

  ngOnInit() {
    this.paymentDetails = Object.assign({}, this.paymentsSearchService.paymentDetail);
    this.isAccountNumber = true;
  }

  public removeSpaces(status: string): any {
    return this.commonService.removeSpacesAndFWDash(status);
  }
}
