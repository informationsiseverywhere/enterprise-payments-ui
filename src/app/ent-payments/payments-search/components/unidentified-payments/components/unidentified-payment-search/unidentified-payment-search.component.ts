import { Component, OnInit, ViewChild, EventEmitter, Output, Input, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';

import { HalProcessor } from 'src/app/shared/services/utilities.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { UnidentifiedPaymentResultsComponent } from '../unidentified-payment-results/unidentified-payment-results.component';
import { PaymentTags, PaymentIdentifierFilterLabels } from 'src/app/ent-payments/payments-search/models/payment-enums.model';
import { PaymentCommonService } from 'src/app/ent-payments/payments-search/services/payment-common.service';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';
import { MultiselectItem } from 'src/app/shared/models/multiselect-item.model';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-unidentified-payment-search',
  templateUrl: './unidentified-payment-search.component.html',
  styleUrls: ['./unidentified-payment-search.component.scss']
})
export class UnidentifiedPaymentSearchComponent implements OnInit {
  @Input() systemDate: any;
  @Input() since: any;
  @Input() until: any;
  @Input() paymentMethod: any;
  @Input() minAmount: any;
  @Input() maxAmount: any;
  @Input() identifierType: any;
  @Input() currency: any;
  @Input() page: any;
  @Input() size: any;
  @Input() status: Array<any> = [];
  @Input() identifier: any;
  @Input() cashWithApplication: any;
  public dropdownMenuObject: any;
  private paymentTags: PaymentTags = new PaymentTags();
  public paymentIdentifierFilterLabels: PaymentIdentifierFilterLabels = new PaymentIdentifierFilterLabels();
  supportData: any;
  startDate: any;
  endDate: any;
  isGreater = false;
  returnAppId: any;
  navParam: any;
  @Output() refreshSummary: EventEmitter<any> = new EventEmitter();
  @ViewChild('resultList', { static: false }) resultList: UnidentifiedPaymentResultsComponent;
  @ViewChild('paymentFilterToggle', { static: false }) paymentFilterToggle: ElementRef;
  paymentMethodDropdown: any = [];
  UnIdentifierTypeDropdown: any = [];
  StatusDropdown: any = [];
  filterTagsList: Array<{ tag: string, name: string }> = [];
  keyUp: Subject<string> = new Subject<string>();
  userId: any;
  paymentId: any;
  public paymentSearchForm: FormGroup;
  public isGreaterAmount = false;
  public isFiltersExpand = false;
  @Input() isPaymentListLoaded = false;
  @ViewChild('filterControlList', { static: false }) filterControlList: ElementRef;
  public isShowAdvanceLink = true;
  multiCurrencyDropdown: any;
  multiCurrencyEnabled: any;
  currencyDropdown: any;
  @Output() multiCurrencyEvent: EventEmitter<any> = new EventEmitter();
  public selectedStatus: Array<MultiselectItem> = [];
  public statusMultiselectSettings = {};
  public CashWithApplicationDropdown: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private paymentCommonService: PaymentCommonService,
    private halProcessor: HalProcessor,
    private fb: FormBuilder,
    private paymentsSearchService: PaymentsSearchService,
    public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService,
    public commonService: CommonService) {
  }

  ngOnInit() {
    this.statusMultiselectSettings = {
      text: "",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      primaryKey: "name",
      showCheckbox: true,
      labelKey: 'name',
      badgeShowLimit: 1,
      autoPosition: false
    };
    this.paymentSearchForm = this.fb.group({
      startDate: ['', ValidationService.dateValidator],
      endDate: ['', ValidationService.dateValidator],
      type: [''],
      userId: [''],
      paymentId: [''],
      paymentMethod: [''],
      minAmount: [''],
      maxAmount: [''],
      currency: [''],
      status: [],
      identifier: [''],
      cashWithApplication: [''],
    });
    this.route.parent.queryParams.subscribe(
      queryParams => {
        this.returnAppId = queryParams.returnAppId;
        this.navParam = queryParams.nav;
        this.since = queryParams.since;
        this.until = queryParams.until;
        this.paymentMethod = this.halProcessor.replacePlusSignsBySpaces(queryParams.paymentMethod);
        this.minAmount = queryParams.minAmount;
        this.maxAmount = queryParams.maxAmount;
        this.identifierType = this.halProcessor.replacePlusSignsBySpaces(queryParams.identifierType);
        this.status = this.commonService.getFilterParams(queryParams.status);
        this.identifier = queryParams.identifier;
        this.cashWithApplication = queryParams.cashWithApplication;
        this.userId = queryParams.filterUserId;
        this.paymentId = queryParams.paymentId;
        this.currency = queryParams.currency;
        if (!(this.since || this.until || this.paymentMethod || this.maxAmount ||
          this.minAmount || this.identifierType || this.userId || this.paymentId || this.currency || this.status || this.identifier || this.cashWithApplication)) {
          this.paymentsSearchService.filterParam = null;
        }
        this.getPaymentsListSupportData();
      });
    this.startDate = this.since;
    this.endDate = this.until;
    this.resetStatusSelectedItems();
    this.filterTagsList = [];
    this.paymentCommonService.initialPaymentFilterOption(this.startDate, this.endDate, this.minAmount, this.maxAmount,
      this.paymentMethod, this.identifierType, this.userId, this.paymentId, this.currency, this.paymentTags, this.selectedStatus.map(e => e.name), this.identifier, this.cashWithApplication, this.filterTagsList);
  }

  public resetStatusSelectedItems() {
    if (this.status && this.status.length > 0) {
      this.selectedStatus = [];
      for (const sd of this.status) {
        this.selectedStatus.push(new MultiselectItem(this.halProcessor.replacePlusSignsBySpaces(sd)));
      }
    } else {
      this.selectedStatus = [];
    }
  }

  public checkAmountValid(filterKey?: any): any {
    if (this.maxAmount !== '' && this.minAmount !== '' && Number(this.maxAmount) < Number(this.minAmount)) {
      this.isGreaterAmount = true;
    } else {
      this.isGreaterAmount = false;
    }
  }

  public convertToUpperCase(event?: any, property?: any): void {
    if (event) { this.form[property].patchValue(event); }
  }

  get form() { return this.paymentSearchForm.controls; }

  public resetIdentifiedPayments(): void {
    this.filterTagsList = [];
    this.startDate = undefined;
    this.endDate = undefined;
    this.selectedStatus = [];
    this.router.navigate(['/ent-payments/payments-search/unidentified-payments'], {
      queryParams: {
        returnAppId: this.returnAppId,
        nav: this.navParam,
        roleSequenceId: this.securityEngineService.roleSequenceId
      }
    });
  }

  public isNotSelecting(): any {
    const currentParams = this.route.snapshot.queryParams;
    for (const key in currentParams) {
      if (currentParams.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  private getPaymentsListSupportData(): void {
    this.paymentsSearchService.getPaymentsListSupportData().subscribe(
      data => {
        if (data) {
          this.supportData = data;
          this.commonService.getMultiselectItems(this, 'Status', this.supportData);
          this.getDropdownItems(PropertyName.Currency);
          this.getDropdownItems(PropertyName.CashWithApplication);
          this.getDropdownItems(PropertyName.paymentMethod);
          this.getDropdownItems(PropertyName.UnIdentifierType);
          this.getDropdownItems(PropertyName.MultiCurrency);
        }
      });
  }

  private getDropdownItems(propertyName?: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        console.log(this.UnIdentifierTypeDropdown)
        if (!this.paymentMethod && propertyName === PropertyName.paymentMethod && sd.defaultValue) {
          this.paymentSearchForm.controls['paymentMethod'].setValue(sd.defaultValue);
        }
        if (!this.identifierType && propertyName === PropertyName.UnIdentifierType && sd.defaultValue) {
          this.paymentSearchForm.controls['type'].setValue(sd.defaultValue);
        }
        if (!this.cashWithApplication && propertyName === PropertyName.CashWithApplication && sd.defaultValue) {
          this.paymentSearchForm.controls['cashWithApplication'].setValue(sd.defaultValue);
        }
        if (!this.currency && propertyName === PropertyName.Currency && sd.defaultValue) {
          this.currency = sd.defaultValue;
        }
      }
    }
    if (this.multiCurrencyDropdown) {
      this.multiCurrencyEnabled = this.multiCurrencyDropdown[0];
      this.multiCurrencyEvent.emit(this.multiCurrencyEnabled);
    }
  }

  public checkValid(event?: any, name?: any): void {
    this[name] = event.formatted;
    if (this.startDate && this.endDate && this.startDate > this.endDate) {
      this.isGreater = true;
    } else {
      this.isGreater = false;
    }
  }

  public removeTagsFilter(name?: string): void {
    if (name === 'since') {
      this.startDate = undefined;
    }
    if (name === 'until') {
      this.endDate = undefined;
    }
    if (name === 'fromAmount') {
      this.minAmount = '';
    }
    if (name === 'toAmount') {
      this.maxAmount = '';
    }
    if (name === 'paymentMethod') {
      this.paymentMethod = '';
    }
    if (name === 'identifierType') {
      this.identifierType = '';
      this.identifier = '';
      this.paymentCommonService.removeTagsItem('identifier', this.filterTagsList);
    }
    if (name === 'userId') {
      this.userId = '';
    }
    if (name === 'paymentId') {
      this.paymentId = '';
    }
    if (name === 'currency') {
      this.currency = '';
    }
    if (name.startsWith("status_")) {
      const statusName = name.substring(name.indexOf("_") + 1);
      this.status = this.status.filter(i => this.commonService.removeSpacesAndFWDash(this.halProcessor.replacePlusSignsBySpaces(i)) != statusName);
      this.resetStatusSelectedItems();
    }
    if (name === 'identifier') {
      this.identifier = '';
    }
    if (name === 'cashWithApplication') {
      this.cashWithApplication = '';
    }
    let filterParam = {};
    filterParam = {
      since: this.startDate,
      until: this.endDate,
      toAmount: this.maxAmount,
      fromAmount: this.minAmount,
      paymentMethod: this.paymentMethod,
      identifierType: this.identifierType,
      userId: this.userId,
      paymentId: this.paymentId,
      currency: this.currency,
      status: this.status,
      identifier: this.identifier,
      cashWithApplication: this.cashWithApplication
    };
    this.paymentsSearchService.filterParam = filterParam;
    this.refreshSummary.emit(true);
    this.paymentCommonService.removeTagsItem(name, this.filterTagsList);
  }

  public togglePayment(): void {
    this.paymentsSearchService.filterParam = null;
    const navigationExtras: NavigationExtras = {
      queryParams: {
        returnAppId: this.returnAppId,
        nav: this.navParam
      }
    };
    this.router.navigate(['/ent-payments/payments-search/identified-payments'], navigationExtras);
  }

  public searchUnidentifiedPayments(): void {
    this.filterTagsList = [];
    this.paymentCommonService.initialPaymentFilterOption(this.startDate, this.endDate, this.minAmount, this.maxAmount,
      this.paymentMethod, this.identifierType, this.userId, this.paymentId, this.currency, this.paymentTags, this.selectedStatus.map(e => e.name), this.identifier, this.cashWithApplication, this.filterTagsList);
    this.paymentCommonService.searchFilter(null, null, this.startDate, this.endDate, this.minAmount, this.maxAmount,
      this.paymentMethod, this.identifierType, this.userId, this.paymentId, this.currency, this.paymentTags, this.selectedStatus.map(e => e.name), this.identifier, this.cashWithApplication, this.filterTagsList);
    this.refreshSummary.emit(true);
    if (this.filterTagsList.length > 0) {
      this.moreFilters('isFiltersExpand');
      this.paymentFilterToggle.nativeElement.classList.remove('show');
    }
  }

  public moreFilters(value?: any): void {
    window.scroll(0, 0);
    this[value] = !this[value];
  }

  public openSecurity(event?: any): void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, 'searchSection', this.multiCurrencyEnabled === 'true' ? undefined : ['searchCurrency']);
  }

  public typesSelected(event?: any): void {
    this.identifier = '';
  }

  public isFilterBadgeExisting(badgeName?: string, badgeList?: Array<{ tag: string, name: string }>): boolean {
    if (badgeList.filter(i => i.name === badgeName).length > 0) {
      return true;
    }
    return false;
  }
}
