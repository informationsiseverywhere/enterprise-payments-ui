import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DateService } from 'src/app/shared/services/date.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { UnidentifiedPaymentResultsComponent } from './unidentified-payment-results/unidentified-payment-results.component';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-unidentified-payments',
  templateUrl: './unidentified-payments.component.html'
})
export class UnidentifiedPaymentsComponent implements OnInit {
  @ViewChild('paymentList', { static: true }) paymentList: UnidentifiedPaymentResultsComponent;
  @ViewChild('resultAction', { static: false }) resultAction: any;
  accountId: any;
  accountNumber: any;
  systemDate: any;
  isShowActions: boolean;
  singleAction: boolean;
  paymentsList: any = [];
  query: string;
  filters: string;
  since: any;
  until: any;
  paymentMethod: any;
  minAmount: any;
  maxAmount: any;
  page: any;
  size: any;
  userId: any;
  paymentId: any;
  identifierType: any;
  multiCurrencyEnabled: any;
  currency: any;
  status: any;
  identifier: any;
  cashWithApplication: any;
  constructor(
    private route: ActivatedRoute,
    public securityEngineService: SecurityEngineService,
    private dateService: DateService,
    private commonService: CommonService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.since = queryParams.since;
        this.until = queryParams.until;
        this.paymentMethod = queryParams.paymentMethod;
        this.minAmount = queryParams.minAmount;
        this.maxAmount = queryParams.maxAmount;
        this.page = queryParams.page;
        this.size = queryParams.size;
        this.userId = queryParams.filterUserId;
        this.paymentId = queryParams.paymentId;
        this.identifierType = queryParams.identifierType;
        this.currency = queryParams.currency;
        this.status = this.commonService.getFilterParams(queryParams.status);
        this.identifier = queryParams.identifier;
        this.cashWithApplication = queryParams.cashWithApplication;
      }
    );
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
  }

  isPaymentEmpty(event: any) {
    this.paymentsList = event;
  }

  reloadSummary(event: any) {
    this.paymentList.reloadPaymentsSearch(event);
  }

  multiCurrencyEnable(event: any) {
    this.multiCurrencyEnabled = event;
  }
}
