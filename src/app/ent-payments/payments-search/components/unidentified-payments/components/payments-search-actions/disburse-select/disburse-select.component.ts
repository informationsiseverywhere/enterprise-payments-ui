import { Component, OnInit } from '@angular/core';

import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { CommonService } from 'src/app/shared/services/common.service';

@Component({
  selector: 'app-disburse-select',
  templateUrl: './disburse-select.component.html',
  styleUrls: ['./disburse-select.component.scss']
})

export class DisburseSelectComponent implements OnInit {
  paymentMethodSelected: any;
  disbursementMethodTypes: any;

  constructor(
    private modal: ModalComponent,
    private paymentsSearchService: PaymentsSearchService,
    public commonService: CommonService) { }

  ngOnInit() {
    if (this.paymentsSearchService.paramsToModal) {
      this.disbursementMethodTypes = this.paymentsSearchService.paramsToModal.disbursementMethodTypes;
      this.paymentMethodSelected = this.disbursementMethodTypes[0];
      this.paymentsSearchService.paramsToModal.paymentMethodSelected = this.paymentMethodSelected;
    }
  }

  showActiveControls(value: any) {
    this.paymentMethodSelected = value;
    this.paymentsSearchService.paramsToModal.paymentMethodSelected = value;
  }

  navigateToAddPlan() {
    this.modal.close(true);
  }
}
