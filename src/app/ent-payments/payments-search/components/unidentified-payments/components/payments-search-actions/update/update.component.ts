import { Component, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { QueryParam } from 'src/app/libs/select/models/select.model';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { DateService } from 'src/app/shared/services/date.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { ValidationService } from 'src/app/libs/form-validation-control/services/validation.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';
import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
  providers: [CurrencyPipe]
})
export class UpdateComponent implements OnInit {
  paymentsUpdateForm: FormGroup;
  payment: any;
  Payment_TypeDropdown: any;
  Policy_SymbolDropdown: any;
  PayableItemDropdown: any;
  Suspense_ReasonDropdown: any;
  systemDate: any;
  dueDate: any;
  isUnValidDateReceived: boolean;
  accountSelected: any;
  holdSuspenseIndicator = false;
  billaccount: any;
  selectedValue: string;
  paymentAmount: any;
  additionalId: any;
  paymentType: any;
  policySymbol: any;
  policyAgreement: any;
  payableItem: any;
  suspenseReason: any;
  paymentId: any;
  paymentDetails: any;
  supportData: any;
  isChanged = false;
  disablePaymentId = false;
  previousBillAccount: string;
  previousGroupAccount: string;
  private previousAgencyAccount: string;
  groupAccountDisplay: any;
  paymentComments: any;
  overrideBusinessGroupEditIndicator = false;
  businessGroup: any;
  accountBusinessGroup: any;
  suspenseReasonDefaultValue: any;
  isSubmitEnabled: boolean;
  businessGroupCode: any;
  queryParam: any;
  apiUrl: string;
  currency: any = 'USD';
  queryParamGroup: any;
  queryParamAgency: any;
  transferToAgency: any;
  transferToDirect: any;
  transferToGroup: any;
  enablePaymentId = true;
  private updatePaymentOptions = [
    { name: PropertyName.BillAccount, securityName: 'paymentUpdateConsumerDirect' },
    { name: PropertyName.GroupAccount, securityName: 'paymentUpdateGroup' },
    { name: PropertyName.AgencyAccount, securityName: 'paymentUpdateAgency' },
  ];
  oneTimePaymentIntegrationDropdown: any;
  oneTimePaymentIntegration: boolean;

  constructor(
    private paymentsSearchService: PaymentsSearchService,
    private loadingService: LoadingService,
    private fb: FormBuilder,
    private dateService: DateService,
    private appConfig: AppConfiguration,
    private hostUrlService: DynamicHostUrlService,
    public commonService: CommonService,
    private modal: ModalComponent,
    private paymentService: PaymentService,
    public securityEngineService: SecurityEngineService
  ) { }

  ngOnInit() {
    this.getPaymentsSupportData();
    this.paymentsUpdateForm = this.fb.group({
      billAccountNumber: ['', Validators.maxLength(30)],
      groupAccount: ['', Validators.maxLength(20)],
      agencyAccount: ['', Validators.maxLength(20)],
      paymentAmount: [''],
      paymentId: ['', Validators.maxLength(10)],
      paymentType: [''],
      policySymbol: [''],
      policyAgreement: ['', Validators.maxLength(25)],
      payableItem: [''],
      dueDate: ['', ValidationService.dateValidator],
      suspenseReason: [''],
      additionalId: ['', Validators.maxLength(20)],
      paymentComments: [''],
      holdSuspenseIndicator: [''],
      overrideBusinessGroupEditIndicator: [''],
      businessGroup: ['']
    });
    if (this.paymentsSearchService.paramsToModal) {
      this.payment = this.paymentsSearchService.paramsToModal;
      this.selectedValue = this.paymentsSearchService.paramsToModal.selectedValue;
      if (this.payment.currency) {
        this.currency = this.payment.currency;
      }
      if (this.payment.body) {
        this.selectedValue = this.checkSelectedValue(this.payment.body);
        this.renderData(this.payment);
      } else {
        this.getPaymentDetails();
      }
    }

    // if (this.paymentsSearchService.multiCurrencyEnabled) {
    const isMultiCurrencyEnable = this.paymentsSearchService.multiCurrencyEnabled;
    const QUERYPARAMS = ((isMultiCurrencyEnable === 'true') ? `type:DirectBill,type:GroupBill,currency:${this.currency}` : 'type:DirectBill,type:GroupBill');
    this.queryParam = new QueryParam('', QUERYPARAMS, null, 1, 10);
    const QUERYPARAMSGroup = ((isMultiCurrencyEnable === 'true') ? `type:Group,currency:${this.currency}` : 'type:Group');
    this.queryParamGroup = new QueryParam('', QUERYPARAMSGroup, null, 1, 10);
    const QUERYPARAMSAgency = ((isMultiCurrencyEnable === 'true') ? `type:Agency,currency:${this.currency}` : 'type:Agency');
    this.queryParamAgency = new QueryParam('', QUERYPARAMSAgency, null, 1, 10);
    this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts';
    // }
    this.getSystemDate();
    this.getPaymentDetailsSupportData(this.payment);
  }

  checkSelectedValue(payment: any): string {
    const selectedValue = this.selectedValue;
    if (selectedValue === PropertyName.BillAccount) {
      if (!payment.transferToDirect) {
        if (!payment.transferToGroup) {
          return PropertyName.AgencyAccount;
        }
        return PropertyName.GroupAccount;
      }
      return PropertyName.BillAccount;
    } else if (selectedValue === PropertyName.GroupAccount) {
      return PropertyName.GroupAccount;
    } else if (selectedValue === PropertyName.AgencyAccount) {
      return PropertyName.AgencyAccount;
    }
  }
  getPaymentDetailsSupportData(payment?: any) {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.getPaymentDetailsSupportData(payment).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.PayableItem);
          this.getDropdownItems(PropertyName.Policy_Symbol);
          this.getDropdownItems(PropertyName.Payment_Type);
          this.getDropdownItems(PropertyName.Suspense_Reason);
          this.changeEnablePaymentIdValidation(PropertyName.enablePaymentId);
          if (!this.policyAgreement) {
            this.paymentsUpdateForm.get('payableItem').setValue(null);
            this.paymentsUpdateForm.get('policySymbol').setValue(null);
          }
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }
  getSystemDate() {
    this.dateService.getDate();
    this.dateService.systemDate.subscribe(
      data => {
        this.systemDate = data;
      });
  }

  getDropdownItems(propertyName?: any) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
  }

  changeEnablePaymentIdValidation(propertyName?: any) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this.enablePaymentId = sd.propertyValues[0] === 'true' ? true : false;
        if (!this.enablePaymentId) {
          this.paymentsUpdateForm.get('paymentId').setValidators([Validators.maxLength(11)]);
          this.paymentsUpdateForm.get('paymentId').updateValueAndValidity();
        }
      }
    }
  }

  selectedGroupAccount(data?: any) {
    this.accountSelected = data;
    if (data) {
      this.accountSelected = data;
      this.billaccount = data.accountNumber;
    }
  }

  selectedAccount(data?: any) {
    this.isSubmitEnabled = (this.businessGroup) ? false : true;
    if (data) {
      this.accountSelected = data;
      this.accountBusinessGroup = this.accountSelected.businessGroup;
      if (this.businessGroup && this.businessGroup !== '') {
        if (this.businessGroupCode === this.accountBusinessGroup) {
          this.isSubmitEnabled = true;
        } else {
          this.isSubmitEnabled = false;
        }
      } else {
        this.isSubmitEnabled = true;
      }
    }
  }

  changeAccount(event?: any) {
    if (event) {
      this.billaccount = event;
    }
  }

  getPaymentDetails() {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.paymentDetails(this.payment).subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        this.paymentDetails = data;
        this.selectedValue = this.checkSelectedValue(data);
        this.renderData(data);
        this.dueDate = this.paymentDetails.dueDate;
      },
      err => {
        this.commonService.handleErrorResponse(err);
      });
  }

  holdSuspenseChanged(event?: any) {
    this.holdSuspenseIndicator = event;
    this.paymentsUpdateForm.get('suspenseReason') && this.paymentsUpdateForm.get('suspenseReason').setValue(undefined);
    this.paymentsUpdateForm.removeControl('suspenseReason');
    if (event) {
      this.paymentsUpdateForm.addControl('suspenseReason', new FormControl('', Validators.required));
      this.suspenseReason = this.suspenseReasonDefaultValue
    }
  }

  public displayError(event?: any): void {
    if (!event) {
      if (this.selectedValue === PropertyName.GroupAccount) {
        this.paymentsUpdateForm.get('groupAccount').setErrors({ required: true });
      } else if (this.selectedValue === PropertyName.AgencyAccount) {
        this.paymentsUpdateForm.get('agencyAccount').setErrors({ required: true });
      }
    } else {
      this.billaccount = event;
    }
  }

  public paymentsUpdate(): void {
    let identifierType = '';
    if (this.selectedValue === PropertyName.BillAccount) {
      identifierType = PropertyName.ConsumerDirect;
    } else if (this.selectedValue === PropertyName.AgencyAccount) {
      identifierType = PropertyName.Agency;
    } else {
      identifierType = PropertyName.Group;
    }
    const body: any = {
      identifier: this.billaccount ? this.billaccount : '',
      identifierType,
      dueDate: this.dueDate ? this.dueDate : null,
      holdSuspenseIndicator: this.holdSuspenseIndicator ? this.holdSuspenseIndicator : false,
      paymentType: this.paymentType ? this.paymentType : '',
      paymentAmount: this.paymentAmount ? this.paymentAmount : '',
      suspenseReason: this.suspenseReason ? this.suspenseReason : '',
      paymentComments: this.paymentComments ? this.paymentComments : '',
      paymentId: this.paymentId ? this.paymentId : '',
      additionalId: this.additionalId ? this.additionalId : '',
      payableItem: '',
      policyNumber: '',
      policySymbol: ''
    };
    if (this.selectedValue === PropertyName.BillAccount) {
      body.payableItem = this.payableItem ? this.payableItem : '';
      body.policyNumber = this.policyAgreement ? this.policyAgreement : '';
      body.policySymbol = this.policySymbol ? this.policySymbol : '';
      body.additionalId = '';
      body.overrideBusinessGroupEditIndicator = this.overrideBusinessGroupEditIndicator ? this.overrideBusinessGroupEditIndicator : false;
      body.businessGroup = this.businessGroup ? this.businessGroup : '';
    }
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.paymentAction(this.payment, body, '_update').subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        let modalAction: any;
        modalAction = {
          action: 'submit',
          status: 'success'
        };
        this.modal.close(modalAction);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      }
    );

  }
  private renderData(payment?: any): void {
    if (payment.body) {
      this.paymentAmount = payment.body.paymentAmount;
      this.billaccount = undefined;
      if ((this.selectedValue === PropertyName.BillAccount && payment.identifierType === PropertyName.ConsumerDirect)
        || (this.selectedValue === PropertyName.GroupAccount && payment.identifierType === PropertyName.Group)
        || (this.selectedValue === PropertyName.AgencyAccount && payment.identifierType === PropertyName.Agency)) {
        this.billaccount = payment.body.billaccount ? payment.body.billaccount : payment.identifier;
      }
      if (!this.billaccount && payment.isChanged) {
        if (this.selectedValue === PropertyName.BillAccount) {
          this.billaccount = payment.body.previousBillAccount;
        } else if (this.selectedValue === PropertyName.AgencyAccount) {
          this.billaccount = payment.body.previousAgencyAccount;
          this.paymentsUpdateForm.controls.agencyAccount.setValue(this.billaccount);
        } else {
          this.billaccount = payment.body.previousGroupAccount;
          this.paymentsUpdateForm.controls.groupAccount.setValue(this.billaccount);
        }
      }
      this.paymentAmount = payment.paymentAmount;
      this.additionalId = payment.body.additionalId;
      this.paymentType = payment.body.paymentType;
      this.dueDate = payment.body.dueDate;
      this.policySymbol = payment.body.policySymbol;
      this.policyAgreement = payment.body.policyAgreement;
      this.payableItem = payment.body.payableItem;
      this.paymentComments = payment.body.paymentComments;
      this.holdSuspenseIndicator = payment.body.holdSuspenseIndicator;
      this.suspenseReason = payment.body.suspenseReason;
      this.paymentId = payment.body.paymentId;
      this.overrideBusinessGroupEditIndicator = payment.body.overrideBusinessGroupEditIndicator;
      this.businessGroup = payment.body.businessGroup;
      this.businessGroupCode = payment.body.businessGroupCode;
      this.transferToAgency = payment.body.transferToAgency;
      this.transferToDirect = payment.body.transferToDirect;
      this.transferToGroup = payment.body.transferToGroup;
    } else {
      if (payment.selectedValue === PropertyName.BillAccount || payment.selectedValue === PropertyName.GroupAccount ||
        payment.selectedValue === PropertyName.AgencyAccount) {
        this.billaccount = payment.identifier;
        if (payment.paymentId) { this.paymentId = payment.paymentId; }
        if (payment.policyAgreement) { this.policyAgreement = payment.policyAgreement; }
      }
      this.billaccount = payment.identifier;
      if (payment.identifierType === PropertyName.Policy ||
        (this.selectedValue === PropertyName.GroupAccount && payment.identifierType != PropertyName.Group)
        || (this.selectedValue === PropertyName.BillAccount && payment.identifierType != PropertyName.ConsumerDirect)
        || (this.selectedValue === PropertyName.AgencyAccount && payment.identifierType != PropertyName.Agency)) {
        this.billaccount = undefined;
      }
      this.paymentAmount = payment.paymentAmount;
      this.additionalId = payment.additionalId;
      this.paymentType = payment.paymentType;
      this.policySymbol = payment.policySymbol;
      this.policyAgreement = payment.policyNumber;
      this.payableItem = payment.payableItem;
      this.paymentComments = payment.paymentComments;
      this.holdSuspenseIndicator = payment.holdSuspenseIndicator;
      this.suspenseReason = payment.suspenseReason;
      this.paymentId = payment.paymentId;
      this.overrideBusinessGroupEditIndicator = payment.overrideBusinessGroupEditIndicator;
      this.businessGroup = payment.businessGroup;
      this.businessGroupCode = payment.businessGroupCode;
      this.transferToAgency = payment.transferToAgency;
      this.transferToDirect = payment.transferToDirect;
      this.transferToGroup = payment.transferToGroup;
    }
    if (this.selectedValue === PropertyName.BillAccount) { this.selectedAccount(); }
    if (payment.isChanged) {
      this.isChanged = payment.isChanged;
    }
    this.checkSecurityOptions();
  }

  public showActiveControls(value?: any): void {
    if (this.selectedValue === PropertyName.BillAccount) {
      this.previousBillAccount = this.billaccount;
    } else if (this.selectedValue === PropertyName.AgencyAccount) {
      this.previousAgencyAccount = this.billaccount;
    } else {
      this.previousGroupAccount = this.billaccount;
    }
    if (value === PropertyName.BillAccount) {
      this.paymentsUpdateForm.controls.groupAccount.setErrors(null);
    }
    const tempSelectedValue = this.selectedValue;
    this.selectedValue = value;
    const body = {
      paymentAmount: this.paymentAmount,
      billaccount: this.billaccount,
      additionalId: this.additionalId,
      paymentId: this.paymentId,
      paymentType: this.paymentType,
      dueDate: this.dueDate,
      policySymbol: this.policySymbol,
      policyAgreement: this.policyAgreement,
      payableItem: this.payableItem,
      paymentComments: this.paymentComments,
      holdSuspenseIndicator: this.holdSuspenseIndicator,
      suspenseReason: this.suspenseReason,
      previousBillAccount: this.previousBillAccount,
      previousGroupAccount: this.previousGroupAccount,
      previousAgencyAccount: this.previousAgencyAccount,
      overrideBusinessGroupEditIndicator: this.overrideBusinessGroupEditIndicator,
      businessGroup: this.businessGroup,
      transferToAgency: this.transferToAgency,
      transferToDirect: this.transferToDirect,
      transferToGroup: this.transferToGroup
    };

    for (const field in this.paymentsUpdateForm.controls) {
      if (field) {
        const control = this.paymentsUpdateForm.get(field);
        if (control) {
          if (control.dirty && control.touched && (control.value != undefined && control.value != '')) {
            this.isChanged = true;
            break;
          }
        }
      }
    }

    this.paymentsSearchService.paramsToModal.body = body;
    if (this.isChanged) {
      this.paymentsSearchService.paramsToModal.selectedValue = tempSelectedValue;
      this.modal.close(value);
    } else {
      this.renderData(this.payment);
    }
  }

  isValidDateReceivedEntered() {
    if (this.dueDate > this.systemDate) {
      return true;
    }
    return false;
  }
  resetDateValidate() {
    this.isUnValidDateReceived = false;
  }
  resetPolicyInfo(event?: any) {
    if (!event) {
      this.policySymbol = undefined;
      this.payableItem = undefined;
      this.policyAgreement = undefined;
    } else {
      this.getDropdownItems(PropertyName.Policy_Symbol);
      this.getDropdownItems(PropertyName.PayableItem);
      this.policyAgreement = event;
    }
  }

  overrideBusinessGroup(event?: any) {
    this.overrideBusinessGroupEditIndicator = event;
    if (this.businessGroup && this.businessGroup !== '') {
      this.isSubmitEnabled = event;
    }
  }

  public checkSecurityOptions(): any {
    if (this.selectedValue) {
      const currentSelectedItem = this.updatePaymentOptions.find(item => item.name === this.selectedValue);
      if (currentSelectedItem && this.securityEngineService.checkVisibility(currentSelectedItem.securityName, PropertyName.Show)) { return; }
    }
    for (const item of this.updatePaymentOptions) {
      if (this.securityEngineService.checkVisibility(item.securityName, PropertyName.Show)) {
        this.showActiveControls(item.name);
        break;
      }
    }
  }

  private getPaymentsSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentsSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getItems(PropertyName.oneTimePaymentIntegration);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    let oneTimePaymentIntegration = 'false';
    this.oneTimePaymentIntegrationDropdown && (oneTimePaymentIntegration = this.oneTimePaymentIntegrationDropdown[0]);
    if (oneTimePaymentIntegration === 'true') {
      this.oneTimePaymentIntegration = true;
      if (this.payment.paymentMethod === PropertyName.AgencySweep) {
        this.disablePaymentId = true;
        this.paymentsUpdateForm.get('paymentId').setValidators([Validators.maxLength(11)]);
        this.paymentsUpdateForm.get('paymentId').updateValueAndValidity();
      }
    }
  }
}
