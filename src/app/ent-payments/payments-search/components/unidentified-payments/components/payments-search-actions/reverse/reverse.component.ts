import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-reverse',
  templateUrl: './reverse.component.html'
})
export class ReverseComponent implements OnInit {
  public reason: string;
  private supportData: any;
  public ReversalReasonDropdown: any = [];
  public paymentsReverseForm: FormGroup;
  public payment: any;

  constructor(
    private paymentsSearchService: PaymentsSearchService,
    private loadingService: LoadingService,
    private fb: FormBuilder,
    private commonService: CommonService,
    private modal: ModalComponent
  ) { }

  ngOnInit() {
    this.paymentsReverseForm = this.fb.group({
      ReversalReason: ['', Validators.required]
    });
    if (this.paymentsSearchService.paramsToModal) {
      this.payment = this.paymentsSearchService.paramsToModal;
    }
    this.getPaymentsUnidentifiedSupportData();
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (propertyName === PropertyName.ReversalReason && sd.defaultValue) {
          this.paymentsReverseForm.controls['ReversalReason'].setValue(sd.defaultValue);
        }
      }
    }
  }
  private getPaymentsUnidentifiedSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.getPaymentsUnidentifiedSupportData(this.payment.paymentSequenceNumber,
      this.payment.postedDate, this.payment.userId, this.payment.batchId, this.payment.distributionDate, '_reverse').subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getDropdownItems(PropertyName.ReversalReason);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      }
    );
  }

  public paymentsReverse(): void {
    const body = {
      reason: this.reason
    };
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.paymentAction(this.payment, body, '_reverse').subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.modal.close(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      }
    );
  }
}
