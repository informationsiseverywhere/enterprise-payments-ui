import { Component, OnInit } from '@angular/core';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';

@Component({
  selector: 'app-suspend',
  templateUrl: './suspend.component.html'
})

export class SuspendComponent implements OnInit {
  public payment: any;
  constructor(
    private paymentsSearchService: PaymentsSearchService,
    private loadingService: LoadingService,
    private commonService: CommonService,
    private modal: ModalComponent
  ) { }

  ngOnInit() {
    if (this.paymentsSearchService.paramsToModal) {
      this.payment = this.paymentsSearchService.paramsToModal;
    }
  }

  public paymentsSuspend(): void {
    const body = {};
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.paymentAction(this.payment, body, '_suspend').subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.modal.close(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      }
    );
  }
}

