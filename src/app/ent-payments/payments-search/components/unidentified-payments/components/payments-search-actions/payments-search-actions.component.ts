import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core'

import { LoadingService } from 'src/app/shared/services/loading.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
    selector: 'app-payments-search-actions',
    templateUrl: './payments-search-actions.component.html'
})
export class PaymentsSearchActionsComponent implements OnInit {
    @Input() payment: any;
    @Input() isPaymentListLoaded: boolean;
    @Output() actionClicked: EventEmitter<any> = new EventEmitter<any>();
    paymentDetails: any;
    updateIndicator = false;
    disburseIndicator = false;
    reverseIndicator = false;
    writeOffIndicator = false;
    suspendIndicator = false;

    constructor(
        private paymentsSearchService: PaymentsSearchService,
        private loadingService: LoadingService,
        private commonService: CommonService,
        public securityEngineService: SecurityEngineService
    ) { }

    ngOnInit() {}

    public eventClick(): void {
        this.paymentsSearchService.paramsToModal = undefined;
        this.getPaymentDetails();
    }

    private getPaymentDetails(): void {
        this.loadingService.toggleLoadingIndicator(true);
        this.paymentsSearchService.paymentDetails(this.payment).subscribe(
            data => {
                this.loadingService.toggleLoadingIndicator(false);
                this.paymentDetails = data;
                this.paymentsSearchService.accountPaymentData = this.paymentDetails;
                this.checkPaymentOptions();
            },
            err => {
                this.commonService.handleErrorResponse(err);
            });
    }

    checkPaymentOptions() {
        this.reverseIndicator = false;
        this.writeOffIndicator = false;
        this.updateIndicator = false;
        this.disburseIndicator = false;
        this.suspendIndicator = false;

        if (this.paymentDetails) {
            for (const sd of this.paymentDetails.links) {
                if (sd.rel === '_update') {
                    this.updateIndicator = true;
                    continue;
                }
                if (sd.rel === '_disburse') {
                    this.disburseIndicator = true;
                    continue;
                }
                if (sd.rel === '_reverse') {
                    this.reverseIndicator = true;
                    continue;
                }

                if (sd.rel === '_suspend') {
                    this.suspendIndicator = true;
                    continue;
                }
                if (sd.rel === '_writeOff') {
                    this.writeOffIndicator = true;
                    continue;
                }
            }
        }
        if (!this.paymentDetails.transferToDirect && !this.paymentDetails.transferToDirect && !this.paymentDetails.transferToAgency) {
          this.updateIndicator = false;
        }
    }

    performAction(action: string) {
        this.actionClicked.emit(action);
    }
}
