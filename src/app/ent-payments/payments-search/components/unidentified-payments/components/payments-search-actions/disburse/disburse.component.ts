import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { QueryParam } from 'src/app/libs/select/models/select.model';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { environment } from 'src/environments/environment';
import { PaymentOptions, PropertyName } from 'src/app/shared/enum/common.enum';
import { PaymentService } from 'src/app/ent-payments/batch-payments/services/payments.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-disburse',
  templateUrl: './disburse.component.html',
  styleUrls: ['./disburse.component.scss']
})
export class DisburseComponent implements OnInit {
  paymentSequenceNumber: string;
  sequenceNumber: string;
  paymentDate: string;
  paymentAmount: number;
  reason: string;
  supportData: any;
  payorData: any;
  Disbursement_ReasonDropdown: any = [];
  paymentsDisburseForm: FormGroup;
  isCheckNumbers = false;
  checkNumber: number;
  payorName: string;
  payorAddress: string;
  paymentMethod: string;
  payment: any;
  paymentMethodSelected: any;
  apiUrl: string;
  queryParam: QueryParam;
  clientSelected: any;
  payorId: any;
  accountPaymentData: any;
  addressSequenceNumber: any;
  profileDetails: any;
  oneTimePaymentIntegrationDropdown: any;
  oneTimePaymentIntegration: boolean;
  public typeData: any = ['Search By Account', 'Search By Payee'];
  selectedType: any;
  public accountNumber: any;
  constructor(
    private loadingService: LoadingService,
    private fb: FormBuilder,
    private alertService: AlertService,
    private modal: ModalComponent,
    private paymentService: PaymentService,
    private commonService: CommonService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    private paymentsSearchService: PaymentsSearchService,
    public securityEngineService: SecurityEngineService) { }

  ngOnInit() {
    this.checkDisburseSelectType();
    this.getPaymentsSupportData();
    if (this.paymentsSearchService.paramsToModal) {
      this.payment = this.paymentsSearchService.paramsToModal;
      this.paymentMethodSelected = this.paymentsSearchService.paramsToModal.paymentMethodSelected;
      if (this.paymentMethodSelected === PropertyName.CreditCard || this.paymentMethodSelected === PropertyName.EFTACH) {
        this.paymentMethodSelected = PropertyName.ManualElectronic;
        if (this.paymentsSearchService.accountPaymentData) {
          this.accountPaymentData = this.paymentsSearchService.accountPaymentData;
        }
      }
    }
    this.paymentsDisburseForm = this.fb.group({
      checkNumber: [0, this.paymentMethodSelected === PropertyName.ManualCheck ?
        Validators.compose([Validators.required, Validators.maxLength(9)]) : Validators.maxLength(9)],
      isCheckNumbers: [false],
      payorName: ['', Validators.required],
      Disbursement_Reason: ['', Validators.required]
    });
    this.getPaymentsUnidentifiedSupportData();
  }

  public showActiveControls(selectedValue?: string): void {
    this.selectedType = selectedValue;
    this.clientSelected = null;
   
    if (this.selectedType === this.typeData[0]) {
      this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.directBilling) + 'v1/billing/accounts';
      this.queryParam = new QueryParam('', 'accountNumber,policyNumber', null, 1, 10);
    } else {
      this.apiUrl = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.client) + 'v1/parties';
      this.queryParam = new QueryParam('', null, '_score', 1, 10);
    }
  }
  public selectedAccount(data?: any): void {
    if (data) {
      this.clientSelected = data;
      this.payorId = this.clientSelected.payorId;
      this.payorName = this.clientSelected.payorName;
      if (this.clientSelected?.payorAddress && this.clientSelected?.payorAddressId) {
        this.payorAddress = this.clientSelected.payorAddress;
        this.addressSequenceNumber = this.clientSelected.payorAddressId;
        this.clientSelected.status = true;
      } else {
        this.payorAddress = null;
        this.payorId = null;
        this.addressSequenceNumber = null;
        this.clientSelected.status = false;
        this.paymentsDisburseForm.controls.payorName.setErrors({ incorrect: true });
      }
    } else {
      this.clientSelected = data;
    }
  }
  selectedClient(data?: any) {
    if (data) {
      this.clientSelected = data;
      this.payorId = this.clientSelected.partyId;
      this.payorName = this.clientSelected.name;
      if (this.clientSelected.addresses) {
        if (this.checkBillingAddress(this.clientSelected.addresses)) {
          const address = this.checkBillingAddress(this.clientSelected.addresses);
          this.payorAddress = address.address;
          this.addressSequenceNumber = address.addressSequence;
          this.clientSelected.status = true;
        } else {
          this.payorAddress = null;
          this.payorId = null;
          this.addressSequenceNumber = null;
          this.clientSelected.status = false;
          this.paymentsDisburseForm.controls.payorName.setErrors({ incorrect: true });
          setTimeout(() => {
            this.alertService.error('Billing Address not found. Please add the address.');
          }, 1000);
        }
      } else {
        this.payorAddress = null;
        this.payorId = null;
        this.addressSequenceNumber = null;
        this.clientSelected.status = false;
        this.paymentsDisburseForm.controls.payorName.setErrors({ incorrect: true });
        setTimeout(() => {
          this.alertService.error('Billing Address not found. Please add the address.');
        }, 1000);
      }
    } else {
      this.clientSelected = data;
    }
  }

  private loadAcceptJSScript(): void {
    const imported = document.createElement('script');
    imported.src = environment.acceptJS.acceptUrl;
    imported.type = environment.acceptJS.type;
    imported.charset = environment.acceptJS.charset;
    document.head.appendChild(imported);
  }

  ngOnDestroy() {
    this.removeJSFile(environment.acceptJS.acceptUrl);
    this.removeJSFile(environment.acceptJS.acceptCoreUrl);
  }

  private removeJSFile(fileName: string): void {
    const allsuspects = document.getElementsByTagName('script');
    for (let i = allsuspects.length; i >= 0; i--) {
      if (allsuspects[i] && allsuspects[i].getAttribute('src') != null &&
        allsuspects[i].getAttribute('src').indexOf(fileName) !== -1) {
        allsuspects[i].parentNode.removeChild(allsuspects[i]);
      }
    }
  }

  private getProfiles(paymentId: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.getProfiles(paymentId).subscribe(
      (data: any) => {
        this.loadingService.toggleLoadingIndicator(false);
        this.profileDetails = data;
        if (!this.profileDetails) {
          setTimeout(() => {
            this.alertService.error('Unable to retrieve payment profile details. Please use alternate methods for disbursement.');
          }, 1000);
          this.modal.close(false);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        setTimeout(() => {
          this.alertService.error('Unable to retrieve payment profile details. Please use alternate methods for disbursement.');
        }, 1000);
        this.modal.close(false);
      });
  }

  public refund(): void {
    let requestBody = {};
    if (this.profileDetails.paymentProfile.bankAccount) {
      requestBody = {
        amount: this.paymentsSearchService.accountPaymentData.paymentAmount,
        bankAccount: this.profileDetails.paymentProfile.bankAccount.accountNumber,
        routingNumber: this.profileDetails.paymentProfile.bankAccount.routingNumber,
        nameOnAccount: this.profileDetails.paymentProfile.bankAccount.nameOnAccount
      };
    } else {
      requestBody = {
        amount: this.paymentsSearchService.accountPaymentData.paymentAmount,
        cardNumber: this.profileDetails.paymentProfile.creditCard.cardNumber,
      };
    }
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.refund(this.accountPaymentData.paymentId, requestBody).subscribe(
      (data: any) => {
        this.loadingService.toggleLoadingIndicator(false);
        this.paymentsSearchService.paramsToModal.paymentMethodSelected = '';
        this.paymentsDisburse(data.transactionId);
      },
      (err: any) => {
        this.commonService.handleErrorResponse(err);
      }
    );

  }

  checkBillingAddress(clientAddress: any) {
    for (const address of clientAddress) {
      if (address.addressType === PropertyName.Billing || address.addressType === PropertyName.BillingMailing) {
        return address;
      }
    }
    return false;
  }
  changePayor() {
    this.payorAddress = null;
    this.payorId = null;
    this.addressSequenceNumber = null;
    this.clientSelected.status = false;
    this.paymentsDisburseForm.controls.payorName.setErrors(null);
  }
  getPaymentsUnidentifiedSupportData() {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.getPaymentsUnidentifiedSupportData(this.payment.paymentSequenceNumber,
      this.payment.postedDate, this.payment.userId, this.payment.batchId, this.payment.distributionDate, PaymentOptions.disburse).subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          if (data) {
            this.supportData = data;
            this.getDropdownItems(PropertyName.DisbursementReason);
          }
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
        }
      );
  }

  getDropdownItems(propertyName: string) {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (propertyName === PropertyName.DisbursementReason && sd.defaultValue) {
          this.paymentsDisburseForm.get('Disbursement_Reason').setValue(sd.defaultValue);
        }
      }
    }
  }

  public submit(): void {
    if (this.paymentMethod === PropertyName.ManualElectronic && this.oneTimePaymentIntegration) {
      this.refund();
    } else {
      this.paymentsDisburse(null);
    }
  }

  paymentsDisburse(transactionId: any) {
    let body: any;
    if (this.paymentMethodSelected === PropertyName.ManualCheck) {
      body = {
        amount: this.payment.paymentAmount,
        checkNumber: this.checkNumber,
        reason: this.reason,
        refundMethod: this.paymentMethodSelected,
        payeeId: this.payorId,
        addressSequenceNumber: this.addressSequenceNumber
      };
    } else {
      body = {
        amount: this.payment.paymentAmount,
        reason: this.reason,
        refundMethod: this.paymentMethodSelected,
        payeeId: this.payorId,
        addressSequenceNumber: this.addressSequenceNumber,
        transactionId
      };
    }

    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.paymentAction(this.payment, body, PaymentOptions.disburse).subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.modal.close(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      }
    );
  }

  private getPaymentsSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentService.getPaymentsSupportData().subscribe(
      data => {
        this.loadingService.toggleLoadingIndicator(false);
        if (data) {
          this.supportData = data;
          this.getItems(PropertyName.oneTimePaymentIntegration);
        }
      },
      () => {
        this.loadingService.toggleLoadingIndicator(false);
      });
  }

  private getItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
      }
    }
    let oneTimePaymentIntegration = 'false';
    this.oneTimePaymentIntegrationDropdown && (oneTimePaymentIntegration = this.oneTimePaymentIntegrationDropdown[0]);
    if (oneTimePaymentIntegration === 'true') {
      this.oneTimePaymentIntegration = true;
      if (this.accountPaymentData.paymentId || this.accountPaymentData.paymentMethod === PropertyName.AgencySweep) {
        this.paymentMethod = PropertyName.ManualElectronic;
        this.loadAcceptJSScript();
        this.getProfiles(this.accountPaymentData.paymentId);
      } else {
        setTimeout(() => {
          this.alertService.error('Unable to retrieve payment profile details. Please use alternate methods for disbursement.');
        }, 1000);
        this.modal.close(false);
      }
    }
  }

  public getFieldNameSecurityConfig(name?: any): string {
    switch (name) {
      case 'Search By Account':
        return 'searchByAccount';
      case 'Search By Payee':
        return 'searchByPayee';
    }
  }

  public checkDisburseSelectType(): void {
    const isSearchByAccountActive: boolean = this.securityEngineService.checkVisibility('searchByAccount', 'Show');
    const isSearchByPayeeActive: boolean = this.securityEngineService.checkVisibility('searchByPayee', 'Show');
    if(isSearchByAccountActive){
      this.showActiveControls(this.typeData[0]);
    }else if(isSearchByPayeeActive){
      this.showActiveControls(this.typeData[1]);
    }
  }
}
