import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoadingService } from 'src/app/shared/services/loading.service';
import { ModalComponent } from 'src/app/libs/modal/modal.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { PaymentsSearchService } from 'src/app/ent-payments/payments-search/services/payments-search.service';
import { PropertyName } from 'src/app/shared/enum/common.enum';

@Component({
  selector: 'app-write-off',
  templateUrl: './write-off.component.html'
})
export class WriteOffComponent implements OnInit {
  public reason: string;
  private supportData: any;
  public WriteOffReasonDropdown: any = [];
  public paymentsWriteOffForm: FormGroup;
  public payment: any;

  constructor(
    private paymentsSearchService: PaymentsSearchService,
    private loadingService: LoadingService,
    private fb: FormBuilder,
    private modal: ModalComponent,
    private commonService: CommonService
  ) { }

  ngOnInit() {
    this.paymentsWriteOffForm = this.fb.group({
      WriteOffReason: ['', Validators.required]
    });
    if (this.paymentsSearchService.paramsToModal) {
      this.payment = this.paymentsSearchService.paramsToModal;
    }
    this.getPaymentsUnidentifiedSupportData();
  }

  private getPaymentsUnidentifiedSupportData(): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.getPaymentsUnidentifiedSupportData(this.payment.paymentSequenceNumber,
      this.payment.postedDate, this.payment.userId, this.payment.batchId, this.payment.distributionDate, '_writeOff').subscribe(
        data => {
          this.loadingService.toggleLoadingIndicator(false);
          if (data) {
            this.supportData = data;
            this.getDropdownItems('WriteOffReason');
          }
        },
        () => {
          this.loadingService.toggleLoadingIndicator(false);
        }
      );
  }

  private getDropdownItems(propertyName: string): void {
    for (const sd of this.supportData.supportData) {
      if (sd.propertyName === propertyName) {
        this[propertyName + 'Dropdown'] = sd.propertyValues;
        if (propertyName === PropertyName.WriteOffReason && sd.defaultValue) {
          this.paymentsWriteOffForm.controls['WriteOffReason'].setValue(sd.defaultValue);
        }
      }
    }
  }

  public paymentsWriteOff(): void {
    const body = {
      reason: this.reason
    };
    this.loadingService.toggleLoadingIndicator(true);
    this.paymentsSearchService.paymentAction(this.payment, body, '_writeOff').subscribe(
      () => {
        this.loadingService.toggleLoadingIndicator(false);
        this.modal.close(true);
      },
      err => {
        this.commonService.handleErrorResponse(err);
      }
    );
  }
}
