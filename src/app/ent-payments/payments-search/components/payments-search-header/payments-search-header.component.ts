import { Component, Input, OnInit } from '@angular/core';
import { PaymentSecurityConfigService } from 'src/app/ent-payments/security-config/security-config.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';

@Component({
  selector: 'app-payments-search-header',
  templateUrl: './payments-search-header.component.html'
})
export class PaymentsSearchHeaderComponent implements OnInit {
  @Input() isPaymentListLoaded = false;

  constructor(public securityEngineService: SecurityEngineService,
    private paymentSecurityConfigService: PaymentSecurityConfigService) { }

  ngOnInit() { }
  
  public openSecurity(event?: any) : void {
    this.paymentSecurityConfigService.openSecuritySection(this.securityEngineService.roleSequenceId, event, 'tableSection');
  }
}
