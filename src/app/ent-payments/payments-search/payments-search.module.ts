import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryModule } from 'src/app/libs/libs.module';
import { PaymentsSearchRoutingModule } from './payments-search-routing.module';

import { PaymentsSearchComponent } from './components/payments-search.component';
import { PaymentsSearchHeaderComponent } from './components/payments-search-header/payments-search-header.component';
import { PaymentsSearchResultsComponent } from './components/payments-search-results/payments-search-results.component';
import {
  PaymentsSearchResultsDetailComponent
} from './components/payments-search-results/payments-search-results-detail/payments-search-results-detail.component';
import { PaymentSearchSectionComponent } from './components/payments-search-section/payments-search-section.component';

@NgModule({
  declarations: [
    PaymentsSearchComponent,
    PaymentsSearchHeaderComponent,
    PaymentsSearchResultsComponent,
    PaymentsSearchResultsDetailComponent,
    PaymentSearchSectionComponent
  ],
  entryComponents: [
    PaymentsSearchResultsDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    LibraryModule,
    PaymentsSearchRoutingModule
  ]
})
export class PaymentsSearchModule { }
