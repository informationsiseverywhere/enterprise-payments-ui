import { Injectable } from '@angular/core';
import { PaymentsSearchService } from '../services/payments-search.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { PaymentIdentifierFilterLabels } from 'src/app/ent-payments/payments-search/models/payment-enums.model';
import { HalProcessor } from 'src/app/shared/services/utilities.service';

@Injectable({
  providedIn: 'root'
})

export class PaymentCommonService {
  public paymentIdentifierFilterLabels: PaymentIdentifierFilterLabels = new PaymentIdentifierFilterLabels();
  constructor(
    public appConfig: AppConfiguration,
    private paymentsSearchService: PaymentsSearchService,
    private halProcessor: HalProcessor,
    public commonService: CommonService) { }

  removeTagsItem(name?: string, filterTagsList?: any) {
    for (let i = 0; i < filterTagsList.length; i++) {
      if (filterTagsList[i].name === name) {
        filterTagsList.splice(i, 1);
      }
    }
  }

  public initialPaymentFilterOption(startDate?: any, endDate?: any, minAmount?: any,
    maxAmount?: any, paymentMethod?: any, identifierType?: any,
    userId?: any, paymentId?: any, currency?: any, paymentTags?: any, status?: any, identifier?: any, cashWithApplication?: any, filterTagsList?: any): void {

    if (startDate) {
      this.removeTagsItem('since', filterTagsList);
      filterTagsList.push({ tag: paymentTags.startDate + this.commonService.convertDateFormat(startDate), name: 'since' });
    }
    if (endDate) {
      this.removeTagsItem('until', filterTagsList);
      filterTagsList.push({ tag: paymentTags.endDate + this.commonService.convertDateFormat(endDate), name: 'until' });

    }
    if (minAmount) {
      this.removeTagsItem('fromAmount', filterTagsList);
      filterTagsList.push({ tag: paymentTags.minAmount + minAmount, name: 'fromAmount' });

    }
    if (maxAmount) {
      this.removeTagsItem('toAmount', filterTagsList);
      filterTagsList.push({ tag: paymentTags.maxAmount + maxAmount, name: 'toAmount' });

    }
    if (paymentMethod) {
      this.removeTagsItem('paymentMethod', filterTagsList);
      filterTagsList.push({ tag: paymentTags.paymentMethod + paymentMethod, name: 'paymentMethod' });

    }
    if (identifierType) {
      this.removeTagsItem('identifierType', filterTagsList);
      filterTagsList.push({ tag: paymentTags.identifierType + identifierType, name: 'identifierType' });
    }
    if (userId) {
      this.removeTagsItem('userId', filterTagsList);
      filterTagsList.push({ tag: paymentTags.userId + userId, name: 'userId' });
    }
    if (paymentId) {
      this.removeTagsItem('paymentId', filterTagsList);
      filterTagsList.push({ tag: paymentTags.paymentId + paymentId, name: 'paymentId' });
    }
    if (currency) {
      this.removeTagsItem('currency', filterTagsList);
      filterTagsList.push({ tag: paymentTags.currency + currency, name: 'currency' });
    }
    if (status && status.length > 0) {
      this.removeTagsItem('status', filterTagsList);
      for (const sd of status) {
        const statusName = this.halProcessor.replacePlusSignsBySpaces(sd);
        filterTagsList.push({ tag: paymentTags.status + statusName, name: 'status_' + this.commonService.removeSpacesAndFWDash(statusName)});
      }
    }
    if (identifier) {
      this.removeTagsItem('identifier', filterTagsList);
      filterTagsList.push({ tag: this.paymentIdentifierFilterLabels[this.commonService.removeSpacesAndFWDash(identifierType)] + ' : ' + identifier, name: 'identifier' });
    }
    if (cashWithApplication) {
      this.removeTagsItem('cashWithApplication', filterTagsList);
      filterTagsList.push({ tag: paymentTags.cashWithApplication + cashWithApplication, name: 'cashWithApplication' });
    }
  }

  public searchFilter(selectedItem?: any, filterKey?: string, startDate?: any, endDate?: any, minAmount?: any, maxAmount?: any, paymentMethod?: any, identifierType?: any,
    userId?: any, paymentId?: any, currency?: any, paymentTags?: any, status?: any, identifier?: any, cashWithApplication?: any, filterTagsList?: any): void {
    if (filterKey === 'startDate') {
      this.removeTagsItem('since', filterTagsList);
      selectedItem.formatted && filterTagsList.push({ tag: paymentTags.startDate + this.commonService.convertDateFormat(selectedItem.formatted), name: 'since' });
      startDate = selectedItem.formatted;
    }
    if (filterKey === 'endDate') {
      this.removeTagsItem('until', filterTagsList);
      selectedItem.formatted && filterTagsList.push({ tag: paymentTags.endDate + this.commonService.convertDateFormat(selectedItem.formatted), name: 'until' });
      endDate = selectedItem.formatted;
    }
    if (filterKey === 'minAmount') {
      this.removeTagsItem('fromAmount', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.minAmount + selectedItem, name: 'fromAmount' });
      minAmount = selectedItem;
    }
    if (filterKey === 'maxAmount') {
      this.removeTagsItem('toAmount', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.maxAmount + selectedItem, name: 'toAmount' });
      maxAmount = selectedItem;
    }
    if (filterKey === 'paymentMethod') {
      this.removeTagsItem('paymentMethod', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.paymentMethod + selectedItem, name: 'paymentMethod' });
      paymentMethod = selectedItem;
    }
    if (filterKey === 'userId') {
      this.removeTagsItem('userId', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.userId + selectedItem, name: 'userId' });
      userId = selectedItem;
    }
    if (filterKey === 'paymentId') {
      this.removeTagsItem('paymentId', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.paymentId + selectedItem, name: 'paymentId' });
      paymentId = selectedItem;
    }
    if (filterKey === 'identifierType') {
      this.removeTagsItem('identifierType', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.identifierType + selectedItem, name: 'identifierType' });
      identifierType = selectedItem;
    }
    if (filterKey === 'currency') {
      this.removeTagsItem('currency', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.currency + selectedItem, name: 'currency' });
      currency = selectedItem;
    }
    if (filterKey === 'status') {
      this.removeTagsItem('status', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.status + selectedItem, name: 'status' });
      status = selectedItem;
    }
    if (filterKey === 'identifier') {
      this.removeTagsItem('identifier', filterTagsList);
      selectedItem && filterTagsList.push({ tag: 'Identifier (' + identifierType + '): ' + selectedItem, name: 'identifier' });
      identifier = selectedItem;
    }
    if (filterKey === 'cashWithApplication') {
      this.removeTagsItem('cashWithApplication', filterTagsList);
      selectedItem && filterTagsList.push({ tag: paymentTags.cashWithApplication + selectedItem, name: 'cashWithApplication' });
      cashWithApplication = selectedItem;
    }

    let filterParam = {};
    filterParam = {
      since: startDate,
      until: endDate,
      toAmount: maxAmount,
      fromAmount: minAmount,
      paymentMethod,
      userId,
      paymentId,
      identifierType,
      currency,
      status,
      identifier,
      cashWithApplication
    };
    this.paymentsSearchService.filterParam = filterParam;
  }
}
