import { Injectable } from '@angular/core';
import { DynamicHostUrlService } from 'src/app/shared/services/dynamic-host-url.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { AppConfiguration } from 'src/app/shared/services/app-config.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { HalProcessor } from 'src/app/shared/services/utilities.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentsSearchService {
  public systemDate: any;
  public paymentDetail: any;
  public filterParam: any;
  public accountPaymentData: any;
  paymentMethodDropdown: any = [];
  paramsToModal: any;
  accountId: any;
  apiUrls: any;
  multiCurrencyEnabled: any;
  constructor(
    private restService: RestService,
    private hostUrlService: DynamicHostUrlService,
    private appConfig: AppConfiguration,
    private halProcessor: HalProcessor,
    public commonService: CommonService) { }

  getPaymentList(since?: string, until?: string, paymentMethod?: string, minAmount?: any, maxAmount?: any, identifierType?: any, userId?: any, paymentId?: any, currency?: any, size?: any, page?: any) {
    const url = this.buildExecutionUrl(since, until, paymentMethod, size, minAmount, maxAmount, identifierType, userId, paymentId, currency, page);
    return this.restService.getByUrl(url);
  }

  getUnidentifieldPaymentList(since?: string, until?: string, paymentMethod?: string, minAmount?: any, maxAmount?: any, identifierType?: any, identifier?: any, userId?: any, paymentId?: any, currency?: any, status?: any, cashWithApplication?: any, size?: any, page?: any) {
    const url = this.buildUrl(since, until, paymentMethod, size, minAmount, maxAmount, identifierType, identifier, userId, paymentId, currency, status, cashWithApplication, page);
    return this.restService.getByUrl(url);
  }

  buildUrl(since?: string, until?: string, paymentMethod?: any, size?: any, minAmount?: any, maxAmount?: any, identifierType?: any, identifier?: any, userId?: any, paymentId?: any, currency?: any, status?: any, cashWithApplication?: any, page?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/suspendedPayments';
    const params: any = [];
    if (since) {
      params.push({ name: 'since', value: since });
    }
    if (until) {
      params.push({ name: 'until', value: until });
    }
    if (minAmount) {
      params.push({ name: 'fromAmount', value: minAmount });
    }
    if (maxAmount) {
      params.push({ name: 'toAmount', value: maxAmount });
    }
    if (paymentMethod) {
      params.push({ name: 'paymentMethod', value: paymentMethod });
    }
    if (identifierType) {
      params.push({ name: 'identifierType', value: identifierType });
    }
    if (userId) {
      params.push({ name: 'userId', value: userId });
    }
    if (paymentId) {
      params.push({ name: 'paymentId', value: paymentId });
    }
    if (currency) {
      params.push({ name: 'currency', value: currency });
    }
    if (identifier) {
      params.push({ name: 'identifier', value: identifier });
    }
    if (status && status.length > 0) {
      for (const sd of status) {
        params.push({ name: 'status', value: this.halProcessor.replacePlusSignsBySpaces(sd) });
      }
    }
    if (cashWithApplication) {
      params.push({ name: 'cashWithApplication', value: cashWithApplication });
    }
    let queryString = '';
    for (const param of params) {
      queryString += param.name + ':' + param.value;
      if (param !== params[params.length - 1]) {
        queryString += ',';
      }
    }
    if (queryString !== '') {
      url += '?filters=' + queryString;
    }

    url = this.commonService.buildSearchQueryUrl(url, page, size);

    return url;

  }

  buildExecutionUrl(since?: string, until?: string, paymentMethod?: any, size?: any, minAmount?: any,
    maxAmount?: any, identifierType?: any, userId?: any, paymentId?: any, currency?: any, page?: any) {
    let url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/payments';
    const params: any = [];
    if (since) {
      params.push({ name: 'since', value: since });
    }
    if (until) {
      params.push({ name: 'until', value: until });
    }
    if (minAmount) {
      params.push({ name: 'fromAmount', value: minAmount });
    }
    if (maxAmount) {
      params.push({ name: 'toAmount', value: maxAmount });
    }
    if (paymentMethod) {
      params.push({ name: 'paymentMethod', value: paymentMethod });
    }
    if (identifierType) {
      params.push({ name: 'identifierType', value: identifierType });
    }
    if (userId) {
      params.push({ name: 'userId', value: userId });
    }
    if (paymentId) {
      params.push({ name: 'paymentId', value: paymentId });
    }
    if (currency) {
      params.push({ name: 'currency', value: currency });
    }

    let queryString = '';
    for (const param of params) {
      queryString += param.name + ':' + param.value;
      if (param !== params[params.length - 1]) {
        queryString += ',';
      }
    }
    if (queryString !== '') {
      url += '?filters=' + queryString;
    }

    url = this.commonService.buildSearchQueryUrl(url, page, size);

    return url;
  }

  getPaymentsListSupportData() {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/payments';
    return this.restService.optionsByUrl(url);
  }

  getPaymentsDetail(paymentSequenceNumber?: any, postedDate?: any, userId?: any, batchId?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/payments/' + paymentSequenceNumber +
      '?postedDate=' + postedDate + '&userId=' + userId + '&batchId=' + batchId;
    return this.restService.getByUrl(url);
  }

  getUnidentifiedPaymentsDetail(payment?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/suspendedPayments/'
      + payment.paymentSequenceNumber + '?postedDate=' + payment.postedDate + '&userId=' + payment.userId + '&batchId=' + payment.batchId + '&distributionDate=' + payment.distributionDate;
    return this.restService.getByUrl(url);
  }

  getIOCPaymentDetails(paymentId?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments) + 'v1/payments/' + paymentId;
    return this.restService.getByUrl(url);
  }
  paymentDetails(payment?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/suspendedPayments/' +
      payment.paymentSequenceNumber + '?postedDate=' + payment.postedDate + '&userId=' + payment.userId + '&batchId=' + payment.batchId + '&distributionDate=' + payment.distributionDate;
    return this.restService.getByUrl(url);
  }
  getPaymentDetailsSupportData(payment?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/suspendedPayments/' +
      payment.paymentSequenceNumber + '?postedDate=' + payment.postedDate + '&userId=' + payment.userId + '&batchId=' + payment.batchId + '&distributionDate=' + payment.distributionDate;
    return this.restService.optionsByUrl(url);
  }
  getPaymentsUnidentifiedSupportData(paymentSequenceNumber?: any, postedDate?: string, userId?: any, batchId?: any, distributionDate?: any, actionType?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/suspendedPayments/' +
      paymentSequenceNumber + '/' + actionType + '?postedDate=' + postedDate + '&userId=' + userId + '&batchId=' + batchId + '&distributionDate=' + distributionDate;
    return this.restService.optionsByUrl(url);
  }
  paymentAction(payment?: any, body?: any, actionType?: any) {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.payments) + 'v1/billing/suspendedPayments/' +
      payment.paymentSequenceNumber + '/' + actionType + '?postedDate=' + payment.postedDate +
      '&userId=' + payment.userId + '&batchId=' + payment.batchId + '&distributionDate=' + payment.distributionDate;
    return this.restService.postByUrl(url, body);
  }
  public getProfiles(paymentId?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments) + 'v1/payments/' + paymentId;
    return this.restService.getByUrl(url);
  }
  public refund(paymentId?: any, body?: any): any {
    const url = this.hostUrlService.getHostUrl(this.appConfig.apiUrls.makePayments) + 'v1/payments/' + paymentId + '/refund';
    return this.restService.postByUrl(url, body);
  }
}
