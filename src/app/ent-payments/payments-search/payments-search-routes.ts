import { Routes } from '@angular/router';

import { CommonComponent } from 'src/app/shared/components/common/common.component';
import { PaymentsSearchComponent } from './components/payments-search.component';

export const paymentsSearchRoutes: Routes = [
  {
    path: '',
    component: CommonComponent,
    children: [
      {
        path: '',
        component: PaymentsSearchComponent
      }
    ]
  }
];

