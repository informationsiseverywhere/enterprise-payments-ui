import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { paymentsSearchRoutes } from './payments-search-routes';


@NgModule({
  imports: [
    RouterModule.forChild(paymentsSearchRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PaymentsSearchRoutingModule {
}
