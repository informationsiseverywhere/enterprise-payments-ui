import { Injectable, Inject } from '@angular/core';
import {
  CanActivate, ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  CanLoad, Route, Router
} from '@angular/router';
import { AsyncSubject, Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';

import { AppConfiguration } from '../shared/services/app-config.service';
import { SsoAuthService } from './sso-auth.service';
import { ForgeRockAuthService } from './forgerock-auth.service';
import { RestService } from '../shared/services/rest.service';
import { LoadingService } from '../shared/services/loading.service';
import { AlertService } from '../libs/alert/services/alert.service';
import { AppLogger } from '../shared/services/app-logger.service';
import { AmazonCognitoAuthService } from './amazon-cognito-auth.service';
import { CommonService } from 'src/app/shared/services/common.service';

const isIFrame = (input: HTMLElement | null): input is HTMLIFrameElement => input !== null && input.tagName === 'IFRAME';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  private isAuthorized: AsyncSubject<any>;
  sessionStorageData: any;
  authUrls: any;
  constructor(
    public appConfig: AppConfiguration,
    private ssoAuthService: SsoAuthService,
    private restService: RestService,
    private loadingService: LoadingService,
    private router: Router,
    private alertService: AlertService,
    private appLogger: AppLogger,
    @Inject(DOCUMENT) private document: any,
    private forgeRockAuthService: ForgeRockAuthService,
    private amazonCognitoAuthService: AmazonCognitoAuthService,
    public commonService: CommonService) {
    this.authUrls = this.appConfig.authUrls;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.isAuthorized = new AsyncSubject<any>();
    this.checkAuthentication();
    return this.isAuthorized.pipe(tap(
      isAuthorized => {
        if (isAuthorized) {
          return true;
        }

        if (isAuthorized === null || isAuthorized === undefined) {
          this.alertService.error('Token expired');
          delay(1000);
          if (this.authUrls.loginMethod === 'SSO') {
            this.restService.logoutSsoUser();
          } else if (this.authUrls.loginMethod === 'Forgerock') {
            this.restService.logoutUser();
          } else if (this.authUrls.loginMethod === 'Cognito') {
            this.restService.logoutAmazonCognitoUser();
          }
        } else {
          if (isAuthorized.status === 401) {
            this.appLogger.updateMessage({ errorCode: isAuthorized, context: 'You are not authorized to view this page.' });
            this.router.navigate(this.appConfig.errorPageLink);
          } else {
            delay(1000);
            if (this.authUrls.loginMethod === 'SSO') {
              this.restService.logoutSsoUser();
            } else if (this.authUrls.loginMethod === 'Forgerock') {
              this.restService.logoutUser();
            } else if (this.authUrls.loginMethod === 'Cognito') {
              this.restService.logoutAmazonCognitoUser();
            }
          }
        }
      }
    ));
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }

  canLoad(route: Route): Observable<boolean> {
    const url = this.document.location.href;
    let params = null;
    if (url.split('?').length > 1) {
      params = new URLSearchParams(url.split('?')[1]);
      this.getQueryParams(params);
    }

    return this.checkLoginType();
  }

  getQueryParams(queryParam: any) {
    const resultQueryParam: any = {};
    this.sessionStorageData = JSON.parse(sessionStorage.getItem('resultQueryParam') || '{}');
    queryParam.forEach((key: any, value: any) => {
      resultQueryParam[value] = key;
    });
    sessionStorage.setItem('resultQueryParam', JSON.stringify(this.commonService.setSessionStorageData(resultQueryParam, this.sessionStorageData, true, 'userId', 'tokenId', 'returnAppId', 'returnUrl', 'cancelUrl')));
  }

  checkLoginType(): Observable<boolean> {
    if (this.authUrls.loginMethod === 'SSO') {
      return this.ssoAuthService.isAuthenticated();
    } else if (this.authUrls.loginMethod === 'Forgerock') {
      return this.forgeRockAuthService.isAuthenticated();
    } else if (this.authUrls.loginMethod === 'Cognito') {
      return this.amazonCognitoAuthService.isAuthenticated();
    }
  }

  checkAuthentication() {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam') || '{}');
    const userId = (resultQueryParam ? resultQueryParam.userId : null);
    const tokenId = (resultQueryParam ? resultQueryParam.tokenId : null);
    if (this.authUrls.loginMethod === 'SSO') {
      if (userId) {
        this.isAuthorized.next(true);
        this.isAuthorized.complete();
      } else {
        this.isAuthorized.next(false);
        this.isAuthorized.complete();
      }
    } else if (this.authUrls.loginMethod === 'Forgerock') {
      if (userId && tokenId) {
        this.validateToken(tokenId);
      } else {
        this.isAuthorized.next(false);
        this.isAuthorized.complete();
      }
    } else if (this.authUrls.loginMethod === 'Cognito') {
      if (userId && tokenId) {
        const awsAccessTokenKey = 'CognitoIdentityServiceProvider.' +
          this.authUrls.appClientId + '.' + userId.toLowerCase() + '.accessToken';
        const uaaFrame = document.getElementById('uaaFrame');
        if (isIFrame(uaaFrame) && uaaFrame.contentWindow) {
          uaaFrame.contentWindow.postMessage(
            {
              action: 'getAccessToken',
              key: awsAccessTokenKey
            }, '*');
        }
        this.isAuthorized.next(true);
        this.isAuthorized.complete();
      } else {
        this.isAuthorized.next(false);
        this.isAuthorized.complete();
      }
    }
  }

  validateToken(tokenId: string) {
    this.loadingService.toggleLoadingIndicator(true);
    this.restService.validateToken(tokenId).subscribe(
      data => {
        if (!data || !data.valid) {
          this.isAuthorized.next(false);
          this.isAuthorized.complete();
        } else {
          this.isAuthorized.next(true);
          this.isAuthorized.complete();
        }
        this.loadingService.toggleLoadingIndicator(false);
      },
      err => {
        if (err && err.errorSubCode && err.errorSubCode === 'Token') {
          this.isAuthorized.next(null);
          this.isAuthorized.complete();
        } else {
          if (err.status === 401) {
            this.isAuthorized.next(err);
            this.isAuthorized.complete();
          } else {
            this.isAuthorized.next(false);
            this.isAuthorized.complete();
          }
        }
      }
    );
  }
}
