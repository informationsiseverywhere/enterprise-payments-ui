import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, NavigationExtras } from '@angular/router';
import { HalProcessor } from 'src/app/shared/services/utilities.service';
import { AppConfiguration } from '../shared/services/app-config.service';
import { AlertService } from '../libs/alert/services/alert.service';
import { SecurityEngineService } from 'src/app/shared/services/security-engine.service';
import { RoutePermissionModel } from 'src/app/shared/models/route-permission.model';

@Injectable({
  providedIn: 'root',
})
export class SecurityConfigGuard implements CanActivate {
  private accessScreenTitle: string = '';

  constructor(
    public appConfig: AppConfiguration,
    private router: Router,
    private alertService: AlertService,
    private halProcessor: HalProcessor,
    private securityEngineService: SecurityEngineService) {
  }

  canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
    this.accessScreenTitle = route && route.data ? route.data.headerTitle : '';
    return this.hasPermissions(route);
  }

  public hasPermissions(route?: ActivatedRouteSnapshot): any {
    if(!this.appConfig.isComponentSecurity) return true;
    this.securityEngineService.dynamicRedirectionSettings = undefined;
    const routeDataPermissions = route && route.data ? route.data.permissions : {};
    if (routeDataPermissions) {
      const permissions = this.transformPermission(routeDataPermissions);
      if (this.isParameterAvailable(permissions.dependOnScreenName) && this.isParameterAvailable(permissions.only)) {
        return this.passingOnlyPermissionsValidation(permissions, route);
      }
    }
    return true;
  }

  private isParameterAvailable(permission): boolean {
    return permission && permission.length > 0;
  }

  public passingOnlyPermissionsValidation(permissions?: any, route?: ActivatedRouteSnapshot) {
    if ((this.halProcessor.isPlainObject(permissions.redirectTo) && !this.isRedirectionWithParameters(permissions.redirectTo))) {
      return this.onlyRedirectCheck(permissions, route);
    }
  }

  private onlyRedirectCheck(permissions?: any, route?: ActivatedRouteSnapshot) {
    const currRoutePermission = this.getRoutePermissionData(permissions, route);
    this.securityEngineService.filterValue = currRoutePermission.filterValue;
    currRoutePermission.notUseHeaderTitleForMsg && (this.accessScreenTitle = ' this ');

    return this.securityEngineService.getSetting(currRoutePermission.roleSequenceId, currRoutePermission.dependOnScreenName).toPromise().then(
      (data: any) => {
        const dependSecurityData = data ? data.fields : null;
        return this.routePermissionVerifying(currRoutePermission, dependSecurityData);
      },
      (err: any) => {
        this.alertService.error(err.error.errorDescription);
        return false;
      }
    );
  }

  private routePermissionVerifying(currRoutePermission?: RoutePermissionModel, dependSecurityData?: any) : boolean{
    if(currRoutePermission.dependOnScreenName[0] !== currRoutePermission.screenName && currRoutePermission.dependOnScreenName[0] !== currRoutePermission.additionalSecurityName){
      this.securityEngineService.dynamicRedirectionSettings = dependSecurityData;
    }
    if (!currRoutePermission.parentName || (currRoutePermission.parentName && dependSecurityData[currRoutePermission.parentName] && dependSecurityData[currRoutePermission.parentName].fieldProperty === 'Show')) {
      if(!currRoutePermission.only.find(item => dependSecurityData[item])) return true;
      for (const [index, child] of currRoutePermission.only.entries()) {
        if (dependSecurityData[child] && dependSecurityData[child].fieldProperty === 'Show') {
          const redirectTo = currRoutePermission.redirectTo[child];
          if (index != 0) this.redirectToAnotherRoute(redirectTo);
          return true;
        }
      }
    }

    if (currRoutePermission.redirectTo.default != null) {
      const paramObject = {};
      paramObject['failedPermissionName'] = encodeURIComponent(this.accessScreenTitle);
      if(currRoutePermission.roleSequenceId) { paramObject['roleSequenceId'] = currRoutePermission.roleSequenceId; }
      if (currRoutePermission.redirectTo.default === ''){
        this.router.navigate(this.appConfig.landingPageLink,{
          queryParams: paramObject,
          skipLocationChange: true
        });
      } else {
        const navigationExtras: NavigationExtras = {
          queryParams: paramObject,
          skipLocationChange: true
        }
        if(currRoutePermission.parentRouteName){
          const navigateCommands = this.transformNavigationCommands(currRoutePermission.redirectTo.default, currRoutePermission.parentRouteName, currRoutePermission.activatedParams);
          this.router.navigate(navigateCommands, navigationExtras);
        }else{
          const navigateCommands = this.transformNavigationCommands(currRoutePermission.redirectTo.default, this.appConfig.landingPageLink[0], currRoutePermission.activatedParams);
          this.router.navigate(navigateCommands, navigationExtras);
        } 
      }
    } else {
      return false;
    }
    return true;
  }

  private getRoutePermissionData(permissions?: any, route?: ActivatedRouteSnapshot): RoutePermissionModel{
    let currRoutePermission = new RoutePermissionModel();
    currRoutePermission.roleSequenceId = this.securityEngineService.roleSequenceId || route.queryParams.roleSequenceId;
    if(permissions){
      currRoutePermission.dependOnScreenName = permissions.dependOnScreenName || '';
      currRoutePermission.parentName = permissions.parentName || '';
      currRoutePermission.only = permissions.only || {};
      currRoutePermission.redirectTo = permissions.redirectTo || {};
      currRoutePermission.notUseHeaderTitleForMsg = permissions.notUseHeaderTitleForMsg || false;
    }
    if(route){
      if(route.data){
        currRoutePermission.screenName = route.data.screenName || '';
        currRoutePermission.additionalSecurityName = route.data.additionalSecurityName || '';
        currRoutePermission.parentRouteName = route.data.parentRouteName || '';
        currRoutePermission.filterValue = route.data.filterValue || '';
      }
      if(route.params){
        currRoutePermission.activatedParams = this.getActivatedParams(route);
      }
    }
    return currRoutePermission;
  }

  private redirectToAnotherRoute(permissionRedirectTo): any {
    if (this.isRedirectionWithParameters(permissionRedirectTo)) {
      const paramObject = {};
      paramObject['failedPermissionName'] = encodeURIComponent(this.accessScreenTitle);
      this.router.navigate([this.appConfig.landingPageLink + '/' + permissionRedirectTo.navigationCommands],{
        queryParams: paramObject,
        skipLocationChange: true
      });
      return;
    }
  }

  private isRedirectionWithParameters(object): any {
    return (this.halProcessor.isPlainObject(object) && (object.navigationCommands || object.navigationExtras));
  }

  private transformPermission(permissions): any {
    const dependOnScreenName = permissions.dependOnScreenName ? this.halProcessor.transformStringToArray(permissions.dependOnScreenName) : '';
    const parentName = permissions.parentName ? this.halProcessor.transformStringToArray(permissions.parentName) : '';
    const only = permissions.only ? this.halProcessor.transformStringToArray(permissions.only) : '';
    const redirectTo = permissions.redirectTo;
    const notUseHeaderTitleForMsg = permissions.notUseHeaderTitleForMsg;
    return {
      dependOnScreenName,
      only,
      parentName,
      redirectTo,
      notUseHeaderTitleForMsg
    };
  }

  private transformNavigationCommands(currentNavigateCommands?: any, parentRouteName?: any, activatedParams?: any): any[] {
    let navigateCommands: any = JSON.parse(JSON.stringify(currentNavigateCommands));
    parentRouteName && navigateCommands.unshift(parentRouteName);
    if(activatedParams){
      for (const [i, item] of navigateCommands.entries()) {
        if(item.startsWith(':') && activatedParams[item.replace(':','')]){
          navigateCommands.splice(i, 1, activatedParams[item.replace(':','')]);
        }
      }
    }
    return navigateCommands;
  }

  public routePermissionSecurityConfigGuard(route?: any, roleSequenceId?: any): void {
    let currRoute = route;
    let currPermissingDependScreenName = '';
    if(roleSequenceId){ this.securityEngineService.roleSequenceId = roleSequenceId; }
    const parentRoute = route.parent ? true : false;
    while(currRoute) {
      if(currRoute.snapshot && currRoute.snapshot.data){
        const currRouteSnapshotData = currRoute.snapshot.data;
        const routePermissionsData = currRouteSnapshotData && currRouteSnapshotData.permissions ? currRouteSnapshotData.permissions : {};
        if (routePermissionsData && currPermissingDependScreenName !== routePermissionsData.dependOnScreenName) {
          currPermissingDependScreenName = routePermissionsData.dependOnScreenName;
          this.hasPermissions(currRoute.snapshot);
        }
      }
      if (parentRoute && currRoute.parent) {
        currRoute = currRoute.parent;
      } else if(!parentRoute && currRoute.children[0]) {
        currRoute = currRoute.children[0];
      } else {
        return;
      }
    }
  }

  private getActivatedParams(route?: ActivatedRouteSnapshot): any{
    let activatedParams: Object = {};
    while(route) {
      if(route.params){ activatedParams = Object.assign(activatedParams, route.params); }
      if(route.parent){
        route = route.parent;
      }else{
        return activatedParams;
      }
    }
    return activatedParams;
  }
}
