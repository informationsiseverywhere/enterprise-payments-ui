import { Injectable } from '@angular/core';
import { AsyncSubject, Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

import { LoadingService } from '../shared/services/loading.service';
import { UserService } from '../shared/services/user-details.service';
import { AlertService } from '../libs/alert/services/alert.service';
import { RestService } from '../shared/services/rest.service';
import { User } from '../shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class SsoAuthService {
  private isAuthorized: AsyncSubject<any>;
  constructor(
    private loadingService: LoadingService,
    private userDetailsService: UserService,
    private alertService: AlertService,
    private restService: RestService
  ) { }

  isAuthenticated(): Observable<boolean> {
    this.isAuthorized = new AsyncSubject<any>();
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    if (resultQueryParam && resultQueryParam.userId) {
      this.getUserData(resultQueryParam.userId);
    } else {
      this.isAuthorized.next(false);
      this.isAuthorized.complete();
    }

    return this.isAuthorized.pipe(tap(
      isAuthorized => {
        if (isAuthorized) {
          return true;
        }
        this.alertService.error('You are not authorized to access this application. Please login with a valid user.');
        delay(1000),
        this.restService.logoutSsoUser();
        return false;
      }
    ));
  }

  public getUserData(userId: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.userDetailsService.getUserData(userId).subscribe(
      (data: any) => {
        this.loadingService.toggleLoadingIndicator(false);
        const userData = data ? data.content[0] : null;
        const currentUser: User = new User(userData);
        this.userDetailsService.currentUser = currentUser;
        this.userDetailsService.currentUserChanged.next(currentUser);
        this.isAuthorized.next(true);
        this.isAuthorized.complete();
      },
      (err: any) => {
        if (err && err.errorSubCode && err.errorSubCode === 'Token') {
          this.isAuthorized.next(null);
          this.isAuthorized.complete();
        } else {
          if (err.status === 401) {
            this.isAuthorized.next(err);
            this.isAuthorized.complete();
          } else {
            this.isAuthorized.next(false);
            this.isAuthorized.complete();
          }
        }
      });
  }

}
