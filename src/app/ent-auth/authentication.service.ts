import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

import { AppConfiguration } from '../shared/services/app-config.service';
import { RestService } from '../shared/services/rest.service';
import { LoadingService } from '../shared/services/loading.service';
import { UserService } from '../shared/services/user-details.service';
import { CommonService } from '../shared/services/common.service';
import { RemoteHeadersService } from '../shared/services/remote-headers.service';
import { User } from '../shared/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  authUrls: any;

  constructor(
    public appConfig: AppConfiguration,
    private restService: RestService,
    private loadingService: LoadingService,
    private userDetailsService: UserService,
    private commonService: CommonService) {
      this.authUrls = this.appConfig.authUrls;
    }


  public getUserData(): void {
    const resultQueryParam = JSON.parse(sessionStorage.getItem('resultQueryParam'));
    this.loadingService.toggleLoadingIndicator(true);
    this.userDetailsService.getUserData(resultQueryParam.userId).subscribe(
      (data: any) => {
        this.loadingService.toggleLoadingIndicator(false);
        const userData = data ? data.content[0] : null;
        const currentUser: User = new User(userData);
        this.userDetailsService.currentUser = currentUser;
        this.userDetailsService.currentUserChanged.next(currentUser);
      },
      (err: any) => {
        this.commonService.handleRedirectErrorPageResponse(err);
      });
  }


  getUserList() {
    const authUrl = this.authUrls.forgeRockAuthUrl + '/users?_queryId=*&_prettyPrint=true';
    const headers = RemoteHeadersService.headersConfig.headers.forgerockHeaders;
    const forgerockHeaders = new HttpHeaders(headers);

    return this.restService.getByUrl(authUrl, forgerockHeaders);
  }
}
