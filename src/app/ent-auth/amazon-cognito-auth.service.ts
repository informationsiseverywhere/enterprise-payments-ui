import { Injectable } from '@angular/core';
import { AsyncSubject, Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AppConfiguration } from '../shared/services/app-config.service';
import { LoadingService } from '../shared/services/loading.service';
import { UserService } from '../shared/services/user-details.service';
import { RestService } from '../shared/services/rest.service';
import { AppLogger } from '../shared/services/app-logger.service';
import { User } from '../shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AmazonCognitoAuthService {
  private isAuthorized: AsyncSubject<any>;
  constructor(
    private restService: RestService,
    public appConfig: AppConfiguration,
    private loadingService: LoadingService,
    private userDetailsService: UserService,
    private appLogger: AppLogger,
    private router: Router) { }

  public isAuthenticated(): Observable<boolean> {
    this.isAuthorized = new AsyncSubject<any>();
    const resultQueryParam: any = JSON.parse(sessionStorage.getItem('resultQueryParam') || '{}');
    if (resultQueryParam && resultQueryParam.userId && resultQueryParam.tokenId) {
      if (!this.userDetailsService.currentUser) {
        this.getUserData(resultQueryParam.userId);
      }
      this.isAuthorized.next(true);
      this.isAuthorized.complete();
    } else {
      this.isAuthorized.next(false);
      this.isAuthorized.complete();
    }

    return this.isAuthorized.pipe(tap(
      isAuthorized => {
        if (isAuthorized) {
          return true;
        }
        if (isAuthorized != null && isAuthorized != undefined && isAuthorized.status === 401) {
          this.appLogger.updateMessage({ errorCode: isAuthorized, context: 'You are not authorized to view this page.' });
          this.router.navigate(this.appConfig.errorPageLink);
        } else {
          delay(1000),
            this.restService.logoutAmazonCognitoUser();
        }
        return false;
      }
    ));
  }

  public getUserData(userId: any): void {
    this.loadingService.toggleLoadingIndicator(true);
    this.userDetailsService.getUserData(userId).toPromise().then(
      (data: any) => {
        this.loadingService.toggleLoadingIndicator(false);
        const userData = data ? data.content[0] : null;
        const currentUser: User = new User(userData);
        this.userDetailsService.currentUser = currentUser;
        this.userDetailsService.currentUserChanged.next(currentUser);
      },
      (err: any) => {
                if (err && err.errorSubCode && err.errorSubCode === 'Token') {
                    this.isAuthorized.next(null);
                    this.isAuthorized.complete();
                } else {
                    if (err.status === 401) {
                        this.isAuthorized.next(err);
                        this.isAuthorized.complete();
                    } else {
                        this.isAuthorized.next(false);
                        this.isAuthorized.complete();
                    }
                }
      });
  }
}
