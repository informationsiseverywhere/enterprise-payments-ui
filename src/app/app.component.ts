import { Component, OnInit, HostListener } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { filter, map } from 'rxjs/operators';
import { SecurityEngineService } from './shared/services/security-engine.service';
import { AppConfiguration } from './shared/services/app-config.service';
import { HeaderService } from './shared/services/header.service';
import { RestService } from './shared/services/rest.service';
import { RemoteHostURLService } from './shared/services/remote-host-url.service';
import { PreviousRouteService } from './shared/services/previous-route.service';
import { CommonService } from './shared/services/common.service';
import { AlertService } from 'src/app/libs/alert/services/alert.service';
import { CommonComponent } from 'src/app/shared/components/common/common.component';
import { NavigationComponent } from 'src/app/ent-payments/navigation/navigation.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  sessionStorageData: any;
  hostUrlConfig: any;

  route: { parentRoute: any; childRoute: any; };
  authUrls: any;
  private componentBeforeNavigation = null;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    public appConfig: AppConfiguration,
    private headerService: HeaderService,
    private restService: RestService,
    private previousRouteService: PreviousRouteService,
    public securityEngineService: SecurityEngineService,
    private alertService: AlertService,
    private commonService: CommonService) {
    this.authUrls = this.appConfig.authUrls;
  }

  @HostListener('window:message', ['$event'])
  onMessage(event) {
    this.messageHandler(event);
  }

  ngOnInit() {
    if (localStorage.getItem('DefaultTheme')) {
      this.commonService.switchStyle(localStorage.getItem('DefaultTheme'));
    } else {
      this.commonService.switchStyle('default-theme');
    }
    this.hostUrlConfig = RemoteHostURLService.hostUrlConfig;
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        let child = this.activatedRoute.firstChild;
        this.route = {
          parentRoute: this.activatedRoute.firstChild.snapshot,
          childRoute: null
        };
        while (child) {
          this.detectComponentChanges(child);
          if (child.firstChild) {
            child = child.firstChild;
          } else if (child.snapshot.data) {
            this.route.childRoute = child.snapshot.data;
            return this.route;
          } else {
            return null;
          }
        }
        return null;
      })
    ).subscribe((routes: any) => {
      if (routes) {
        this.handleRoutes(routes);
      }
    });
    if (this.authUrls.loginMethod === 'Cognito') {
      const uaaFrame = document.createElement('iframe');
      uaaFrame.src = this.authUrls.uaaCommonLogin + 'error';
      uaaFrame.id = 'uaaFrame';
      uaaFrame.setAttribute('class', 'd-none');
      uaaFrame.onload = function () { uaaFrame.setAttribute('uaa-loaded', 'true'); };
      const body = document.getElementsByTagName('body')[0];
      body.appendChild(uaaFrame);
    }
  }

  public messageHandler(event) {
    const { action, value } = event.data;
    switch (action) {
      case 'returnAccessToken':
        if (!value) { this.restService.logoutUser(); }
        break;
      default: break;
    }
  }

  private handleRoutes(routes: any) {
    const parentRoute = routes.parentRoute.data;
    const queryParams = routes.parentRoute.queryParams;
    const childRoute = routes.childRoute;
    if (!queryParams.roleSequenceId && this.securityEngineService.roleSequenceId) { this.securityEngineService.roleSequenceId = undefined; }
    this.headerService.setHeaderText(parentRoute);
    this.titleService.setTitle((parentRoute.title + ' | ' + childRoute.headerTitle));
    const failedPermissionName = (queryParams ? queryParams.failedPermissionName : null);
    if (failedPermissionName) {
      const warnMsg = `You do not have permission to access ${decodeURIComponent(failedPermissionName)} page. You are redirected to ${childRoute?.headerTitle} page.`;
      setTimeout(() => {
        this.alertService.error(warnMsg);
      }, 100);
    }
    this.handleReturnCancelUrl(queryParams);
    this.appendAQueryParam(queryParams);
  }

  private handleReturnCancelUrl(queryParams: any) {
    const sessionStorageData = JSON.parse(sessionStorage.getItem('resultQueryParam') || '{}');
    if (sessionStorageData && sessionStorageData.returnAppId && !queryParams.returnAppId) {
      delete sessionStorageData.returnAppId;
      delete sessionStorageData.cancelUrl;
      delete sessionStorageData.returnUrl;
      sessionStorage.setItem('resultQueryParam', JSON.stringify(sessionStorageData));
    }
    if (queryParams.returnAppId && (queryParams.cancelUrl || queryParams.returnUrl)) {
      sessionStorage.setItem('resultQueryParam', JSON.stringify(this.commonService.setSessionStorageData(queryParams, sessionStorageData, false, 'userId', 'tokenId', 'returnAppId', 'returnUrl', 'cancelUrl')));
    }
  }

  appendAQueryParam(queryParams: any) {
    const type = this.headerService.headerText.getValue();
    const urlTree = this.router.createUrlTree([], {
      queryParams: {
        type: (queryParams.type ? queryParams.type : type.type),
        returnAppId: queryParams.returnAppId,
        addressSequence: queryParams.addressSequence,
        partyId: queryParams.partyId,
        since: queryParams.since,
        userId: null,
        tokenId: null,
        returnUrl: null,
        cancelUrl: null,
        failedPermissionName: null
      },
      queryParamsHandling: 'merge',
      preserveFragment: true
    });
    this.router.navigateByUrl(urlTree);
  }

  public detectComponentChanges(child: ActivatedRoute): void{
    if(child?.component){
      const childComponent = child.component;
      if (this.isNonCommonComponent(childComponent) && this.componentBeforeNavigation !== childComponent) {
        this.focusToMainHeader();
      }
      this.isNonCommonComponent(childComponent) && (this.componentBeforeNavigation = childComponent);
    }
  }

  public isNonCommonComponent(component: any): boolean{
    return (component && component != NavigationComponent && component != CommonComponent);
  }

  public focusToMainHeader(): void{
    const mainHeader = Array.from(document.querySelectorAll('#pb360-navbar-header'));
    if (mainHeader?.length > 0) {  
      const mainHeaderEle = mainHeader.pop() as HTMLElement;
      setTimeout(() => {
        mainHeaderEle.focus();
      },);
    }
  }
}
