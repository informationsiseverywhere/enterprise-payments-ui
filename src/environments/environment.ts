// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular.json`.
// Supported Login method are SSO, Forgerock and Cognito

export const environment = {
  production: false,
  acceptJS: {
    acceptUrl: 'https://jstest.authorize.net/v1/Accept.js',
    acceptCoreUrl: 'https://jstest.authorize.net/v1/AcceptCore.js',
    type: 'text/javascript',
    charset: 'utf-8',
    authData: {
      clientKey: '5wyyJxDD2w8ePjFCb29ZDFn6ZHMvv42sHJdzWHnCQ2V53DG44sDZREgRC353tw5V',
      apiLoginID: '9eWaHZ9KZ2va'
    }
  },
};
