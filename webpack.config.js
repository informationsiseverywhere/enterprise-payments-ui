const jsonImporter = require('node-sass-json-importer');
var path = require('path');
module.exports = {
    module: {
        rules: [{
            test: /\.scss$|\.sass$/,
            use: [
                {
                    loader: require.resolve('sass-loader'),
                    options: {
                        implementation: require('sass'),
                        sassOptions: {
                            // bootstrap-sass requires a minimum precision of 8
                            precision: 8,
                            importer: jsonImporter(),
                            outputStyle: 'expanded',
                            includePaths: [
                                path.resolve('src/styles'),
                                path.resolve('./node_modules')
                            ]
                        }
                    },
                }
            ],
        }],
    }
}